'use strict';

let app = angular.module('menciusapp', [
    'ngCookies'
    , 'ngResource'
    , 'ngSanitize'
    , 'ngRoute'
    , 'ui.router'
    , 'ui.bootstrap'
    , 'ngAnimate'
    , 'ui.keypress'
    , 'mgo-angular-wizard'
    , 'angulike'
    , 'angular-intro'
    , 'credit-cards'
    , 'countrySelect'
    , 'angleSelect'
    , 'angularMoment'
    , 'angularUtils.directives.dirPagination',
    , 'ng-fusioncharts'
    , 'tableSort'
    , 'ngHandsontable'
    //,'ui.select'
]);
app.config([
    '$routeProvider',
    '$locationProvider',
    '$httpProvider',
    function ($routeProvider, $locationProvider, $httpProvider) {
        let access = {
            // these tie into the number of roles we have
            // TODO: fix this stupid role setup
            any: 31, // 11111
            visitor: 1, // 00001
            user: 30, // 11110
            admin: 4 // 00100
        };
        //Enable cors calls
        $httpProvider.defaults.useXDomain = true;

        $routeProvider
            .when('/', {
                templateUrl: 'pages/landing/login.html',
                //controller: 'loginCtrl',
                access: access.any
            })
            .when('/login', {
                templateUrl: 'pages/landing/login.html',
                access: access.any
            })
            .when('/register', {
                templateUrl: 'pages/landing/register.html',
                access: access.user
            })
            .when('/user/verify/:accessCode', {
                templateUrl: 'pages/verify/verify.html',
                access: access.visitor
            })
            .when('/about', {
                templateUrl: 'pages/landing/about.html',
                access: access.user
            })
            .when('/contact', {
                templateUrl: 'pages/search/search.html',
                access: access.user
            })
            .when('/usersAdmin', {
                templateUrl: 'pages/landing/usersAdmin.html',
                access: access.user
            })
            .when('/profile', {
                templateUrl: 'pages/profile/profile.html',
                access: access.user
            })
            .when('/home', {
                templateUrl: 'pages/accounts/active.html',
                access: access.user
            })
            .when('/add_uploadset', {
                templateUrl: 'pages/add_uploadset/add_uploadset.html',
                access: access.user
            })
            .when('/secret_sauce', {
                templateUrl: 'pages/secret_sauce/secret_sauce.html',
                access: access.user
            })
            .when('/search', {
                templateUrl: 'pages/search/search.html',
                access: access.user
            })
            .when('/pnl', {
                templateUrl: 'pages/pnl/pnl.html',
                access: access.user
            })
            .when('/payout', {
                templateUrl: 'pages/accounting/payout/payout.html',
                access: access.user
            })
            .when('/accounting_search', {
                templateUrl: 'pages/accounting/accounting_search/accountingSearch.html',
                access: access.user
            })
            .when('/edit_account/:account_key', {
                templateUrl: 'pages/edit_account/edit_account.html',
                access: access.user
            })
            .when('/add_resource', {
                templateUrl: 'pages/add_resource/add_resource.html',
                access: access.user
            })
            .when('/active_accounts', {
                templateUrl: 'pages/accounts/active.html',
                access: access.user
            })
            .when('/warm_accounts', {
                templateUrl: 'pages/accounts/warm.html',
                access: access.user
            })
            .when('/analytics', {
                templateUrl: 'pages/analytics/dashboard/dashboard.html',
                access: access.user
            })
            .when('/account_life', {
                templateUrl: 'pages/analytics/account_life/accountLife.html',
                access: access.user
            })
            .when('/launch_rates', {
                templateUrl: 'pages/analytics/launch/launch.html',
                access: access.user
            })
            .when('/accounts', {
                templateUrl: 'pages/resources/accounts/accounts.html',
                access: access.user
            })
            .when('/batches', {
                templateUrl: 'pages/resources/batches/batches.html',
                access: access.user
            })
            .when('/cards', {
                templateUrl: 'pages/resources/cards/cards.html',
                access: access.user
            })
            .when('/urls', {
                templateUrl: 'pages/resources/urls/urls.html',
                access: access.user
            })
            .when('/verticals', {
                templateUrl: 'pages/resources/verticals/verticals.html',
                access: access.user
            })
            .when('/statuses', {
                templateUrl: 'pages/resources/statuses/statuses.html',
                access: access.user
            })
            .when('/vms', {
                templateUrl: 'pages/resources/virtualMachines/virtualMachines.html',
                access: access.user
            })
            .when('/test', {
                templateUrl: 'pages/resources/test/test.html',
                access: access.user
            })
            .when('/datatable', {
                templateUrl: 'pages/datatabledemo/datatable.html',
                access: access.user
            })
            .when('/admindashboard', {
                templateUrl: 'pages/dashboard/admindashboard.html',
                access: access.user
            })
            .otherwise({
                templateUrl: 'pages/404.html',
                access: access.user
            });
    }
]);

app.run([
    '$rootScope',
    '$location',
    '$cookieStore',
    '$http',
    'authService',
    function ($rootScope, $location, $cookieStore) {
        $rootScope.urlRoot = '';
        $rootScope.lastSearch = '';
        $rootScope.userRoles = {
            visitor: 1, // 0001
            user_media: 2, // 0010
            user_upload: 4, // 0100
            user_funder: 8, // 1000
            user_accountManager: 16,
            admin: 31 // 1111
        };

        $rootScope.accessLevels = {
            any: 7, // 111
            visitor: 1, // 001
            user: 14, // 1110
            admin: 4 // 100
        };

        $rootScope.user = $cookieStore.get('user') || {
            username: '',
            isLogged: false,
            role: $rootScope.userRoles.visitor,
            va: true
        };

        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            $rootScope.error = null;
            $rootScope.success = null;

            $rootScope.current = current;
            $rootScope.next = next;

            // Check to see if the next page is public
            if (!(next.access & $rootScope.user.role)) {
                // Check if the current user has a profile or is an admin
                if (
                    Number($rootScope.user.role) == $rootScope.userRoles.user_media ||
                    Number($rootScope.user.role) == $rootScope.userRoles.user_upload ||
                    Number($rootScope.user.role) == $rootScope.userRoles.user_funder ||
                    Number($rootScope.user.role) == $rootScope.userRoles.admin
                ) {
                    let lastRoute = $cookieStore.get('current_route');
                    if (lastRoute) {
                        $location.path(lastRoute);
                    } else {
                        $location.path('/home');
                    }
                } else if ($rootScope.user.role == $rootScope.userRoles.visitor) {
                    $location.path('/');
                } else {
                    $location.path('/login');
                }
            }
        });

        //Save Last Visited route cookie
        $rootScope.$on('$routeChangeSuccess', function () {
            $cookieStore.put('current_route', $location.path());
        });
    }
]);
