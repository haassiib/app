'use strict';

angular.module('menciusapp').controller('editAccountCtrl', [
    '$rootScope',
    '$scope',
    '$http',
    '$routeParams',
    '$cookieStore',
    '$location',
    '$modal',
    '$sce',
    'statusService',
    'accountService',
    'safeUrlService',
    'adsetService',
    'accountHistoryService',
    'batchService',
    'verticalService',
    'codeService',
    'contentService',
    'pnlService',
    'accountCommentService',
    'campaignService',
    'voluumService',
    'virtualMachineService',
    'adespressoService',
    function (
        $rootScope,
        $scope,
        $http,
        $routeParams,
        $cookieStore,
        $location,
        $modal,
        $sce,
        statusService,
        accountService,
        safeUrlService,
        adsetService,
        accountHistoryService,
        batchService,
        verticalService,
        codeService,
        contentService,
        pnlService,
        accountCommentService,
        campaignService,
        voluumService,
        virtualMachineService,
        adespressoService
    ) {
        $scope.role = $rootScope.user.role;
        $scope.loading = true;
        $scope.disableUpdateCopy = false;
        $scope.dev = false;
        $scope.ageRange = Ages;
        $scope.users = [
            'hasib',
            'Joe',
            'Justin',
            'Luke',
            'Mike',
            'Marcus',
            'Ryan',
            'Scott',
            'Stan',
            'Trey',
            'Valerie',
            'Zach',
            'Zoe'
        ];
        $scope.currentUser = $rootScope.user.username.toLowerCase();
        $scope.conversionEvents = ConversionEvents;
        $scope.account = {};
        $scope.safeurls = [];
        $scope.batches = [];
        $scope.verticals = [];
        $scope.launched;
        $scope.initialBoost;
        $scope.isAdmin = $scope.role & ($rootScope.userRoles.admin == $scope.role);
        $scope.accountValidForCopy = false;
        $scope.vmSelected =
            $scope.vmSelected === undefined ? $scope.account.vm_name : $scope.vmSelected;

        $(document).ready(function () {
            $('.select2basic').select2({
                dropdownParent: $("#myModal")
            });
        });

        $scope.vmChanged = function (v) {
            $scope.account.vm_key = v.vm_key;
        };

        $scope.batchChanged = function (batch) {
            $scope.account.batch = batch.name;
            $scope.account.batchDescription = batch.description;
        };

        $scope.checkAdmin = function () {
            var admin = false;
            if ($rootScope.user.role & ($rootScope.userRoles.admin == $rootScope.user.role)) {
                console.log('user is an admin');
                admin = true;
            }
            return admin;
        };
        $scope.convertToLocalTime = Tools.convertToLocalTime;
        $scope.accountTypes = CardTypes;
        $scope.pageSize = 12;
        $scope.currentPage = 1;
        $scope.preview_view = false;
        $scope.totals = {
            threeday: {},
            sevenday: {},
            lifetime: {
                run_time: 0,
                revenue: 0,
                expenses: 0
            },
            audited: {
                run_time: 0,
                revenue: 0,
                expenses: 0
            }
        };
        $scope.yes = 'Yes';
        $scope.no = 'No';
        $scope.adespressoTotals = {};

        /****COPY****/
        $scope.newAd = function (parent_index) {
            var adTemplate = { new: true };
            $scope.campaign[parent_index].ads.push(adTemplate);
        };

        $scope.newAdSet = function () {
            let adsetTemplate = {
                campaign_key: $scope.campaign[0].campaign_key,
                new: true,
                ads: [{ new: true }],
                disabled: 0
            };
            $scope.campaign.push(adsetTemplate);
        };

        $scope.duplicateAdsetCurrent = function (index) {
            //deep copy object
            let newAdset = JSON.parse(JSON.stringify($scope.campaign[index]));
            newAdset.new = true;
            newAdset.disabled = 0;
            delete newAdset.$$hashKey;
            delete newAdset.adset_key;
            newAdset.ads.forEach(ad => {
                ad.new = true;
                ad.disabled = 0;
                delete ad.$$hashKey;
                delete ad.content_key;
            });
            $scope.campaign.push(newAdset);
            console.log($scope.campaign);
        };

        $scope.duplicateAdsetNew = function (adset) {
            console.log('Adset:', adset);
            swal(
                {
                    title: 'Redirecting...',
                    text: 'You will lose all unsaved changes!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Continue',
                    closeOnConfirm: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        console.log('is confirm: ', isConfirm);
                        $location.path('/add_uploadset').search({ aid: adset.adset_key });
                        window.location.href = $location.absUrl();
                    }
                }
            );
        };

        $scope.removeAd = function (parent_index, index) {
            let ad = $scope.campaign[parent_index].ads[index];
            swal(
                {
                    title: 'WARNING - Ad Deletion',
                    text: 'This will destroy the ad. It will not be recoverable. ',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Delete Ad.',
                    closeOnConfirm: true
                },
                function (isConfirm) {
                    console.log('is confirm: ', isConfirm);
                    if (isConfirm) {
                        deleteAd(ad, parent_index, index);
                    }
                }
            );
        };

        function deleteAd(ad, parent_index, index) {
            contentService.httpDeleteContent(ad, (res, err) => {
                if (err) {
                    console.log('err:', err);
                    swal('Error!', 'There was an issue deleteing your ad.', 'error');
                } else {
                    console.log('res: ', res);
                    swal('Success!', 'Your Ad was deleted.', 'success');
                    //removes ad from UI
                    $scope.campaign[parent_index].ads.splice(index, 1);
                }
            });
        }

        $scope.removeAdSet = function (adset, $index) {
            swal(
                {
                    title: 'WARNING - Adset Deletion',
                    text: 'This will destroy the adset. It will not be recoverable. ',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Delete Adset.',
                    closeOnConfirm: true
                },
                function (isConfirm) {
                    console.log('is confirm: ', isConfirm);
                    console.log('adset: ', adset);
                    if (isConfirm) {
                        deleteAdset(adset, $index);
                    }
                }
            );
        };

        function deleteAdset(adset, index) {
            if (adset && adset.adset_key) {
                adsetService.httpDeleteAdset(adset, (res, err) => {
                    if (err) {
                        console.log('err:', err);
                    } else {
                        console.log('res: ', res);
                        //removes adset from UI
                        $scope.campaign.splice(index, 1);
                    }
                });
            } else {
                $scope.campaign.splice(index, 1);
            }
        }

        $scope.disableAdset = function (index) {
            //console.log("parent i: ", parent_index, " | adset i: ", index);
            $scope.campaign[index].disabled = $scope.campaign[index].disabled_checkbox === false ? 0 : 1;
            //console.log("adset: ",$scope.campaign[index]);
        };

        // grab account adsets and content
        function getAccountCopy() {
            adsetService.httpGetCopyByAccount((res, err) => {
                if (err) {
                    console.log('Get Copy Error: ', err);
                    swal('Error', 'Unable to get Copy: ' + err, 'warning');
                } else {
                    $scope.loading = false;
                    if (res) {
                        $scope.campaign = res.filter(c => {
                            return c.adset_key != null;
                        });
                    } else {
                        console.log('No Copy Found: ', res);
                    }
                }
            });
        }
        getAccountCopy();

        $scope.updateCopy = function () {
            $scope.disableUpdateCopy = true;
            console.log('$scope.campaign: ', $scope.campaign);
            //All camps with no adset_key need to be inserted
            var newCamps = $scope.campaign.filter(camp => !camp.adset_key);
            // var oldCamps = $scope.campaign.filter(camp => camp.changed);
            var oldCamps = $scope.campaign.filter(camp => camp.adset_key);
            //console.log("new camps", newCamps);
            //console.log("old camps", oldCamps);

            if (newCamps.length) {
                console.log('NEW ADSETS');
                adsetService.httpPostAdset(newCamps, (res, err) => {
                    if (err) {
                        console.log('err: ', err);
                        swal('Error', 'Unable to Upload: ' + err, 'warning');
                    } else {
                        //console.log("New Camps res:", res);
                        if (oldCamps.length) {
                            //console.log("NEW CAMPS -> OLD CAMPS");
                            adsetService.httpPutAdset($routeParams.account_key, oldCamps, (res, err) => {
                                if (err) {
                                    console.log('err: ', res.data);
                                    swal('Error', 'Unable to Upload: [' + err + '] ' + data.code, 'warning');
                                } else {
                                    //$scope.status = res;
                                    swal('Success!', 'Your copy info has been updated.', 'success');
                                    //console.log("success with old.");
                                    getAccountCopy();
                                    $scope.disableUpdateCopy = false;
                                }
                            });
                        } else {
                            swal('Success!', 'Your copy info has been updated.', 'success');
                            //console.log("success without old.");
                            getAccountCopy();
                            $scope.disableUpdateCopy = false;
                        }
                    }
                });
            } else if (oldCamps.length) {
                // console.log("OLD ADSETS");
                // TODO: this can be improved by reducing the amount of camps/data sent to server
                // no need to send all adsets back everytime
                // new should have new flag
                adsetService.httpPutAdset($routeParams.account_key, oldCamps, (res, err) => {
                    if (err) {
                        console.log('err: ', res.data);
                        swal('Error', 'Unable to Upload: [' + err + '] ' + data.code, 'warning');
                    } else {
                        //$scope.status = res;
                        swal('Success!', 'Your copy info has been updated.', 'success');
                        getAccountCopy();
                        $scope.disableUpdateCopy = false;
                    }
                });
            } else {
                swal('Error', 'No Adsets to update.', 'warning');
                console.log('error no camps');
                $scope.disableUpdateCopy = false;
            }
        };

        /****ACCOUNT****/
        function getAccountCodeHistory() {
            codeService.httpGetCodeHistory($routeParams.account_key, (res, err) => {
                if (err) {
                    console.log('Error retrieving code history: ', err);
                    swal('Error', 'Unable to Retrieve Code History : ' + err, 'warning');
                } else {
                    $scope.loading = false;
                    //console.log("Get Code History Res: ", res);
                    $scope.account.codeHistory = res;
                }
            });
        }

        async function getAccountDetails() {
            // console.log("get account details");
            await accountService.httpGetAccountDetails((res, err) => {
                if (err) {
                    console.log('Account Details Error: ', err);
                    swal('Error', 'Account Details Error: ' + err, 'warning');
                } else {
                    $scope.loading = false;
                    $scope.account = res;

                    if ($scope.account.angle)
                        $scope.account.angle = Tools.CapitalizeFirstLetter($scope.account.angle);

                    if ($scope.account.owner)
                        $scope.account.owner = Tools.CapitalizeFirstLetter($scope.account.owner);

                    if ($scope.account.launched)
                        $scope.launched = moment($scope.account.launched, 'YYYY-MM-DD').format('MM-DD-YYYY');

                    if ($scope.account.initialBoost)
                        $scope.initialBoost = moment($scope.account.initialBoost, 'YYYY-MM-DD').format(
                            'MM-DD-YYYY'
                        );

                    if ($scope.account.creation)
                        $scope.creation = moment($scope.account.creation, 'YYYY-MM-DD').format('MM-DD-YYYY');

                    if ($scope.account.claimed && $scope.account.claimed == 'yes')
                        $scope.accountValidForCopy = true;

                    if ($scope.account.accountHistoryService)
                        $scope.account.history = $scope.account.histor.ymap(item => {
                            item.created = Tool.convertToLocalTime(item.created);
                            return item;
                        });

                    if ($scope.isAdmin) {
                        codeService.httpGetAccountCode($routeParams.account_key, (res, err) => {
                            if (err) {
                                console.log('Error retrieving codes: ', err);
                                swal('Error', 'Unable to Retrieve Credit Cards : ' + err, 'warning');
                            } else {
                                $scope.loading = false;
                                // console.log("Get Code Res: ", res);
                                if (res.code) {
                                    $scope.account.cc = res;
                                    console.log('Account: ', $scope.account);
                                }
                            }
                        });

                        getAccountCodeHistory();
                    }
                }
            });
        }
        getAccountDetails();

        // console.log("$scope.account in the editaccount ctrl => ", $scope.account);

        // Available Virtual Machine List
        virtualMachineService.httpGetVms((res, err) => {
            if (err) {
                console.log(err);
                swal('Error', 'Could not retrieve Virtual Machine List.', 'warning');
            } else {
                $scope.vms = res;
            }
        });

        statusService.httpGetAllStatus((res, err) => {
            if (err) {
                console.log(err);
                swal('Error', 'Unable to get Statuses: ' + err, 'warning');
            } else {
                $scope.status = res;
            }
        });

        batchService.httpGetBatches((res, err) => {
            if (err) {
                console.log(err);
                swal('Error', 'Unable to get Batches: ' + err, 'warning');
            } else {
                $scope.batches = res;
            }
        });

        verticalService.httpGetVerticals((res, err) => {
            if (err) {
                console.log(err);
                swal('Error', 'Unable to get Verticas: ' + err, 'warning');
            } else {
                $scope.verticals = res;
            }
        });

        $scope.addAccountComment = function (newacctcom) {
            let account = {
                account_key: $scope.account.account_key,
                comment: $scope.account.newComment
            };
            accountCommentService.httpPostAccountComment(account, (res, err) => {
                if (err) {
                    console.log(err);
                    swal('Error', 'There was an issue saving the comment.\nRefresh and try again.', 'error');
                } else {
                    console.log('successful insert');
                    swal('Success', 'Comment was successfully inserted.', 'success');
                    $scope.account.newComment = null;
                    getAccountDetails();
                }
            });
        };

        $scope.updateAccount = function (account) {
            // Update Account
            if ($scope.initialBoost) $scope.account.initialBoost = $scope.initialBoost;

            if ($scope.launched) $scope.account.launched = $scope.launched;

            if ($scope.creation) $scope.account.creation = $scope.creation;

            /*if ($scope.account.total_spend && typeof $scope.account.total_spend == 'string') {
                          //console.log($scope.account.total_spend);
                          $scope.account.total_spend = $scope.account.total_spend.split(',').join('');
                          //console.log($scope.account.total_spend);
                      }*/

            accountService.httpPutUpdateAccountDetails(account, (res, err) => {
                if (err) {
                    console.log(err);
                    swal('Error', 'Unable to Update Account Details: ' + err, 'warning');
                } else {
                    if (account.commentChanged) {
                        account.comment = account.last_account_comment;
                        accountCommentService.httpPostAccountComment(account, (res, err) => {
                            if (err) {
                                console.log('account comment update error: ', err);
                                swal('Error', 'Unable to Update Account Comment: ' + err, 'warning');
                                $scope.loading = false;
                            } else {
                                //console.log("account comment history success: ", res);
                                getAccountDetails();
                            }
                        });
                    }
                    if (account.statusChanged) {
                        accountHistoryService.httpPutAccountHistory([account], (res, err) => {
                            if (err) {
                                console.log('Update Account History Error: ', err);
                                swal('Error', 'Update Account History Error: ' + err, 'warning');
                            } else {
                                console.log('account status history success: ', res);
                                swal('Success!', 'Your account info has been updated.', 'success');
                                getAccountDetails();
                            }
                        });
                        console.log(account.status);
                        if (
                            account.status.toLowerCase() === 'spending, in pl' ||
                            account.status.toLowerCase() === 'reupload' ||
                            account.status.toLowerCase() === 'in progress'
                        ) {
                            //TODO this will error out
                            pnlService.httpPostPnlItem([account.account_key], (res, err) => {
                                if (err) {
                                    console.log(err);
                                    swal(
                                        'Error',
                                        'Unable to insert Pnl Item. Please insert manually for today. Error: ' + err,
                                        'warning'
                                    );
                                    $scope.loading = false;
                                } else {
                                    console.log('insert new pnl item success', res);
                                }
                            });
                        }
                    } else {
                        swal('Success!', 'Your account info has been updated.', 'success');
                        getAccountDetails();
                    }
                }
            });
        };

        $scope.updateCode = function (code) {
            //console.log("code:", code);
            codeService.httpPutAccountCode([code], (res, err) => {
                if (err) {
                    console.log(err);
                    swal('Error!', 'Credit Card not updated.\n' + err, 'warning');
                } else {
                    //console.log(res);
                    swal('Success!', 'Credit Card Updated', 'success');
                    //Reset Page Values
                }
            });
        };

        $scope.updateUrl = function (account) {
            let safeUrl = {
                safe_url_key: account.safe_url_key,
                url: account.safe_url,
                status: account.safe_url_status,
                claimed: account.safe_url_claimed
            };

            safeUrlService.httpPutSafeUrls([safeUrl], (res, err) => {
                if (err) {
                    console.log(err);
                    swal('Error!', 'Safe Url not updated.\n' + err, 'warning');
                } else {
                    console.log(res);
                    swal('Success!', 'Safe Url Updated', 'success');
                    //Reset Page Values
                }
            });
        };

        $scope.updateCampaignUrl = function (account) {
            if ($scope.selectedCampaign[0]) {
                console.log($scope.selectedCampaign[0]);
                let camp = {
                    campaign_key: account.campaign_key,
                    campaign_name: $scope.selectedCampaign[0].name,
                    voluum_id: $scope.selectedCampaign[0].id,
                    url: createCampaignUrl($scope.selectedCampaign[0].url)
                };

                campaignService.httpUpdateCampaigns([camp], (res, err) => {
                    if (err) {
                        swal(
                            'Error' + err,
                            'Failure updated campaign. Please refresh the page and try again.\n' + res.statusText,
                            'warning'
                        );
                    } else {
                        account.campaign_url = createCampaignUrl($scope.selectedCampaign[0].url);
                        account.voluum_id = $scope.selectedCampaign[0].id;
                        account.campaign_name = $scope.selectedCampaign[0].name;
                    }
                });
            }
        };

        $scope.filterCampaigns = function () {
            // clear campaign url - filters have changed
            if ($scope.account && $scope.account.campaignUrl) delete $scope.account.campaignUrl;

            let angle, vertical;
            if ($scope.account) {
                angle =
                    $scope.account.angle === null || typeof $scope.account.angle === 'undefined'
                        ? ''
                        : $scope.account.angle;
                vertical =
                    $scope.account.vertical === null || typeof $scope.account.vertical === 'undefined'
                        ? ''
                        : $scope.account.vertical;
                //console.log("filter:a ",angle , "| v ", vertical);
            }
            //console.log($scope.allCampaigns);
            let allCamps = JSON.parse(JSON.stringify($scope.allCampaigns));
            $scope.filteredCampaigns = allCamps.filter(c => ~c.name.indexOf(vertical)); //.filter(c =>~c.name.indexOf(angle))
        };

        $scope.selectedCampaign = [];
        $scope.voluumCampaigns = [];
        /* Working Voluum Auth and Campaign GET */
        function getVoluumCampaigns() {
            console.log('token: ', $cookieStore.get('voluum_token'));
            console.log('time exp: ', $cookieStore.get('voluum_exp'));
            let token = $cookieStore.get('voluum_token');
            if (token) {
                $scope.loadingCamps = true;
                $http({
                    method: 'GET',
                    url: '/voluum/campaigns',
                    headers: { 'x-token': token }
                }).then(
                    function (data) {
                        console.log('Voluum Campaign Success');
                        $scope.selectedCampaign = data.data.campaigns[0];
                        $scope.allCampaigns = data.data.campaigns
                            .sort(Tools.generateSortFunction([{ name: 'name', reverse: true }]))
                            .filter(c => !(~c.name.indexOf('traffic') || ~c.name.indexOf('Traffic')));
                        $scope.filterCampaigns();
                        console.log('$scope.selectedCampaign => ', $scope.selectedCampaign);
                    },
                    function (data) {
                        console.log('Voluum Campaign Failure: ', data);
                    }
                );
            } else {
                $scope.refreshCamps();
            }
        }

        $scope.refreshCamps = function () {
            $http({
                method: 'GET',
                url: '/voluum/auth'
            }).then(
                function (data) {
                    console.log('Voluum Auth Success:');
                    $cookieStore.put('voluum_token', data.data.token);
                    $cookieStore.put('voluum_exp', data.data.expirationTimestamp);
                    getVoluumCampaigns();
                },
                function (data) {
                    console.log('Voluum Auth Failure: ', data);
                }
            );
        };

        function createCampaignUrl(campaignUrlTemplate) {
            console.log('campaignUrlTemplate => ', campaignUrlTemplate);
            if (!campaignUrlTemplate) return;

            let userInitials =
                $rootScope.user.fname.charAt(0).toUpperCase() +
                $rootScope.user.lname.charAt(0).toUpperCase();
            let keyword =
                $scope.account.fname.split(' ').join('') + $scope.account.lname.split(' ').join('');
            //console.log("keyword: ", keyword);
            //console.log("fname: ", $scope.uploadset.account.fname.split(' ').join(''))
            //console.log("lname: ", $scope.uploadset.account.lname.split(' ').join(''));
            let newCampaignUrl =
                campaignUrlTemplate.split('?')[0] +
                '?pid=' +
                $scope.account.pixel +
                '&ad=[ad]&keyword=' +
                userInitials +
                keyword;
            console.log('newCampaignUrl: ', newCampaignUrl);
            return newCampaignUrl;
        }

        $scope.showAccountDetails = function () {
            console.log('ACCOUNT: ', $scope.account);
        };

        /****PNL****/
        $scope.pnlChanged = function (itemIndex, pnlIndex) {
            //console.log("pnl item changed. need to update pnl items.");
            console.log('Parent index: ', itemIndex, 'pnl Index: ', pnlIndex);
            //console.log($scope.items);
            $scope.items[itemIndex].pnlChanged = true;
            $scope.items[itemIndex].pnl[pnlIndex].pnlChanged = true;

            if (itemIndex == 0 && pnlIndex == 0) {
                $scope.account.budgetChanged = true;
                $scope.account.budget = $scope.items[itemIndex].pnl[pnlIndex].expenses;
            }
        };

        $scope.pullLifetimeSpendFromAdespresso = async function () {
            await adespressoService.httpGetLifetimeSpendByAccount(
                $scope.account.account_key,
                $scope.account.adespresso_key,
                (res, err) => {
                    if (err) {
                        console.log(err);
                        swal('Error', 'Unable to get Adespresso Lifetime Spend: ' + err, 'warning');
                    } else {
                        $scope.adespressoTotals = res;
                        if (Array.isArray(res.data)) {
                            swal('Success', 'Expense Successfully Pulled From Adespresso', 'success');
                            getPnl();
                            console.log('$scope.adesspressoTotals => ', $scope.adespressoTotals);
                        } else {
                            swal('Error', res.data, 'error');
                        }
                    }
                }
            );
        };

        $scope.saveChangedPnl = function () {
            let itemsToSave = [];
            $scope.items.forEach(item => {
                console.log('item: ', item);
                if (item.pnlChanged) {
                    item.pnl.forEach(p => {
                        if (p.pnlChanged) {
                            itemsToSave.push(p);
                        }
                    });
                }
            });
            //console.log("Items to save: ");
            //.log(itemsToSave);

            console.log('itemstosave: ', itemsToSave);
            if (itemsToSave.length > 0) {
                pnlService.httpPutPnlItems(itemsToSave, (res, err) => {
                    if (err) {
                        console.log('error saving');
                        swal('Error', 'There was an error saving pnl items:\n' + err, 'error');
                    } else {
                        //console.log("Pnl Update Success: ", res);

                        if ($scope.items[0].account_key) {
                            //console.log("scope item: ", $scope.items[0].account_key);
                            accountService.httpPutUpdateAccountSpend(
                                [$scope.items[0].account_key],
                                (res, err) => {
                                    if (err) {
                                        console.log('error saving');
                                        swal('Error', 'There was an error updating total spend:\n' + err, 'error');
                                    } else {
                                        swal('Success', 'Pnl Items Successfully Saved', 'success');
                                        getPnl();
                                        getAccountDetails();
                                    }
                                }
                            );
                        } else {
                            swal('Success', 'Pnl Items Successfully Saved', 'success');
                            getPnl();
                        }
                    }
                });
            }

            if ($scope.account.budgetChanged) {
                let accountToUpdate = [
                    {
                        account_key: $scope.account.account_key,
                        budget: $scope.account.budget
                    }
                ];
                //console.log("account budget to update:", accountToUpdate);

                accountService.httpPutUpdateAccountBudget(accountToUpdate, (res, err) => {
                    if (err) {
                        console.log(err);
                        swal(
                            'Error',
                            'Unable to update budget. Please try again or update manually. Error: ' + err,
                            'warning'
                        );
                        $scope.loading = false;
                    } else {
                        console.log('update account budget success');
                    }
                });
            }
        };

        $scope.saveAudit = function () {
            if ($scope.account.auditChanged) {
                $scope.account.auditRevenue = parseFloat(
                    $scope.totals.audited.revenue.toString().replace(/,/g, '')
                );
                $scope.account.auditExpenses = parseFloat(
                    $scope.totals.audited.expenses.toString().replace(/,/g, '')
                );
                console.log(
                    '$scope.account.auditExpenses => ',
                    $scope.account.auditExpenses,
                    ' $scope.totals.audited.expenses => ',
                    $scope.totals.audited.expenses
                );
                accountService.httpPutUpdateAccountAudit($scope.account, (res, err) => {
                    if (err) {
                        console.log('account audit update error: ', err);
                        swal('Error', 'Unable to Update Account Audit: ' + err, 'warning');
                    } else {
                        console.log('account audit success: ', res);
                        swal('Success', 'Account Audit Saved', 'success');
                    }
                });
                // getPnl();
            }
        };

        function getPnl() {
            // console.log("get pnl");
            pnlService.httpGetPnlByAccount((res, err) => {
                if (err) {
                    console.log('Error getting PnL: ', err);
                    swal('Error', 'Unable to get PnL: ' + err, 'warning');
                } else {
                    let PnlByContent = [];
                    // console.log("Account PnL", res);
                    if (res.length !== 0) {
                        res.forEach(a => {
                            let tempObj = {};
                            Object.assign(tempObj, a);
                            //console.log("Temp Obj: ", tempObj);
                            if (!PnlByContent[a.content_key]) {
                                PnlByContent[a.content_key] = {
                                    pnl: [],
                                    account_key: a.account_key,
                                    content_key: a.content_key,
                                    disabled: a.disabled
                                };
                            }
                            PnlByContent[a.content_key.toString()].pnl[a.pnl_key.toString()] = tempObj;
                        });

                        PnlByContent = PnlByContent.filter(a => typeof a !== 'undefined');
                        PnlByContent.forEach(a => {
                            a.pnl = a.pnl
                                .filter(p => typeof p !== 'undefined')
                                .sort(Tools.generateSortFunction([{ name: 'created', reverse: true }]));
                            if (a.pnl.length < $scope.pageSize) {
                                for (let i = $scope.pageSize; a.pnl.length < $scope.pageSize; i--) {
                                    a.pnl.push({});
                                }
                            }
                        });
                        $scope.items = PnlByContent.sort(
                            Tools.generateSortFunction([
                                { name: 'disabled', reverse: false },
                                { name: 'content_key', reverse: true }
                            ])
                        );
                        // console.log("ITEMS: ", $scope.items);
                        setTodaysValuePercentages();
                        let totals = calcTotals($scope.items);
                        Object.assign($scope.totals.lifetime, totals);
                        //TODOc: create audited totals
                        let auditTotals = {
                            audited: $scope.account.audited,
                            revenue: $scope.account.audit_revenue
                                ? $scope.account.audit_revenue
                                : $scope.totals.lifetime.revenue,
                            expenses: $scope.account.audit_expenses
                                ? $scope.account.audit_expenses
                                : $scope.totals.lifetime.expenses
                        };
                        Object.assign($scope.totals.audited, auditTotals);
                        // console.log("Scope totals . audited =:> ", $scope.totals.audited);
                    } else {
                        $scope.items = [{ pnl: [{}, {}, {}, {}, {}, {}, {}] }];
                        let auditTotals = {
                            audited: $scope.account.audited,
                            revenue: $scope.account.audit_revenue
                                ? $scope.account.audit_revenue
                                : $scope.totals.lifetime.revenue,
                            expenses: $scope.account.audit_expenses
                                ? $scope.account.audit_expenses
                                : $scope.totals.lifetime.expenses
                        };
                        Object.assign($scope.totals.audited, auditTotals);
                        // console.log("Scope totals . audited =:> ", $scope.totals.audited);
                    }
                }
            });
        }
        getPnl();

        function getThreeDaySevenDay() {
            pnlService.httpGetNDaysByAccount(3, (res, err) => {
                if (err) {
                    console.log('Error getting 3 day PnL: ', err);
                    swal('Error', 'Unable to get 3 day PnL: ' + err, 'warning');
                } else {
                    // console.log("success: ", res);
                    if (res && res.length >= 3) {
                        let totals = calcTotals(res);
                        Object.assign($scope.totals.threeday, totals);
                    }
                }
            });

            pnlService.httpGetNDaysByAccount(7, (res, err) => {
                if (err) {
                    console.log('Error getting 7 day PnL: ', err);
                    swal('Error', 'Unable to get 7day PnL: ' + err, 'warning');
                } else {
                    // console.log("success: ", res);
                    if (res && res.length >= 7) {
                        let totals = calcTotals(res);
                        Object.assign($scope.totals.sevenday, totals);
                    }
                }
            });
        }
        getThreeDaySevenDay();

        function isToday(momentDate) {
            const TODAY = moment().startOf('day');
            return momentDate.isSame(TODAY, 'd');
        }

        function isYesterday(momentDate) {
            const YESTERDAY = moment()
                .subtract(1, 'days')
                .startOf('day');
            return momentDate.isSame(YESTERDAY, 'd');
        }

        function setTodaysValuePercentages() {
            $scope.items.forEach(item => {
                if (item.pnl[0] && isToday(moment(item.pnl[0].created))) {
                    if (
                        !item.pnl[0].visits ||
                        item.pnl[0].visits === 0 ||
                        !item.pnl[0].expenses ||
                        item.pnl[0].expenses === 0
                    )
                        return (item.pnl[0].cpc = 0);

                    let hour = moment().format('HH');
                    let minute = moment().format('mm');
                    // console.log(`hour: ${hour} | minute: ${minute}`);

                    let percentDay = (parseInt(hour) + parseInt(minute) / 60) / 24;
                    // CALCULTE CPC BASED ON PERCENTAGE OF DAY
                    let estCpc = (item.pnl[0].expenses * percentDay) / item.pnl[0].visits;
                    // console.log(`percDay: ${percentDay} | estCpc: ${estCpc}`);
                    item.pnl[0].cpc = estCpc;
                    item.pnl[0].breakeven =
                        typeof item.pnl[0].lpctr !== 'number' || item.pnl[0].lpctr <= 0
                            ? 0
                            : (estCpc / item.pnl[0].lpctr) * 100;
                }
            });
        }

        function calcTotals(pnlItems) {
            if (pnlItems) {
                let items = [];
                if (pnlItems && pnlItems.length && pnlItems[0].pnl) {
                    pnlItems.forEach(item => {
                        let i = item.pnl.filter(p => p.pnl_key);
                        items = [...items, ...i];
                    });
                } else {
                    items = pnlItems;
                }

                let totals = items.reduce((totals, item) => {
                    //console.log(item);
                    if (!totals.revenue) totals.revenue = 0;
                    if (!totals.expenses) totals.expenses = 0;
                    if (!totals.clicks) totals.clicks = 0;
                    if (!totals.visits) totals.visits = 0;

                    totals.revenue += typeof item.revenue === 'number' ? item.revenue : 0;
                    totals.expenses += typeof item.expenses === 'number' ? item.expenses : 0;
                    totals.clicks += typeof item.clicks === 'number' ? item.clicks : 0;
                    totals.visits += typeof item.visits === 'number' ? item.visits : 0;

                    return totals;
                }, {});

                //TODO: get run time
                totals.profit = totals.revenue - totals.expenses;
                totals.roi = (totals.profit / totals.expenses) * 100;
                totals.lpctr = (totals.clicks / totals.visits) * 100;
                //console.log(totals.profit / totals.expenses * 100);
                return totals;
            }
        }

        //TODO: Fix this, these values should be coming from the directive //Repeated in PnL
        $scope.dateChange = function (field, date) {
            $scope[field] = date;
            //console.log("date: ", date, " | typeof: ", typeof date);
        };

        $scope.statusChanged = function (account) {
            console.log('status changed. need to update account history.');
            account.statusChanged = true;
            if (account.status === 'Recovered') {
                account.recovered = 'yes';
            }
        };

        $scope.recoveredChanged = function (account) {
            console.log('recovered status changed.');
            if (account.recovered === 'yes') {
                account.status = 'Recovered';
                $scope.statusChanged(account);
            } else if (account.recovered === 'no') {
                account.status = 'Hidden';
                $scope.statusChanged(account);
            }
            account.owner = Tools.CapitalizeFirstLetter($rootScope.user.username);
            //console.log("user: ", $scope.user, " | Account:", account);
        };

        $scope.commentChanged = function (account) {
            //console.log("comment changed. need to update account comments");
            account.commentChanged = true;
        };

        $scope.countryChanged = function (account) {
            //console.log("country changed. need to update account adset");
            account.countryChanged = true;
        };

        $scope.auditedChanged = function (account) {
            console.log('audit changed. need to update account audit values');
            $scope.account.auditChanged = true;
        };

        $scope.budgetChanged = function (account) {
            console.log('budget changed. need to update pnl exp and account budget values');
            $scope.account.budgetChanged = true;
            console.log('inside the budgetChagned method in the controller => ', $scope.items);
            if ($scope.items[0]) {
                if ($scope.items[0].pnl[0]) {
                    //update pnl item
                    $scope.items[0].pnlChanged = true;
                    $scope.items[0].pnl[0].pnlChanged = true;
                    // update account budget
                    $scope.items[0].pnl[0].expenses = account.budget;

                    //TODO: once daily % based budget is calced add this line
                    //$scope.account.pnl[0].items[0].budget = account.budget;
                }
            }
        };

        $scope.isDead = function (status) {
            if (
                status &&
                (status.toLowerCase().indexOf('dead') >= 0 || status.toLowerCase().indexOf('hidden') >= 0)
            )
                return true;
            return false;
        };

        /***CONTROLLER HELPERS****/
        $scope.tabs = [
            { heading: 'Copy', active: true },
            { heading: 'Account', active: false },
            { heading: 'VM', active: false },
            { heading: 'PnL', active: false }
        ];
        function setTab(newTab) {
            $scope.tabs = $scope.tabs.map(tab => {
                if (tab.heading === newTab) {
                    tab.active = true;
                } else {
                    tab.active = false;
                }
                return tab;
            });
        }

        const copyContentToClipboard = ($scope.copyContentToClipboard = function ($event) {
            let element = $event.target;
            var $temp = $('<input>');
            $('body').append($temp);
            $temp.val($(element).text()).select();
            document.execCommand('copy');
            $temp.remove();
        });

        $(document).ready(function () {
            let tab = Tools.getParameterByName('tab');
            if (tab) setTab(tab);
            // console.log(
            //   "$scope.account in doc ready => ",
            //   $scope.account.lname.split("Profiles")
            // );
            // console.log("$scope.totals => ", $scope.totals);
            // console.log("scope acct => ", $scope.account);
            // `http://74.124.10.97//#/vms/${$scope.account.UUID}/console`
            $scope.vmFrame = $sce.trustAsResourceUrl(
                `http://74.124.10.97/#/`

            );
        });

        $scope.tabClick = function (tab) {
            // console.log("tab click: ", tab);
            let stateObj = { tab: tab };
            let uri = Tools.updateQueryParameter('tab', tab, window.location.href);
            // console.log("uri: ", uri);
            history.pushState(stateObj, 'tab', uri);
        };

        $scope.applyColorFilter = function (val) {
            if (val > 0) {
                return 'profit';
            } else if (val < 0) {
                return 'loss';
            } else {
                return 'nonet';
            }
        };

        /* MODAL SECTION */

        $scope.openCodeModal = function () {
            var codeModalInstance = $modal.open({
                templateUrl: 'pages/edit_account/modals/code_modal.html',
                backdrop: true,
                windowClass: 'modal',
                controller: CodeModalInstanceCtrl,
                resolve: {
                    account: function () {
                        return $scope.account;
                    }
                }
            });

            codeModalInstance.result.then(
                function (code) {
                    //console.log($scope.account);
                    //console.log(code);
                    $scope.account.cc = code;
                    getAccountCodeHistory();
                    getAccountDetails();
                },
                function () {
                    console.log('Code Url Modal dismissed at: ' + new Date());
                }
            );
        };

        $scope.openUrlModal = function () {
            var urlModalInstance = $modal.open({
                templateUrl: 'pages/edit_account/modals/url_modal.html',
                controller: UrlModalInstanceCtrl,
                resolve: {
                    safeSite: function () {
                        return $scope.account.safe_url;
                    },
                    account: function () {
                        return $scope.account;
                    }
                }
            });

            urlModalInstance.result.then(
                function (safeurl) {
                    $scope.account.safe_url = safeurl;
                },
                function () {
                    console.log('Safe Url Modal dismissed at: ' + new Date());
                }
            );
        };

        $scope.openPnlModal = function (index) {
            let pnlItem = {};
            let valid = false;
            //find valid adset
            let validAdsets = $scope.campaign.filter(adset => {
                if (adset.disabled === 1 || adset.disabled === true) return false;
                if (adset.ads.length === 0) return false;

                return true;
            });

            if ($scope.items[index].account_key && $scope.items[index].content_key) {
                Object.assign(pnlItem, $scope.items[index]);
                console.log('exists');
                valid = true;
            } else if (
                validAdsets[0] &&
                validAdsets[0].ads[0] &&
                validAdsets[0].ads[0].content_key &&
                validAdsets[0].ads[0].account_key
            ) {
                //doesnt exist but has valid ads for pnl
                //TODO: this is logically flawed -> need to give user the ability to pick which adset to use, not just select first
                pnlItem.account_key = $scope.campaign[0].ads[0].account_key;
                pnlItem.content_key = $scope.campaign[0].ads[0].content_key;
                console.log('valid');
                valid = true;
            } else {
                //no valid ads
                console.log('invalid');
            }
            if (valid) {
                let pnlModalInstance = $modal.open({
                    templateUrl: 'pages/edit_account/modals/pnl_modal.html',
                    controller: PnlModalInstanceCtrl,
                    resolve: {
                        item: function () {
                            return pnlItem;
                        }
                    }
                });

                pnlModalInstance.result.then(
                    function (item) {
                        //recall get PNL Here.
                        console.log('SUCCESS PNL MODAL: ', item);
                        getPnl();
                    },
                    function () {
                        console.log('PnL Modal dismissed at: ' + new Date());
                    }
                );
            } else {
                swal(
                    'Error',
                    'Account not configured properly for PnL. Check that ads exist and are in Spending Status.',
                    'error'
                );
            }
        };
    }
]);

let CodeModalInstanceCtrl = function ($scope, $modalInstance, accountService, codeService, account) {
    //console.log(account);
    $scope.safecodes = [];
    $scope.allcodes = [];
    $scope.selected = {};
    $scope.accountTypes = CardTypes;
    $scope.codeExists = 'exists';

    codeService.httpGetAccountCodes(false, (res, err) => {
        if (err) {
            console.log(err);
            swal('Error', 'Could not retrieve account codes.', 'warning');
        } else {
            $scope.codes = res;
            if ($scope.codes.length > 0) {
                //console.log("codes:", res);
                $scope.selected.cc = $scope.codes[0].code;
                $scope.current = account.cc;
            }
        }
    });

    $scope.ok = function () {
        console.log('$scope.selected: ', $scope.selected);
        console.log('scope.codeExists: ', $scope.codeExists);
        let newCode;
        if ($scope.codeExists === 'exists') {
            let codeobj = $scope.codes.filter(cc => {
                return parseInt(cc.code) === parseInt($scope.selected.cc);
            });
            newCode = {
                code: codeobj[0].code,
                code_key: codeobj[0].code_key,
                code_type: codeobj[0].code_type,
                account_key: account.account_key,
                changed: true,
                claimed: true
            };
        } else if ($scope.codeExists === 'new') {
            newCode = {
                number: $scope.newCard.number,
                accounttype: $scope.newCard.accounttype,
                cvc: $scope.newCard.cvc,
                zip: $scope.newCard.zip,
                exp_month: $scope.newCard.exp_month,
                exp_year: $scope.newCard.exp_year,
                account_key: account.account_key,
                changed: true,
                claimed: true
            };
        }
        let codeset = [newCode];
        let oldCode;
        if (account.cc) {
            oldCode = {
                code_key: account.cc.code_key,
                claimed: false,
                changed: false
            };
        }
        if (oldCode) codeset.splice(0, 0, oldCode);

        console.log('newCode: ', newCode);
        console.log('oldCode: ', oldCode);
        console.log('codeSet:', codeset);

        if ($scope.codeExists === 'exists') {
            existingCard(newCode, codeset);
        } else if ($scope.codeExists === 'new') {
            newCard(newCode, oldCode);
        }
    };

    function newCard(newCode, oldCode) {
        console.log('new card function');
        codeService.httpPutAccountCode([oldCode], (res, err) => {
            if (err) {
                console.log('error: ', err);
                swal('Error', 'Unable to Update Credit Card: ' + err, 'error');
            } else {
                //console.log("old code update success: ", res);
                codeService.httpPostAccountCodes([newCode], (res, err) => {
                    if (err) {
                        console.log('Unable to Insert New Account Code: ', err);
                        swal('Error', 'Unable to Insert New Credit Card: ' + err, 'error');
                    } else {
                        //console.log("New code insert success: ", res);
                        newCode.code_key = res.insertId;
                        accountService.httpPutUpdateAccountCode(newCode, (res, err) => {
                            if (err) {
                                console.log(err);
                                swal('Error', 'Unable to Update Account Credit Card information: ' + err, 'error');
                            } else {
                                //console.log("Account Code Update Success res:", res);
                                codeService.httpPostCodeHistory([newCode], (res, err) => {
                                    if (err) {
                                        console.log(err);
                                        swal('Error', 'Unable to Update Credit Card History: ' + err, 'error');
                                    } else {
                                        //console.log("updated code history succcess:", res);
                                        $modalInstance.close(newCode);
                                        swal('Success!', 'Your credit card info has been updated.', 'success');
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    function existingCard(newCode, codeset) {
        console.log('existing card function');
        accountService.httpPutUpdateAccountCode(newCode, (res, err) => {
            if (err) {
                console.log(err);
                swal('Error', 'Unable to Update Credit Card information: ' + err, 'warning');
            } else {
                //console.log("Card Update Success res:", res);
                codeService.httpPutAccountCode(codeset, (res, err) => {
                    if (err) {
                        console.log('error: ', err);
                    } else {
                        //console.log("success: ", res);
                        $modalInstance.close(newCode);
                        swal('Success!', 'Your credit card info has been updated.', 'success');
                    }
                });
            }
        });
    }

    $scope.cancel = function () {
        console.log('DISMISS');
        $modalInstance.dismiss('cancel');
    };

    $scope.creditCardUpdate = function () {
        console.log('Change event: ', $scope.selected);
    };
};

let UrlModalInstanceCtrl = function (
    $scope,
    $modalInstance,
    safeUrlService,
    accountService,
    safeSite,
    account
) {
    //console.log(account);
    $scope.safeurls = [];
    $scope.allurls = [];
    $scope.selected = {};

    safeUrlService.httpGetSafeUrls((res, err) => {
        if (err) {
            console.log(err);
        } else {
            //console.log("res:",res);
            $scope.allurls = res;
            $scope.safeurls = res.filter(url => url.claimed === 'no');

            if ($scope.safeurls.length) {
                $scope.selected = {
                    safeurl: $scope.safeurls[0].url,
                    current: safeSite
                };
            } else {
                $modalInstance.close(account.safe_url);
                swal('Error', 'No usable Urls. Please go to Add Resources, and add more', 'warning');
            }
        }
    });

    $scope.ok = function () {
        var newSafeUrlObj = {};
        var oldSafeUrlObj = {};
        for (var i = 0; i < $scope.allurls.length; i++) {
            //console.log($scope.allurls[i].url);
            if ($scope.allurls[i].url == $scope.selected.safeurl) {
                newSafeUrlObj = $scope.allurls[i];
                newSafeUrlObj.claimed = 'yes';
            } else if ($scope.allurls[i].url == account.safe_url) {
                //console.log($scope.allurls[i].safe_url_key);
                oldSafeUrlObj = $scope.allurls[i];
                oldSafeUrlObj.claimed = 'no';
            }
        }

        safeUrlService.httpPutSafeUrls([newSafeUrlObj], (res, err) => {
            if (err) {
                console.log('Update New Safe Url Err: ', err);
                swal('Error', 'Unable to Update New Safe Url: ' + err, 'warning');
            } else {
                //success
                //console.log("safe url 1 updated: ", newSafeUrlObj );
                safeUrlService.httpPutSafeUrls([oldSafeUrlObj], (result, error) => {
                    if (error) {
                        console.log('Update Old Safe Url Err: ', error);
                        swal('Error', 'Unable to Update Old Url: ' + err, 'warning');
                    } else {
                        //console.log("safe url 2 updated: ", oldSafeUrlObj);
                        accountService.httpPutUpdateAccountSafeUrl(newSafeUrlObj, (res, err) => {
                            if (err) {
                                console.log('Update Account Safe Url Err', err);
                                swal('Error', 'Unable to Update Account Url: ' + err, 'warning');
                            } else {
                                swal('Success!', 'Safe Urls have been updated.', 'success');
                                $modalInstance.close($scope.selected.safeurl);
                            }
                        });
                    }
                });
            }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.safeUrlUpdate = function () {
        //console.log('Change event: ', $scope.selected.safeurl);
    };
};

let PnlModalInstanceCtrl = function ($scope, $modalInstance, pnlService, item) {
    //console.log(item.pnl)
    $scope.items = item.pnl;
    $scope.new = {
        account_key: item.account_key,
        content_key: item.content_key
    };
    $scope.new.created = '';

    $scope.ok = function () {
        console.log('OKAY');
        console.log('$scope.new in the pnlModalInstanceCtrl => ', $scope.new);
        let exists = [];

        if ($scope.items) {
            exists = $scope.items.filter(item => {
                if (item.created) {
                    console.log($scope.new.created);
                    console.log(item.created);
                    let a = moment($scope.new.created, 'MM-DD-YYYY').startOf('day');
                    let b = moment(item.created).startOf('day');
                    console.log(a.format());
                    console.log(b.format());
                    console.log(b.diff && b.diff(a, 'days') === 0);
                    if (b.diff && b.diff(a, 'days') === 0) return true;

                    return false;
                }
            });
        }

        console.log(exists);
        if (exists.length > 0) {
            swal('WARNING', 'A PnL item for that date already exists!', 'warning');
        } else {
            //TODO: check values:
            console.log('$scope new:', $scope.new);

            pnlService.httpPostPnlItems([$scope.new], (res, err) => {
                if (err) {
                    console.log('error saving');
                    swal('Error', 'There was an error saving pnl items:\n' + err, 'error');
                } else {
                    console.log('save success: ', res);
                    swal('Success', 'Pnl Items Successfully Saved', 'success');
                    $modalInstance.close($scope.new);
                }
            });
        }
    };

    $scope.cancel = function () {
        console.log('DISMISS');
        $modalInstance.dismiss('cancel');
    };
};

//VM initializer LEAVE AT BOTTOM OF PAGE!!

// $scope.init = async function() {
//   $("#iframe").load(() => {
//     $(
//       "#xo-app > div > div:nth-child(3) > div > div > div > div:nth-child(2) > div > div:nth-child(1) > div > div"
//     ).trigger("click");
//   });
// };

// $timeout($scope.init);
