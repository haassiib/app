'use strict';

angular.module('menciusapp').controller('addUploadsetCtrl', ['$rootScope', '$scope', '$http', '$routeParams', '$timeout', '$location', '$cookieStore',
    'alertService', 'accountService', 'safeUrlService', 'verticalService', 'campaignService', 'adsetService',
    function ($rootScope, $scope, $http, $routeParams, $timeout, $location, $cookieStore,
        alertService, accountService, safeUrlService, verticalService, campaignService, adsetService) {
        //console.log($rootScope.user);
        $scope.loadingCamps = false;
        $scope.isValidUploadset = false;
        $scope.availableAccounts = [];
        $scope.accountHasUrl = false;
        $scope.newUrl = false;
        $scope.allUrls = [];
        $scope.availableUrls = [];
        $scope.availableUrlsCount = 0;
        $scope.urldropdown = false;
        $scope.urlKey = '';
        $scope.urlToggle = function (bool) {
            $scope.urldropdown = bool;
            var list = document.getElementById("urllist");
            //console.log("Val: ", bool, " | list: ", list);
        }

        $scope.verticals = [];
        $scope.ageRange = Ages;
        $scope.conversionEvents = ConversionEvents;

        $scope.preview_view = false;
        $scope.uploadset = [
            {
                "adsetDetails": {},
                "ads": [{}]
            }
        ];

        function init() {
            $scope.isValidUploadset = false;
            $scope.accountHasUrl = false;
            $scope.angleKey = '';
            $scope.urlKey = '';

            // If adset id query parameter exists, get data and fill in document
            if ($routeParams && $routeParams.aid) {
                adsetService.httpGetAdsetContent($routeParams.aid, (res, err) => {
                    if (err) {
                        console.log("error: ", err);
                    }
                    else {
                        //console.log("ADSET INFO: ", res);
                        let adset = res[0][0];
                        let ads = res[1];
                        console.log("Adset: ", adset)
                        console.log("Ads: ", ads);
                        let adsetDetail = {
                            audience: adset.audience,
                            audienceNetwork: adset.network === 1 ? true : false,
                            budget: adset.budget,
                            country: adset.country,
                            desktop: adset.desktop === 1 ? true : false,
                            instagram: adset.instagram === 1 ? true : false,
                            interests: adset.interests,
                            maxAge: adset.age_max,
                            minAge: adset.age_min,
                            mobile: adset.mobile === 1 ? true : false,
                            rhs: adset.rhs === 1 ? true : false,
                            sex: adset.sex
                        }
                        let adsDetail = [];
                        ads.forEach(ad => {
                            let a = {
                                body: typeof ad.body === 'undefined' ? null : ad.body,
                                comment: typeof ad.comment === 'undefined' ? null : ad.comment,
                                headline: typeof ad.headline === 'undefined' ? null : ad.headline,
                                link_to_graphic: typeof ad.link_to_graphic === 'undefined' ? null : ad.link_to_graphic,
                                newsfeed_link: typeof ad.newsfeed_link === 'undefined' ? null : ad.newsfeed_link,
                                thumbnail: typeof ad.thumbnail === 'undefined' ? null : ad.thumbnail
                            };
                            adsDetail.push(a);
                        });
                        Object.assign($scope.uploadset[0].adsetDetails, adsetDetail);
                        $scope.uploadset[0].ads = adsDetail;
                    }
                });
            }

            accountService.httpGetReadyAccounts((res, err) => {
                if (err) {
                    console.log(err);
                    swal("Error", "Unable to get Available Accounts: " + err, "warning");
                }
                else {
                    //console.log(res);
                    $scope.availableAccounts = res;
                }
            });

            safeUrlService.httpGetSafeUrls((res, err) => {
                if (err) {
                    console.log(err);
                    swal("Error", "Unable to Get Safe Urls: " + err, "warning");
                }
                else {
                    $scope.allUrls = res;
                    $scope.availableUrls = res.filter(url => url.claimed === 'no');
                    $scope.availableUrlsCount = $scope.availableUrls.length;
                }
            });

            verticalService.httpGetVerticals((res, err) => {
                if (err) {
                    console.log(err);
                    swal("Error", "Unable to get Verticals: " + err, "warning");
                }
                else {
                    //console.log(res);
                    $scope.verticals = res;
                    //$scope.statusLoading = false;
                }
            });

        }
        init();

        $scope.newAdSet = function () {
            var adsetTemplate = {
                "account": {},
                "safe_url": {},
                "adsetDetails": {},
                "ads": [{}]
            };
            $scope.uploadset.push(adsetTemplate);
            console.log("New Uploadset Added. Current State:\n", $scope.uploadset);
        };

        $scope.removeAdSet = function (adset_index) {
            $scope.uploadset.splice(adset_index, 1);
        };

        $scope.duplicateAdSet = function (index) {
            //TODO: look up improving this deep copy a(maybe angular.copy?)
            //deep copy object
            var newAdset = JSON.parse(JSON.stringify($scope.uploadset[index]));
            var adsetTemplate = {
                "adsetDetails": newAdset.adsetDetails,
                "ads": newAdset.ads.map(ad => {
                    return {
                        "headline": (ad.headline == undefined ? undefined : ad.headline),
                        "body": (ad.body == undefined ? undefined : ad.body),
                        "link_to_graphic": (ad.link_to_graphic == undefined ? undefined : ad.link_to_graphic),
                        "newsfeed_link": (ad.newsfeed_link == undefined ? undefined : ad.newsfeed_link),
                        "comment": (ad.comment == undefined ? undefined : ad.comment)
                    }
                })
            };
            //console.log(adsetTemplate);
            $scope.uploadset.push(adsetTemplate);
        };

        $scope.newAd = function (parent_index) {
            // console.log('inside newAd()');
            let adTemplate = {};
            $scope.uploadset[parent_index].ads.push(adTemplate);
        };

        $scope.removeAd = function (parent_index, index) {
            $scope.uploadset[parent_index].ads.splice(index, 1);
        };

        $scope.duplicateAd = function (parent_index, index) {
            //TODO: Incomplete
            //var newAd = $scope.uploadset[parent_index].ads[index];
            //console.log("New Ad: ", newAd);
            //$scope.uploadset[parent_index].ads.push(newAd);
        };

        $scope.accountChange = function (uploadset) {
            //console.log('inside accountChange()\nuploadset.account:\n', uploadset.account);
            if ($scope.account && $scope.account.campaignUrl) $scope.account.campaignUrl = undefined;
            $scope.angleKey = uploadset.account.angle == null ? undefined : uploadset.account.angle;

            let safe_url = $scope.allUrls.filter((url) => url.safe_url_key == uploadset.account.safe_url_key);
            if (safe_url[0]) {
                $scope.availableUrls.push(safe_url[0]);
                $scope.urlKey = safe_url[0].url;
                $scope.uploadset.safe_url = safe_url[0];
                //console.log($scope.uploadset.safe_url);
            }
            else if ($scope.availableUrls.length > $scope.availableUrlsCount) {
                //remove set url
                delete $scope.uploadset.safe_url;
                $scope.urlKey = '';
                //remove all elements that have been added (i.e. unavailable urls)
                let amountToRemove = $scope.availableUrls.length - $scope.availableUrlsCount;
                $scope.availableUrls.splice($scope.availableUrlsCount, amountToRemove);
            }
            //TODO: Set/Clear fields from account{}
        };

        $scope.safeUrlClick = function (u) {
            // Used for DDL Display
            $scope.urldropdown = true;
            document.getElementById("urllist").focus();

            //console.log(u);
            $scope.urlKey = u.url;
            $scope.uploadset.safe_url = u;
        };

        $scope.hasSafeUrl = function (account) {
            //console.log("account in hasSafeUrl:", account);
            if (account.safe_url_key) {
                $scope.accountHasUrl = true;
            }
            else {
                $scope.accountHasUrl = false;
            }
        };

        $scope.createUploadset = function () {
            if (!$scope.validUploadset($scope.uploadset)) {
                console.log('invalid uploadset');
                return;
            }

            if ($scope.newUrl) {
                var urlexists = $scope.allUrls.filter(url => url.url == $scope.urlKey);
                console.log("url exists: ", urlexists);

                if (urlexists.length > 0) {
                    swal("Error", "Url Already Exists", "warning");
                }
                else {
                    var safe_url = [{
                        url: $scope.urlKey
                    }];

                    safeUrlService.httpPostNewSafeUrl(safe_url, (res, err) => {
                        if (err) {
                            console.log(err);
                            swal("Error", "Invalid Url: " + err, "warning");
                        }
                        else {
                            safe_url[0].safe_url_key = res.insertId;

                            let uploadset = {
                                campaign: $scope.uploadset,
                                safe_url: safe_url[0],
                                account: $scope.uploadset.account
                            }
                            //console.log("New URL.\nUploadset: ", $scope.uploadset);

                            campaignService.httpAddUploadset(uploadset, (result, error) => {
                                if (error) {
                                    console.log(error);
                                    swal("Error", "Unable to Upload: " + error, "warning");
                                }
                                else {
                                    //TODO: THIS COULE CAUSE ERROR - POOR LOGIC
                                    if ($scope.newUrl && $scope.accountHasUrl) {
                                        var urlToUpdate = {
                                            safe_url_key: $scope.uploadset.safe_url.safe_url_key,
                                            url: $scope.uploadset.safe_url.url,
                                            claimed: 'no'
                                        }
                                        safeUrlService.httpPutSafeUrls([urlToUpdate], (res, err) => {
                                            if (err) {
                                                console.log("Error: ", err);
                                            }
                                            else {
                                                //console.log("Successfully update old safe url: ", res);
                                                swal({
                                                    title: "Account - " + result.uploadset.account.fname + " " + result.uploadset.account.lname,
                                                    text: "Uploadset Saved!",
                                                    type: "success"
                                                });
                                                ResetPage();
                                            }
                                        });
                                    }
                                    else {
                                        swal({
                                            title: "Account - " + result.uploadset.account.fname + " " + result.uploadset.account.lname,
                                            text: "Uploadset Saved!",
                                            type: "success"
                                        });
                                        ResetPage();
                                    }
                                }
                            });
                        }
                    });
                }
            }
            else {
                let uploadset = {
                    campaign: $scope.uploadset,
                    safe_url: $scope.uploadset.safe_url,
                    account: $scope.uploadset.account
                }
                console.log("Valid Url.\nUploadset: ", $scope.uploadset);

                campaignService.httpAddUploadset(uploadset, (res, err) => {
                    if (err) {
                        console.log(err);
                        swal("Error", "Unable to Upload: " + err, "warning");
                    }
                    else {
                        swal({
                            title: "Account - " + res.uploadset.account.fname + " " + res.uploadset.account.lname,
                            text: "Uploadset Saved!",
                            type: "success"
                        });

                        ResetPage();
                        $scope.urlKey = '';
                        $scope.angleKey = '';
                    }
                });
            }

        };

        $scope.validUploadset = function (uploadset) {
            let invalidCount = 0;

            if (!uploadset.safe_url) {
                if ($scope.urlKey == '') {
                    invalidCount += 1;
                    $scope.newUrl = false;
                }
                else {
                    $scope.newUrl = true;
                }

            } else if (uploadset.safe_url.url != $scope.urlKey) {
                $scope.newUrl = true;
            } else {
                $scope.newUrl = false;
            }

            if (uploadset.account) {
                if (uploadset.account.vertical) {
                    if (uploadset.account.vertical.toString() === '')
                        invalidCount += 1;
                } else {
                    invalidCount += 1;
                }
            }
            else {
                invalidCount += 1;
            }

            for (var u of uploadset) {
                if (!u.adsetDetails) {
                    invalidCount += 1;
                }
                else {
                    if (!u.adsetDetails.sex) {
                        invalidCount += 1;
                    }
                    if (!u.adsetDetails.minAge) {
                        invalidCount += 1;
                    }
                    if (!u.adsetDetails.maxAge) {
                        invalidCount += 1;
                    }
                    if (!u.adsetDetails.country) {
                        invalidCount += 1;
                    }
                    if (!u.adsetDetails.budget) {
                        invalidCount += 1;
                    }
                }
            };
            //console.log(uploadset);
            //console.log("invalidCount: ",invalidCount);
            if (invalidCount != 0) {
                $scope.isValidUploadset = false;
                return false;
            }

            $scope.isValidUploadset = true;
            return true;
        };

        $scope.consoleLogUploadset = function () {
            $scope.validUploadset($scope.uploadset);
            console.log($scope.uploadset);
            //console.log("new url: ", $scope.newUrl, " | hasUrl: ", $scope.accountHasUrl);
            //console.log("url key: ", $scope.urlKey);
            //console.log("safe url obj: ", $scope.uploadset.safe_url);
            //console.log("SCOPE:", $scope);
        }

        function ResetPage() {
            delete $scope.uploadset;
            $scope.uploadset = [
                {
                    "account": {},
                    "safe_url": {},
                    "adsetDetails": {},
                    "ads": [{}]
                }
            ];
            init();
        }

        $scope.campaignSelected = function (account) {
            if ($scope.selectedCampaign[0]) {
                //console.log($scope.selectedCampaign[0][0]);
                account.campaignUrl = createCampaignUrl($scope.selectedCampaign[0][0].url);
                account.voluumId = $scope.selectedCampaign[0][0].id;
                account.name = $scope.selectedCampaign[0][0].name;
            }
            //console.log($scope.uploadset.account);
        };

        $scope.filterCampaigns = function () {
            // clear campaign url - filters have changed
            if ($scope.account && $scope.account.campaignUrl) delete $scope.account.campaignUrl;

            let angle, vertical;
            if ($scope.uploadset.account) {
                angle = $scope.uploadset.account.angle === null || typeof $scope.uploadset.account.angle === 'undefined' ? '' : $scope.uploadset.account.angle;
                vertical = $scope.uploadset.account.vertical === null || typeof $scope.uploadset.account.vertical === 'undefined' ? '' : $scope.uploadset.account.vertical;
                //console.log("filter:a ",angle , "| v ", vertical);
            }
            //console.log($scope.allCampaigns);
            let allCamps = JSON.parse(JSON.stringify($scope.allCampaigns));
            $scope.filteredCampaigns = allCamps.filter(c => ~c.name.indexOf(vertical));//.filter(c =>~c.name.indexOf(angle))
        }

        $scope.selectedCampaign = [];
        $scope.voluumCampaigns = [];
        /* Working Voluum Auth and Campaign GET */
        function getVoluumCampaigns() {
            //console.log("token: ", $cookieStore.get('voluum_token'));
            //console.log("time exp: ", $cookieStore.get('voluum_exp'));
            let token = $cookieStore.get('voluum_token');
            if (token) {
                $scope.loadingCamps = true;
                $http({
                    method: 'GET',
                    url: '/voluum/campaigns',
                    json: true,
                    headers: { 'x-token': token }
                }).then(function (data) {
                    console.log("Voluum Campaign Success");
                    $scope.selectedCampaign = data.data.campaigns[0];
                    $scope.allCampaigns = data.data.campaigns.sort(Tools.generateSortFunction([{ name: 'name', reverse: true }])).filter(c => !(~c.name.indexOf('traffic') || ~c.name.indexOf('Traffic')));
                    $scope.filterCampaigns();
                    $scope.loadingCamps = false;
                }, function (data) {
                    console.log("Voluum Campaign Failure: ", data);
                    $scope.loadingCamps = false;
                });
            }
            else {
                $scope.refreshCamps();
            };
        };

        $scope.refreshCamps = function () {
            $scope.loadingCamps = true;
            $http({
                method: 'GET',
                url: '/voluum/auth',
                json: true
            }).then(function (data) {
                console.log("Voluum Auth Success:");
                $cookieStore.put("voluum_token", data.data.token);
                $cookieStore.put("voluum_exp", data.data.expirationTimestamp);
                getVoluumCampaigns();
            }, function (data) {
                console.log("Voluum Auth Failure: ", data);
                $scope.loadingCamps = false;
            });
        };
        $scope.refreshCamps();

        function createCampaignUrl(campaignUrlTemplate) {
            if (!campaignUrlTemplate)
                return;

            let userInitials = $rootScope.user.fname.charAt(0).toUpperCase() + $rootScope.user.lname.charAt(0).toUpperCase();
            let keyword = $scope.uploadset.account.fname.split(' ').join('') + $scope.uploadset.account.lname.split(' ').join('');
            //console.log("keyword: ", keyword); 
            //console.log("fname: ", $scope.uploadset.account.fname.split(' ').join(''))
            //console.log("lname: ", $scope.uploadset.account.lname.split(' ').join(''));
            let newCampaignUrl = campaignUrlTemplate.split('?')[0] + '?pid=' + $scope.uploadset.account.pixel + '&keyword=' + userInitials + keyword;
            //console.log("newCampaignUrl: ", newCampaignUrl);
            return newCampaignUrl;
        }
    }
]);
