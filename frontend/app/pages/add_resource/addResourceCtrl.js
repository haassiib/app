"use strict";

app.controller("addResourceCtrl", [
  "$scope",
  "$http",
  "$rootScope",
  "$timeout",
  "$location",
  "safeUrlService",
  "batchService",
  "accountService",
  "codeService",
  "verticalService",
  "virtualMachineService",
  function(
    $scope,
    $http,
    $rootScope,
    $timeout,
    $location,
    safeUrlService,
    batchService,
    accountService,
    codeService,
    verticalService,
    virtualMachineService
  ) {
    $scope.accounts = [{}];
    $scope.safe_URLs = [{}];
    $scope.cards = [{}];
    $scope.verticals = [{}];
    $scope.batches = [];
    $scope.codes = [];
    $scope.codesddl = [];
    $scope.vms = [{}];

    $scope.fundingTypes = ["credit card", "netbanking"];
    $scope.accountTypes = CardTypes;

    $scope.newUrl = false;
    $scope.allUrls = [];
    $scope.availableUrls = [];
    $scope.urldropdown = [false];
    $scope.keys = [""];
    $scope.toggle = function(bool, index, parent) {
      //console.log("bool: ", bool, " | index: ", index);
      $scope.urldropdown[index] = bool;
      var list = document.getElementById("urllist" + index);
    };
    $scope.safeUrlClick = function(u, index, parentindex) {
      // Used for DDL Display
      // console.log(u, '\nIndex: ', index, '\nparentindex: ', parentindex);

      $scope.urldropdown[parentindex] = true;
      document.getElementById("urllist" + parentindex).focus();

      $scope.keys[parentindex] = u.url;
      $scope.accounts[parentindex].url = u.url;
    };
    $scope.keyPress = function(index, key) {
      //console.log("scope key: ", $scope.key);
      //console.log("key[" + index + "]: ", key[index]);
      var urls = $scope.availableUrls.filter(
        url => url.url.indexOf(key[index]) !== -1
      );
      //console.log("matching urls: ", urls);

      if (urls.length == 0) {
        //no safe url found, add to accounts.url
        $scope.accounts[index].url = key[index];
      } else if (urls.length == 1) {
        //safe url found add to accounts.url
        $scope.accounts[index].url = key[index];
      } else {
        //clear accounts.url until one is found
        //TODO: any then?
      }
    };

    function init() {
      $scope.accounts = [{}];
      $scope.safe_URLs = [{}];
      $scope.cards = [{}];
      $scope.verticals = [{}];

      $scope.codes = [];
      $scope.codesddl = [];
      $scope.vms = [{}];

      $scope.newUrl = false;
      $scope.allUrls = [];
      $scope.availableUrls = [];
      $scope.urldropdown = [false];
      $scope.keys = [""];

      batchService.httpGetBatches((res, err) => {
        if (err) {
          console.log(err);
          swal("Error", "Unable to get Batches: " + err, "warning");
        } else {
          $scope.batches = res;
        }
      });

      // Available Codes
      codeService.httpGetAccountCodes(false, (res, err) => {
        if (err) {
          console.log(err);
          swal("Error", "Could not retrieve account codes.", "warning");
        } else {
          $scope.codes = res;
          $scope.codesddl = $scope.codes.map(
            code => code.code_type + " | " + code.code
          );
          //console.log("Received All Available Codes: ", $scope.codes);
        }
      });

      // Available Virtual Machine List
      virtualMachineService.httpGetVms((res, err) => {
        if (err) {
          console.log(err);
          swal("Error", "Could not retrieve Virtual Machine List.", "warning");
        } else {
          $scope.vms = res;
        }
      });

      // Available URL list
      safeUrlService.httpGetSafeUrls((res, err) => {
        if (err) {
          console.log(err);
          swal("Error", "Unable to get Safe URLs" + err, "warning");
        } else {
          $scope.allUrls = res;
          $scope.availableUrls = res.filter(url => url.claimed === "no");
          $scope.urlsddl = $scope.availableUrls.map(url => url.url);
          //console.log("availableUrls: ", $scope.availableUrls);
        }
      });
    }
    init();
    // Accounts
    $scope.addAccount = function(index) {
      var accountTemplate = {};
      $scope.accounts.push(accountTemplate);
      $scope.keys.push("");
      $scope.urldropdown.push(false);
    };

    $scope.removeAccount = function(index) {
      $scope.accounts.splice(index, 1);
      $scope.keys.splice(index, 1);
      $scope.urldropdown.splice(index, 1);
    };

    // URLS
    $scope.addURL = function(index) {
      var urlTemplate = {};
      $scope.safe_URLs.push(urlTemplate);
      console.log(index);
    };

    $scope.removeURL = function(index) {
      $scope.safe_URLs.splice(index, 1);
      console.log($scope.safe_URLs);
    };

    // Cards
    $scope.addCard = function(index) {
      var cardTemplate = {};
      $scope.cards.push(cardTemplate);
      console.log($scope.cards);
    };

    $scope.removeCard = function(index) {
      $scope.cards.splice(index, 1);
      //console.log($scope.cards);
      console.log(index);
    };

    // Helper Function
    var validAccount = function(accounts) {
      var valid = true;
      var rowValid = false;
      var i;
      for (i = 0; i < accounts.length; i++) {
        if (
          typeof accounts[i].fname !== "undefined" &&
          typeof accounts[i].lname !== "undefined" &&
          typeof accounts[i].pixel !== "undefined"
        ) {
          rowValid = true;
        } else {
          rowValid = false;
        }
        valid = valid && rowValid;
      }
      return valid;
    };

    var transformAccounts = function(accounts) {
      console.log(accounts);

      return accounts.map(account => {
        console.log("ACCOUNT CC => ", account.cc);
        if (account.cc) {
          //Find Code object that matches Selected item
          var code = account.cc.split(" | ");
          var thisCC = $scope.codes.filter(cc => {
            return cc.code == code[1];
          });
          account.cc = thisCC[0];
        }

        if (account.vm.name) {
          let thisVm = $scope.vms.filter(vm => {
            return vm.name === account.vm.name;
          });
          account.vm.name = thisVm[0].name;
          account.vm.UUID = thisVm[0].UUID;
          account.vm.vm_key = thisVm[0].vm_key;

          console.log(
            "key => ",
            account.vm.vm_key,
            " uuid => ",
            account.vm.UUID,
            " name => ",
            account.vm.name
          );
        }
      });
    };

    $scope.fundingTypeChange = function(account) {
      if (account.funding_type == "netbanking") {
        //console.log('clear cc');
        delete account.cc;
      }
    };

    //TODO: this needs to be cleaned up for safe urls!
    $scope.insertAccounts = function() {
      console.log("Accounts: ", $scope.accounts);

      if ($scope.accounts.length < 1) {
        console.log("No Accounts to enter");
      } else if (validAccount($scope.accounts)) {
        //console.log("before: ", $scope.accounts);
        transformAccounts($scope.accounts);
        //console.log("after: ", $scope.accounts);

        // Create New URL list
        var newUrls = [];
        var existingUrls = [];
        var errors = 0;
        $scope.keys.forEach(key => {
          var exists = $scope.allUrls.filter(u => u.url === key);
          if (exists.length == 0) {
            console.log("key: ", key);
            if (key === "") {
              //no url
              //errors += 1;
            } else {
              newUrls.push({ url: key });
            }
          } else if (exists.length == 1) {
            existingUrls.push(exists[0]);
          } else {
            //error
            errors += 1;
          }
        });
        console.log("existing: ", existingUrls);
        console.log("new urls: ", newUrls);
        console.log("error: ", errors);

        if (errors == 0) {
          // Insert URLS
          safeUrlService.httpPostNewSafeUrl(newUrls, (res, err) => {
            if (err) {
              console.log(err);
              swal("New Urls Error", err, "warning");
            } else {
              console.log("NEW URL RES", res);
              // var startId = res.insertId;
              // var count = res.affectedRows;
              safeUrlService.httpGetSafeUrls((res, err) => {
                if (err) {
                  console.log(err);
                  swal("Error", "Unable to Add Safe URLs" + err, "warning");
                } else {
                  if (res.affectedRows) {
                    console.log(
                      "Warning: " +
                        res.affectedRows +
                        " rows updated. " +
                        res.message
                    );
                  }

                  $scope.allUrls = res;
                  $scope.availableUrls = res.filter(
                    url => url.claimed === "no"
                  );
                  $scope.urlsddl = $scope.availableUrls.map(url => url.url);

                  //This also updates the credit cards table
                  accountService.httpPostNewAccounts(
                    $scope.accounts,
                    (res, err) => {
                      if (err) {
                        //console.log("ERROR: ", err);
                        swal("Error", err.data.code, "warning");
                      } else {
                        console.log("POST new account res:", res);
                        var accountUrls = $scope.accounts
                          .map(account => account.url)
                          .filter(url => url != undefined);
                        console.log("accountsUrls: ", accountUrls);

                        if (accountUrls.length > 0) {
                          var safeUrlsToUpdate = $scope.allUrls.filter(url => {
                            return accountUrls.find(au => au === url.url);
                          });
                          console.log($scope.allUrls);
                          console.log(
                            "SAFE URLS TO UPDATE: ",
                            safeUrlsToUpdate
                          );
                          var safe_urls = safeUrlsToUpdate.map(url => {
                            url.claimed = "yes";
                            return url;
                          });
                          if (safeUrlsToUpdate.length > 0) {
                            safeUrlService.httpPutSafeUrls(
                              safe_urls,
                              (res, err) => {
                                if (err) {
                                  console.log(err);
                                  swal("Error", err, "warning");
                                } else {
                                  console.log("PUT safe url res:", res);
                                  swal(
                                    "Success!",
                                    "New Accounts Added.",
                                    "success"
                                  );
                                  // Reset Page values and data
                                  console.log("re initializing data");
                                  init();
                                }
                              }
                            );
                          } else {
                            console.log("No URLS to update as claimed.");
                            swal("Success!", "New Accounts Added.", "success");
                            // Reset Page values and data
                            console.log("re initializing data");
                            init();
                          }
                        } else {
                          console.log("PUT accounts res:", res);
                          swal("Success!", "New Accounts Added.", "success");

                          // Reset Page values and data
                          console.log("re initializing data");
                          init();
                        }
                      }
                    }
                  );
                }
              });
            }
          });
        } else {
          console.log("safe url errors:", errors);
          swal("Error", errors + " Safe Urls Errors", "warning");
        }
      } else {
        console.log("Invalid or Missing Account Info");
        swal("Error", "Invalid or Missing field information", "warning");
      }
    };

    $scope.insertURLs = function(safe_URLs) {
      console.log("Working to Create a new safe URL");

      var exists = [];
      var hasFieldData = true;
      safe_URLs.forEach(newurl => {
        if (!newurl.url) {
          hasFieldData = false;
        }
        var url = $scope.allUrls.filter(url => url.url === newurl.url);
        if (url.length > 0) exists.push(url[0]);
      });
      console.log(exists);
      console.log(hasFieldData);

      if (safe_URLs.length > 0 && exists.length == 0 && hasFieldData) {
        safeUrlService.httpPostNewSafeUrl(safe_URLs, (res, err) => {
          if (err) {
            console.log(err);
            swal("Error", "Unable to add urls:\n" + err, "warning");
          } else {
            console.log(res);
            swal("Success!", "Safe Urls have been inserted.", "success");

            // Reset Page values and data
            console.log("re-initializing data");
            init();
          }
        });
      } else if (!hasFieldData) {
        console.log("Missing Field Information");
        swal("Error", "Missing Field Information", "warning");
      } else if (exists.length > 0) {
        var badurls = "";
        exists.forEach(url => (badurls += url.url + "\n "));
        swal("Error", "These URLs already exist:\n" + badurls, "warning");
      } else {
        console.log("No Safe URLs to enter.");
      }
    };

    var validCards = function(codes) {
      let count = [];
      count = codes
        .map(code => {
          return code.number === undefined ||
            code.cvc === undefined ||
            code.exp_month === undefined ||
            code.exp_year === undefined ||
            code.zip === undefined ||
            code.accounttype === undefined
            ? false
            : true;
        })
        .filter(item => item === true);

      console.log("count: ", count);
      if (count.length < codes.length) {
        console.log("cards invalid");
        return false;
      } else {
        console.log("cards valid");
        return true;
      }
    };

    var validateCards = function(codes) {
      let count = [];
      count = codes
        .map(code => {
          return code.number === undefined ? false : true;
        })
        .filter(item => item == true);

      console.log("count: ", count);
      if (count.length < codes.length) {
        console.log("cards invalid");
        return false;
      } else {
        console.log("cards valid");
        return true;
      }
    };

    $scope.insertCards = function(cards) {
      console.log("Working to create cards.");
      if (cards.length < 1) {
        console.log("No cards to enter.");
      } else if (validateCards(cards)) {
        console.log("cards were valid: \n", cards);
        codeService.httpPostAccountCodes(cards, (res, err) => {
          if (err) {
            console.log(err);
            swal("Error", err, "warning");
          } else {
            console.log(res);
            swal("Success!", "Account Codes Added.", "success");

            // Reset Page values and data
            console.log("re initializing data");
            init();
          }
        });
      } else {
        console.log("Invalid Card values");
        swal("Error", "Invalid or Missing field information", "warning");
      }
    };

    $scope.insertVerticals = function(verticals) {
      verticals = verticals.map(v => {
        return Tools.CapitalizeFirstLetter(v.name);
      });

      verticalService.httpPostInsertVertical(verticals, (res, err) => {
        if (err || res.status > 299) {
          swal(
            "Error",
            "Unsuccessful insert, try again or contact support.",
            "error"
          );
        } else {
          swal("Success!", "Successfully inserted new Verticals.", "success");
          init();
        }
      });
    };

    $scope.addVertical = function(index) {
      let verticalTemplate = {};
      $scope.verticals.push(verticalTemplate);
    };

    $scope.removeVertical = function(index) {
      $scope.verticals.splice(index, 1);
      //console.log($scope.verticals);
      console.log(index);
    };
  }
]);
