﻿"use strict";

app.controller("secretSauceCtrl", [
    "$scope",
    "$http",
    "$rootScope",
    "$cookieStore",
    "alertService",
    "authService",
    "contentService",
    "$timeout",
    "accountService",
    function (
        $scope,
        $http,
        $rootScope,
        $cookieStore,
        alertService,
        authService,
        contentService,
        $timeout,
        accountService
    ) {
        $rootScope.user.password = "";
        $rootScope.user.remember = "";
        $scope.loading = true;
        $scope.verticalReverse = false;
        $scope.geoReverse = false;
        $scope.sexReverse = false;
        $scope.list = true;
        $scope.grid = false;


        $scope.geos = ["All Geos"];
        $scope.filteredAccounts = [];
        //$scope.verticals = Verticals;
        $scope.verticals = ["All Verticals"];
        $scope.angles = ["All Angles"];
        $scope.sortByFilters = ["ROI", "Lifetime Spend", "Revenue", "Profit"];

        $scope.sexes = ["Select All", "Both Sexes", "Men", "Women"];
        $scope.userFilter = "All Users";
        $scope.angleFilter = "All Angles";
        //$scope.verticalFilter = [];
        $scope.verticalFilter = "All Verticals";
        $scope.geoFilter = "All Geos";
        $scope.sortByFilter = "ROI";

        $scope.users = [
            "All Users",
            "Ashif",
            "Joe",
            "Justin",
            "Luke",
            "Marcus",
            "Mike",
            "Ryan",
            "Scott",
            "Stan",
            "Trey",
            "Valerie",
            "Zach",
            "Zoe"
        ];

        $scope.showHide = function (listGrid) {
            //list default true
            if (listGrid === 'list') {
                $scope.list = true;
                $scope.grid = false;
            }
             //grid default false
            if (listGrid === 'grid') {
                $scope.grid = true;
                $scope.list = false;
            }
        };
        $scope.dtFrom = moment()
            .subtract(1, "months")
            .format("MM-DD-YYYY");
        $scope.dtTo = moment().format("MM-DD-YYYY");

        const onlyUnique = (value, index, self) => {
            return self.indexOf(value) === index;
        };

        const contentPromise = () => {
            return new Promise((resolve, reject) => {
                contentService.httpGetPerformantContent((res, err) => {
                    if (err) {
                        console.log(err);
                        swal("Error", "Unable to get Ad Content: " + err, "warning");
                        reject(err);
                    } else {
                        //console.log("res: ", res);
                        let allAds = res.ads;
                        resolve(allAds);
                    }
                });
            });
        };

        const getAllAdsData = async () => {
            try {
                const allAds = await contentPromise();
                $scope.allAds = allAds;

                //console.log("$scope.allAds => ", $scope.allAds);

                //set angles in scope
                $scope.angles = allAds
                    .map(ad => {
                        if (
                            ad.angle !== null &&
                            ad.angle !== undefined &&
                            ad.angle !== ""
                        ) {
                            return ad.angle;
                        }
                    })
                    .filter(onlyUnique);
                $scope.angles.push("All Angles");

                //Set Geos in scope
                $scope.geos = allAds
                    .map(ad => {
                        if (
                            ad.country !== null &&
                            ad.country !== undefined &&
                            ad.country !== ""
                        ) {
                            return ad.country;
                        }
                    })
                    .filter(onlyUnique);
                $scope.geos.push("All Geos");

                //Set verticals in scope
                $scope.verticals = allAds
                    .map(ad => {
                        return ad.vertical;
                    })
                    .filter(onlyUnique)
                    .sort();
                //$scope.verticals.push("All Verticals");
                //console.log("$scope.verticals :", $scope.verticals);

                // console.log(
                //   "$scope.allAds => ",
                //   $scope.allAds.sort((a, b) => b.roi - a.roi)
                // );

                //Set Filtered ads in scope
                $scope.filteredAds = filterAds(
                    $scope.allAds,
                    $scope.userFilter,
                    $scope.angleFilter,
                    $scope.verticalFilter,
                    $scope.geoFilter,
                    $scope.sortByFilter,
                    $scope.dtFrom,
                    $scope.dtTo
                );
            } catch (error) {
                if (error) {
                    console.error(
                        "error in the secretSauceCtrl getAllAdsData method > ",
                        error
                    );
                }
            }
        };

        $scope.saveContent = function (ad) {
            contentService.httpPutPerformantContent(ad, (res, err) => {
                if (err) {
                    console.log(err);
                    swal("Error", "Unable to save Ad Content: " + err, "warning");
                } else {
                    console.log("SUCCESS? :", res);
                    swal("Success", "Successfully saved content." + err, "success");
                    init();
                }
            });
        };

        function filterAds(
            ads,
            userFilter,
            angleFilter,
            verticalFilter,
            geoFilter,
            sortByFilter,
            dtFrom,
            dtTo
        ) {
            let filteredAds = ads
                .filter(e => {
                    if (moment(e.created) !== NaN || moment(e.created !== null)) {
                        return (
                            moment(e.created).isSameOrAfter(dtFrom) &&
                            moment(e.created).isSameOrBefore(dtTo)
                        );
                    }
                })
                .filter(a => {
                    //   console.log("a => ", a);
                    if (userFilter === "All Users") {
                        return a;
                    } else {
                        return a.owner === userFilter;
                    }
                })
                .filter(b => {
                    // console.log("b => ", b);
                    if (angleFilter === "All Angles") {
                        return b;
                    } else {
                        return b.angle === angleFilter;
                    }
                })
                .filter(c => {
                    // console.log("c => ", c);
                    if (verticalFilter === "All Verticals") {
                        return c;
                    } else {
                        return c.vertical === verticalFilter;
                    }
                })
                .filter(g => {
                    // console.log("g => ", g);
                    if (geoFilter === "All Geos") {
                        return g;
                    } else {
                        return g.country === geoFilter;
                    }
                })
                .filter(z => {
                    if (z.roi >= 100 && z.profit >= 1000) {
                        return z;
                    }
                })
                .sort((a, b) => {
                    if (sortByFilter === "ROI") {
                        return b.roi - a.roi;
                    }
                    if (sortByFilter === "Lifetime Spend") {
                        return b.total_spend - a.total_spend;
                    }
                    if (sortByFilter === "Revenue") {
                        return b.revenue - a.revenue;
                    }
                    if (sortByFilter === "Profit") {
                        return b.profit - a.profit;
                    }
                });

            //"ROI", "Lifetime Spend", "Revenue", "Profit"

            // console.log("filteredAds => ", filteredAds);
            $scope.dataLoaded = true;
            return filteredAds;
        }

        const filterChanged = ($scope.filterChanged = function () {
            $scope.filteredAds = filterAds(
                $scope.allAds,
                $scope.userFilter,
                $scope.angleFilter,
                $scope.verticalFilter,
                $scope.geoFilter,
                $scope.sortByFilter,
                $scope.dtFrom,
                $scope.dtTo
            );
        });

        const init = async () => {
            await $timeout(getAllAdsData);
        };

        init();
    }
]);
