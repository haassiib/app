'use strict';

app.controller('contactCtrl', ['$scope', '$http', '$rootScope', '$location', 'alertService',
    function($scope, $http, $rootScope, $location, alertService) {

        $scope.disableSend = true;
        $scope.contactUsMessage = "";
        $scope.subject = "";

        $scope.$on('Report-Bug-Clicked', function () {
            setTimeout(function() {
                // $scope.subject = $rootScope.current.templateUrl;
                // $scope.subject = $rootScope.next.templateUrl;
            },200);
        });

        $scope.updateContactUsForm = function() {
            // $scope.subject = $rootScope.current.templateUrl;

            if (($scope.bugDetails != "") && ($scope.bugLocation != "")) {
                $scope.disableSend = false;
            } else {
                $scope.disableSend = true;
            }
        }

        $scope.sendBugToTech = function() {
            alertService.add('bug_sent', "Thanks!  Your bug has been sent.");

            var sendBugContent = {
                method: "POST",
                url: $rootScope.urlRoot + '/bugs',
                headers: {
                    'Content-Type': 'application/json',
                    "x-username": $rootScope.user.username,
                    "x-role": $rootScope.user.role 
                },
                data: {
                    'page': $scope.bugLocation,
                    'description': $scope.bugDetails
                }
            };

            // send email
            $http(sendBugContent).success(function(data) {
                //console.log("successfully sent email");
                console.log(data);
                swal("Success!", "Your bug has been reported to development.", "success");

            }).error(function(data, status) {
                console.log(data);
                console.log(status);
            });
        }
    }
]);
