"use strict";

app.controller("activeAccountsCtrl", [
  "$scope",
  "$http",
  "$rootScope",
  "$location",
  "$cookieStore",
  "alertService",
  "$timeout",
  "campaignService",
  "accountService",
  "codeService",
  "statusService",
  "safeUrlService",
  "accountHistoryService",
  "pnlService",
  "accountCommentService",
  "authService",
  function(
    $scope,
    $http,
    $rootScope,
    $location,
    $cookieStore,
    alertService,
    $timeout,
    campaignService,
    accountService,
    codeService,
    statusService,
    safeUrlService,
    accountHistoryService,
    pnlService,
    accountCommentService,
    authService
  ) {
    $scope.loading = true;
    $scope.role = Number($rootScope.user.role);
    $scope.user = $rootScope.user.username;
    $scope.users = [
      "All Users",
      "hasib",
      "Joe",
      "Justin",
      "Luke",
      "Marcus",
      "Mike",
      "Ryan",
      "Scott",
      "Stan",
      "Trey",
      "Valerie",
      "Zach",
      "Zoe"
    ];
    $scope.state = "Active";
    $scope.statuses = [
      "All Accounts",
      "Active",
      "Dead",
      "Processing",
      "Hidden"
    ];
    // Account Settings
    $scope.as = {};
    if ($rootScope.userSettings) {
      // console.log("rootscope user settings existed");
      $scope.as = JSON.parse(JSON.stringify($rootScope.userSettings));
    }

    $scope.as.userFilter = $cookieStore.get("active_userFilter")
      ? Tools.CapitalizeFirstLetter($cookieStore.get("active_userFilter"))
      : Tools.CapitalizeFirstLetter($scope.user);
    $scope.as.statusFilter = $cookieStore.get("active_statusFilter")
      ? Tools.CapitalizeFirstLetter($cookieStore.get("active_statusFilter"))
      : Tools.CapitalizeFirstLetter($scope.state);
    $scope.as.showAccount =
      $cookieStore.get("active_sort_account") != null
        ? $cookieStore.get("active_sort_account")
        : 0;
    $scope.as.showVertical =
      $cookieStore.get("active_sort_vertical") != null
        ? $cookieStore.get("active_sort_vertical")
        : 0;
    $scope.as.showAngle =
      $cookieStore.get("active_sort_angle") != null
        ? $cookieStore.get("active_sort_angle")
        : 0;
    $scope.as.showCountry =
      $cookieStore.get("active_sort_country") != null
        ? $cookieStore.get("active_sort_country")
        : 0;
    $scope.as.showBudget =
      $cookieStore.get("active_sort_budget") != null
        ? $cookieStore.get("active_sort_budget")
        : 0;
    $scope.as.showSpend =
      $cookieStore.get("active_sort_spend") != null
        ? $cookieStore.get("active_sort_spend")
        : 0;
    $scope.as.showLaunched =
      $cookieStore.get("active_sort_launched") != null
        ? $cookieStore.get("active_sort_launched")
        : 0;
    $scope.as.showCC =
      $cookieStore.get("active_sort_cc") != null
        ? $cookieStore.get("active_sort_cc")
        : 0;
    $scope.as.showURL =
      $cookieStore.get("active_sort_url") != null
        ? $cookieStore.get("active_sort_url")
        : 0;
    $scope.as.showRecovered =
      $cookieStore.get("active_sort_recovered") != null
        ? $cookieStore.get("active_sort_recovered")
        : 0;
    $scope.as.showStatus =
      $cookieStore.get("active_sort_status") != null
        ? $cookieStore.get("active_sort_status")
        : 0;
    $scope.as.lastSort =
      $cookieStore.get("active_last_sort") != null
        ? $cookieStore.get("active_last_sort")
        : null;
    $scope.as.lastOrder =
      $cookieStore.get("active_last_order") != null
        ? $cookieStore.get("active_last_order")
        : null;
    //console.log($scope.as);

    function init() {
      campaignService.httpGetCampaigns((res, err) => {
        if (err) {
          console.log(err);
          swal("Error", "Error Retrieving Campaigns: " + err, "warning");
        } else {
          //console.log("Campaigns success: ", res);
          $scope.allAccounts = res.filter(
            acct => acct.status !== "Created" && acct.status !== "Ready"
          );
          $scope.filterAccounts();
          // apply last sort
          $scope.sort($scope.as.lastSort, $scope.as.lastOrder);
          $scope.loading = false;
        }
      });

      statusService.httpGetStatus((res, err) => {
        if (err) {
          console.log(err);
          swal("Error", "Error Retrieving Statuses: " + err, "warning");
        } else {
          $scope.status = res;
        }
      });
      $scope.allAccounts = [...new Set($scope.accounts)];
    }
    init();
      //Set color to selected row
      $scope.selectedRow = null;
      $scope.changeSeletedtRowColor = function (index) {
          $scope.selectedRow = index;
      }
    $scope.saveAll = function() {
      let changedAccounts = $scope.accounts.filter(
        a => typeof a.changed !== "undefined" && a.changed === true
      );
      //console.log("Accounts to Update:", changedAccounts);
      if (changedAccounts.length > 0) {
        accountService.httpPutUpdateAccounts(changedAccounts, (res, err) => {
          if (err) {
            console.log("error:", err);
            swal(
              "Error",
              "An error was encountered updating the Accounts",
              "error"
            );
          } else {
            console.log("Put Accounts success");
            let codes = [];
            //TODO: change this to map
            changedAccounts.forEach(account => {
              if (account.code_key && account.code_status) {
                let code = {
                  account_key: account.account_key,
                  code_key: account.code_key,
                  status: account.code_status,
                  changed: false,
                  claimed: true
                };
                //console.log("Code: ", code);
                codes.push(code);
              }
            });

            let safe_urls = [];
            changedAccounts.forEach(account => {
              if (account.safe_url_key && account.safe_url_status) {
                let safe_url = {
                  safe_url_key: account.safe_url_key,
                  url: account.safe_url,
                  status: account.safe_url_status,
                  claimed: account.safe_url_claimed
                };
                //console.log("Safe URL: ", safe_url);
                safe_urls.push(safe_url);
              }
            });
            let commentAccounts = changedAccounts.filter(a => a.commentChanged);
            if (commentAccounts.length > 0) {
              commentAccounts.forEach(
                a => (a.comment = a.last_account_comment)
              );
              accountCommentService.httpPostAccountsComments(
                commentAccounts,
                (res, err) => {
                  if (err) {
                    console.log(err);
                    swal(
                      "Error",
                      "Unable to Update Accounts Comments: " + err,
                      "warning"
                    );
                    $scope.loading = false;
                  } else {
                    console.log("Post Account Comments Success");
                  }
                }
              );
            }

            let statusAccounts = changedAccounts.filter(a => a.statusChanged);
            if (statusAccounts.length > 0) {
              accountHistoryService.httpPutAccountHistory(
                statusAccounts,
                (res, err) => {
                  if (err) {
                    console.log(err);
                    swal(
                      "Error",
                      "Unable to Update Account History: " + err,
                      "warning"
                    );
                    $scope.loading = false;
                  } else {
                    console.log("Put Account History success");
                  }
                }
              );
              let pnlAccountKeys = statusAccounts.reduce(
                (pnlAccts, account) => {
                  if (
                    account.status.toLowerCase() === "spending, in pl" ||
                    account.status.toLowerCase() === "reupload" ||
                    account.status.toLowerCase() === "in progress"
                  )
                    pnlAccts.push(account.account_key);
                  return pnlAccts;
                },
                []
              );
              //console.log(pnlAccountKeys);
              if (pnlAccountKeys.length > 0) {
                pnlService.httpPostPnlItem(pnlAccountKeys, (res, err) => {
                  if (err) {
                    console.log(err);
                    swal(
                      "Error",
                      "Unable to insert Pnl Item. Please insert manually for today. Error: " +
                        err,
                      "warning"
                    );
                    $scope.loading = false;
                  } else {
                    console.log("insert new pnl item success");
                  }
                });
              }
            }

            let budgetAccounts = changedAccounts.filter(a => a.budgetChanged);
            if (budgetAccounts.length > 0) {
              let pnlAccountsToUpdate = budgetAccounts.reduce(
                (accounts, account) => {
                  let a = {
                    account_key: account.account_key,
                    expenses: account.budget,
                    date: moment().format("YYYY-MM-DD")
                  };
                  accounts.push(a);
                  return accounts;
                },
                []
              );
              console.log("accounts to update:", pnlAccountsToUpdate);

              pnlService.httpPutExpensesByAccount(
                pnlAccountsToUpdate,
                (res, err) => {
                  if (err) {
                    console.log(err);
                    swal(
                      "Error",
                      "Unable to update Pnl expense. Please try again or update manually. Error: " +
                        err,
                      "warning"
                    );
                    $scope.loading = false;
                  } else {
                    console.log("update account budget success");
                  }
                }
              );
            }

            if (codes.length > 0) {
              codeService.httpPutAccountCode(codes, (res, err) => {
                if (err) {
                  console.log(err);
                  swal(
                    "Error!",
                    "Account Cards not updated.\n" + err,
                    "warning"
                  );
                } else {
                  if (safe_urls.length > 0) {
                    //console.log("Safe Url: ", safe_urls);
                    safeUrlService.httpPutSafeUrls(safe_urls, (res, err) => {
                      if (err) {
                        console.log(err);
                        swal(
                          "Error!",
                          "Safe_Url Status not updated.\n" + err,
                          "warning"
                        );
                      } else {
                        console.log("Safe Url Updated");
                        swal("Success!", "Account Updated Updated", "success");
                        init();
                      }
                    });
                  } else {
                    console.log("Account Update success");
                    swal("Success!", "Accounts Updated", "success");
                    init();
                  }
                }
              });
            } else if (safe_urls.length > 0) {
              console.log("Safe Urls: ", safe_urls);
              safeUrlService.httpPutSafeUrls(safe_urls, (res, err) => {
                if (err) {
                  console.log(err);
                  swal(
                    "Error!",
                    "Safe_Url Status not updated.\n" + err,
                    "warning"
                  );
                } else {
                  console.log("Safe Url Updated, no codes");
                  swal("Success!", "Account Updated Updated", "success");
                  //Reset Page Values
                  init();
                }
              });
            } else {
              swal("Success!", "Accounts Updated", "success");
              //Reset Page Values
              init();
            }
          }
        });
      } else {
        swal("Warning", "No Account Changes to Save.", "warning");
      }
    };

    $scope.filterAccounts = function() {
      $scope.accounts = JSON.parse(JSON.stringify($scope.allAccounts));
      //console.log("user: ", $scope.as.userFilter, " | status ", $scope.as.statusFilter);
      filterByUser($scope.as.userFilter);
      filterByStatus($scope.as.statusFilter);
      $scope.accounts = filterOutDupes($scope.accounts, "account_key");
      $scope.sort($scope.as.lastSort, $scope.as.lastOrder);
      // console.log("filtered accounts: ", $scope.accounts);
    };

    //TODO: create a closure with all accounts on init()
    //TODO: save all filter status types as arrays right away
    //TODO: on status change, set scope.accounts, then filter.
    //TODO: then on reinit, reset accounts

    function filterOutDupes(myArr, prop) {
      return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
      });
    }

    function filterByUser(user) {
      $scope.accounts =
        user === "All Users"
          ? $scope.allAccounts
          : $scope.allAccounts.filter(
              acct =>
                Tools.CapitalizeFirstLetter(acct.owner) ===
                Tools.CapitalizeFirstLetter(user)
            );
    }

    function filterByStatus(status) {
      if (status === "Active") {
        $scope.accounts = $scope.accounts.filter(
          acct =>
            acct.status.toLowerCase().indexOf("dead") < 0 &&
            acct.status !== "Hidden"
        );
      } else if (status === "Dead") {
        $scope.accounts = $scope.accounts
          .filter(acct => acct.status.toLowerCase().indexOf("dead") >= 0)
          .sort(Tools.generateSortFunction([{ name: "dead", reverse: false }]));
      } else if (status === "Hidden") {
        $scope.accounts = $scope.accounts.filter(
          acct => acct.status === "Hidden"
        );
      } else if (status === "Processing") {
        $scope.accounts = $scope.accounts.filter(
          acct => acct.recovered && acct.recovered === "processing"
        );
      } else {
        let activeAccounts = $scope.accounts.filter(
          acct =>
            acct.status.toLowerCase().indexOf("dead") < 0 &&
            acct.status !== "Hidden"
        );
        let deadAccounts = $scope.accounts.filter(
          acct => acct.status.toLowerCase().indexOf("dead") >= 0
        );
        let hiddenAccounts = $scope.accounts.filter(
          acct => acct.status === "Hidden"
        );
        $scope.accounts = [
          ...activeAccounts,
          ...deadAccounts,
          ...hiddenAccounts
        ];
      }
    }

    $scope.sort = function(field, order) {
      //console.log("Sort. Field: ", field, " | order: ", order);
      $scope.as.lastSort = field;
      $scope.as.lastOrder = order;
      $cookieStore.put("active_last_sort", field);
      $cookieStore.put("active_last_order", order);

      let propsToSort = [];
      let ts = {
        name: field,
        reverse: order === "ascending" ? true : false
      };
      propsToSort.push(ts);

      let snd;
      if (field === "vertical" || field === "angle") {
        snd = {
          name: "country",
          reverse: order == "ascending" ? true : false
        };
        //console.log("snd1: ", snd);
      } else if (field == "country") {
        snd = {
          name: "vertical",
          reverse: order == "ascending" ? true : false
        };
        //console.log("snd2: ", snd);
      }
      if (snd) propsToSort.push(snd);

      $scope.accounts.sort(Tools.generateSortFunction(propsToSort));
    };

    $scope.accountChanged = function(account) {
      account.changed = true;
    };

    $scope.budgetChanged = function(account) {
      account.budgetChanged = true;
    };

    $scope.statusChanged = function(account) {
      console.log("status changed. need to update account history.");
      account.statusChanged = true;
      if (account.status === "Recovered") {
        account.recovered = "yes";
      }
    };

    $scope.recoveredChanged = function(account) {
      console.log("recovered status changed.");
      if (account.recovered === "yes") {
        account.status = "Recovered";
        $scope.statusChanged(account);
      } else if (account.recovered === "no") {
        account.status = "Hidden";
        $scope.statusChanged(account);
      }
      account.owner = Tools.CapitalizeFirstLetter($scope.user);
      //console.log("user: ", $scope.user, " | Account:", account);
    };

    $scope.commentChanged = function(account) {
      console.log("comment changed. need to update account comments");
      account.commentChanged = true;
    };

    $scope.updateConfig = function() {
      // string
      $cookieStore.put("active_userFilter", $scope.as.userFilter);
      $cookieStore.put("active_statusFilter", $scope.as.statusFilter);
      $cookieStore.put("active_last_sort", $scope.as.lastSort);
      $cookieStore.put("active_last_order", $scope.as.lastOrder);
      // true/false
      $cookieStore.put("active_sort_account", ~~$scope.as.showAccount);
      $cookieStore.put("active_sort_vertical", ~~$scope.as.showVertical);
      $cookieStore.put("active_sort_angle", ~~$scope.as.showAngle);
      $cookieStore.put("active_sort_country", ~~$scope.as.showCountry);
      $cookieStore.put("active_sort_budget", ~~$scope.as.showBudget);
      $cookieStore.put("active_sort_spend", ~~$scope.as.showSpend);
      $cookieStore.put("active_sort_launched", ~~$scope.as.showLaunched);
      $cookieStore.put("active_sort_cc", ~~$scope.as.showCC);
      $cookieStore.put("active_sort_url", ~~$scope.as.showURL);
      $cookieStore.put("active_sort_recovered", ~~$scope.as.showRecovered);
      $cookieStore.put("active_sort_status", ~~$scope.as.showStatus);

      let activeSettings = JSON.parse(JSON.stringify($scope.as));
      //console.log("Active Settings: ", activeSettings);
      Object.assign($rootScope.userSettings, activeSettings);
      // PUSH TO DB
      authService.httpPutUserSettings((res, err) => {
        if (err) {
          console.log("error updated user settings: ", err);
        } else {
          //console.log("update success: ", res);
        }
      });
    };

    $scope.copyToClipboard = function(field, account) {
      console.log("Account: ", account);
      console.log(account.campaign_url, account.pixel);
      console.log(typeof account.campaign_url, typeof account.pixel);
      let valueToClip;
      if (field === "pixel") {
        valueToClip = account.pixel ? account.pixel : "error";
      } else if (field === "campaign_url") {
        valueToClip = account.campaign_url ? account.campaign_url : "error";
      } else {
        valueToClip = "error";
      }

      if (valueToClip === "error") {
        swal(
          "Error",
          "There was an issue copying item to clipboard. Verify the value exists.",
          "error"
        );
        return;
      }

      let textArea = document.createElement("textarea");
      textArea.style.position = "fixed";
      textArea.style.top = 0;
      textArea.style.left = 0;
      // Ensure it has a small width and height. Setting to 1px / 1em
      // doesn't work as this gives a negative w/h on some browsers.
      textArea.style.width = "2em";
      textArea.style.height = "2em";
      // We don't need padding, reducing the size if it does flash render.
      textArea.style.padding = 0;
      // Clean up any borders.
      textArea.style.border = "none";
      textArea.style.outline = "none";
      textArea.style.boxShadow = "none";
      // Avoid flash of white box if rendered for any reason.
      textArea.style.background = "transparent";
      textArea.value = valueToClip;

      document.body.appendChild(textArea);
      textArea.select();
      try {
        let successful = document.execCommand("copy");
        let msg = successful ? "successful" : "unsuccessful";
        console.log("Copying text command was " + msg);
      } catch (err) {
        console.log("Oops, unable to copy: ", err);
      }
      document.body.removeChild(textArea);
    };

    $scope.isDead = function(status) {
      if (
        status.toLowerCase().indexOf("dead") >= 0 ||
        status.toLowerCase().indexOf("hidden") >= 0
      )
        return true;
      return false;
    };

    $scope.onhover = function(event, index) {
      //console.log("event: ", event);
      //console.log("index: ", index);
      //var left = e.clientx + "px";
      //var top = e.clienty + "px";
      //var div = document.getelementbyid(divid);
      //div.style.left = left;
      //div.style.top = top;
      //$("#" + divid).toggle();
      //return false;
    };

    $scope.setRowSelector = function($event) {
      //console.log(arguments);
      //console.log($event);
      $("tr").removeClass("selected");
      let row = $event.currentTarget;
      row.classList.add("selected");
    };

    $scope.setRowClass = function(status) {
      if (status.toLowerCase().indexOf("dead") >= 0 || status === "Hidden") {
        return "dead_acct";
      } else if (
        status === "Problem" ||
        status === "Photo Lockout" ||
        status === "Phone Lockout"
      ) {
        return "action_acct";
      } else if (status === "Spending, In PL") {
        return "good_acct";
      } else if (
        status === "Reupload" ||
        status === "Reupload Complete" ||
        status === "Finished Upload" ||
        status === "Resolved" ||
        status === "Optimize"
      ) {
        return "warn_acct";
      } else if (status === "Needs Upload" || status === "Needs Bump") {
        return "needs_acct";
      } else if (status === "Recovered") {
        return "rec_acct";
      }
    };
  }
]);
