"use strict";

app.controller("warmAccountsCtrl", [
  "$scope",
  "$http",
  "$rootScope",
  "$location",
  "alertService",
  "$timeout",
  "accountService",
  "accountHistoryService",
  "accountCommentService",
  function(
    $scope,
    $http,
    $rootScope,
    $location,
    alertService,
    $timeout,
    accountService,
    accountHistoryService,
    accountCommentService
  ) {
    $scope.loading = true;
    $scope.user = $rootScope.user.username;
    $scope.role = Number($rootScope.user.role);
    $scope.users = [
      "Ashif",
      "Joe",
      "Justin",
      "Luke",
      "Marcus",
      "Mike",
      "Ryan",
      "Scott",
      "Stan",
      "Trey",
      "Valerie",
      "Zach",
      "Zoe"
    ];

    function init() {
      accountService.httpGetWarmingAccounts((res, err) => {
        if (err) {
          console.log(err);
        } else {
          //console.log("all pulled accounts: ", res);
          $scope.accounts = res;
          $scope.loading = false;
        }
      });
      $scope.status = ["Ready", "Warming", "Dead"];
    }
    init();

    $scope.saveAccount = function(account, index) {
      $scope.loading = true;
      accountService.httpPutUpdateAccounts([account], (res, err) => {
        if (err) {
          console.log(err);
          swal("Error", "Unable to Update Account: " + err, "warning");
          $scope.loading = false;
        } else {
          console.log("update account success: ", res);
          if (account.commentChanged) {
            account.comment = account.last_account_comment;
            accountCommentService.httpPostAccountComment(
              account,
              (res, err) => {
                if (err) {
                  console.log("Update account comment error: ", err);
                  swal(
                    "Error",
                    "Unable to Update Account Comment: " + err,
                    "warning"
                  );
                  $scope.loading = false;
                } else {
                  console.log("account comment history success: ", res);
                }
              }
            );
          }
          // Update Account History With Status Change
          if (account.statusChanged) {
            accountHistoryService.httpPutAccountHistory(
              [account],
              (res, err) => {
                if (err) {
                  console.log(err);
                  swal(
                    "Error",
                    "Unable to Update Account History: " + err,
                    "warning"
                  );
                  $scope.loading = false;
                } else {
                  console.log("account history success: ", res);
                  init();
                  swal(
                    "Success",
                    "The account was successfully updated",
                    "success"
                  );
                }
              }
            );
          } else {
            init();
            swal("Success", "The account was successfully updated", "success");
          }
          //Reset Page Values
        }
      });
    };

    $scope.saveAll = function() {
      let changedAccounts = $scope.accounts.filter(
        a => typeof a.changed !== "undefined" && a.changed === true
      );
      console.log("Accounts to Update:", changedAccounts);
      if (changedAccounts.length > 0) {
        accountService.httpPutUpdateAccounts(changedAccounts, (res, err) => {
          if (err) {
            console.log("error:", err);
            swal(
              "Error",
              "An error was encountered updating the Accounts",
              "error"
            );
          } else {
            console.log("Put Accounts Success.");
            let historyAccounts = changedAccounts.filter(a => a.statusChanged);
            if (historyAccounts.length > 0) {
              accountHistoryService.httpPutAccountHistory(
                historyAccounts,
                (res, err) => {
                  if (err) {
                    console.log(err);
                    swal(
                      "Error",
                      "Unable to Update Account History: " + err,
                      "warning"
                    );
                    $scope.loading = false;
                  } else {
                    console.log("account history success: ", res);
                    swal(
                      "Success",
                      "Successfully updated all edited accounts.",
                      "success"
                    );
                    init();
                  }
                }
              );
            } else {
              swal(
                "Success",
                "Successfully updated all edited accounts.",
                "success"
              );
              init();
            }
            let commentAccounts = changedAccounts.filter(a => a.commentChanged);
            if (commentAccounts.length > 0) {
              console.log("TODO: update account comments en masse");
            }
          }
        });
      } else {
        swal("Warning", "No Account Changes to Save.", "warning");
      }
    };

    $scope.deleteAccount = function(id, index) {
      console.log("Account ids: ", id, index);
      swal(
        {
          title: "WARNING - Account Deletion",
          text:
            "This will delete all data tied to this account, including history and ",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Delete Account!",
          closeOnConfirm: false
        },
        function(isConfirm) {
          let accounts = [{ account_key: id }];
          accountService.httpDeleteAccounts(accounts, (res, err) => {
            if (err) {
              swal("Warning!", "Account deletion failed.", "warning");
              console.log("error deleting account: ", err);
            } else {
              swal("Deleted!", "Account has been deleted.", "success");
              init();
            }
          });
        }
      );
    };

    $scope.sort = function(field, order) {
      //console.log("Sort. Field: ", field, " | order: ", order);
      let propsToSort = [];
      let ts = {
        name: field,
        reverse: order === "ascending" ? true : false
      };
      propsToSort.push(ts);
      $scope.accounts.sort(Tools.generateSortFunction(propsToSort));
    };

    $scope.accountChanged = function(account) {
      account.changed = true;
    };

    $scope.statusChanged = function(account) {
      console.log("status changed. need to update account history.");
      account.statusChanged = true;
    };

    $scope.commentChanged = function(account) {
      console.log("comment changed. need to update account comments");
      account.commentChanged = true;
    };
  }
]);
