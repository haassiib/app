﻿"use strict";

app.controller("datatableCtrl", [
  "$scope",
  "$http",
  "$rootScope",
  "$location",
  "alertService",
  "$timeout",
  "datatableService",

  function (
    $scope,
    $http,
    $rootScope,
    $location,
    alertService,
    $timeout,
    datatableService
  ) {
    $scope.loading = true;
    $scope.user = $rootScope.user.username;
    $scope.role = $rootScope.user.role;
    $scope.batches = [{}];
    $scope.batchesToUpdate = [{}];
    $scope.success = false;
    $scope.error = false;
    $scope.exp_month = "abc";
    $scope.exp_year = "abcd";
    $scope.zip = "abce";
    $scope.selectedColumns = [
      "name",
      "description",
      "safe_spend",
      "proxy",
      "warm_time",
      "safe_ads",
      "pre_warming_activities",
      "pre_launch_activities"
    ];
      //Date range picker
      $('#datepicker').datepicker({
          //orientation: "top auto"
      });
      //Date range picker
      $('.select2').select2();
      $('.select2basic').select2();
      
      //Date range picker
      $('#reservation').daterangepicker();
      //Date range picker with time picker
      $('#reservationtime').daterangepicker({
          timePicker: true,
          timePickerIncrement: 30,
          locale: {
              format: 'MM/DD/YYYY hh:mm A'
          }
      });

      //Date range as a button
      $('#daterange-btn').daterangepicker(
          {
              ranges: {
                  'Today': [moment(), moment()],
                  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                  'This Month': [moment().startOf('month'), moment().endOf('month')],
                  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
          },
          function (start, end) {
              $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
          }
      )
// Selecct 2
    $scope.itemArray = [
      { id: 1, name: 'Select Bacth' }
    ];

    $scope.selectedItem = $scope.itemArray[0];
// Selecct 2
// ng-handsontable
    $scope.batchess = [];
    $scope.settings = {
      minSpareRows: 20,
      rowHeaders: true,
      colHeaders: true,
      rowHeights: 30,
      height: 550,
      colWidths: [45, 100, 260, 260, 180, 180, 180, 180],
      manualColumnResize: true,
      manualRowResize: true,
      contextMenu: ['row_above', 'row_below', 'remove_row']
    };

    $scope.dt = function () {
      console.log("Accounts: ", $scope.batchess)
    };
// ng-handsontable

    function init() {
      datatableService.httpGetBatches((res, err) => {
        if (err) {
          console.log("Error Retrieving Batches: ", err);
        } else {
          $scope.batches = res;
          console.log("Scope batches in controller => ", $scope.batches);
        }
        $scope.loading = false;
      });
    }
    init();

      $scope.addBatches = async function () {
        await datatableService.httpPostInsertBatch(
          [Tools.CapitalizeFirstLetter($scope.newBatchName)],
          $scope.newBatchDescription,
          $scope.newSafeSpend,
          $scope.newProxy,
          $scope.newWarmTime,
          $scope.newSafeAds,
          $scope.newPreWarmingActivities,
          $scope.newPreLaunchActivities,
          (res, err) => {
            if (err || res.status > 299) {
              swal(
                "Error",
                "Unsuccessful insert, try again or contact support.",
                "error"
              );
            } else {
              swal("Success!", "Successfully inserted new Batch.", "success");
              $scope.newBatchName = null;
              $scope.newBatchDescription = null;
              $scope.newSafeSpend = null;
              $scope.newProxy = null;
              $scope.newWarmTime = null;
              $scope.newSafeAds = null;
              $scope.newPreWarmingActivities = null;
              $scope.newPreLaunchActivities = null;
              init();
            }
          }
        );
      };

      $scope.batchChanged = function (batch) {
        batch.changed = true;
      };

      $scope.updateBatches = async function () {
        let changedBatches = $scope.batches.filter(
          a => typeof a.changed !== "undefined" && a.changed === true
        );
        console.log("changedBatches => ", changedBatches);
        await datatableService.httpPutUpdateBatches(changedBatches, (res, err) => {
          if (err || res.status > 299) {
            swal(
              "Error",
              "Unsuccessful update of batches, try again or contact support.",
              "error"
            );
          } else {
            swal("Success!", "Successfully updated Batches.", "success");
            init();
          }
        });
      };
    }
  ]
);
