﻿"use strict";

app.controller("admindashboardCtrl", [
  "$scope",
  "$http",
  "$rootScope",
  "$location",
  "$cookieStore",
  "$timeout",
  "campaignService",
  function(
    $scope,
    $http,
    $rootScope,
    $location,
    $cookieStore,
    $timeout,
    campaignService
  ) {
    $scope.loading = true;
    $scope.aloading = true;
    $scope.role = Number($rootScope.user.role);
    $scope.user = $rootScope.user.username;

    $scope.myDataSource = {
      chart: {
        caption: "Market Share of Web Servers",
        plottooltext: "<b>$percentValue</b> of web servers run on $label servers",
        showlegend: "1",
        showpercentvalues: "1",
        legendposition: "bottom",
        usedataplotcolorforlabels: "1",
        theme: "fusion"
      },
      data: [
        {
          label: "Apache",
          value: "32647479"
        },
        {
          label: "Microsoft",
          value: "22100932"
        },
        {
          label: "Zeus",
          value: "1437644"
        },
        {
          label: "Other",
          value: "18674221"
        }
      ]
    };
    
  }]
);
