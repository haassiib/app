'use strict';

angular.module('menciusapp').controller('accountingSearchCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  '$cookieStore',
  '$filter',
  'searchService',
  'statusService',
  function($rootScope, $scope, $http, $cookieStore, $filter, searchService, statusService) {
    $scope.loading = true;
    $scope.accounts = [];
    $scope.statuses = [];
    $scope.status = 'In Progress';
    $scope.columns = [];
    $scope.defaultColumns = [
      'account_name',
      'total_spend',
      'safe_spend',
      'warm_time',
      'run_time',
      'proxy_type',
      'safe_ad_count'
    ];

    const columnFilter = $cookieStore.get('search_columns');
    if (columnFilter) {
      $scope.selectedColumns = columnFilter;
    } else {
      $scope.selectedColumns = $scope.defaultColumns;
    }

    $scope.totalSpendOrder = false;
    $scope.format = 'mm-dd-yy';
    $scope.dtFrom = moment().subtract(7, 'days');
    $scope.dtFrom = moment($scope.dtFrom).format('MM-DD-YYYY');
    $scope.dtTo = moment().format('MM-DD-YYYY');

    //TODO: Fix this, these values should be coming from the directive
    $scope.dateChange = function(field, date) {
      $scope[field] = date;
      //console.log($scope[field]);
    };

    statusService.httpGetAllStatus((res, err) => {
      if (err) {
        console.log(err);
        //throw error
      } else {
        res.splice(0, 0, 'Select All');
        //console.log("status res:",res);
        $scope.statuses = res;

        $scope.loading = false;
      }
    });

    //TODO: switch this to service
    $scope.search = function() {
      // console.log("dateto: ", $scope.dtTo);
      // console.log("datefrom: ", $scope.dtFrom);

      var getSearchConfig = {
        method: 'GET',
        url: $rootScope.urlRoot + '/accounts/search',
        headers: {
          'Content-Type': 'application/json',
          'x-username': $rootScope.user.username,
          'x-role': $rootScope.role,
          'x-status': $scope.status,
          'x-from': moment($scope.dtFrom, 'MM-DD-YYYY').format(),
          'x-to': moment($scope.dtTo, 'MM-DD-YYYY').format()
        }
      };
      //console.log(getSearchConfig);

      $http(getSearchConfig).then(
        function(data) {
          //console.log("Retrieved search results from db: ",data.data);
          $scope.accounts = data.data;
          if ($scope.accounts.length > 0) {
            $scope.columns = Object.keys($scope.accounts[0]).sort();
            //console.log("columns: ", $scope.columns);
          }
        },
        function(data, status) {
          if (data.status > 200) {
            console.log('could not retrieve search results, something went wrong.');
            // console.log("data: ", data);
            //return callback(data, status);
          }
        }
      );
    };
    $scope.search();

    $scope.sort = function(field, order) {
      //console.log('sort() in ' + order + ' order called on the ' + field + ' field.');
      let propsToSort = [];
      let ts = {
        name: field,
        reverse: order === 'ascending' ? true : false
      };
      propsToSort.push(ts);
      $scope.accounts.sort(Tools.generateSortFunction(propsToSort));
    };

    // $(document).ready(function() {

    // });
  }
]);
