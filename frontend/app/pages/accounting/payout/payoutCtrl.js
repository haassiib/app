﻿"use strict";

angular.module("menciusapp").controller("payoutCtrl", [
  "$rootScope",
  "$scope",
  "$http",
  "$cookieStore",
  "accountService",
  "pnlService",
  function(
    $rootScope,
    $scope,
    $http,
    $cookieStore,
    accountService,
    pnlService
  ) {
    $scope.loading = true;
    $scope.role = $rootScope.user.role;
    $scope.currentUser = Tools.CapitalizeFirstLetter($rootScope.user.username);
    $scope.ownerFilter = $scope.currentUser;
    $scope.disableUpdateCopy = false;
    $scope.dev = false;
    $scope.ageRange = Ages;
    $scope.users = [
      "Ashif",
      "Joe",
      "Justin",
      "Luke",
      "Mike",
      "Marcus",
      "Ryan",
      "Scott",
      "Stan",
      "Trey",
      "Valerie",
      "Zach",
      "Zoe"
    ];
    console.log($scope.currentUser);
    console.log($scope.ownerFilter);
    $scope.totals = {}; //Gets set in init after accounts are pulled

    function initStoredValues() {
      // Setup User Filter
      if ($cookieStore.get("payout_user_filter")) {
        $scope.ownerFilter = Tools.CapitalizeFirstLetter(
          $cookieStore.get("payout_user_filter")
        );
      } else {
        $scope.ownerFilter = Tools.CapitalizeFirstLetter($scope.currentUser);
        $cookieStore.put("payout_user_filter", $scope.ownerFilter);
      }
      // Setup Start Date
      if ($cookieStore.get("payout_start_date")) {
        $scope.startDateFilter = Tools.CapitalizeFirstLetter(
          $cookieStore.get("payout_start_date")
        );
      } else {
        $scope.startDateFilter = moment()
          .startOf("month")
          .format("MM-DD-YYYY");
        $cookieStore.put("payout_start_date", $scope.startDateFilter);
      }
      // Setup End Date
      if ($cookieStore.get("payout_end_date")) {
        $scope.endDateFilter = Tools.CapitalizeFirstLetter(
          $cookieStore.get("payout_end_date")
        );
      } else {
        $scope.endDateFilter = moment()
          .endOf("month")
          .format("MM-DD-YYYY");
        $cookieStore.put("payout_end_date", $scope.endDateFilter);
      }
      // Last Field Sort
      $scope.lastSort =
        $cookieStore.get("payout_last_sort") != null
          ? $cookieStore.get("payout_last_sort")
          : null;
      $scope.lastOrder =
        $cookieStore.get("payout_last_sort") != null
          ? $cookieStore.get("payout_last_sort")
          : null;
    }
    initStoredValues();

    function init() {
      pnlService.httpGetPayoutsByRangeAndUser(
        $scope.startDateFilter,
        $scope.endDateFilter,
        $scope.ownerFilter,
        (res, err) => {
          if (err) {
            console.log("error saving");
            swal(
              "Error",
              "There was an error getting payouts:\n" + err,
              "error"
            );
            $scope.loading = false;
          } else {
            console.log("RESULT: \n", res);
            $scope.accounts = res;
            calculateTotals(res);
            $scope.loading = false;
          }
        }
      );
    }
    init();
    //Created to reinitialize page for date/owner change
    $scope.runPayouts = init;

    $scope.changeOwner = function() {
      $cookieStore.put("payout_user_filter", $scope.ownerFilter);
    };

    $scope.changeStartDate = function() {
      $cookieStore.put("payout_start_date", $scope.startDateFilter);
    };

    $scope.changeEndDate = function() {
      $cookieStore.put("payout_end_date", $scope.endDateFilter);
    };

    $scope.sort = function(field, order) {
      console.log("called sort");
      $scope.lastSort = field;
      $scope.lastOrder = order;
      $cookieStore.put("payout_last_sort", field);
      $cookieStore.put("payout_last_order", order);
      //console.log("Sort. Field: ", field, " | order: ", order);
      let propsToSort = [];
      let ts = {
        name: field,
        reverse: order == "ascending" ? true : false
      };
      propsToSort.push(ts);
      $scope.accounts.sort(Tools.generateSortFunction(propsToSort));
    };

    function exportToCsv(filename, rows) {
      var processRow = function(row) {
        var finalVal = "";
        for (var j = 0; j < row.length; j++) {
          var innerValue = row[j] === null ? "" : row[j].toString();
          if (row[j] instanceof Date) {
            innerValue = row[j].toLocaleString();
          }
          var result = innerValue.replace(/"/g, '""');
          if (result.search(/("|,|\n)/g) >= 0) result = '"' + result + '"';
          if (j > 0) finalVal += ",";
          finalVal += result;
        }
        return finalVal + "\n";
      };

      var csvFile = "";
      for (var i = 0; i < rows.length; i++) {
        csvFile += processRow(rows[i]);
      }

      var blob = new Blob([csvFile], { type: "text/csv;charset=utf-8;" });
      if (navigator.msSaveBlob) {
        // IE 10+
        navigator.msSaveBlob(blob, filename);
      } else {
        var link = document.createElement("a");
        if (link.download !== undefined) {
          // feature detection
          // Browsers that support HTML5 download attribute
          var url = URL.createObjectURL(blob);
          link.setAttribute("href", url);
          link.setAttribute("download", filename);
          link.style.visibility = "hidden";
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
      }
    }

    $scope.auditDate = function(account_key) {
      console.log(account_key);

      accountService.httpUpdateAuditDate(account_key, (res, err) => {
        if (err) {
          console.log("error");
        } else {
          console.log("success");
        }
      });
    };

    $scope.exportCSV = function(accounts) {
      var rows = [
        ["Account", "Revenue", "Expenses", "Profit", "Dead Date", "Status"]
      ];
      for (var a in accounts) {
        let account = accounts[a];
        rows.push([
          account.fname + " " + account.lname,
          account.audit_revenue,
          account.audit_expenses,
          account.audit_revenue - account.audit_expenses,
          account.dead,
          account.status
        ]);
      }
      exportToCsv("report.csv", rows);
    };

    function convertToCSV(objArray) {
      let array = typeof objArray != "object" ? JSON.parse(objArray) : objArray;
      let str = "";
      for (let i = 0; i < array.length; i++) {
        let line = "";
        for (var index in array[i]) {
          if (line != "") line += ",";

          line += array[i][index];
        }

        str += line + "\r\n";
      }
      return str;
    }

    //HELPERS
    function calculateTotals(accounts) {
      console.log("scope tots: ", $scope.totals);
      // calucate audit rev for all accounts
      $scope.totals.audit_revenue = accounts.reduce((t, a) => {
        console.log("typeof :", typeof a.audit_revenue);
        return t + (typeof a.audit_revenue === "number" ? a.audit_revenue : 0);
      }, 0);
      // calculate audit exp for all accounts
      $scope.totals.audit_expenses = accounts.reduce((t, a) => {
        return (
          t + (typeof a.audit_expenses === "number" ? a.audit_expenses : 0)
        );
      }, 0);
      // calculate profit from calculated rev and expense
      $scope.totals.audit_profit =
        $scope.totals.audit_revenue - $scope.totals.audit_expenses;
      console.log("scope tots: ", $scope.totals);
    }
  }
]);
