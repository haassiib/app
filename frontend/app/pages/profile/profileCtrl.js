"use strict";

angular.module("menciusapp").controller("profileCtrl", [
  "$rootScope",
  "$scope",
  "$http",
  "alertService",
  "$timeout",
  "$location",
  "$cookieStore",
  "authService",
  function(
    $rootScope,
    $scope,
    $http,
    alertService,
    $timeout,
    $location,
    $cookieStore,
    authService
  ) {
    $scope.saveProfile = function() {
      console.log("TODO: setup user profile and update");
    };

    $scope.resetPassword = function() {
      let password = $scope.newpass;
      let passwordver = $scope.newpassver;

      // if ($rootScope.user.password !== $scope.oldpass) {

      //     //TODO: validation/error handling
      //     console.log('password reset failure!');
      //     return;
      // }
      if ($scope.newpass !== $scope.newpassver) {
        //TODO: validation/error handling
        console.log("password reset failure!");
        return;
      } else {
        swal(
          {
            title: "Are You Sure You Want To Reset Your Password?",
            text: "You password will be changed and you will be logged out!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Reset",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: true
          },
          function(isConfirm) {
            if (isConfirm) {
              //Change password and then log out
              authService.httpResetPassword(
                "stan",
                password,
                (result, error) => {
                  if (error) {
                    console.log("reset password error!");
                    throw error;
                  } else {
                    console.log("reset password call successful.");
                    authService.logout();
                    swal("Success", "Your password has been reset.", "success");
                  }
                }
              );
            }
          }
        );
      }
    };
  }
]);
