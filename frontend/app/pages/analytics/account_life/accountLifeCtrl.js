﻿'use strict';

app.controller('accountLifeCtrl', [
  '$scope',
  '$http',
  '$rootScope',
  '$location',
  '$cookieStore',
  '$timeout',
  'analyticsService',
  'accountService',
  'verticalService',
  'campaignService',
  'adsetService',
  'countryService',
  'batchService',
  'accountHistoryService',
  'moment',
  'statusService',
  'codeService',
  function(
    $scope,
    $http,
    $rootScope,
    $location,
    $cookieStore,
    $timeout,
    analyticsService,
    accountService,
    verticalService,
    campaignService,
    adsetService,
    countryService,
    batchService,
    accountHistoryService,
    moment,
    statusService,
    codeService
  ) {
    $scope.loading = true;
    $scope.aloading = true;
    $scope.role = Number($rootScope.user.role);
    $scope.user = $rootScope.user.username;

    $scope.allData = {};

    $scope.verticals = [];
    $scope.verticalNames = [];
    $scope.campaigns = [];
    $scope.adsets = [];
    $scope.geos = [{}];
    $scope.batches = [{}];
    $scope.accounts = [];
    $scope.filteredAccounts = [];
    $scope.columns = [];
    $scope.history = [];
    // $scope.successful = 0;

    $scope.batchFilter = 'All Batches';
    $scope.userFilter = 'All Users';
    $scope.angleFilter = 'All Angles';
    $scope.cardFilter = 'All Cards';
    $scope.verticalFilter = 'All Verticals';
    $scope.geoFilter = 'All Geos';
    $scope.sortType = 'Angles';
    $scope.sortReverse;
    $scope.launch_statuses = ['Successful', 'Disapproved', 'Flagged'];

    $scope.defaultColumns = [
      'account_name',
      'angle',
      'vertical',
      'batch',
      'geo',
      'card_type',
      'status',
      'days_alive',
      'total_spend',
      'total_revenue',
      'ROI'
    ];

    $scope.users = [
      'All Users',
      'Ashif',
      'Joe',
      'Justin',
      'Luke',
      'Marcus',
      'Mike',
      'Ryan',
      'Scott',
      'Stan',
      'Trey',
      'Valerie',
      'Zach',
      'Zoe'
    ];

    $scope.dtFrom = moment()
      .subtract(1, 'months')
      .format('MM-DD-YYYY');
    $scope.dtTo = moment().format('MM-DD-YYYY');

    const onlyUnique = (value, index, self) => {
      return self.indexOf(value) === index;
    };

    const getAnalyticsDataByDateRangeForAccountLifePromise = () => {
      let start = moment($scope.dtFrom, 'MM-DD-YYYY').format('YYYY-MM-DD');
      let end = moment($scope.dtTo, 'MM-DD-YYYY').format('YYYY-MM-DD');
      // console.log(
      //   'start: ',
      //   start,
      //   ' - end: ',
      //   end,
      //   ' | $scope.dtFrom: ',
      //   $scope.dtFrom,
      //   ' - $scope.dtTo: ',
      //   $scope.dtFrom
      // );

      return new Promise((resolve, reject) => {
        analyticsService.httpGetAnalyticsDataByDateRangeForAccountLife(start, end, (res, err) => {
          if (err) {
            reject(err);
            console.error('get getAnalyticsDataByDateRangeForAccountLife error ', err);
          } else {
            //concat the name into name var on scope
            let allData = res.map(acc => {
              acc.account_name = acc.first_name + ' ' + acc.last_name;
              return acc;
            });
            // console.log('allData in the analyticsByDaterange => ', allData);
            resolve(allData);
          }
        });
      });
    };

    const getFilteredAccounts = function(
      accounts,
      cardFilter,
      userFilter,
      angleFilter,
      verticalFilter,
      batchFilter,
      geoFilter,
      start,
      end
    ) {
      let filteredAccounts = accounts
        .filter(a => {
          if (userFilter === 'All Users') {
            return a;
          } else {
            return a.owner === userFilter;
          }
        })
        .filter(b => {
          if (angleFilter === 'All Angles') {
            return b;
          } else if (b.angle !== undefined) {
            return b.angle === angleFilter;
          }
        })
        .filter(c => {
          if (verticalFilter === 'All Verticals') {
            return c;
          } else {
            return c.vertical === verticalFilter;
          }
        })
        .filter(d => {
          if (batchFilter === 'All Batches') {
            $scope.batchesFiltered = false;
            return d;
          } else {
            $scope.batchesFiltered = true;
            return d.batch === batchFilter;
          }
        })
        .filter(g => {
          if (geoFilter === 'All Geos') {
            return g;
          } else {
            return g.geo === geoFilter;
          }
        })
        .filter(h => {
          if (cardFilter === 'All Cards') {
            return h;
          } else {
            return h.card_type === cardFilter;
          }
        })
        .map(k => {
          k.ROI = ((k.total_revenue - k.total_spend) / k.total_spend) * 100;
          k.ROI = `${Math.trunc(k.ROI)}%`;
          return k;
        });

      $scope.avgTotalSpend = (
        filteredAccounts.reduce((total, acc) => {
          total += acc.total_spend;
          return total;
        }, 0) / filteredAccounts.length
      ).toFixed(2);

      $scope.avgTotalRevenue = (
        filteredAccounts.reduce((total, acc) => {
          total += acc.total_revenue;
          return total;
        }, 0) / filteredAccounts.length
      ).toFixed(2);

      let avgTotalROI =
        (($scope.avgTotalRevenue - $scope.avgTotalSpend) / $scope.avgTotalSpend) * 100;
      $scope.avgTotalROI = Math.trunc(avgTotalROI);

      //a.roi = ((totals.revenue - totals.expenses) / totals.expenses) * 100;

      $scope.filteredAccounts = filteredAccounts;
      return filteredAccounts;
    };

    const getAllData = async () => {
      try {
        const allData = {
          accounts: await getAnalyticsDataByDateRangeForAccountLifePromise()
        };

        //set angles in scope
        allData.angles = allData.accounts
          .map(acc => {
            if (acc.angle !== null && acc.angle !== undefined && acc.angle !== '') {
              return acc.angle;
            }
          })
          .filter((v, i, a) => a.indexOf(v) === i);
        //add temp all angles var
        allData.angles.push('All Angles');

        //set batches with account batches in scope
        allData.batches = allData.accounts
          .map(acc => {
            if (acc.batch !== null && acc.batch !== undefined && acc.batch !== '') {
              return acc.batch;
            }
          })
          .filter((v, i, a) => a.indexOf(v) === i);
        allData.batches.push('All Batches');

        //set All verticals
        allData.verticals = allData.accounts
          .map(acc => {
            // console.log("acc.vertical", acc.vertical);
            return acc.vertical;
          })
          .filter((v, i, a) => a.indexOf(v) === i);
        allData.verticals.push('All Verticals');

        //set all geos
        allData.geos = allData.accounts
          .map(acc => {
            if (acc.geo !== null) {
              return acc.geo;
            }
          })
          .filter(onlyUnique);
        allData.geos.push('All Geos');

        //set all statuses
        allData.statuses = allData.accounts
          .map(acc => {
            if (acc.status !== null) {
              return acc.status;
            }
          })
          .filter(onlyUnique);
        allData.statuses.push('All Statuses');

        //set all cards
        allData.cards = allData.accounts
          .map(acc => {
            if (acc.card_type !== undefined && acc.card_type !== '') {
              return acc.card_type;
            }
          })
          .filter((v, i, a) => a.indexOf(v) === i);
        allData.cards.push('All Cards');

        $scope.accounts = allData.accounts;
        $scope.allData = allData;
        $scope.cards = allData.cards.sort();
        $scope.statuses = allData.statuses.sort();
        $scope.verticals = allData.verticals.sort();
        $scope.batches = allData.batches.sort();
        $scope.geos = allData.geos.sort();
        $scope.angles = allData.angles.sort();

        let start = moment($scope.dtFrom, 'MM-DD-YYYY').format('YYYY-MM-DD');
        let end = moment($scope.dtTo, 'MM-DD-YYYY').format('YYYY-MM-DD');

        $scope.filteredAccounts = getFilteredAccounts(
          $scope.accounts,
          $scope.cardFilter,
          $scope.userFilter,
          $scope.angleFilter,
          $scope.verticalFilter,
          $scope.batchFilter,
          $scope.geoFilter,
          start,
          end
        );
        // console.log(
        //   "scope allData in the getAllData method => ",
        //   $scope.allData
        // );
        $scope.loading = false;
        return allData;
      } catch (error) {
        if (error) {
          console.error('error in getAllData in the accountLifeCtrl => ', error);
        }
      }
    };

    const filterChanged = ($scope.filterChanged = async function() {
      // console.log("$scope.dtFrom => ", $scope.dtFrom);
      // console.log("$scope.dtTo => ", $scope.dtTo);
      let start = moment($scope.dtFrom, 'MM-DD-YYYY').format('YYYY-MM-DD');
      let end = moment($scope.dtTo, 'MM-DD-YYYY').format('YYYY-MM-DD');
      $scope.loading = true;
      let newAccounts = await getAnalyticsDataByDateRangeForAccountLifePromise();
      await newAccounts;

      $scope.filteredAccounts = await getFilteredAccounts(
        newAccounts,
        $scope.cardFilter,
        $scope.userFilter,
        $scope.angleFilter,
        $scope.verticalFilter,
        $scope.batchFilter,
        $scope.geoFilter,
        start,
        end
      );

      await $scope.filteredAccounts;
      console.log('$scope.filteredAccounts after changed filter => ', $scope.filteredAccounts);
      $scope.loading = false;
    });

    const init = async () => {
      await $timeout(getAllData);
    };

    init();

    $(document).ready(function() {
      $('.btn-default').click(function() {
        $('.collapse').collapse('toggle');
      });
    });
  }
]);
