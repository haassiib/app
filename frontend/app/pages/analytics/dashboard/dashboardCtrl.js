﻿"use strict";

app.controller("dashboardCtrl", [
  "$scope",
  "$http",
  "$rootScope",
  "$location",
  "$cookieStore",
  "$timeout",
  "campaignService",
  function(
    $scope,
    $http,
    $rootScope,
    $location,
    $cookieStore,
    $timeout,
    campaignService
  ) {
    $scope.loading = true;
    $scope.aloading = true;
    $scope.role = Number($rootScope.user.role);
    $scope.user = $rootScope.user.username;
    $scope.detailFilters = [
      "Angle",
      "Country",
      "Gender",
      "Vertical",
      "Select All"
    ];
    $scope.detailFilter = "Select All";
    $scope.timeFrames = [
      "Today",
      "Yesterday",
      "Last 7 Days",
      "Last 30 Days",
      "Select All"
    ];
    $scope.timeFrame = "Select All";

    campaignService.httpGetTotalSpendByCampaign((res, err) => {
      if (err) {
        console.log(err);
        swal("Error", "Error Retrieving Campaigns: " + err, "warning");
      } else {
        //console.log("Campaigns success: ", res);
        $scope.spendCampaigns = res;
        $scope.loading = false;
      }
    });

    let getRoiCamps = function(timeFrame) {
      campaignService.httpRoiByCampaign(timeFrame, (res, err) => {
        if (err) {
          console.log(err);
          swal("Error", "Error Retrieving Campaigns: " + err, "warning");
        } else {
          console.log("Campaigns success: ", res);
          $scope.allRoiCampaigns = res;
          $scope.roiCampaigns = getFilteredRoiCampaigns($scope.detailFilter);
          $scope.aloading = false;
        }
      });
    };
    getRoiCamps($scope.timeFrame);

    $scope.asort = function(field, order) {
      //console.log("Sort. Field: ", field, " | order: ", order);
      $scope.roiLastSort = field;
      $scope.roiLastOrder = order;
      $cookieStore.put("roi_campaigns_last_sort", field);
      $cookieStore.put("roi_campaigns_last_order", order);

      let propsToSort = [];
      let ts = {
        name: field,
        reverse: order === "ascending" ? true : false
      };
      propsToSort.push(ts);
      $scope.roiCampaigns.sort(Tools.generateSortFunction(propsToSort));
    };

    function getFilteredRoiCampaigns(field) {
      let filter = field.toLowerCase();
      let camps = JSON.parse(JSON.stringify($scope.allRoiCampaigns));
      if (filter === "select all") {
        return camps;
      }
      let newCamp = {};
      camps.forEach(camp => {
        if (newCamp && newCamp[camp[filter]]) {
          newCamp[camp[filter]].push(camp);
        } else {
          newCamp[camp[filter]] = [];
          newCamp[camp[filter]].push(camp);
        }
      });
      //console.log(newCamp);
      let filterKeys = Object.keys(newCamp);
      let filteredCamps = [];
      //console.log(`keys: ${filterKeys}`);
      filterKeys.forEach(k => {
        let single = newCamp[k].reduce((totals, i) => {
          totals.no_campaigns = (totals.no_campaigns || 0) + i.no_campaigns;
          totals.revenue = (totals.revenue || 0) + i.revenue;
          totals.expenses = (totals.expenses || 0) + i.expenses;
          return totals;
        }, {});
        single[filter] = k;
        single.profit = single.revenue - single.expenses;
        single.roi = single.profit / single.expenses;
        filteredCamps.push(single);
      });
      //console.log(filteredCamps);
      return filteredCamps;
    }

    $scope.detailFilterChange = function() {
      //console.log("filter changed: ", $scope.detailFilter);
      $scope.roiCampaigns = getFilteredRoiCampaigns($scope.detailFilter);
    };

    $scope.detailFilterCheck = function(field) {
      let filter = $scope.detailFilter.toLowerCase();
      if (filter === "select all") return true;
      else if (filter === field) return true;
      else return false;
    };

    $scope.sort = function(field, order) {
      //console.log("Sort. Field: ", field, " | order: ", order);
      $scope.lastSort = field;
      $scope.lastOrder = order;
      $cookieStore.put("dashboard_last_sort", field);
      $cookieStore.put("dashboard_last_order", order);

      let propsToSort = [];
      let ts = {
        name: field,
        reverse: order === "ascending" ? true : false
      };
      propsToSort.push(ts);
      $scope.spendCampaigns.sort(Tools.generateSortFunction(propsToSort));
    };

    $scope.timeFrameChange = function() {
      switch ($scope.timeFrame) {
        case "Today":
          getRoiCamps(0);
          break;
        case "Yesterday":
          getRoiCamps(1);
          break;
        case "Last 7 Days":
          getRoiCamps(7);
          break;
        case "Last 30 Days":
          getRoiCamps(30);
          break;
        case "Select All":
          getRoiCamps("Select All");
          break;
        default:
          getRoiCamps("Select All");
      }
    };
  }
]);
