﻿app.controller('launchCtrl', [
    '$scope',
    '$http',
    '$rootScope',
    '$location',
    '$cookieStore',
    '$timeout',
    'accountService',
    'verticalService',
    'campaignService',
    'adsetService',
    'countryService',
    'batchService',
    'accountHistoryService',
    'moment',
    'statusService',
    'analyticsService',
    function(
        $scope,
        $http,
        $rootScope,
        $location,
        $cookieStore,
        $timeout,
        accountService,
        verticalService,
        campaignService,
        adsetService,
        countryService,
        batchService,
        accountHistoryService,
        moment,
        statusService,
        analyticsService
    ) {
        $scope.loading = true;
        $scope.aloading = true;
        $scope.role = Number($rootScope.user.role);
        $scope.user = $rootScope.user.username;

        $scope.verticals = [];
        $scope.verticalNames = [];
        $scope.geos = [{}];
        $scope.batches = [{}];
        $scope.accounts = [];
        $scope.filteredAccounts = [];
        $scope.columns = [];
        $scope.history = [];
        // $scope.successful = 0;
        $scope.dataLoaded = false;

        $scope.batchFilter = 'All Batches';
        $scope.userFilter = 'All Users';
        $scope.angleFilter = 'All Angles';
        $scope.cardFilter = 'All Cards';
        $scope.verticalFilter = 'All Verticals';
        $scope.geoFilter = 'All Geos';
        $scope.sortType = 'Angles';
        $scope.sortReverse;
        $scope.launch_statuses = ['Successful', 'Disapproved', 'Flagged'];

        $scope.defaultColumns = [
            'launch_status',
            'account_name',
            'angle',
            'vertical',
            'batch',
            'geo',
            'card_type',
            'status',
            'total_spend'
        ];

        const columnFilter = $cookieStore.get('search_columns');

        if (columnFilter) {
            $scope.selectedColumns = columnFilter;
            // console.log(columnFilter);
        } else {
            $scope.selectedColumns = $scope.defaultColumns;
        }

        $scope.users = [
            'All Users',
            'Ashif',
            'Joe',
            'Justin',
            'Luke',
            'Marcus',
            'Mike',
            'Ryan',
            'Scott',
            'Stan',
            'Trey',
            'Valerie',
            'Zach',
            'Zoe'
        ];

        /* $scope.dtFrom = moment()
            .subtract(1, 'months')
            .format('MM-DD-YYYY');
        $scope.dtTo = moment().format('MM-DD-YYYY'); */

        $scope.dtFrom = '02-07-2018';
        $scope.dtTo = '03-07-2018';

        $scope.launchData = {
            chart: {
                caption: 'Launch Rates: ',
                subCaption: `Select filters below`,
                numberPrefix: 'Count: ',
                showPercentInTooltip: '0',
                decimals: '1',
                useDataPlotColorForLabels: '1',
                theme: 'fusion'
            },
            data: [{
                    label: 'Flagged',
                    value: '105',
                    color: '#E9303C'
                },
                {
                    label: 'Disapproved',
                    value: '49',
                    color: '#FEBB23'
                },
                {
                    label: 'Successful',
                    value: '346',
                    color: '#1AA455'
                },
                {
                    label: 'Problem',
                    value: '30',
                    color: '#1C80C3'
                }
            ]
        };
        // #91ED91

        const setLaunchGraphData = async filteredAccounts => {
            const ld = {
                one: 0,
                two: 0,
                three: 0,
                four: 0
            };
            await filteredAccounts.forEach(acc => {
                // console.log(acc);
                if (acc.launch_key === 1) {
                    ld.one++;
                }
                if (acc.launch_key === 2) {
                    ld.two++;
                }
                if (acc.launch_key === 3) {
                    ld.three++;
                }
                if (acc.launch_key === 4) {
                    ld.four++;
                }
            });
            // console.log('ld: ', ld);
            $scope.launchData.data[2].value = ld.one;
            $scope.launchData.data[1].value = ld.two;
            $scope.launchData.data[3].value = ld.three;
            $scope.launchData.data[0].value = ld.four;
        };

        const onlyUnique = (value, index, self) => self.indexOf(value) === index;

        const getAnalyticsDataByDateRangeForLaunchRatesPromise = () => {
            const start = moment($scope.dtFrom, 'MM-DD-YYYY').format('YYYY-MM-DD');
            const end = moment($scope.dtTo, 'MM-DD-YYYY').format('YYYY-MM-DD');

            return new Promise((resolve, reject) => {
                analyticsService.httpGetAnalyticsDataByDateRangeForLaunchRates(start, end, (res, err) => {
                    if (err) {
                        reject(err);
                        console.error('get getAnalyticsDataByDateRange error ', err);
                    } else {
                        // concat the name into name var on scope
                        const allData = res.map(acc => {
                            acc.account_name = `${acc.first_name} ${acc.last_name}`;
                            return acc;
                        });
                        // console.log("allData in the analyticsByDaterange => ", allData);
                        resolve(allData);
                    }
                });
            });
        };

        const getFilteredAccounts = function(
            accounts,
            cardFilter,
            userFilter,
            angleFilter,
            verticalFilter,
            batchFilter,
            geoFilter,
            start,
            end
        ) {
            // console.log('accounts passed into getFilteredAccounts => ', accounts);
            const filteredAccounts = accounts
                .filter(a => {
                    if (userFilter === 'All Users') {
                        return a;
                    }
                    return a.owner === userFilter;
                })
                .filter(b => {
                    if (angleFilter === 'All Angles') {
                        return b;
                    }
                    if (b.angle !== undefined) {
                        return b.angle === angleFilter;
                    }
                })
                .filter(c => {
                    if (verticalFilter === 'All Verticals') {
                        return c;
                    }
                    return c.vertical === verticalFilter;
                })
                .filter(d => {
                    if (batchFilter === 'All Batches') {
                        $scope.batchesFiltered = false;
                        return d;
                    }
                    $scope.batchesFiltered = true;
                    return d.batch === batchFilter;
                })
                .filter(g => {
                    if (geoFilter === 'All Geos') {
                        return g;
                    }
                    return g.geo === geoFilter;
                })
                .filter(h => {
                    if (cardFilter === 'All Cards') {
                        return h;
                    }
                    return h.card_type === cardFilter;
                });

            $scope.avgTotalSpend = (
                filteredAccounts.reduce((total, acc) => {
                    total += acc.total_spend;
                    return total;
                }, 0) / filteredAccounts.length
            ).toFixed(2);

            $scope.filteredAccounts = filteredAccounts;
            // console.log('$scope.filteredAccounts in getFilteredAccounts => ', $scope.filteredAccounts);
            return filteredAccounts;
        };

        const getAllData = async () => {
            try {
                const allData = {
                    accounts: await getAnalyticsDataByDateRangeForLaunchRatesPromise()
                };

                // set angles in scope
                allData.angles = allData.accounts
                    .map(acc => {
                        if (acc.angle !== null && acc.angle !== undefined && acc.angle !== '') {
                            return acc.angle;
                        }
                    })
                    .filter((v, i, a) => a.indexOf(v) === i);
                // add temp all angles var
                allData.angles.push('All Angles');

                // set batches with account batches in scope
                allData.batches = allData.accounts
                    .map(acc => {
                        if (acc.batch !== null && acc.batch !== undefined && acc.batch !== '') {
                            return acc.batch;
                        }
                    })
                    .filter((v, i, a) => a.indexOf(v) === i);
                allData.batches.push('All Batches');

                // set All verticals
                allData.verticals = allData.accounts
                    .map(
                        acc =>
                        // console.log("acc.vertical", acc.vertical);
                        acc.vertical
                    )
                    .filter((v, i, a) => a.indexOf(v) === i);
                allData.verticals.push('All Verticals');

                // set all geos
                allData.geos = allData.accounts
                    .map(acc => {
                        if (acc.geo !== null) {
                            return acc.geo;
                        }
                    })
                    .filter(onlyUnique);
                allData.geos.push('All Geos');

                // set all statuses
                allData.statuses = allData.accounts
                    .map(acc => {
                        if (acc.status !== null) {
                            return acc.status;
                        }
                    })
                    .filter(onlyUnique);
                allData.statuses.push('All Statuses');

                // set all cards
                allData.cards = allData.accounts
                    .map(acc => {
                        if (acc.card_type !== undefined && acc.card_type !== '') {
                            return acc.card_type;
                        }
                    })
                    .filter((v, i, a) => a.indexOf(v) === i);
                allData.cards.push('All Cards');

                $scope.accounts = allData.accounts;
                $scope.allData = allData;
                $scope.cards = allData.cards.sort();
                $scope.statuses = allData.statuses.sort();
                $scope.verticals = allData.verticals.sort();
                $scope.batches = allData.batches.sort();
                $scope.geos = allData.geos.sort();
                $scope.angles = allData.angles.sort();

                const start = moment($scope.dtFrom, 'MM-DD-YYYY').format('YYYY-MM-DD');
                const end = moment($scope.dtTo, 'MM-DD-YYYY').format('YYYY-MM-DD');

                $scope.filteredAccounts = getFilteredAccounts(
                    $scope.accounts,
                    $scope.cardFilter,
                    $scope.userFilter,
                    $scope.angleFilter,
                    $scope.verticalFilter,
                    $scope.batchFilter,
                    $scope.geoFilter,
                    start,
                    end
                );
                await setLaunchGraphData($scope.filteredAccounts);
                $scope.dataLoaded = true;
                // console.log('scope allData in the getAllData method => ', $scope.allData);
                $scope.loading = false;
                return allData;
            } catch (error) {
                if (error) {
                    console.error('error in getAllData in the launchCtrl => ', error);
                }
            }
        };

        const filterChanged = ($scope.filterChanged = async function() {
            const start = moment($scope.dtFrom, 'MM-DD-YYYY').format('YYYY-MM-DD');
            const end = moment($scope.dtTo, 'MM-DD-YYYY').format('YYYY-MM-DD');
            $scope.loading = true;
            const newAccounts = await getAnalyticsDataByDateRangeForLaunchRatesPromise();
            await newAccounts;

            $scope.filteredAccounts = await getFilteredAccounts(
                newAccounts,
                $scope.cardFilter,
                $scope.userFilter,
                $scope.angleFilter,
                $scope.verticalFilter,
                $scope.batchFilter,
                $scope.geoFilter,
                start,
                end
            );

            // console.log('$scope.filteredAccounts after changed filter => ', $scope.filteredAccounts);
            await $scope.filteredAccounts;
            await setLaunchGraphData($scope.filteredAccounts);
            $scope.loading = false;
        });

        const init = async () => {
            await $timeout(getAllData);
        };

        init();

        $(document).ready(() => {
            init();
            $('.btn-default').click(() => {
                $('.collapse').collapse('toggle');
            });
        });
    }
]);