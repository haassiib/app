'use strict';

angular.module('menciusapp').controller('searchCtrl', [
    '$rootScope',
    '$scope',
    '$http',
    '$cookieStore',
    '$timeout',
    '$filter',
    'searchService',
    'statusService',
    function (
        $rootScope,
        $scope,
        $http,
        $cookieStore,
        $timeout,
        $filter,
        searchService,
        statusService
    ) {
        $scope.loading = true;
        $scope.accounts = [];
        $scope.statuses = [];
        $scope.status = 'Select All';
        $scope.columns = [];

        $scope.batchFilter = 'All Batches';
        $scope.userFilter = 'All Users';
        $scope.angleFilter = 'All Angles';
        $scope.cardFilter = 'All Cards';
        $scope.verticalFilter = 'All Verticals';
        $scope.geoFilter = 'All Geos';
        $scope.defaultColumns = [
            'owner',
            'status',
            'batch',
            'angle',
            'geo',
            'total_spend',
            'safe_spend',
            'source',
            'safe_ad_count'
        ];

        $scope.users = Users;
        $scope.status = null;
        $scope.batches = null;
        $scope.angles = Angles;
        $scope.geos = Geos;
        $scope.verticals = Verticals;
        $scope.cards = CardTypes;

        //Column Multiselect
        const columnFilter = $cookieStore.get('search_columns');
        if (columnFilter) {
            $scope.selectedColumns = columnFilter;
            // console.log(columnFilter);
        } else {
            $scope.selectedColumns = $scope.defaultColumns;
        }
        $scope.selectedColumns = $scope.defaultColumns;
        //Default search term
        $scope.searchTerm = '';

        //TODO: Fix this, these values should be coming from the directive
        $scope.dateChange = function (field, date) {
            $scope[field] = date;
            //console.log($scope[field]);
        };

        const onlyUnique = (value, index, self) => {
            return self.indexOf(value) === index;
        };

        statusService.httpGetAllStatus((res, err) => {
            if (err) {
                console.log(err);
                //throw error
            } else {
                res.splice(0, 0, 'Select All');
                //console.log("status res:",res);
                $scope.statuses = res;

                $scope.loading = false;
            }
        });

        //Load All Accounts
        function init() {
            searchService.httpSearchAccountsBySearchTerm((res, err) => {
                if (err) {
                    console.log("Error Retrieving Batches: ", err);
                } else {
                    $scope.accounts = res;
                    if ($scope.accounts.length > 0) {
                        $scope.columns = Object.keys($scope.accounts[0]).sort();
                    }
                    console.log("Scope batches in controller => ", $scope.accounts);
                }
                $scope.loading = false;
            });
        }
        init();

        $scope.sort = function (field, order) {
            //console.log('sort() in ' + order + ' order called on the ' + field + ' field.');
            let propsToSort = [];
            let ts = {
                name: field,
                reverse: order === 'ascending' ? true : false
            };
            propsToSort.push(ts);
            $scope.accounts.sort(Tools.generateSortFunction(propsToSort));
        };
    }
]);
