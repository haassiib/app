'use strict';

app.controller('navCtrl', [
  '$scope',
  '$http',
  'authService',
  '$rootScope',
  '$timeout',
  '$cookieStore',
  '$location',
  function($scope, $http, authService, $rootScope, $timeout, $cookieStore, $location) {
    if ($cookieStore.get('user')) {
      $scope.isLoggedIn = $cookieStore.get('user').isLogged;
      $scope.username = $cookieStore.get('user').username;
    } else {
      $scope.isLoggedIn = false;
    }
    $scope.navSelectedIndex = null;

    // reset tabs
    $scope.login = false;
    $scope.about = false;

    function resetTabs() {
      $scope.warm = false;
      $scope.accounts = false;
      $scope.uploadset = false;
      $scope.pnl = false;
      $scope.payout = false;
      $scope.analytics = false;
      $scope.accounting = false;
      $scope.search = false;
      $scope.add = false;
      $scope.resources = false;
      $scope.secret = false;
      $scope.bug = false;
      $scope.user = false;
    }
    resetTabs();

    $scope.getURL = function() {
      //console.log("Bug Report Clicked");
      $rootScope.$broadcast('Report-Bug-Clicked');
    };

    $scope.getUserSettings = function() {
      authService.httpGetUserSettings((res, err) => {
        if (err) {
          console.log('error: ', err);
        } else {
          //console.log("Retrieved User Settings:", res);
          $rootScope.userSettings = res;
          //   console.log("TODO: set cookie values?");
        }
      });
    };

    $scope.setTabs = function(role) {
      //default tabs
      $scope.bug = true;
      $scope.user = true;

      if (!$scope.viewType) {
        $scope.accounts = true;
        if ((role & $rootScope.userRoles.admin) == $rootScope.userRoles.admin) {
          // console.log('Admin');
          $scope.add = true;
          $scope.pnl = true;
          $scope.payout = true;
          $scope.analytics = true;
          $scope.accounting = true;
          $scope.uploadset = true;
          $scope.resources = true;
          $scope.secret = true;
          $scope.search = true;
        } else if (
          (role & $rootScope.userRoles.user_accountManager) ==
          $rootScope.userRoles.user_accountManager
        ) {
          // console.log('Account manager');
          $scope.add = true;
          $scope.pnl = false;
          $scope.payout = false;
          $scope.analytics = false;
          $scope.accounting = false;
        }
      } else {
        $scope.warm = true;
        $scope.add = true;
      }
    };

    // set tabs
    if ($scope.isLoggedIn) {
      $scope.setTabs($scope.$parent.user.role);
      $scope.getUserSettings();
    }

    $scope.toggle = function() {
      $scope.isLoggedIn = !$scope.isLoggedIn;
    };

    $scope.logout = function() {
      authService.logout();
      // Hard reload page from server
      // location.reload(true);

      //TODO clear rootScope Settings
      console.log('TODO: clear root scope user settings!!');
    };

    $scope.navClicked = function(index) {
      $scope.navSelectedIndex = index;
    };

    $scope.$on('someoneLoggedIn', function() {
      //TODO: logic error if user is logged in and logs in again
      //suggestion: Redirect from login if users logged in
      $scope.toggle();

      // add logic to show only certain tabs for logins who have specific roles
      $scope.setTabs($rootScope.user.role);
      $scope.getUserSettings();
      $scope.username = $rootScope.user.username;
    });

    $scope.changeView = function() {
      let logo = document.getElementById('nav-logo');
      if ($scope.viewType) {
        logo.src = '../images/logo-hor-confucius.svg';
        $location.path('/warm_accounts');
      } else {
        logo.src = '../images/logo-hor-mencius.svg';
        $location.path('/active_accounts');
      }
      resetTabs();
      $scope.setTabs($rootScope.user.role);
      $cookieStore.put('viewType', $scope.viewType);
    };

    $scope.$on('logOut', function() {
      $scope.toggle();
      $scope.tasks = false;
      $scope.accounts = false;
      $scope.warm = false;
      $scope.search = false;
      $scope.bug = false;
      $scope.user = false;
      $scope.dead = false;
      $scope.pnl = false;
      $scope.payout = false;
      $scope.analytics = false;
      $scope.accounting = false;
      $scope.uploadset = false;
      $scope.add = false;
      $scope.resouces = false;
      $scope.secret = false;
    });

    $scope.$on('tasksCountUpdate', function(event, data) {
      // console.log(data);
      $scope.numberOfTasks = data.number;
    });

    //console.log("cookie view: ", $cookieStore.get('viewType'));
    $scope.viewType = false;
    if ($cookieStore.get('viewType')) {
      $scope.viewType = true;
      $scope.changeView();
    }

    $rootScope.$on('$stateChangeSuccess', function() {
      console.log('navCtrl State Changed success');
    });
  }
]);
