"use strict";

app.controller("loginCtrl", [
  "$scope",
  "$http",
  "authService",
  "$rootScope",
  "$injector",
  "$cookieStore",
  function($scope, http, authService, $rootScope, $injector, $cookieStore) {
    $scope.remember = false;

    if (typeof $cookieStore.get("user") !== "undefined") {
      let remember = $cookieStore.get("user").remember;
      if (remember) {
        scope.username = $cookieStore.get("user").username;
        scope.remember = remember;
      }
    }

    $scope.login = function() {
      let user = {
        username: $scope.username,
        remember: $scope.remember,
        password: $scope.password
      };
      $rootScope.user = user;
      authService.login(
        user,
        function(message, data) {
          // console.log("first funct => ", message, " = ", data);
        },
        function(message, data) {
          // console.log("second funct => ", message);
          return data;
        }
      );
    };
  }
]);
