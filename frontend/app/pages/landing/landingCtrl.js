'use strict';

app.controller('landingCtrl', ['$scope', '$http', '$rootScope', '$location', '$timeout', '$cookieStore', 'alertService', 'authService',
    function ($scope, $http, $rootScope, $location, $timeout, $cookieStore, alertService, authService) {
        $rootScope.user.password = '';
        $rootScope.user.remember = '';
        $scope.noData = false;
        $scope.showInitialLanding = true;

        // set up carousel
        $(document).ready(function() {
            $('#quote-carousel').carousel({
                pause: true,
                interval: 3000,
            });
        });

        $scope.utc = moment().utc().format();

        $scope.utc = moment().utc(-5).format();

        $scope.sendToLoginPage = function() {
            if ($rootScope.user.isLogged != true)
                $location.path('/login');
        };

        $scope.$on('newSearch', function() {
            $scope.noData = false;
            console.log('test');
        });

        $scope.$on('search', function() {
            $scope.noData = false;
            console.log('cleared out error message');
        });

        $scope.$on('noData', function() {
            console.log('made it to the landing controller');
            $scope.noData = true;
            console.log($scope.noData);
        });

        $scope.guestLogin = function() {
            console.log('guest login');
            authService.guestLogin();
        };
    }
]);
