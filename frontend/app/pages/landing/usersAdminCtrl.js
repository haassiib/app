"use strict";

angular.module("menciusapp").controller("usersAdminCtrl", [
  "$rootScope",
  "$scope",
  "$http",
  "alertService",
  "$timeout",
  "$location",
  "$cookieStore",
  "authService",
  function(
    $rootScope,
    $scope,
    $http,
    alertService,
    $timeout,
    $location,
    $cookieStore,
    authService
  ) {
    $scope.loading = true;
    $scope.user = $rootScope.user.username;
    $scope.role = $rootScope.user.role;
    $scope.users = [{}];
    $scope.disableInput = false;
    $scope.show = true;

    function init() {
      authService.httpGetUsers((res, err) => {
        if (err) {
          console.log("Error Retrieving Users: ", err);
        } else {
          $scope.users = res;
          console.log("Scope Useres in controller => ", $scope.users);
        }
        $scope.loading = false;
      });
    }
    init();
//Table Checkbox
// This property will be bound to checkbox in table header
$scope.allItemsSelected = false;

// This executes when entity in table is checked
$scope.selectEntity = function () {
    // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
    for (var i = 0; i < $scope.users.length; i++) {
        if (!$scope.users[i].isChecked) {
            $scope.allItemsSelected = false;
            return;
        }
    }

    //If not the check the "allItemsSelected" checkbox
    $scope.allItemsSelected = true;
};

// This executes when checkbox in table header is checked
$scope.selectAll = function () {
    // Loop through all the entities and set their isChecked property
    for (var i = 0; i < $scope.users.length; i++) {
        $scope.users[i].isChecked = $scope.allItemsSelected;
    }
};

//Table Checkbox

$scope.viewUser = function(rowIndex) {
  for(var i=0; i<$scope.users.length; i++){
    if(rowIndex === $scope.users[i].employee_key)
    $scope.vUser = $scope.users[i];
  }
  $scope.disableInput = true;
};
$scope.editUser = function(rowIndex) {
  for(var i=0; i<$scope.users.length; i++){
    if(rowIndex === $scope.users[i].employee_key)
    $scope.vUser = $scope.users[i];
  }
  $scope.disableInput = false;
};
 $scope.deleteUserByID = function(rowIndex) {
  $scope.vUser = $scope.users[rowIndex];
  $scope.disableInput = false;
};
$scope.bulkBatchDelete = function(rowIndex) {
  $scope.vUser = $scope.users[rowIndex];
  $scope.disableInput = false;
};

    $scope.saveProfile = function() {
      console.log("TODO: setup user profile and update");
    };

    $scope.saveNewUser = function() {
      let password = $scope.newpass;
      let passwordver = $scope.newpassver;
      let user = $scope.username;
      let fName = $scope.firstName;
      let lName = $scope.lastName;
      console.log("The click methid is getting called in the controller");

      if ($scope.newpass !== $scope.newpassver) {
        //TODO: validation/error handling
        console.log("New User Not Added!");
        return;
      } else {
        swal(
          {
            title: "Are You Sure You Want To Save New User?",
            text: "New user will be saved!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Save",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: true
          },
          function(isConfirm) {
            if (isConfirm) {
              //Check is username exists and add if not
              authService.register(
                user,
                fName,
                lName,
                password,
                (result, error) => {
                  if (error) {
                    console.log("Error adding new user!");
                    throw error;
                  } else if (typeof result.data === "string") {
                    swal("Fail", "Username already exists.", "error");
                  } else if (typeof result.data === "object") {
                    console.log("New user added successfully. => ", result);
                    swal("Success", "New user has been added!.", "success");
                  }
                }
              );
            }
          }
        );
      }
    };

    $scope.resetPassword = function() {
      let password = $scope.newpass;
      let passwordver = $scope.newpassver;

      if ($rootScope.user.password !== $scope.oldpass) {
        //TODO: validation/error handling
        console.log("password reset failure!");
        return;
      } else if ($scope.newpass !== $scope.newpassver) {
        //TODO: validation/error handling
        console.log("password reset failure!");
        return;
      } else {
        swal(
          {
            title: "Are You Sure You Want To Reset Your Password?",
            text: "You password will be changed and you will be logged out!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Reset",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: true
          },
          function(isConfirm) {
            if (isConfirm) {
              //Change password and then log out
              authService.httpResetPassword(
                $rootScope.user,
                password,
                (result, error) => {
                  if (error) {
                    console.log("reset password error!");
                    throw error;
                  } else {
                    console.log("reset password call successful.");
                    authService.logout();
                    swal("Success", "Your password has been reset.", "success");
                  }
                }
              );
            }
          }
        );
      }
    };
  }
]);
