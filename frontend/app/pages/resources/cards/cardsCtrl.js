﻿'use strict';

app.controller('cardsCtrl', [
    '$scope',
    '$http',
    '$rootScope',
    '$location',
    'alertService',
    '$timeout',
    'codeService',
    function ($scope,
        $http,
        $rootScope,
        $location,
        alertService,
        $timeout,
        codeService) {
        $scope.loading = true;
        $scope.user = $rootScope.user.username;
        $scope.role = $rootScope.user.role;
        $scope.filterTable = null;
        $scope.accountTypes = CardTypes;
        $scope.inUse = [
            '',
            '1',
            '0'
        ]
        $scope.status = [
            '',
            'open',
            'closed'
        ]
        /* $scope.filterChanged = function() {

            $scope.filterTable = '';
        }; */
        // Number of Table Rows per page
        $scope.register = {};
        $scope.register.selectedRowPerPage = "25";

        $scope.register.rowPerPage = [
            { value: "10", text: "10 Rows" },
            { value: "25", text: "25 Rows" },
            { value: "50", text: "50 Rows" },
            { value: "100", text: "100 Rows" }
        ];

        codeService.httpGetAccountCodes(true, (res, err) => {
            if (err) {
                //throw error
                console.log(err);
            }
            else {
                console.log(res);
                $scope.codes = res;
                $scope.loading = false;
            }
        });
        console.log('Cards: ', $scope.codes);

        //Table Checkbox
        // This property will be bound to checkbox in table header
        $scope.allItemsSelected = false;

        // This executes when entity in table is checked
        $scope.selectEntity = function () {
            // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
            for (var i = 0; i < $scope.codes.length; i++) {
                if (!$scope.codes[i].isChecked) {
                    $scope.allItemsSelected = false;
                    return;
                }
            }

            //If not the check the "allItemsSelected" checkbox
            $scope.allItemsSelected = true;
        };

        // This executes when checkbox in table header is checked
        $scope.selectAll = function () {
            // Loop through all the entities and set their isChecked property
            for (var i = 0; i < $scope.codes.length; i++) {
                $scope.codes[i].isChecked = $scope.allItemsSelected;
            }
        };

        // Cards
        $scope.addCard = function (index) {
            var cardTemplate = {};
            $scope.cards.push(cardTemplate);
            console.log($scope.cards);
        };

        $scope.removeCard = function (index) {
            $scope.cards.splice(index, 1);
            //console.log($scope.cards);
            console.log(index);
        };

        $scope.saveCode = function (index, code) {
            console.log("here with: ", code);
            codeService.httpPutAccountCode([code], (res, err) => {
                if (err) {
                    //throw error
                    console.log(err);

                    swal("Error!", "Credit Card not updated.\n" + err, "warning");
                }
                else {
                    console.log(res);
                    swal("Success!", "Credit Card updated.", "success");
                }
            });
        };

        var validCards = function (codes) {
            let count = [];
            count = codes
                .map(code => {
                    return code.number === undefined ||
                        code.cvc === undefined ||
                        code.exp_month === undefined ||
                        code.exp_year === undefined ||
                        code.zip === undefined ||
                        code.accounttype === undefined
                        ? false
                        : true;
                })
                .filter(item => item === true);

            console.log("count: ", count);
            if (count.length < codes.length) {
                console.log("cards invalid");
                return false;
            } else {
                console.log("cards valid");
                return true;
            }
        };
        /* Validate Card */
        var validateCards = function (codes) {
            let count = [];
            count = codes
                .map(code => {
                    return code.number === undefined ? false : true;
                })
                .filter(item => item == true);

            console.log("count: ", count);
            if (count.length < codes.length) {
                console.log("cards invalid");
                return false;
            } else {
                console.log("cards valid");
                return true;
            }
        };
        /*Insert Card */
        $scope.insertCards = function (cards) {
            console.log("Working to create cards.");
            if (cards.length < 1) {
                console.log("No cards to enter.");
            } else if (validateCards(cards)) {
                console.log("cards were valid: \n", cards);
                codeService.httpPostAccountCodes(cards, (res, err) => {
                    if (err) {
                        console.log(err);
                        swal("Error", err, "warning");
                    } else {
                        console.log(res);
                        swal("Success!", "Account Codes Added.", "success");

                        // Reset Page values and data
                        console.log("re initializing data");
                        init();
                    }
                });
            } else {
                console.log("Invalid Card values");
                swal("Error", "Invalid or Missing field information", "warning");
            }
        };

        /*Table sorting */
        $scope.sort = function (field, order) {
            let propsToSort = [];
            let ts = {
                name: field,
                reverse: order === 'ascending' ? true : false
            };
            propsToSort.push(ts);
            $scope.codes.sort(Tools.generateSortFunction(propsToSort));
        }
    }]
)