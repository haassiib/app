﻿'use strict';

app.controller('verticalsCtrl', [
    '$scope', '$http', '$rootScope', '$location', 'alertService', '$timeout', 'verticalService',
    function ($scope, $http, $rootScope, $location, alertService, $timeout, verticalService) {
        $scope.loading = true;
        $scope.user = $rootScope.user.username;
        $scope.role = $rootScope.user.role;
        $scope.verticals = [{}];

        function init() {

            verticalService.httpGetVerticals((res, err) => {
                if (err) {
                    console.log("Error Retrieving Verticals: ", err);
                }
                else {
                    $scope.verticals = res;
                }
                $scope.loading = false;
            });

        }
        init();

        $scope.addVerticals = function () {
            verticalService.httpPostInsertVertical([Tools.CapitalizeFirstLetter($scope.newVert)], (res, err) => {
                if (err || res.status > 299) {
                    swal("Error", "Unsuccessful insert, try again or contact support.", "error");
                }
                else {
                    swal("Success!", "Successfully inserted new Vertical.", "success");
                    $scope.newVert = null;
                    init();
                }
            });
        };
    }]
);