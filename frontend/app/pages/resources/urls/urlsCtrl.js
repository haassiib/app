﻿'use strict';

app.controller('urlsCtrl', [
    '$scope',
    '$http',
    '$rootScope',
    '$location',
    'alertService',
    '$timeout',
    'safeUrlService',
    function ($scope, $http, $rootScope, $location, alertService, $timeout, safeUrlService) {
        $scope.loading = true;
        $scope.user = $rootScope.user.username;
        $scope.role = $rootScope.user.role;
        $scope.tableFilter = $rootScope.rootTableFilter;
        $scope.urls = [{}];

        //safeUrlService.httpGetSafeUrls((res, err) => {
        //    if (err) {
        //        console.log("Error Retrieving Safe Urls: ", err);
        //    }
        //    else {
        //        $scope.urls = res;
        //    }
        //    $scope.loading = false;
        //});

        safeUrlService.httpGetDetailedSafeUrls((res, err) => {
            if (err) {
                console.log("Error Retrieving Safe Urls: ", err);
            }
            else {
                $scope.urls = res;
            }
            $scope.loading = false;
        });

        $scope.saveAllUrls = function (url) {

            let cardsToUpdate = $scope.urls.filter(url => url.changed);
            if (cardsToUpdate.length < 1) {
                swal('Warning', 'No edited cards to update.', 'warning');
                return;
            }
            safeUrlService.httpPutSafeUrls(cardsToUpdate, (res, err) => {
                if (err) {
                    console.log("Error updating safe url: ", err);
                    swal('Error', 'There was an issue copying item to clipboard. Verify the value exists.', 'error');
                }
                else {
                    console.log("Successful safe url update");
                    swal('Success', 'Safe Urls Successfully Updated', 'success');
                }
            });
        }

        $scope.urlChanged = function (url) {
            url.changed = true;
        };

        $scope.sort = function (field, order) {
            let propsToSort = [];
            let ts = {
                name: field,
                reverse: order === 'ascending' ? true : false
            };
            propsToSort.push(ts);
            $scope.urls.sort(Tools.generateSortFunction(propsToSort));
        }
    }]
);