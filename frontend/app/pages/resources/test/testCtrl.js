'use strict';

app.controller('testCtrl', [
    '$scope', '$http', '$rootScope', '$location', 'alertService', '$timeout',
    function ($scope, $http, $rootScope, $location, alertService, $timeout) {
        let user = $rootScope.user.username;
        $scope.role = $rootScope.user.role;
        $scope.preview_view = false;

        $scope.test = "This is the test value.";
        $scope.ad = {
            user: user,
            content_key: 50,
            headline: 'You Will NOT Believe This! Look What She Did In Just Four Days!',
            body: 'Facebook Ads Made Easy with Mencius!',
            newsfeed_link: 'Easier than your OTHER FB Ad Tool!',
            link_to_graphic: 'https://1drv.ms/v/s!AqW1zwQ1RvANgge5jl2F64WFNngt',
            url: 'HTTP://WWW.TESTURL.COM/CHECK-THIS-OUT'
        }

        $scope.mobile = {
            user: user,
            headline: 'Mobile is going to look Great!',
            body: 'Facebook Mobile Ads Made Easy with Mencius!',
            newsfeed_link: 'Easier than your OTHER FB Ad Tool!',
            link_to_graphic: 'https://1drv.ms/v/s!AqW1zwQ1RvANgge5jl2F64WFNngt',
            url: 'HTTP://WWW.TESTURL.COM/CHECK-THIS-OUT'
        }

        $scope.switch = function(){
            console.log("preview view:", $scope.preview_view);
            $scope.preview_view = !$scope.preview_view;
        };

        $scope.pci = {
            pnl_key: 191,
            account_key: 123,
            content_key: 72,
            adset_key: 75,
            headline: 'tetetet',
            body: 'sdfsdfsdf',
            newsfeed_link: 'hghghghg',
            link_to_graphic: 'asdfasdf.com',
            thumbnail: null,
            comment: 'asdfasfd',
            roi: 106.8966,
            revenue: 600,
            expenses: 290,
            profit: 310,
            country: 'American Samoa',
            angle: 'Fishbutts',
            name: 'Diet'
        };
    }]
);