﻿'use strict';

app.controller('accountsCtrl', [
    '$scope', '$http', '$rootScope', '$timeout', 'accountService',
    function ($scope, $http, $rootScope, $timeout, accountService) {
        $scope.loading = true;
        $scope.user = $rootScope.user.username;
        $scope.role = $rootScope.user.role;

        accountService.httpGetAccounts((res, err) => {
            if (err) {
                //throw error
                console.log(err);
            }
            else {
                //console.log(res);
                $scope.accounts = res;
                $scope.loading = false; 
            }
        });
    }]
)