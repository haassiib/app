﻿'use strict';

app.controller('statusesCtrl', [
    '$scope',
    '$http',
    '$rootScope',
    '$location',
    'alertService',
    '$timeout',
    'statusService',
    function (
        $scope,
        $http,
        $rootScope,
        $location,
        alertService,
        $timeout,
        statusService) {
        $scope.loading = true;
        $scope.user = $rootScope.user.username;
        $scope.role = $rootScope.user.role;
        $scope.status = [{}];

        statusService.httpGetStatus((res, err) => {
            if (err) {
                console.log("Get Statuses Error: ", err);
            }
            else {
                $scope.statuses = res;
            }
            $scope.loading = false;
        });
    }]
);