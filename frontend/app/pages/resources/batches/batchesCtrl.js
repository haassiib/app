﻿"use strict";

app.controller("batchesCtrl", [
  "$scope",
  "$http",
  "$rootScope",
  "$location",
  "alertService",
  "$timeout",
  "batchService",
  function(
    $scope,
    $http,
    $rootScope,
    $location,
    alertService,
    $timeout,
    batchService
  ) {
    $scope.loading = true;
    $scope.user = $rootScope.user.username;
    $scope.role = $rootScope.user.role;
    $scope.batches = [{}];
    $scope.disableInput = false;
    $scope.show = true;
    $scope.batchesToUpdate = [{}];
    $scope.vBatch = [{}];
    $scope.uBatch = [{}];

    // Number of Table Rows per page
    $scope.register = {};
    $scope.register.selectedRowPerPage = "25";

    $scope.register.rowPerPage = [
      {value: "10", text: "10 Rows"},
      {value: "25", text: "25 Rows"}, 
      {value: "50", text: "50 Rows"}, 
      {value: "100", text: "100 Rows"}
    ];


    // Column multiselect
    $scope.selectedColumns = [
      "name",
      "description",
      "safe_spend",
      "proxy",
      "warm_time",
      "safe_ads",
      "pre_warming_activities",
      "pre_launch_activities"
    ];

    //Load All Batch
    function init() {
      batchService.httpGetBatches((res, err) => {
        if (err) {
          console.log("Error Retrieving Batches: ", err);
        } else {
          $scope.batches = res;
          console.log("Scope batches in controller => ", $scope.batches);
        }
        $scope.loading = false;
      });
    }
      init();

    //Set color to selected row
      $scope.selectedRow = null;
      $scope.changeSeletedtRowColor = function (index) {
          $scope.selectedRow = index;
      }

    //Table Checkbox
    // This property will be bound to checkbox in table header
    $scope.allItemsSelected = false;

    // This executes when entity in table is checked
    $scope.selectEntity = function () {
        // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
        for (var i = 0; i < $scope.batches.length; i++) {
            if (!$scope.batches[i].isChecked) {
                $scope.allItemsSelected = false;
                return;
            }
        }
        //If not the check the "allItemsSelected" checkbox
        $scope.allItemsSelected = true;
    };

    // This executes when checkbox in table header is checked
    $scope.selectAll = function () {
        // Loop through all the entities and set their isChecked property
        for (var i = 0; i < $scope.batches.length; i++) {
            $scope.batches[i].isChecked = $scope.allItemsSelected;
        }
    };

      $scope.openBatchModal = function () {
          var batchModalInstance = $modal.open({
              templateUrl: 'pages/resources/batches/modals/batch_modal.html',
              controller: batchModalInstanceCtrl,
              resolve: {
                  batch: function () {
                      return $scope.batch;
                  }
              }
          });

          batchModalInstance.result.then(
              function (batch) {
                  //console.log($scope.account);
                  //console.log(code);
                  $scope.account.cc = code;
              },
              function () {
                  console.log('Batch Url Modal dismissed at: ' + new Date());
              }
          );
      };
      let batchModalInstanceCtrl = function ($scope, $modalInstance, batchService, batch) {
          $scope.accountTypes = CardTypes;
          $scope.codeExists = 'exists';

          batchService.httpGetAccountCodes(false, (res, err) => {
              if (err) {
                  console.log(err);
                  swal('Error', 'Could not retrieve account codes.', 'warning');
              } else {
                  $scope.codes = res;
                  if ($scope.batch.length > 0) {
                      //console.log("codes:", res);
                      $scope.selected.cc = $scope.batch[0].code;
                      $scope.current = account.cc;
                  }
              }
          });

          $scope.ok = function () {
              console.log('$scope.selected: ', $scope.selected);
              console.log('scope.codeExists: ', $scope.codeExists);
              let newCode;
              if ($scope.codeExists === 'exists') {
                  let codeobj = $scope.codes.filter(cc => {
                      return parseInt(cc.code) === parseInt($scope.selected.cc);
                  });
                  newCode = {
                      code: codeobj[0].code,
                      code_key: codeobj[0].code_key,
                      code_type: codeobj[0].code_type,
                      account_key: account.account_key,
                      changed: true,
                      claimed: true
                  };
              } else if ($scope.codeExists === 'new') {
                  newCode = {
                      number: $scope.newCard.number,
                      accounttype: $scope.newCard.accounttype,
                      cvc: $scope.newCard.cvc,
                      zip: $scope.newCard.zip,
                      exp_month: $scope.newCard.exp_month,
                      exp_year: $scope.newCard.exp_year,
                      account_key: account.account_key,
                      changed: true,
                      claimed: true
                  };
              }
              let codeset = [newCode];
              let oldCode;
              if (account.cc) {
                  oldCode = {
                      code_key: account.cc.code_key,
                      claimed: false,
                      changed: false
                  };
              }
              if (oldCode) codeset.splice(0, 0, oldCode);

              console.log('newCode: ', newCode);
              console.log('oldCode: ', oldCode);
              console.log('codeSet:', codeset);

              if ($scope.codeExists === 'exists') {
                  existingCard(newCode, codeset);
              } else if ($scope.codeExists === 'new') {
                  newCard(newCode, oldCode);
              }
          };

          $scope.cancel = function () {
              console.log('DISMISS');
              $modalInstance.dismiss('cancel');
          };

          $scope.creditCardUpdate = function () {
              console.log('Change event: ', $scope.selected);
          };
      };

    //View modal
    $scope.viewBatch = function(rowNum) {
      let rowIndex = rowNum - 1;
      $scope.vBatch = $scope.batches[rowIndex];
    };

    //Update modal
    $scope.updateBatch = function(rowNum) {
      let rowIndex = rowNum - 1;
      $scope.uBatch = $scope.batches[rowIndex];
      /* $scope.uBatch.name = $scope.batches[rowIndex].name;
      $scope.uBatch.description = $scope.batches[rowIndex].description;
      $scope.uBatch.safe_spend = $scope.batches[rowIndex].safe_spend;
      $scope.uBatch.proxy = $scope.batches[rowIndex].proxy;
      $scope.uBatch.warm_time = $scope.batches[rowIndex].warm_time;
      $scope.uBatch.safe_ads = $scope.batches[rowIndex].safe_ads;
      $scope.uBatch.pre_warming_activities = $scope.batches[rowIndex].pre_warming_activities;
      $scope.uBatch.pre_launch_activities = $scope.batches[rowIndex].pre_launch_activities;  */
    };

    $scope.addBatches = async function() {
      await batchService.httpPostInsertBatch(
        [Tools.CapitalizeFirstLetter($scope.newBatchName)],
        $scope.newBatchDescription,
        $scope.newSafeSpend,
        $scope.newProxy,
        $scope.newWarmTime,
        $scope.newSafeAds,
        $scope.newPreWarmingActivities,
        $scope.newPreLaunchActivities,
        (res, err) => {
          if (err || res.status > 299) {
            swal(
              "Error",
              "Unsuccessful insert, try again or contact support.",
              "error"
            );
          } else {
            swal("Success!", "Successfully inserted new Batch.", "success");
            $scope.newBatchName = null;
            $scope.newBatchDescription = null;
            $scope.newSafeSpend = null;
            $scope.newProxy = null;
            $scope.newWarmTime = null;
            $scope.newSafeAds = null;
            $scope.newPreWarmingActivities = null;
            $scope.newPreLaunchActivities = null;
            init();
          }
        }
      );
    };
    
    /* Track data changes */
    $scope.batchChanged = function(batch) {
      batch.changed = true;
    };

    /* Bulk Update Batches */
    $scope.updateBatches = async function() {
      let changedBatches = $scope.batches.filter(
        a => typeof a.changed !== "undefined" && a.changed === true
      );
      console.log("changedBatches => ", changedBatches);
      await batchService.httpPutUpdateBatches(changedBatches, (res, err) => {
        if (err || res.status > 299) {
          swal(
            "Error",
            "Unsuccessful update of batches, try again or contact support.",
            "error"
          );
        } else {
          swal("Success!", "Successfully updated Batches.", "success");
          init();
        }
      });
    };
    /* Remove single Batch *|* ready */
    $scope.removeBatch = function(batch_key, rowNum) {
      let index = rowNum-1;
      swal(
        {
          title: 'WARNING - Ad Deletion',
          text: 'This will destroy the ad. It will not be recoverable. ',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Delete Batch.',
          closeOnConfirm: true
        },
        function(isConfirm) {
          console.log('is confirm: ', isConfirm);
          if (isConfirm) {
            deleteBatch(batch_key, index);
          }
        }
      );
    };
    function deleteBatch(batch_key, index) {
      batchService.httpDeleteBatch(batch_key, (res, err) => {
        if (err) {
          console.log('err:', err);
          swal('Error!', 'There was an issue deleteing your batch.', 'error');
        } else {
          console.log('res: ', res);
          swal('Success!', 'Your batch was deleted.', 'success');
          //removes ad from UI
          $scope.batches.splice(index, 1);
        }
      });
      };

      /* Remove Slected Batches *|* ready */
      $scope.removeBatches = async function () {
          let selectedBatches = $scope.batches.filter(
              a => typeof a.isChecked !== "undefined" && a.isChecked === true
          );
          console.log("selectedBatches => ", selectedBatches);
          swal(
              {
                  title: 'WARNING - Ad Deletion',
                  text: 'This will destroy the ad. It will not be recoverable. ',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#DD6B55',
                  confirmButtonText: 'Delete Batch.',
                  closeOnConfirm: true
              },
              function (isConfirm) {
                  console.log('is confirm: ', isConfirm);
                  if (isConfirm) {
                      deleteBatches(selectedBatches);
                  }
              }
          );
      };
      function deleteBatches(selectedBatches) {
          batchService.httpDeleteBatches(selectedBatches, (res, err) => {
              if (err) {
                  console.log('err:', err);
                  swal('Error!', 'There was an issue deleteing selected batch.', 'error');
              } else {
                  console.log('res: ', res);
                  swal('Success!', 'Selected Batch Deleted.', 'success');
                  //Rload batch
                  init();
              }
          });
      };
  }
]);
