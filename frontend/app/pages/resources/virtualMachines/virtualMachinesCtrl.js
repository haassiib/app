﻿"use strict";

app.controller("virtualMachinesCtrl", [
  "$scope",
  "$http",
  "$rootScope",
  "$location",
  "alertService",
  "$timeout",
  "virtualMachineService",
  function(
    $scope,
    $http,
    $rootScope,
    $location,
    alertService,
    $timeout,
    virtualMachineService
  ) {
    $scope.loading = true;
    $scope.user = $rootScope.user.username;
    $scope.role = $rootScope.user.role;
    $scope.vms = [{}];
    $scope.vmSearchFilter = "";

    function init() {
      virtualMachineService.httpGetVms((res, err) => {
        if (err) {
          console.log("Error Retrieving Vms: ", err);
        } else {
          $scope.vms = res;
          console.log("Scope vms in controller => ", $scope.vms);
        }
        $scope.loading = false;
      });
    }
    init();

    //Table Checkbox
   
    $scope.model = {};
	
    // This property will be bound to checkbox in table header
    $scope.allItemsSelected = false;

    // This executes when entity in table is checked
    $scope.selectEntity = function () {
        // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
        for (var i = 0; i < $scope.vms.length; i++) {
            if (!$scope.vms[i].isChecked) {
                $scope.model.allItemsSelected = false;
                return;
            }
        }

        //If not the check the "allItemsSelected" checkbox
        $scope.allItemsSelected = true;
    };

    // This executes when checkbox in table header is checked
    $scope.selectAll = function () {
        // Loop through all the entities and set their isChecked property
        for (var i = 0; i < $scope.vms.length; i++) {
            $scope.vms[i].isChecked = $scope.allItemsSelected;
        }
    };

//Table Checkbox

    $scope.addVms = async function() {
      await virtualMachineService.httpPostInsertVm(
        [Tools.CapitalizeFirstLetter($scope.newVmName)],
        $scope.newVmUUID,
        (res, err) => {
          if (err || res.status > 299) {
            swal(
              "Error",
              "Unsuccessful insert, try again or contact support.",
              "error"
            );
          } else {
            swal("Success!", "Successfully inserted new Batch.", "success");
            $scope.newVmName = null;
            $scope.newVmUUID = null;
            init();
          }
        }
      );
    };
    $scope.search = function() {
      console.log("dateto: ", $scope.dtTo);
      console.log("datefrom: ", $scope.dtFrom);

      var getSearchConfig = {
        method: "GET",
        url: $rootScope.urlRoot + "/vms/search",
        headers: {
          "Content-Type": "application/json",
          "x-username": $rootScope.user.username,
          "x-role": $rootScope.role,
          "x-status": $scope.vmSearchFilter
        }
      };
      //console.log(getSearchConfig);

      $http(getSearchConfig).then(
        function(data) {
          //console.log("Retrieved search results from db: ",data.data);
          $scope.vms = data.data;
          if ($scope.accounts.length > 0) {
            $scope.columns = Object.keys($scope.accounts[0]).sort();
            //console.log("columns: ", $scope.columns);
          }
        },
        function(data, status) {
          if (data.status > 200) {
            console.log(
              "could not retrieve search results, something went wrong."
            );
            console.log("data: ", data);
            //return callback(data, status);
          }
        }
      );
    };
    $scope.search();
  }
]);
