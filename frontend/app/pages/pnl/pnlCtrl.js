'use strict';

angular.module('menciusapp').controller('pnlCtrl', [
  '$rootScope',
  '$scope',
  '$http',
  '$cookieStore',
  'accountService',
  'pnlService',
  function($rootScope, $scope, $http, $cookieStore, accountService, pnlService) {
    $scope.loading = true;
    $scope.verticals = Verticals;
    $scope.geos = [];

    // PnL user Settings
    $scope.ps = {};
    function initStoredValues() {
      $scope.ps.dateFilter = $scope.dnow = moment().format('MM-DD-YYYY');
      $scope.ps.startDateFilter = moment().format('MM-DD-YYYY');
      $scope.ps.endDateFilter = moment().format('MM-DD-YYYY');
      $scope.ps.geoFilter = 'All Geos';
      $scope.ps.verticalFilter = 'All Verticals';
      $scope.expanded = false;
      // Setup User Filter
        
        //$scope.ps.userFilter = 'All Users';
     $scope.user = $rootScope.user.username;

        $('.overlayscroller').overlayScrollbars({});

      $scope.users = [
        'All Users',
        'Ashif',
        'Joe',
        'Justin',
        'Luke',
        'Marcus',
        'Mike',
        'Ryan',
        'Scott',
        'Stan',
        'Trey',
        'Valerie',
        'Zach',
        'Zoe'
      ];
      if ($cookieStore.get('pnl_user_filter')) {
        $scope.ps.userFilter = Tools.CapitalizeFirstLetter($cookieStore.get('pnl_user_filter'));
      } else {
        $scope.ps.userFilter = Tools.CapitalizeFirstLetter($scope.user);
        $cookieStore.put('pnl_user_filter', $scope.ps.userFilter);
      }

      $scope.currentUser = $scope.user.toLowerCase();
      let pnlFilterTest = $cookieStore.get('pnl_geo_filter');
      // Setup Geo Filter
      if ($cookieStore.get('pnl_geo_filter')) {
        $scope.ps.geoFilter = Tools.CapitalizeFirstLetter($cookieStore.get('pnl_geo_filter'));
      } else {
        $scope.ps.geoFilter = 'All Geos';
        $cookieStore.put('pnl_geo_filter', $scope.ps.geoFilter);
      }

      // Setup Vertical Filter
      if ($cookieStore.get('pnl_vertical_filter')) {
        $scope.ps.verticalFilter = Tools.CapitalizeFirstLetter(
          $cookieStore.get('pnl_vertical_filter')
        );
      } else {
        $scope.ps.verticalFilter = 'All Verticals';
        $cookieStore.put('pnl_vertical_filter', $scope.ps.verticalFilter);
      }

      // Setup Status Filter
      $scope.status = 'All Accounts';
      $scope.statuses = ['All Accounts', 'Spending', 'Alive', 'Dead'];
      if ($cookieStore.get('pnl_status_filter')) {
        $scope.ps.statusFilter = Tools.CapitalizeFirstLetter($cookieStore.get('pnl_status_filter'));
      } else {
        $scope.ps.statusFilter = Tools.CapitalizeFirstLetter($scope.status);
        $cookieStore.put('pnl_status_filter', $scope.ps.statusFilter);
      }

      // Setup TimeFrame Filter
      $scope.timeFrame = 'Today';
      $scope.timeFrames = [
        'Today',
        'Yesterday',
        'Last 3 Days',
        'Last 7 Days',
        'Month-to-Date',
        'Last 30 Days',
        'Custom Date',
        'Custom Range'
      ];
      if ($cookieStore.get('pnl_time_filter')) {
        $scope.ps.timeFrameFilter = $cookieStore.get('pnl_time_filter');
      } else {
        $scope.ps.timeFrameFilter = $scope.timeFrame;
        $cookieStore.put('pnl_time_filter', $scope.ps.timeFrameFilter);
      }

      $scope.ps.lastSort =
        $cookieStore.get('pnl_last_sort') != null ? $cookieStore.get('pnl_last_sort') : null;
      $scope.ps.lastOrder =
        $cookieStore.get('pnl_last_order') != null ? $cookieStore.get('pnl_last_order') : null;
    }

    initStoredValues();

    const onlyUnique = (value, index, self) => {
      return self.indexOf(value) === index;
    };

    function init() {
      //console.log("Init called.");
      $scope.loading = true;
      let ContentByAccts = [];

      // This was done in haste - reevaluate for a better solution
      let pnlstart;
      let pnlend;
      switch ($scope.ps.timeFrameFilter) {
        case 'All Time':
          pnlend = moment().format('YYYY-MM-DD');
          pnlstart = moment()
            .subtract(720, 'days')
            .format('YYYY-MM-DD');
          break;
        case 'Today':
          pnlstart = moment().format('YYYY-MM-DD');
          pnlend = moment().format('YYYY-MM-DD');
          break;
        case 'Yesterday':
          pnlend = moment()
            .subtract(1, 'days')
            .format('YYYY-MM-DD');
          pnlstart = moment()
            .subtract(1, 'days')
            .format('YYYY-MM-DD');
          break;
        case 'Last 3 Days':
          pnlend = moment().format('YYYY-MM-DD');
          pnlstart = moment()
            .subtract(3, 'days')
            .format('YYYY-MM-DD');
          break;
        case 'Last 7 Days':
          pnlend = moment().format('YYYY-MM-DD');
          pnlstart = moment()
            .subtract(7, 'days')
            .format('YYYY-MM-DD');
          break;
        case 'Month-to-Date':
          pnlend = moment().format('YYYY-MM-DD');
          pnlstart = moment()
            .startOf('month')
            .format('YYYY-MM-DD');
          break;
        case 'Last 30 Days':
          pnlend = moment().format('YYYY-MM-DD');
          pnlstart = moment()
            .subtract(30, 'days')
            .format('YYYY-MM-DD');
          break;
        case 'Custom Date':
          pnlend = moment($scope.ps.dateFilter, 'MM-DD-YYYY').format('YYYY-MM-DD');
          pnlstart = moment($scope.ps.dateFilter, 'MM-DD-YYYY').format('YYYY-MM-DD');
          break;
        case 'Custom Range':
          pnlend = moment($scope.ps.endDateFilter, 'MM-DD-YYYY').format('YYYY-MM-DD');
          pnlstart = moment($scope.ps.startDateFilter, 'MM-DD-YYYY').format('YYYY-MM-DD');
          break;
      }
      // console.log("start", pnlstart);
      // console.log("end", pnlend);

      pnlService.httpGetPnl(pnlstart + ' 00:00:00', pnlend + ' 23:59:59', (res, err) => {
        if (err) {
          console.log('Pnl Error:', err);
          $scope.loading = false;
        } else {
          //console.log("Pnl Success: ", res);
          let allPnl = res.data;

          allPnl.forEach(a => {
            let tempObj = {};
            Object.assign(tempObj, a);
            if (ContentByAccts[a.account_key.toString()]) {
              if (!ContentByAccts[a.account_key.toString()].contents[a.content_key.toString()]) {
                //Account Exists, Content and Pnl Do Not
                ContentByAccts[a.account_key.toString()].contents[a.content_key] = {
                  pnl: [],
                  content_key: a.content_key
                };
                ContentByAccts[a.account_key.toString()].contents[a.content_key.toString()].pnl[
                  a.pnl_key.toString()
                ] = tempObj;
              } else {
                //Content Exists, just add PNL
                //Maybe even verify pnl doesn't exists to avoid overwritting or find duplicates?
                ContentByAccts[a.account_key.toString()].contents[a.content_key.toString()].pnl[
                  a.pnl_key.toString()
                ] = tempObj;
              }
            } else {
              //Account Doesn't Exist
              ContentByAccts[a.account_key.toString()] = {
                contents: [],
                account_key: a.account_key,
                fname: a.fname,
                lname: a.lname,
                pixel: a.pixel,
                status: a.status,
                media_buyer: a.media_buyer,
                geo: a.geo,
                angle: a.angle,
                vertical: a.vertical,
                owner: a.owner,
                total_spend: a.total_spend
              };
              ContentByAccts[a.account_key.toString()].contents[a.content_key.toString()] = {
                pnl: [],
                content_key: a.content_key
              };
              ContentByAccts[a.account_key.toString()].contents[a.content_key.toString()].pnl[
                a.pnl_key.toString()
              ] = tempObj;
            }
          });
          // Contents need to be cleaned up because arrays are full of undefineds, as the engine creates them
          // thinking the IDs are indexs
          $scope.allAccounts = ContentByAccts.filter(a => typeof a !== 'undefined');
          $scope.allAccounts.forEach(a => {
            // console.log("a in the init => ", a);
            a.contents = a.contents.filter(c => typeof c !== 'undefined');
            a.contents.forEach(c => {
              c.pnl = c.pnl
                .filter(p => typeof p !== 'undefined')
                .map(p => {
                  p.oldcreated = p.created;
                  //p.created = Tools.convertToLocalTime(p.created);
                  return p;
                })
                .sort(Tools.generateSortFunction([{ name: 'created', reverse: true }]));
              setTodaysValuePercentages(c);
            });
            a.contents.sort(Tools.generateSortFunction([{ name: 'content_key', reverse: true }]));
          });

          //set all geos
          $scope.geos = $scope.allAccounts
            .map(acc => {
              if (acc.geo !== null && acc.geo !== undefined) {
                return acc.geo;
              }
            })
            .filter(onlyUnique);
          $scope.geos.push('All Geos');
          $scope.geos = $scope.geos.sort();

          // console.log("All Accounts PNL:", $scope.allAccounts);
          //console.log("Length: ", $scope.allAccounts.length)
          setAccountFilters();
          $scope.loading = false;
        }
        // console.log("init finished.");
      });
    }
    init();
    //TODO: this may be more wisely done on the server or in the db to save data transfered
    function setAccountFilters() {
      $scope.accounts = JSON.parse(JSON.stringify($scope.allAccounts));
      $scope.accounts = $scope.accounts
        .filter(a => {
          if ($scope.ps.userFilter == 'All Users') return true;
          return $scope.ps.userFilter === Tools.CapitalizeFirstLetter(a.owner);
        })
        .filter(a => {
          if ($scope.ps.geoFilter == 'All Geos') {
            return true;
          } else {
            return $scope.ps.geoFilter === Tools.CapitalizeFirstLetter(a.geo);
          }
        })
        .filter(a => {
          if ($scope.ps.verticalFilter == 'All Verticals') {
            return true;
          } else {
            return $scope.ps.verticalFilter === Tools.CapitalizeFirstLetter(a.vertical);
          }
        })
        .filter(a => {
          if ($scope.ps.statusFilter === 'Spending') {
            if (a.status === 'Spending, In PL' || a.status === 'Needs Bump') {
              return true;
            } else {
              return false;
            }
          } else if ($scope.ps.statusFilter === 'Alive') {
            if (a.status.toLowerCase().indexOf('dead') < 0) return true;
            return false;
          } else if ($scope.ps.statusFilter === 'Dead') {
            if (a.status.toLowerCase().indexOf('dead') >= 0) return true;
            return false;
          }
          //return true for any other case (including all status)
          return true;
        })
        .filter(a => {
          if ($scope.ps.timeFrameFilter === 'All Time') {
            return true;
          }
          a.contents = a.contents.filter(content => {
            content.pnl = content.pnl.filter(item => {
              const REFERENCE = moment();
              const TODAY = REFERENCE.clone().endOf('day');
              const YESTERDAY = REFERENCE.clone()
                .subtract(1, 'days')
                .startOf('day');
              const THREE_DAYS = REFERENCE.clone()
                .subtract(3, 'days')
                .startOf('day');
              const SEVEN_DAYS = REFERENCE.clone()
                .subtract(7, 'days')
                .startOf('day');
              const FIRST_DAY_OF_MONTH = REFERENCE.clone()
                .startOf('month')
                .startOf('day');
              const THIRTY_DAYS = REFERENCE.clone()
                .subtract(30, 'days')
                .startOf('day');
              const CUSTOM_DATE = moment(
                moment($scope.ps.dateFilter, 'MM-DD-YYYY').format('YYYY-MM-DD HH:MM:SS')
              ).startOf('day');
              const START_DATE = moment(
                moment($scope.ps.startDateFilter, 'MM-DD-YYYY').format('YYYY-MM-DD HH:MM:SS')
              ).startOf('day');
              const END_DATE = moment(
                moment($scope.ps.endDateFilter, 'MM-DD-YYYY').format('YYYY-MM-DD HH:MM:SS')
              ).startOf('day');

              function isToday(momentDate) {
                return momentDate.isSame(TODAY, 'd');
              }
              function isYesterday(momentDate) {
                return momentDate.isSame(YESTERDAY, 'd');
              }
              function isWithinThreeDays(momentDate) {
                return (
                  momentDate.isSameOrBefore(YESTERDAY, 'd') &&
                  momentDate.isSameOrAfter(THREE_DAYS, 'd')
                );
              }
              function isWithinSevenDays(momentDate) {
                return (
                  momentDate.isSameOrBefore(YESTERDAY, 'd') &&
                  momentDate.isSameOrAfter(SEVEN_DAYS, 'd')
                );
              }
              function isMonthToDate(momentDate) {
                return (
                  momentDate.isSameOrBefore(TODAY, 'd') &&
                  momentDate.isSameOrAfter(FIRST_DAY_OF_MONTH, 'd')
                );
              }

              function isWithinThirtyDays(momentDate) {
                return (
                  momentDate.isSameOrBefore(YESTERDAY, 'd') &&
                  momentDate.isSameOrAfter(THIRTY_DAYS, 'd')
                );
              }
              function isCustomDate(momentDate) {
                return momentDate.isSame(CUSTOM_DATE, 'd');
              }
              function isCustomDateRange(momentDate) {
                return (
                  momentDate.isSameOrBefore(END_DATE, 'd') &&
                  momentDate.isSameOrAfter(START_DATE, 'd')
                );
              }
              //console.log("ref: ", REFERENCE);

              let itemDate = moment(item.oldcreated);
              if ($scope.ps.timeFrameFilter === 'Today') {
                return isToday(itemDate);
              } else if ($scope.ps.timeFrameFilter === 'Yesterday') {
                return isYesterday(itemDate);
              } else if ($scope.ps.timeFrameFilter === 'Last 3 Days') {
                return isWithinThreeDays(itemDate);
              } else if ($scope.ps.timeFrameFilter === 'Last 7 Days') {
                return isWithinSevenDays(itemDate);
              } else if ($scope.ps.timeFrameFilter === 'Month-to-Date') {
                return isMonthToDate(itemDate);
              } else if ($scope.ps.timeFrameFilter === 'Last 30 Days') {
                return isWithinThirtyDays(itemDate);
              } else if ($scope.ps.timeFrameFilter === 'Custom Date') {
                return isCustomDate(itemDate);
              } else if ($scope.ps.timeFrameFilter === 'Custom Range') {
                return isCustomDateRange(itemDate);
              } else {
                console.log('invalid date filter');
              }
              return false;
            });

            if (content.pnl.length > 0) return true;
            return false;
          });

          if (a.contents.length > 0) return true;
          return false;
        });

      console.log('$scope.accounts :', $scope.accounts);
      //Set Table total values
      setAccountTotals($scope.accounts);
      setFooterTotals($scope.accounts);
      $scope.sort($scope.ps.lastSort, $scope.ps.lastOrder);
    }

    function setAccountTotals(accounts) {
      accounts.forEach(a => {
        let totals = {
          revenue: 0.0,
          expenses: 0.0,
          cpc: 0,
          epc: 0,
          epv: 0,
          ctr: 0,
          ctrcount: 0,
          breakeven: 0,
          clicks: 0,
          visits: 0,
          count: 0
        };

        a.contents.forEach(c => {
          c.pnl.forEach(p => {
            //console.log(" p : ", p);
            totals.revenue += typeof p.revenue === 'number' ? p.revenue : 0;
            totals.expenses += typeof p.expenses === 'number' ? p.expenses : 0;
            totals.epc += typeof p.cpm === 'number' ? p.epc : 0;
            totals.epv += typeof p.epv === 'number' ? p.epv : 0;
            totals.ctr += typeof p.lpctr === 'number' ? p.lpctr : 0;
            totals.ctrcount += typeof p.lpctr === 'number' ? 1 : 0;
            totals.clicks += typeof p.clicks === 'number' ? p.clicks : 0;
            totals.visits += typeof p.visits === 'number' ? p.visits : 0;
            totals.count += 1;
          });
        });

        a.revenue = totals.revenue;
        a.expenses = totals.expenses;
        a.profit = totals.revenue - totals.expenses;

        if (a.profit === 0) {
          a.roi = 0;
        } else if (totals.expenses === 0) {
          a.roi = ((totals.revenue - 0) / 1) * 100;
        } else {
          a.roi = ((totals.revenue - totals.expenses) / totals.expenses) * 100;
        }

        a.cpc = totals.visits === 0 || totals.expenses === 0 ? 0 : totals.expenses / totals.visits;
        a.ctr = totals.ctr / totals.ctrcount;
        a.epc = totals.epc;
        a.epv = totals.epv;
        a.breakeven = a.cpc === 0 || a.ctr === 0 ? 0 : a.cpc / (a.ctr / 100);
        a.clicks = totals.clicks;
        a.visits = totals.visits;
        a.count = totals.count;
      });
      // console.log("Accounts with totals: ", accounts);
    }

    function setFooterTotals(accounts) {
      $scope.totals = {};
      let totals = {
        revenue: 0.0,
        expenses: 0.0,
        profit: 0.0,
        roi: 0,
        cpc: 0,
        epc: 0,
        epv: 0,
        breakeven: 0,
        ctr: 0,
        clicks: 0,
        visits: 0,
        count: 0
      };
      accounts.forEach(a => {
        // console.log("a => ", a);
        totals.revenue += typeof a.revenue === 'number' ? a.revenue : 0;
        totals.expenses += typeof a.expenses === 'number' ? a.expenses : 0;
        totals.cpc += typeof a.cpc === 'number' ? a.cpc : 0;
        totals.epc += typeof a.epc === 'number' ? a.epc : 0;
        totals.epv += typeof a.epv === 'number' ? a.epv : 0;
        totals.ctr += typeof a.ctr === 'number' ? a.ctr : 0;
        totals.clicks += typeof a.clicks === 'number' ? a.clicks : 0;
        totals.visits += typeof a.visits === 'number' ? a.visits : 0;
        totals.count += 1;
      });

      if (accounts.length && accounts.length > 0) {
        totals.revenue = totals.revenue;
        totals.expenses = totals.expenses;
        totals.profit = totals.revenue - totals.expenses;
        totals.roi = ((totals.revenue - totals.expenses) / totals.expenses) * 100;
        totals.cpc =
          totals.visits === 0 || totals.expenses === 0 ? 0 : totals.expenses / totals.visits;
        totals.epc = totals.epc / totals.count;
        totals.epv = totals.epv / totals.count;
        totals.breakeven =
          totals.cpc === 0 || totals.ctr === 0 ? 0 : totals.cpc / (totals.ctr / 100);
        totals.ctr = totals.ctr / totals.count;
        totals.epc = totals.cpc / totals.ctr;
      }
      //console.log("Footer Totals:", totals);
      $scope.totals = totals;
    }

    function setTodaysValuePercentages(item) {
      function isToday(momentDate) {
        const TODAY = moment().startOf('day');
        return momentDate.isSame(TODAY, 'd');
      }
      // console.log('isToday: ', isToday(moment(item.pnl[0].oldcreated)));
      //console.log("pnl item: ");console.log(item.pnl[0]);

      if (item.pnl[0] && isToday(moment(item.pnl[0].oldcreated))) {
        let hour = moment().format('HH');
        let minute = moment().format('mm');
        // console.log(`hour: ${hour} | minute: ${minute}`);
        let percentDay = (parseInt(hour) + parseInt(minute) / 60) / 24;
        // CALCULTE CPC BASED ON PERCENTAGE OF DAY
        if (
          !item.pnl[0].visits ||
          item.pnl[0].visits === 0 ||
          !item.pnl[0].expenses ||
          item.pnl[0].expenses === 0
        ) {
          item.pnl[0].cpc = 0;
        } else {
          let estCpc = (item.pnl[0].expenses * percentDay) / item.pnl[0].visits;
          item.pnl[0].cpc = estCpc;
          item.pnl[0].breakeven =
            typeof item.pnl[0].lpctr !== 'number' || item.pnl[0].lpctr === 0
              ? 0
              : estCpc / (item.pnl[0].lpctr * 100);
        }
        // CALCULATE EXPENSE BASED ON PERCENTAGE OF DAY
        let estExp = item.pnl[0].expenses * percentDay;
        //console.log(`percDay: ${percentDay} | estExp: ${estExp}`);
        item.pnl[0].budget = estExp;
      }
    }

    $scope.toggleDetails = function($event, selector) {
      // toggle icon
      //console.log("Click Event:", $event.target, ' | selector: ', selector );
      $event.target.classList.toggle('fa-plus');
      $event.target.classList.toggle('fa-minus');

      // toggle element view
      let elementsToToggle = document.getElementsByClassName(selector);
      Array.prototype.forEach.call(elementsToToggle, el => {
        el.classList.toggle('in');
      });
    };

    $scope.saveAllChanged = async function() {
      //create array of each ads pnl item arrays
      let pnlArrays = [];
      // console.log("$scope.accounts in saveAllChanged => ", $scope.accounts);

      await $scope.accounts.forEach(a => {
        // console.log("a => ", a);
        if (a.fieldChanged) {
          pnlArrays.push(
            a.contents.map(c => {
              if (c.fieldChanged) return c.pnl.filter(p => p.fieldChanged);
            })
          );
        }
      });
      //flatten that array of arrays of arrays
      let pnlItems = [].concat.apply([], [].concat.apply([], pnlArrays));
      // console.log("pnlItems in saveAllChanged => ", pnlItems);

      await pnlService.httpPutPnlItems(pnlItems, async (res, err) => {
        if (err) {
          console.log('error saving');
          swal('Error', 'There was an error saving pnl items:\n' + err, 'error');
        } else {
          // console.log("save success: ", res);
          let changedAccounts = $scope.accounts
            .filter(a => a.fieldChanged)
            .map(a => {
              if (a.account_key) return a.account_key;
            });
          if (changedAccounts.length > 0) {
            await accountService.httpPutUpdateAccountSpend(changedAccounts, (res, err) => {
              if (err) {
                console.log('error saving');
                swal('Error', 'There was an error updating total spend:\n' + err, 'error');
              } else {
                swal('Success', 'Pnl Items Successfully Saved', 'success');
              }
            });
          } else {
            swal('Success', 'Pnl Items Successfully Saved', 'success');
          }
        }
      });
    };

    $scope.exportCSV = function() {
      let dateText;

      switch ($scope.ps.timeFrameFilter) {
        case 'Today':
          dateText = moment().format('MM-DD-YYYY');
          break;
        case 'Yesterday':
          dateText =
            moment().format('MM-DD-YYYY') +
            'to' +
            moment()
              .subtract(1, 'days')
              .format('MM-DD-YYYY');
          break;
        case 'Last 3 Days':
          dateText =
            moment().format('MM-DD-YYYY') +
            'to' +
            moment()
              .subtract(3, 'days')
              .format('MM-DD-YYYY');
          break;
        case 'Last 7 Days':
          dateText =
            moment().format('MM-DD-YYYY') +
            'to' +
            moment()
              .subtract(7, 'days')
              .format('MM-DD-YYYY');
          break;
        case 'Last 30 Days':
          dateText =
            moment().format('MM-DD-YYYY') +
            'to' +
            moment()
              .subtract(30, 'days')
              .format('MM-DD-YYYY');
          break;
        case 'Custom Date':
          dateText = moment().format('MM-DD-YYYY') + 'to' + $scope.ps.dateFilter;
          break;
        case 'Custom Range':
          dateText = $scope.ps.startDateFilter + 'to' + $scope.ps.endDateFilter;
          break;
      }
      let fileTitle = $scope.ps.userFilter.replace(' ', '') + dateText;
      let exportedFilename = fileTitle + '.csv' || 'export.csv';

      let formattedAccts = transformAccounts($scope.accounts);
      let formattedData = addHeaders(formattedAccts);
      let csv = convertToCSV(formattedData);

      let blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
      if (navigator.msSaveBlob) {
        // IE 10+
        navigator.msSaveBlob(blob, exportedFilename);
      } else {
        let link = document.createElement('a');
        if (link.download !== undefined) {
          // feature detection
          // Browsers that support HTML5 download attribute
          let url = URL.createObjectURL(blob);
          link.setAttribute('href', url);
          link.setAttribute('download', exportedFilename);
          link.style.visibility = 'hidden';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        } else {
          alert('File export not supported by your browser.');
        }
      }
    };

    function transformAccounts(accounts) {
      let transformedAccounts = [];
      accounts.forEach(account => {
        transformedAccounts.push({
          Account: account.fname + ' ' + account.lname,
          Vertical: typeof account.vertical === 'undefined' ? 'N/A' : account.vertical,
          Angle: typeof account.angle === 'undefined' ? 'N/A' : account.angle,
          Geo: typeof account.geo === 'undefined' ? 'N/A' : account.geo,
          Revenue: typeof account.revenue === 'undefined' ? 0 : account.revenue,
          Expenses: typeof account.expenses === 'undefined' ? 0 : account.expenses,
          Profit: typeof account.profit === 'undefined' ? 0 : account.profit,
          ROI: typeof account.roi === 'undefined' ? 0 : account.roi,
          CPC: typeof account.cpc === 'undefined' ? 0 : account.cpc,
          EPC: typeof account.epc === 'undefined' ? 0 : account.epc,
          EPV: typeof account.epv === 'undefined' ? 0 : account.epv,
          CTR: typeof account.ctr === 'undefined' ? 0 : account.ctr,
          BEEPC: typeof account.breakeven === 'undefined' ? 0 : account.breakeven,
          Owner: typeof account.owner === 'undefined' ? 'N/A' : account.owner
        });
      });
      return transformedAccounts;
    }

    function addHeaders(accounts) {
      let headers = {
        Account: 'Account',
        Vertical: 'Vertical',
        Angle: 'Angle',
        Geo: 'Geo',
        Revenue: 'Revenue',
        Expenses: 'Expenses',
        Profit: 'Profit',
        ROI: 'ROI',
        CPC: 'CPC',
        EPC: 'EPC',
        EPV: 'EPV',
        CTR: 'CTR',
        BEEPC: 'BE EPC',
        Owner: 'Owner'
      };
      accounts.unshift(headers);
      return accounts;
    }

    function convertToCSV(objArray) {
      let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
      let str = '';
      for (let i = 0; i < array.length; i++) {
        let line = '';
        for (var index in array[i]) {
          if (line != '') line += ',';

          line += array[i][index];
        }

        str += line + '\r\n';
      }
      return str;
    }

    $scope.updateFilter = function() {
      init();

      /*
                console.log("Apply Filters.");
                //console.log("Filter by: ", $scope.userFilter);
                $cookieStore.put('pnl_user_filter', $scope.ps.userFilter);
                //console.log("Filter by: ", $scope.statusFilter);
                $cookieStore.put('pnl_status_filter', $scope.ps.statusFilter);
                //console.log("Filter by: ", $scope.timeFrameFilter);
                $cookieStore.put('pnl_time_filter', $scope.ps.timeFrameFilter);
                setAccountFilters();
                */
    };

    $scope.sort = function(field, order) {
      $scope.lastSort = field;
      $scope.lastOrder = order;
      $cookieStore.put('pnl_last_sort', field);
      $cookieStore.put('pnl_last_order', order);
      //console.log("Sort. Field: ", field, " | order: ", order);
      let propsToSort = [];
      let ts = {
        name: field,
        reverse: order == 'ascending' ? true : false
      };
      propsToSort.push(ts);
      $scope.accounts.sort(Tools.generateSortFunction(propsToSort));
    };

    $scope.setRowClass = function(status) {
      if (status.toLowerCase().indexOf('dead') >= 0 || status === 'Hidden') {
        return 'dead_acct';
      } else if (status === 'Problem' || status === 'Photo Lockout' || status === 'Phone Lockout') {
        return 'action_acct';
      } else if (status === 'Spending, In PL') {
        return 'good_acct';
      } else if (
        status === 'Reupload' ||
        status === 'Reupload Complete' ||
        status === 'Finished Upload' ||
        status === 'Resolved' ||
        status === 'Optimize'
      ) {
        return 'warn_acct';
      } else if (status === 'Needs Upload' || status === 'Needs Bump') {
        return 'needs_acct';
      } else if (status === 'Recovered') {
        return 'rec_acct';
      }
    };

    //TODO: Fix this, these values should be coming from the directive //Repeated in edit account
    $scope.dateChange = function(field, date) {
      $scope[field] = date;
      // console.log(
      //   "date: ",
      //   date,
      //   " | typeof: ",
      //   typeof date,
      //   " | field: ",
      //   field
      // );
    };

    $scope.fieldChanged = function($index, $parent, $parentparent) {
      //console.log(`field changed: pp : ${$parentparent} | p : ${$parent}, | index ${$index}`);
      $scope.accounts[$parentparent].fieldChanged = true;
      $scope.accounts[$parentparent].contents[$parent].fieldChanged = true;
      $scope.accounts[$parentparent].contents[$parent].pnl[$index].fieldChanged = true;
    };

    $scope.expenseChanged = function($index, $parent, $parentparent) {
      console.log(`TODO: update expense for the account. the function exists in services.`);
    };

    /* Working Voluum Auth and Campaign GET */
    $http({
      method: 'GET',
      url: '/voluum/auth'
    }).then(
      function(data) {
        // console.log("Voluum Auth Success:");
        $cookieStore.put('voluum_token', data.data.token);
        $cookieStore.put('voluum_exp', data.data.expirationTimestamp);
      },
      function(data) {
        console.log('Voluum Auth Failure: ', data);
      }
    );

    function debounce(fn, delay) {
      var timer = null;
      return function() {
        var self = this,
          args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function() {
          fn.apply(self, args);
        }, delay);
      };
    }
    //Bob says to remember
    const getVoluumReport = function() {
      //console.log("token: ", $cookieStore.get('voluum_token'));
      $scope.voluumLoading = true;
      $http({
        method: 'GET',
        url: '/voluum/report/' + $scope.ps.timeFrameFilter.toString().toLowerCase(),
        headers: {
          'x-token': $cookieStore.get('voluum_token'),
          'x-date': moment().format('YYYY-MM-DD'),
          'x-custom': $scope.dateFilter,
          'x-user': $scope.user
        }
      }).then(
        function(data) {
          // console.log("Voluum Report Success:", data);
          init();
          $scope.voluumLoading = false;
        },
        function(data) {
          console.log('Voluum Report Failure: ', data);
          $scope.voluumLoading = false;
        }
      );
    };

    $scope.getVoluumReport = debounce(getVoluumReport, 1000);
  }
]);
