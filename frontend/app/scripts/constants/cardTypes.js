﻿"use strict";

let CardTypes = [
  "Amex Gold Luke",
  "Amex Delta Luke",
  "Amex Platinum Luke",
  "Amex Gold Stan",
  "Amex Platinum Stan",
  "Amex Plum Stan",
  "Amex Delta Stan",
  "Amex SPG Stan",
  "Amex Corporate Card Stan",
  "Amex Gold WGB",
  "Amex Platinum  WGB",
  "Amex SPG  WG Utopian",
  "BOA Travel Card",
  "BOA Charge Card",
  "BOA Corporate Card",
  "Chase Ink",
  "CapitalOne Spark",
  "D's Card",
  "Regions",
  "SPG Luke"
];
