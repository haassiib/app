﻿'use strict';


let ConversionEvents = ['Purchase',
                        'Clicks to Site',
                        'View Content',
                        'Initiate Checkout',
                        'Add Payment Info',
                        'Lead',
                        'Complete Registration',
                        'Search',
                        'Add to Cart',
                        'Add to Wishlist']