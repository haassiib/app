﻿'use strict'

let Ages = [];

let MIN_AGE = 13
let MAX_AGE = 64;

for (let i = MIN_AGE; i <= MAX_AGE; i++) {
    Ages.push(i.toString());
}

Ages.push("65+");