﻿"use strict";

let Geos = [
    "All Geos",
    "United States of America",
    "Canada",
    // "Chile",
    "United Kingdom",
    "Australia",
    "France",
    "Germany",
    "New Zealand",
    "South Africa",
    "Ireland",
    "Switzerland",
    "Spain",
    "Belgium",
    "India",
    "Argentina",
    "Italy",
    "Norway",
    "Austria",
    "Brazil",
    "Denmark",
    "Singapore",
    "Albania"
];

// Leave this until we find out if they prefer 3-letter code or full name
// let Geos = [
//   "All Geos",
//   "USA",
//   "CAN",
//   "GBR",
//   "AUS",
//   "FRA",
//   "DEU",
//   "NZL",
//   "ZAF",
//   "IRL",
//   "CHE",
//   "ESP",
//   "BEL",
//   "IND",
//   "ARG",
//   "ITA",
//   "NOR",
//   "AUT",
//   "BRA",
//   "DNK",
//   "SGP",
//   "ALB"
// ];