﻿

(function () {
    var indexOf = [].indexOf || function (item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

    angular.module('countrySelect', [])
        .directive('countrySelect', ['countryService', function (countryService) {
        return {
            restrict: 'AE',
            replace: true,
            scope: {
                priorities: '@csPriorities',
                only: '@csOnly',
                except: '@csExcept'
            },
            template: `<select class="form-control" ng-options="country.name as country.name for country in countries" ng-change="selectCountryChange(selected);"> <option value="" ng-if="isSelectionOptional"></option> </select>`,
            controller: [
                '$scope', '$attrs', function ($scope, $attrs) {
                    var countryCodesIn, findCountriesIn, includeOnlyRequestedCountries, removeCountry, removeExcludedCountries, separator, updateWithPriorityCountries;
                    separator = {
                        code: '-',
                        name: '────────────────────',
                        disabled: true
                    };
                    countryCodesIn = function (codesString) {
                        var codes;
                        codes = codesString ? codesString.split(',') : [];
                        return codes.map(function (code) {
                            return code.trim();
                        });
                    };
                    findCountriesIn = (function (_this) {
                        return function (codesString) {
                            var country, countryCodes, i, len, ref, ref1, results;
                            countryCodes = countryCodesIn(codesString);
                            ref = _this.countries;
                            results = [];
                            for (i = 0, len = ref.length; i < len; i++) {
                                country = ref[i];
                                if (ref1 = country.code, indexOf.call(countryCodes, ref1) >= 0) {
                                    results.push(country);
                                }
                            }
                            return results;
                        };
                    })(this);
                    removeCountry = (function (_this) {
                        return function (country) {
                            return _this.countries.splice(_this.countries.indexOf(country), 1);
                        };
                    })(this);
                    includeOnlyRequestedCountries = (function (_this) {
                        return function () {
                            if (!$scope.only) {
                                return;
                            }
                            return _this.countries = findCountriesIn($scope.only);
                        };
                    })(this);
                    removeExcludedCountries = function () {
                        var country, i, len, ref, results;
                        if (!$scope.except) {
                            return;
                        }
                        ref = findCountriesIn($scope.except);
                        results = [];
                        for (i = 0, len = ref.length; i < len; i++) {
                            country = ref[i];
                            results.push(removeCountry(country));
                        }
                        return results;
                    };
                    updateWithPriorityCountries = (function (_this) {
                        return function () {
                            var i, len, priorityCountries, priorityCountry, ref, results;
                            priorityCountries = findCountriesIn($scope.priorities);
                            if (priorityCountries.length === 0) {
                                return;
                            }
                            _this.countries.unshift(separator);
                            ref = priorityCountries.reverse();
                            results = [];
                            for (i = 0, len = ref.length; i < len; i++) {
                                priorityCountry = ref[i];
                                removeCountry(priorityCountry);
                                results.push(_this.countries.unshift(priorityCountry));
                            }
                            return results;
                        };
                    })(this);
                    countryService.httpGetCountries((res, err) => {
                        if (err) {
                            console.log(err);
                            swal("Error", "Unable to get Countries: " + err, "warning");
                        }
                        else {
                            var allCountries = res;
                            //console.log("allCountries: ", allCountries);
                            this.countries = allCountries.slice();
                            includeOnlyRequestedCountries();
                            removeExcludedCountries();
                            updateWithPriorityCountries();
                            $scope.countries = this.countries;
                            //console.log("scope.countries: ", $scope.countries);
                            $scope.selected = $scope.countries[0];
                        }
                    });
                    return $scope.isSelectionOptional = $attrs.csRequired === void 0;
                }
            ]
        };
    }]);
}).call(this);