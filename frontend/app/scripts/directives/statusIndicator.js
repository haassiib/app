﻿'user strict'

//Creates a Colored Circle to Indicate Status By Color
angular.module('menciusapp')
    .directive('statusIndicator', function () {
        return {
            restrict: 'E',
            scope: {
                status: '='
            },
            replace: true,
            template: '<button class="btn btn-status" ng-model="status" ng-class="setStatusClass();" ></button>',
            link: function ($scope, elem, attr, ctrl) {
                
                $scope.setStatusClass = function () {
                    let status = $scope.status;
                    if (status.toLowerCase().indexOf('dead') >= 0 || status === 'Hidden') {
                        return 'dead_acct';
                    }
                    else if (status === 'Problem' || status === 'Photo Lockout' || status === 'Phone Lockout') {
                        return 'action_acct';
                    }
                    else if (status === 'Spending, In PL') {
                        return 'good_acct';
                    }
                    else if (status === 'Reupload' || status === 'Reupload Complete' || status === 'Finished Upload' || status === 'Resolved' || status === 'Optimize') {
                        return 'warn_acct';
                    }
                    else if (status === 'Needs Upload' || status === 'Needs Bump') {
                        return 'needs_acct';
                    }
                    else if (status === 'Recovered') {
                        return 'rec_acct';
                    }
                };               
            }
        }
    });