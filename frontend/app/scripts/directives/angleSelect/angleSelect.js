﻿

(function () {
    var indexOf = [].indexOf || function (item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

    angular.module('angleSelect', []).directive('angleSelect', ['accountService', function (accountService) {
        return {
            restrict: 'E',
            replace: false,
            require: 'ngModel',
            templateUrl: 'scripts/directives/angleSelect/angleSelect.html',
            controller: [
                '$scope', '$attrs', function ($scope, $attrs) {
                    var separator;
                    separator = {
                        code: '-',
                        name: '────────────────────',
                        disabled: true
                    };
                    $scope.angledropdown = false;

                    let limit = (typeof $scope.uploadset !== 'undefined') ? 15 : undefined;
                    accountService.httpGetAngles(limit,(res, err) => {
                        if (err) {
                            console.log(err);
                            swal("Error", "Unable to get Angles: " + err, "warning");
                        }
                        else {
                            //Get angle array and filter out bad data
                            $scope.angles = res.filter(angle => angle.angle != undefined);                            
                            //console.log("Filtered angles: ", $scope.angles);
                            if ($scope.uploadset && $scope.uploadset.account) {
                                $scope.angleKey = $scope.uploadset.account.angle || '';
                            }
                            else if ($scope.account) {
                                $scope.angleKey = $scope.account.angle || '';
                            }

                        }
                    });

                    $scope.angleToggle = function (bool) {
                        $scope.angledropdown = bool;
                        //var list = document.getElementById("anglelist");
                        //console.log("Val: ", bool, " | list: ", list);
                    }

                    $scope.angleClick = function (angle, index) {
                        $scope.angledropdown = true;
                        document.getElementById("anglelist").focus();

                        $scope.angleKey = angle;
                        if ($scope.uploadset) {
                            $scope.uploadset.account == undefined ? $scope.uploadset.account = { angle: angle } : $scope.uploadset.account.angle = angle;
                        }
                        else if ($scope.account) {
                            $scope.account.angle = angle;
                        }
                        else {
                            console.log("Error adding angle, no parent object found.");
                        }
                    };

                    $scope.keyPress = function (angleKey) {
                        let angles = $scope.angles.filter(angle => angle.angle.indexOf(angleKey) !== -1);

                        if (angles.length == 0) {
                            //no angle found | new angle
                            if ($scope.uploadset){
                                $scope.uploadset.account == undefined ? $scope.uploadset.account = { angle: angleKey } : $scope.uploadset.account.angle = angleKey;
                            }
                            else if ($scope.account) {
                                $scope.account.angle = angleKey;
                            }
                            else {
                                console.log("Error adding angle, no parent object found.");
                            }

                        } else if (angles.length == 1) {
                            //angle found
                            if ($scope.uploadset) {
                                $scope.uploadset.account == undefined ? $scope.uploadset.account = { angle: angles[0].angle } : $scope.uploadset.account.angle = angles[0].angle;
                            }
                            else if ($scope.account) {
                                $scope.account.angle = angles[0].angle;
                            }
                            else {
                                console.log("Error adding angle, no parent object found.");
                            }
                        }
                        else if (angles.length > 1) {
                            //multiple found, use current key
                            if ($scope.uploadset) {
                                $scope.uploadset.account == undefined ? $scope.uploadset.account = { angle: angleKey } : $scope.uploadset.account.angle = angleKey;
                            }
                            else if ($scope.account) {
                                $scope.account.angle = angleKey;
                            }
                            else {
                                console.log("Error adding angle, no parent object found.");
                            }
                        }
                    }
                }
            ]
        };
    }]);
}).call(this);