﻿'use strict'

angular.module('menciusapp')
    .directive('urlddl', [function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: 'scripts/directives/urlddl/urlddl.html'
        }
    }]
);

angular.module('menciusapp')
    .directive('urlddlmulti', [function () {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: 'scripts/directives/urlddl/urlddlmulti.html'
        }
    }]
); 