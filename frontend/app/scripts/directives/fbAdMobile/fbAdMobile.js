﻿'use strict';

angular.module('menciusapp')
    .directive('fbAdMobile', [ '$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            replace: true,
            require: 'ngModel',
            templateUrl: 'scripts/directives/fbAdMobile/fbAdMobile.html',
            link: function ($scope, $attrs, ngModel) {
                var user = $rootScope.user.username;
                //Set Default Values
                let content = {
                    user: user,
                    content_key: Math.floor(Math.random() * 1000), 
                    //headline: 'You Will NOT Believe This! Look What She Did In Just Four Days!',
                    //body: 'Facebook Ads Made Easy with Mencius!',
                    //newsfeed_link: 'Easier than your OTHER FB Ad Tool!',
                    //graphic_link: 'http://boostlikes.com/blog/wp-content/uploads/2015/12/Facebook-Grid.jpg',
                    url: 'https://www.testurl.com/check-this-out'
                }
                //Replace Default with Ad Values
                Object.assign(content, $scope.ad);
                Object.assign($scope.ad, content);
                
                function createMedia(media_url) {

                    let source = '';
                    let media;
                    let id = Tools.getParameterByName('id', media_url);
                    let url = media_url.split('/');
                    let s_url = media_url.split(' ');
                    let src;
                    if (s_url[1]) {
                        src = s_url[1].split('"')[1].replace('embed', 'download');
                    }
                    //console.log("ID: ", id, " | url array: ", url, " | src: ", src);

                    if (id !== null && id !== "") {
                        // url id exists
                        //TODO: this should do more advanced checking for video url
                        source = 'https://drive.google.com/uc?export=download&id=' + id;
                        media = document.createElement('video');
                        media.src = source;
                        media.style.height = "100%";
                        media.style.width = "100%";
                        media.setAttribute("controls", "controls"); 
                        media.setAttribute("preload", "none");
                    }
                    else if (url.length > 0 && url[4] === "d") {
                            
                        source = 'https://drive.google.com/uc?export=download&id=' + url[5];
                        media = document.createElement('video');
                        media.src = source;
                        media.style.height = "100%";
                        media.style.width = "100%";
                        media.setAttribute("controls", "controls");
                        media.setAttribute("preload", "none"); 
                    }
                    else if (url.length > 0 && url[2] === "1drv.ms") {

                        source = media_url;
                        media = document.createElement('video');
                        media.src = source;
                        media.style.height = "100%";
                        media.style.width = "100%";
                        media.setAttribute("controls", "controls");
                        media.setAttribute("preload", "none"); 
                    }
                    else if (src) {

                        media = document.createElement('video');
                        media.src = src;
                        media.style.height = "100%";
                        media.style.width = "100%";
                        media.setAttribute("controls", "controls");
                        media.setAttribute("preload", "none");
                    }
                    else {
                        media = new Image();
                        media.onerror = function () {
                            //image didn't load
                            console.log("error retrieving image");
                            media.src = defaultSrc;
                        }
                        media.style.height = "100%";
                        media.style.width = "100%";
                        media.src = media_url;
                    }
                       
                    let divid = 'fb-mobile-media-';
                    if ($scope.ad.content_key)
                        divid += $scope.ad.content_key;

                    let mediaDiv = document.getElementById(divid);
                    if(mediaDiv)
                        mediaDiv.appendChild(media);
                };

                let defaultSrc = 'https://boostlikes.com/blog/wp-content/uploads/2015/12/Facebook-Grid.jpg'; //image 
                //let defaultSrc = 'https://drive.google.com/file/d/0B4eNiJAmbJNtUlpRa0dkcmJOemc/view?usp=sharing'; //google video
                //let defaultSrc = '<iframe src="https://onedrive.live.com/embed?cid=0DF0463504CFB5A5&resid=DF0463504CFB5A5%21379&authkey=ACON0xImsR8IqWE" width="320" height="180" frameborder="0" scrolling="no" allowfullscreen></iframe>' //onedrive video
                let media_url = defaultSrc;
                let valid = Tools.isValidUrl($scope.ad.link_to_graphic);
                setTimeout(function () {
                    if (valid)
                        media_url = $scope.ad.link_to_graphic;
                    createMedia(media_url);
                }, 50);
            }
        };
    }]);

/*

*/