﻿'use strict';

angular.module('menciusapp')
    .directive('currency', ['$filter', function ($filter) {
    return {
        require: 'ngModel',
        link: function (elem, $scope, attrs, ngModel) {
            //console.log("elem: ", elem);
            //console.log("attrs: ", attrs);
            ngModel.$formatters.push(function (val) {
                return $filter('currency')(val)
            });
            ngModel.$parsers.push(function (val) {
                return val.replace(/[\$,]/, '')
            });
        }
    }
}])