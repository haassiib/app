﻿'use strict'

angular.module('menciusapp')
    .directive('switch', function () {
        return {
            restrict: "E",
            replace: true,
            require: 'ngModel',
            templateUrl: 'scripts/directives/switch/switch.html',
            link: function ($scope, elem, attr, ctrl) {
                //TODO: 
                console.log("inside switch");
                console.log("scope:", $scope);
                $scope.label = attr.label
            }
        }
    });