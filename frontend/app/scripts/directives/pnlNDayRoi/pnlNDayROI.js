﻿'use strict';

angular.module('menciusapp')
    .directive('pnlRoi', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            replace: true,
            require: 'ngModel',
            templateUrl: 'scripts/directives/pnlNDayROI.html',
            link: function ($scope, $attrs, ngModel) {
                console.log("Inside PNL N-Day ROI");
            }
        };
    }]);