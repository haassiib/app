﻿"use strict";

angular.module("menciusapp").directive("jqdatepicker", function() {
  return {
    restrict: "A",
    require: "ngModel",
    link: function(scope, element, attrs, ctrl) {
      $(element).datepicker({
        dateFormat: "mm-dd-yy",
        maxDate: new Date(),
        onSelect: function(date) {
          ctrl.$setViewValue(date);
          ctrl.$render();
          scope.$apply();
        }
      });
    }
  };
});
