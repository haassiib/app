﻿'user strict'

angular.module('menciusapp')
    .directive('columnMultiSelect', function ($cookieStore) {
    return {
        restrict: 'E',
        scope: {
            model: '=',
            options: '=',
        },
        templateUrl: 'scripts/directives/columnMultiSelect/columnMultiSelect.html',
        controller: function ($scope) {
            console.log("inside multi directive");
            $scope.toggleDropdown = function () {
                console.log("here");
                $scope.open = !$scope.open;
            };

            $scope.selectAll = function () {
                $scope.model = [];
                angular.forEach($scope.options, function (item, index) {
                    $scope.model.push(item);
                });
            };

            $scope.deselectAll = function () {
                $scope.model = [];
            };

            $scope.toggleSelectItem = function (option) {
                var intIndex = -1;
                angular.forEach($scope.model, function (item, index) {
                    if (item == option) {
                        intIndex = index;
                    }
                });

                if (intIndex >= 0) {
                    $scope.model.splice(intIndex, 1);
                    $cookieStore.put('search_columns', $scope.model);
                } else {
                    $scope.model.push(option);
                    $cookieStore.put('search_columns', $scope.model);
                }
            };

            //TODO: fix this later
            //NOT working 100% of the time
            $scope.checkedItem = function (option) {
                return $scope.model.includes(option);
            };

        }
    }
});