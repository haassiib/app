﻿'use strict';

angular.module('menciusapp')
    .filter('percent', function () {
        return function (val) {
            let percentage = val;
            if (typeof percentage !== 'object' && typeof percentage !== 'undefined') {

                percentage = parseInt(percentage);
                if (isNaN(percentage) || percentage === -1)
                    return 0;
                percentage = percentage.toFixed(0) + '%';

            }
            return percentage;
        };
    });

angular.module('menciusapp')
    .directive('percent', ['$filter', function ($filter) {
        return {
            require: 'ngModel',
            link: function (elem, $scope, attrs, ngModel) {
                ngModel.$formatters.push(function (val) {
                    return $filter('percent')(val)
                });
                ngModel.$parsers.push(function (val) {
                    return val.replace(/[\%,]/, '')
                });
            }
        }
    }]);