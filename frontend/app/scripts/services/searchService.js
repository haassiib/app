'use strict';

angular.module('menciusapp').factory('searchService', [
  '$http',
  '$rootScope',
  function searchText($http, $rootScope) {
    return {
      httpSearchAccountsBySearchTerm: function(callback) {
        var getSearchConfig = {
          method: 'GET',
          url: $rootScope.urlRoot + "/search",
          headers: {
            'Content-Type': 'application/json',
            'x-username': $rootScope.user.username,
            'x-role': $rootScope.role
          }
        };

        console.log(getSearchConfig);

        $http(getSearchConfig).then(function(data) {
          console.log('searchService retrieved search results from db: ', data.data);
          return callback(data.data);
        });
      }
    };
  }
]);
