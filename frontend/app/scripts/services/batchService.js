﻿"use strict";

angular.module("menciusapp").factory("batchService", [
  "$http",
  "$rootScope",
  "$cookieStore",
  "$location",
  function statusService($http, $rootScope, $cookieStore, $location) {
    return {
      /* Get All Batch *** Ready */ 
      httpGetBatches: function(callback) {
        var getBatchesConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/batches",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getBatchesConfig).then(
          function(data) {
            console.log("Retrieved batches from db");
            return callback(data.data.map(batch => batch));
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      /* Insert Batch *** Ready */
      httpPostInsertBatch: function(
        batchName,
        BatchDescription,
        safeSpend,
        proxy,
        warmTime,
        safeAds,
        preWarmingActivities,
        preLaunchActivities,
        callback
      ) {
        var postInsertBatchesConfig = {
          method: "POST",
          url: $rootScope.urlRoot + "/batches",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            newBatch: {
              name: batchName,
              description: BatchDescription,
              safe_spend: safeSpend,
              proxy: proxy,
              warm_time: warmTime,
              safe_ads: safeAds,
              pre_warming_activities: preWarmingActivities,
              pre_launch_activities: preLaunchActivities
            }
          }
        };
        $http(postInsertBatchesConfig).then(
          function(data) {
            console.log("Data :", data);
            console.log("successful post");
            return callback(data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      /* Bulk Update Batch *|* Ready but not using*/
      httpPutUpdateBatches: function(batches, callback) {
        var updateBatchesConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/batches",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            batches: batches
          }
        };
        $http(updateBatchesConfig).then(
          function(data) {
            console.log("Updated batches in db");
            console.log(data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },

      /* Update Single Batch *** Ongoing*/
      httpPutUpdateBatch: function(batch_key, callback) {
        var updateBatchesConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/batches",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            batch: batch_key
          }
        };
        $http(updateBatchesConfig).then(
          function(data) {
            console.log("Updated batches in db");
            console.log(data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },

      /* Delete single Batch *** ready*/
      httpDeleteBatch: function(batch_key, callback) {
        var deleteBatchConfig = {
          method: "DELETE",
          url: $rootScope.urlRoot + "/batches/" + batch_key,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            batch_key: batch_key
          }
        };

        $http(deleteBatchConfig).then(function(data) {
          console.log("data:", data);
          console.log("args:", arguments);

          if (data.status >= 400) {
            console.log("could not delete batch, something went wrong");
            console.log(data.status);
            return callback(data, data.status);
          }
          return callback(data);
        });
        },
        /* Delete Batches *** ready*/
        httpDeleteBatches: function (selectedBatches, callback) {
            var deleteBatchesConfig = {
                method: "DELETE",
                url: $rootScope.urlRoot + "/batches",
                headers: {
                    "Content-Type": "application/json",
                    "x-username": $rootScope.user.username,
                    "x-role": $rootScope.user.role
                },
                data: {
                    selectedBatches: selectedBatches
                }
            };
            $http(deleteBatchesConfig).then(function (data) {
                console.log("data:", data);
                console.log("args:", arguments);

                if (data.status >= 400) {
                    console.log("could not delete batch, something went wrong");
                    console.log(data.status);
                    return callback(data, data.status);
                }
                return callback(data);
            });
        }
    };
  }
]);
