'use strict';

angular.module('menciusapp').factory('analyticsService', [
  '$http',
  '$rootScope',
  '$cookieStore',
  '$location',
  '$routeParams',
  function analyticsService($http, $rootScope, $cookieStore, $location, $routeParams) {
    return {
      httpGetAnalyticsDataByDateRangeForAccountLife: function(dtFrom, dtTo, callback) {
        // console.log('dtFrom: ', dtFrom, ' - dtTo: ', dtTo);
        var getAnalyticsDataByDateRangeForAccountLifeConfig = {
          method: 'GET',
          url: `${$rootScope.urlRoot}/analytics/account_life/${dtFrom}/${dtTo}`,
          headers: {
            'Content-Type': 'application/json',
            'x-username': $rootScope.user.username,
            'x-role': $rootScope.user.role
          }
        };

        $http(getAnalyticsDataByDateRangeForAccountLifeConfig).then(function(data) {
          console.log(
            'Retrieved all analtics data from db in httpGetAnalyticsDataByDateRangeForAccountLife'
          );
          return callback(data.data);
        });
      },
      httpGetAnalyticsDataByDateRangeForLaunchRates: function(dtFrom, dtTo, callback) {
        var getAnalyticsDataByDateRangeForLaunchRatesConfig = {
          method: 'GET',
          url: `${$rootScope.urlRoot}/analytics/launch_rates/${dtFrom}/${dtTo}`,
          headers: {
            'Content-Type': 'application/json',
            'x-username': $rootScope.user.username,
            'x-role': $rootScope.user.role
          }
        };

        $http(getAnalyticsDataByDateRangeForLaunchRatesConfig).then(function(data) {
          console.log(
            'Retrieved all analtics data from db in httpGetAnalyticsDataByDateRangeForLaunchRates'
          );
          return callback(data.data);
        });
      }
    };
  }
]);

/*
httpGetCopyByAccount: function(callback) {
        let getCopyByAccountConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/edit_copy/" + $routeParams.account_key,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };
        $http(getCopyByAccountConfig).then(
          function(data) {
            // console.log("received account ads from db");
            //console.log("Account Adsets/Content:", data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
*/
