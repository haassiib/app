﻿"use strict";

angular.module("menciusapp").factory("adsetService", [
  "$http",
  "$rootScope",
  "$routeParams",
  "$cookieStore",
  "$location",
  "alertService",
  function adsetService(
    $http,
    $rootScope,
    $routeParams,
    $cookieStore,
    $location,
    alertService
  ) {
    return {
      httpGetCopyByAccount: function(callback) {
        let getCopyByAccountConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/edit_copy/" + $routeParams.account_key,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };
        $http(getCopyByAccountConfig).then(
          function(data) {
            // console.log("received account ads from db");
            //console.log("Account Adsets/Content:", data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPutAdset: function(account_key, campaign, callback) {
        console.log(campaign);
        if (campaign.length < 0) {
          console.log("No campaign length");
        }
        var updateAdsetDetailsConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/account/copy/" + account_key,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            copy: campaign
          }
        };

        $http(updateAdsetDetailsConfig).then(
          function(data) {
            if (data.status > 204) {
              console.log("Error adding Copy:\n", data);
              //console.log("Account Adsets/Content:", data);
              return callback(data, data.status);
            } else {
              console.log("received account ads from db");
              //console.log("Account Adsets/Content:", data);
              return callback(data.data);
            }
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPostAdset: function(campaign, callback) {
        var insertAdsetsConfig = {
          method: "POST",
          url: $rootScope.urlRoot + "/account/copy/",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            copy: campaign
          }
        };

        $http(insertAdsetsConfig).then(
          function(data) {
            console.log("Account Copy Inserted!", data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpDeleteAdset: function(adset, callback) {
        var deleteContentConfig = {
          method: "DELETE",
          url: $rootScope.urlRoot + "/account/copy/" + adset.adset_key,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            adset: adset
          }
        };

        $http(deleteContentConfig).then(
          function(data) {
            console.log("data:", data);

            return callback(data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpGetAdsetContent: function(adset_key, callback) {
        let getAdsetContentConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/adset/" + adset_key,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };
        $http(getAdsetContentConfig).then(
          function(data) {
            console.log("received adset and content from db");
            //console.log("Adsets/Content:", data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpGetAdsets: function(callback) {
        let getAdsetsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/adset",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };
        $http(getAdsetsConfig).then(
          function(data) {
            console.log("received adsets from db");
            //console.log("Adsets/Content:", data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      }
    };
  }
]);
