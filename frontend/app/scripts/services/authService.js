"use strict";

angular.module("menciusapp").factory("authService", [
  "$http",
  "$rootScope",
  "$cookieStore",
  "$location",
  "alertService",
  function authService(
    $http,
    $rootScope,
    $cookieStore,
    $location,
    alertService
  ) {
    return {
      authorize: function(accessLevel, role) {
            if (role === undefined)
                role = $rootScope.user.role;
        return accessLevel & role;
      },
      httpGetUsers: function(callback) {
        var getUsersConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/usersAdmin",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getUsersConfig).then(
          function(data) {
            console.log("Retrieved batches from db");
            return callback(data.data.map(users => users));
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      logMeIn: function() {
        alert("working");
      },
      isLoggedIn: function(user) {
        if (user === undefined) user = $rootScope.user;
        return user.role === userRoles.user || user.role === userRoles.admin;
      },
      register: function(user, fName, lName, pass, cb, error) {
        const signUpConfig = {
          method: "POST",
          url: $rootScope.urlRoot + "/signup",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            username: user,
            firstName: fName,
            lastName: lName,
            password: pass
          }
        };

        $http(signUpConfig)
          .then(cb)
          .catch(error);
      },
      login: function(user, success, error) {
        //TODO: fix. security issue. leaves password in rootscope.
        //TODO: Fix this broken error success method when reworking roles based permissions
        $rootScope.user = user;

        var loginConfig = {
          method: "POST",
          url: $rootScope.urlRoot + "/user/login",
          headers: {
            "Content-Type": "application/json",
            "x-username": user.username,
            "x-password": user.password,
            "x-rememberme": user.remember
          }
        };

        $http(loginConfig)
          .then(
            function(data) {
              // console.log("Data in the authService file .then => ", data);
              // set values for user and authtoken in rootscope and http defaults (for future calls)
              var $authToken = data.token;
              $http.defaults.headers.common["x-auth"] = $authToken;

              // use the alert service to notify that a user is logged in
              //alertService.add("login", "Welcome, " + $rootScope.user.username + "!");

              //console.log ('token :' + $rootScope.user.authtoken);
              // console.log("login data:", data.data);
              $rootScope.user = user;
              $rootScope.user.isLogged = true;
              $rootScope.user.role = JSON.stringify(data.data.role);
              $rootScope.user.fname = data.data.fname;
              $rootScope.user.lname = data.data.lname;
              //update cookies
              $cookieStore.put("user", $rootScope.user);
              $cookieStore.put("token", $authToken);

              $rootScope.$broadcast("someoneLoggedIn");

              // Control the first page a user sees when they log in
              // TODO: Improve this - creaate a function with rules (such as showing previous page they were on)
              if (
                Number($rootScope.user.role) ==
                  $rootScope.userRoles.user_media ||
                Number($rootScope.user.role) ==
                  $rootScope.userRoles.user_upload ||
                Number($rootScope.user.role) == $rootScope.userRoles.admin
              ) {
                  $location.path("/active_accounts");
              } else if (
                Number($rootScope.user.role) == $rootScope.userRoles.user_funder
              ) {
                $location.path("/pnl");
              } else if (
                Number($rootScope.user.role) ==
                $rootScope.userRoles.user_accountManager
              ) {
                $location.path("/add_resource");
              } else {
                  $location.path("/active_accounts");
              }
              success(
                "this is the success handler in the authService.js file => ",
                data
              );
            },
            error(() => swal("Bad Login Combo", "Please Try Again", "error"))

            // function(data, status) {
            //   error(
            //     "this is the error handler in the authService.js file => ",
            //     data
            //   );
            //   Errors.httpResponse(data);
            //   swal("Bad Login Combo", "Please Try Again", "error");
            //   //TODO alert service
            //   // alertService.add('login_error', "Sorry, incorrect user name or password combination. If the account has not yet been verified, please check your email.");
            // }
          )
          .catch(err => {
            Errors.httpResponse(err);
          });
      },
      logout: function(success, error) {
        // console.log("Logging out!");

        $cookieStore.remove("token");
        if ($cookieStore.get("user").remember) {
          let loggedOutUser = {
            username: $cookieStore.get("user").username,
            isLogged: false,
            role: $rootScope.userRoles.visitor,
            remember: true
          };
          $cookieStore.put("user", loggedOutUser);
        } else {
          $cookieStore.put("user", undefined);
        }

        //reset rootScope user
        $rootScope.user.username = "";
        $rootScope.user.password = "";
        $rootScope.user.fname = "";
        $rootScope.user.lname = "";
        $rootScope.user.remember = null;
        $rootScope.user.isLogged = false;
        $rootScope.user.role = $rootScope.userRoles.visitor;

          $rootScope.$broadcast("logOut");
          //res.sendfile(__dirname + '/frontend/app/login.html');
          //window.location.href = '/frontend/app/login.html';
        $location.path("/login");
      },
      httpResetPassword: function(user, password, callback) {
        var resetPasswordConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/user/profile",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            username: user.username,
            password: password
          }
        };

        $http(resetPasswordConfig).then(
          function(data) {
            // console.log("http reset password called");
            return callback(data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpGetUserSettings: function(callback) {
        let getUserSettingsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/user/settings",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getUserSettingsConfig).then(
          function(data) {
            // console.log("Retrieved User Settings.");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPutUserSettings: function(callback) {
        let getUserSettingsConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/user/settings",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            userSettings: $rootScope.userSettings
          }
        };

        $http(getUserSettingsConfig).then(
          function(data) {
            // console.log("Updated User Settings.");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      }
    };
  }
]);
