﻿"use strict";

angular.module("menciusapp").factory("safeUrlService", [
  "$http",
  "$rootScope",
  "$cookieStore",
  "$location",
  "alertService",
  function statusService(
    $http,
    $rootScope,
    $cookieStore,
    $location,
    alertService
  ) {
    return {
      httpGetSafeUrls: function(callback) {
        var getSafeUrlsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/safe_urls",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getSafeUrlsConfig).then(
          function(data) {
            // console.log("Retrieved safe urls from db");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPutSafeUrls: function(safeUrls, callback) {
        var updateSafeUrlConfig = {
          method: "PUT",
          url: "/safe_urls/update/",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            safe_urls: safeUrls
          }
        };

        $http(updateSafeUrlConfig).then(
          function(data) {
            // console.log("Updated safe url in db");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPostNewSafeUrl: function(safeUrls, callback) {
        //TODO: we should stop empty urls here AND deal with them on the backend
        var insertSafeUrlConfig = {
          method: "POST",
          url: "/safe_urls",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            safeUrls: safeUrls
          }
        };

        $http(insertSafeUrlConfig).then(
          function(data) {
            data.status == 204
              ? console.log("No urls to create")
              : console.log("Created safe url in db");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpGetDetailedSafeUrls: function(callback) {
        var getSafeUrlsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/safe_urls/detailed",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getSafeUrlsConfig).then(
          function(data) {
            // console.log("Retrieved detailed safe urls from db");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      }
    };
  }
]);
