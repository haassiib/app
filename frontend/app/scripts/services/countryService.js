﻿"use strict";

angular.module("menciusapp").factory("countryService", [
  "$http",
  "$rootScope",
  "$cookieStore",
  "$location",
  function statusService($http, $rootScope, $cookieStore, $location) {
    return {
      httpGetCountries: function(callback) {
        var getCountriesConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/countries",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getCountriesConfig).then(
          function(data) {
            // console.log("Retrieved countries from db");
            //console.log(data.data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      }
    };
  }
]);
