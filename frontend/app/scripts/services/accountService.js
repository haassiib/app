﻿"use strict";

angular.module("menciusapp").factory("accountService", [
  "$http",
  "$rootScope",
  "$routeParams",
  "$cookieStore",
  "$location",
  "alertService",
  function accountService(
    $http,
    $rootScope,
    $routeParams,
    $cookieStore,
    $location,
    alertService
  ) {
    return {
      httpGetAccounts: function(callback) {
        var getAccountsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/accounts",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getAccountsConfig).then(
          function(data) {
            console.log("Received accounts from db");
            // console.log(data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpGetAllWithDetails: function(callback) {
        var getAccountsWithDetailsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/launch_rates",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getAccountsWithDetailsConfig).then(
          function(data) {
            console.log("Received accounts from db");
            // console.log(data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },

      httpGetWarmingAccounts: function(callback) {
        //console.log('inside http get-route: ', $routeParams.account_key);
        var getWarmingAccountsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/accounts/warming",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getWarmingAccountsConfig).then(
          function(data) {
            console.log("Received warming accounts from db");
            // console.log(data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpGetReadyAccounts: function(callback) {
        //console.log('inside http get-route: ', $routeParams.account_key);
        var getReadyAccountsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/accounts/ready",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getReadyAccountsConfig).then(
          function(data) {
            console.log("Received ready accounts from db");
            // console.log(data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpGetAccountDetails: function(callback) {
        //console.log('inside http get-route: ', $routeParams.account_key);
        var getAccountDetailsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/edit_account/" + $routeParams.account_key,
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getAccountDetailsConfig).then(
          function(data) {
            // console.log("Received account details from db");
            // console.log(data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpGetAngles: function(limit, callback) {
        let getAnglesConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/account/angles",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role,
            "x-limit": limit
          }
        };

        $http(getAnglesConfig).then(
          function(data) {
            // console.log("Received angles from db");
            // console.log(data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpGetAccountByContentId(content_keys, callback) {
        var getAccountByContentConfig = {
          method: "POST",
          url: $rootScope.urlRoot + "/account/info/content",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            content_keys: content_keys
          }
        };

        $http(getAccountByContentConfig).then(
          function(data) {
            console.log("Received account info from db");
            // console.log(data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPutUpdateAccounts: function(accounts, callback) {
        var updateAccountConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/accounts",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            accounts: accounts
          }
        };

        $http(updateAccountConfig).then(
          function(data) {
            console.log("Updated accounts in db");
            // console.log(data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPutUpdateAccountDetails: function(account, callback) {
        var updateAccountDetailsConfig = {
          method: "PUT",
          //TODO: rename this for consistancy
          url:
            $rootScope.urlRoot +
            "/account/info/update/" +
            $routeParams.account_key,
            json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            account: account
          }
        };

        $http(updateAccountDetailsConfig).then(
          function(data) {
            console.log("data.status:", data.status);
            console.log("Account Details Updated!", data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPutUpdateAccountSafeUrl: function(safe_url, callback) {
        var updateAccountSafeUrlConfig = {
          method: "PUT",
          url:
            $rootScope.urlRoot +
            "/account/safe_url/update/" +
            $routeParams.account_key,
            json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            safe_url: safe_url
          }
        };

        $http(updateAccountSafeUrlConfig).then(
          function(data) {
            console.log("data.status:", data.status);
            console.log("Account Safe Url Update!", data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPutUpdateAccountCode: function(code, callback) {
        var updateAccountCodeConfig = {
          method: "PUT",
          url:
            $rootScope.urlRoot +
            "/account/code/update/" +
            $routeParams.account_key,
            json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            code: code
          }
        };

        $http(updateAccountCodeConfig).then(
          function(data) {
            console.log("data.status:", data.status);
            console.log("Account Code Update!", data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPutUpdateAccountSpend: function(accounts, callback) {
        var httpPutAccountSpendConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/accounts/spend",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            accounts: accounts
          }
        };
        $http(httpPutAccountSpendConfig).then(
          function(data) {
            //console.log("data.status:", data.status);
            console.log("Updated Account Spend!");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPutUpdateAccountBudget: function(accounts, callback) {
        var httpPutAccountBudgetConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/accounts/budget",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            accounts: accounts
          }
        };
        $http(httpPutAccountBudgetConfig).then(
          function(data) {
            //console.log("data.status:", data.status);
            console.log("Updated Account Budget!");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPutUpdateAccountAudit: function(account, callback) {
        var httpPutAccountAuditConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/account/audit",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            account: account
          }
        };
        $http(httpPutAccountAuditConfig).then(
          function(data) {
            //console.log("data.status:", data.status);
            console.log("Updated Account Audit!");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPostNewAccounts: function(accounts, callback) {
        var httpPostNewAccountsConfig = {
          method: "POST",
          url: $rootScope.urlRoot + "/accounts",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            accounts: accounts
          }
        };

        $http(httpPostNewAccountsConfig).then(
          function(data) {
            console.log("data.status:", data.status);
            console.log("New Accounts Created!", data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpDeleteAccounts: function(accounts, callback) {
        console.log("accounts to delete: ", accounts);
        var deleteAccountConfig = {
          method: "DELETE",
          url: $rootScope.urlRoot + "/accounts",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            accounts: accounts
          }
        };

        $http(deleteAccountConfig).then(
          function(data) {
            console.log("data:", data);
            return callback(data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpUpdateAuditDate: function(account_key, callback) {
        console.log("accounts to update: ", account_key);
        var updateAccountConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/account/audit/",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            account_key: account_key,
            account: {
              account_key: account_key
            }
          }
        };

        $http(updateAccountConfig).then(
          function(data) {
            return callback(data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      }
    };
  }
]);
