﻿"use strict";

angular.module("menciusapp").factory("codeService", [
    "$http",
    "$rootScope",
    "$routeParams",
    "$cookieStore",
    "$location",
    "alertService",
    function accountService(
        $http,
        $rootScope,
        $routeParams,
        $cookieStore,
        $location,
        alertService
    ) {
        return {

            httpGetAccountCode: function (account_key, callback) {
                //console.log("get account code");
                var getAccountCodeConfig = {
                    method: "GET",
                    url: $rootScope.urlRoot + "/account/code/" + account_key,
                    headers: {
                        "Content-Type": "application/json",
                        "x-username": $rootScope.user.username,
                        "x-role": $rootScope.user.role
                    }
                };

                $http(getAccountCodeConfig).then(
                    function (data) {
                        //console.log("data.status:", data.status);
                        //console.log(data);
                        if (!data.data) {
                            console.log("No code found for account");
                        } else {
                            //console.log("Received single account code number from db");
                        }
                        return callback(data.data);
                    },
                    function (data, status) {
                        Errors.httpResponse(data);
                        return callback(data.status, data);
                    }
                );
            },

            httpGetAccountCodes: function (all, callback) {
                //console.log("get account codes");
                var getAccountCodesConfig = {
                    method: "GET",
                    url: $rootScope.urlRoot + "/account/codes/" + all,
                    headers: {
                        "Content-Type": "application/json",
                        "x-username": $rootScope.user.username,
                        "x-role": $rootScope.user.role
                    }
                };

                $http(getAccountCodesConfig).then(
                    function (data) {
                        // console.log("data.status:", data.status);
                        // console.log("Get all account codes from db"); //\n', data);
                        return callback(data.data);
                    },
                    function (data, status) {
                        Errors.httpResponse(data);
                        return callback(data.status, data);
                    }
                );
            },

            httpPostAccountCodes: function (codes, callback) {
                // console.log("CODES: ", codes);
                var postAccountCodesConfig = {
                    method: "POST",
                    url: $rootScope.urlRoot + "/account/codes/",
                    headers: {
                        "Content-Type": "application/json",
                        "x-username": $rootScope.user.username,
                        "x-role": $rootScope.user.role
                    },
                    data: {
                        codes: codes
                    }
                };

                $http(postAccountCodesConfig).then(
                    function (data) {
                        // console.log("Posted accounts to db");
                        return callback(data.data);
                    },
                    function (data, status) {
                        Errors.httpResponse(data);
                        return callback(data.status, data);
                    }
                );
            },

            httpPutAccountCode: function (codes, callback) {
                var putAccountCodesConfig = {
                    method: "PUT",
                    url: $rootScope.urlRoot + "/account/code/",
                    headers: {
                        "Content-Type": "application/json",
                        "x-username": $rootScope.user.username,
                        "x-role": $rootScope.user.role
                    },
                    data: {
                        codes: codes
                    }
                };

                $http(putAccountCodesConfig).then(
                    function (data) {
                        // console.log("Put accounts into db");
                        return callback(data.data);
                    },
                    function (data, status) {
                        Errors.httpResponse(data);
                        return callback(data.status, data);
                    }
                );
            },

            httpGetCodeHistory: function (account_key, callback) {
                let getCodeHistoryConfig = {
                    method: "GET",
                    url: $rootScope.urlRoot + "/accounts/codes/history/" + account_key,
                    headers: {
                        "Content-Type": "application/json",
                        "x-username": $rootScope.user.username,
                        "x-role": $rootScope.user.role
                    }
                };

                $http(getCodeHistoryConfig).then(
                    function (data) {
                        // console.log("Get Code History Success.");
                        return callback(data.data);
                    },
                    function (data, status) {
                        Errors.httpResponse(data);
                        return callback(data.status, data);
                    }
                );
            },

            httpPostCodeHistory: function (items, callback) {
                var postCodeHistoryConfig = {
                    method: "POST",
                    url: $rootScope.urlRoot + "/accounts/codes/history",
                    headers: {
                        "Content-Type": "application/json",
                        "x-username": $rootScope.user.username,
                        "x-role": $rootScope.user.role
                    },
                    data: {
                        items: items
                    }
                };

                $http(postCodeHistoryConfig).then(
                    function (data) {
                        // console.log("Post Code History Success.");
                        return callback(data.data);
                    },
                    function (data, status) {
                        Errors.httpResponse(data);
                        return callback(data.status, data);
                    }
                );
            }
        };
    }
]);
