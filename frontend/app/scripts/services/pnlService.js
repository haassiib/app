﻿"use strict";

angular.module("menciusapp").factory("pnlService", [
  "$http",
  "$rootScope",
  "$routeParams",
  "$cookieStore",
  "$location",
  "alertService",
  function pnlService(
    $http,
    $rootScope,
    $routeParams,
    $cookieStore,
    $location,
    alertService
  ) {
    return {
      httpGetPnl: async function(start, end, callback) {
        //console.log("get pnl");
        let getPnlConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/pnl",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role,
            "x-start": start,
            "x-end": end
          }
        };

        $http(getPnlConfig).then(
          function(data) {
            // console.log("Retrieve PNL data success. => ", data);
            //console.log(data);
            return callback(data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data, data.status);
          }
        );
      },
      httpGetPnlByAccount: function(callback) {
        //console.log("get pnl");
        let getPnlByAcctConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/pnl/" + $routeParams.account_key,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getPnlByAcctConfig).then(
          function(data) {
            //console.log(data.data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data, status);
          }
        );
      },
      httpGetNDaysByAccount: function(n, callback) {
        //console.log("get pnl");
        let getPnlNdaysByAcctConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/pnl/days/" + $routeParams.account_key,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role,
            "x-days": n
          }
        };

        $http(getPnlNdaysByAcctConfig).then(
          function(data) {
            //console.log(data.data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data, status);
          }
        );
      },
      httpGetNDaysForROI: function(n, callback) {
        //console.log("get pnl");
        let getPnlNdaysForROIConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/pnl/days/roi",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role,
            "x-days": n
          }
        };

        $http(getPnlNdaysForROIConfig).then(
          function(data) {
            //console.log(data.data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data, status);
          }
        );
      },
      httpPostPnlItems: function(items, callback) {
        //console.log("post items pnl");
        //TODO: calculate timezone before insert

        let postPnlConfig = {
          method: "POST",
          url: $rootScope.urlRoot + "/pnl",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            items: items,
            created: moment().format("YYYY-MM-DD") + " 06:00:00"
          }
        };

        $http(postPnlConfig).then(
          function(data) {
            // console.log("Post New Pnl item success.");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPostPnlItem: function(account_keys, callback) {
        //console.log("post items pnl");
        // TODO: calculate time zone properly

        let postPnlConfig = {
          method: "POST",
          url: $rootScope.urlRoot + "/pnl/new",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            account_keys: account_keys,
            created: moment().format("YYYY-MM-DD") + " 06:00:00"
          }
        };

        $http(postPnlConfig).then(
          function(data) {
            // console.log("Post New Pnl item success");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPutPnlItems: function(items, callback) {
        //console.log("put items pnl");
        let putPnlConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/pnl",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            items: items
          }
        };

        $http(putPnlConfig).then(
          function(data) {
            // console.log("Put pnl success.");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data, data.status);
          }
        );
      },
      httpPutExpensesByAccount: function(items, callback) {
        //console.log("put items pnl");
        let putPnlConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/pnl/expenses",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            items: items
          }
        };

        $http(putPnlConfig).then(
          function(data) {
            // console.log("Put pnl success.");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data, data.status);
          }
        );
      },
      httpGetPayoutsByRangeAndUser: function(
        startdate,
        enddate,
        owner,
        callback
      ) {
        //console.log("get pnl");
        let getPnlByAcctConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/payouts",
          headers: {
            "Content-Type": "application/json",
            "x-username": Tools.CapitalizeFirstLetter($rootScope.user.username),
            "x-role": $rootScope.user.role,
            "x-startdate": startdate,
            "x-enddate": enddate,
            "x-owner": Tools.CapitalizeFirstLetter(owner)
          }
        };

        $http(getPnlByAcctConfig).then(
          function(data) {
            //console.log(data.data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data, status);
          }
        );
      }
    };
  }
]);
