﻿'use strict';

angular.module('menciusapp')
    .factory('accountCommentService', ['$http', '$rootScope', '$routeParams', '$cookieStore', '$location', 'alertService',
        function accountService($http, $rootScope, $routeParams, $cookieStore, $location, alertService) {

            return {
                httpGetAccountComments: function (account_key, callback) {
                    var getAccountCommentsConfig = {
                        method: "GET",
                        url: $rootScope.urlRoot + '/account/comments/' + account_key,
                        headers: {
                            'Content-Type': 'application/json',
                            "x-username": $rootScope.user.username,
                            "x-role": $rootScope.user.role
                        }
                    };

                    $http(getAccountCommentsConfig).then(
                        function (data) {
                            console.log("Inserted accounts comments from db");
                            // console.log(data);
                            return callback(data);
                        },
                        function (data, status) {
                            Errors.httpResponse(data);
                            return callback(data.status, data);
                        }
                    );
                },
                httpPostAccountComment: function (account, callback) {
                    var postAccountCommentConfig = {
                        method: "POST",
                        url: $rootScope.urlRoot + '/account/comment/',
                        headers: {
                            'Content-Type': 'application/json',
                            "x-username": $rootScope.user.username,
                            "x-role": $rootScope.user.role
                        },
                        data: {
                            account: account
                        }
                    };

                    $http(postAccountCommentConfig).then(
                        function (data) {
                            console.log("Inserted accounts comments from db");
                            // console.log(data);
                            return callback(data);
                        },
                        function (data, status) {
                            Errors.httpResponse(data);
                            return callback(data.status, data);
                        }
                    );
                },
                httpPostAccountsComments: function (accounts, callback) {
                    var postAccountsCommentsConfig = {
                        method: "POST",
                        url: $rootScope.urlRoot + '/accounts/comments/',
                        headers: {
                            'Content-Type': 'application/json',
                            "x-username": $rootScope.user.username,
                            "x-role": $rootScope.user.role
                        },
                        data: {
                            accounts: accounts
                        }
                    };

                    $http(postAccountsCommentsConfig).then(
                        function (data) {
                            console.log("Inserted accounts comments from db");
                            // console.log(data);
                            return callback(data);
                        },
                        function (data, status) {
                            Errors.httpResponse(data);
                            return callback(data.status, data);
                        }
                    );
                }
            };
        }
    ]);