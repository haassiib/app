"use strict";

angular.module("menciusapp").factory("adespressoService", [
  "$http",
  "$rootScope",
  "$cookieStore",
  "$location",
  function adespressoService($http, $rootScope, $cookieStore, $location) {
    return {
      httpGetLifetimeSpendByAccount: function(
        accountKey,
        adespressoId,
        callback
      ) {
        $http({
          method: "GET",
          url:
            $rootScope.urlRoot +
            "/adespresso/lifetime/" +
            accountKey +
            "/" +
            adespressoId
        }).then(
          function(data) {
            console.log("Get Adespresso Lifetime Spend Success:", data);
            callback(data);
            // init();
            // $scope.voluumLoading = false;
          },
          function(data) {
            console.log("Get Adespresso Lifetime Spend Failure: ", data);
            // $scope.voluumLoading = false;
          }
        );
      }
    };
  }
]);
