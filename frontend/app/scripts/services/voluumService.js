﻿"use strict";

angular.module("menciusapp").factory("voluumService", [
  "$http",
  "$rootScope",
  "$cookieStore",
  "$location",
  function statusService($http, $rootScope, $cookieStore, $location) {
    return {
      httpAuthVoluum: function() {
        $http({
          method: "GET",
          url: "/voluum/auth"
        }).then(
          function(data) {
            console.log("Voluum Auth Success:");
            $cookieStore.put("voluum_token", data.data.token);
            $cookieStore.put("voluum_exp", data.data.expirationTimestamp);
            callback(data);
          },
          function(data) {
            Errors.httpResponse(data);
            callback(data.status, data);
          }
        );
      },
      httpGetReports: function(token, callback) {
        //TODO verify token is valid

        $http({
          method: "GET",
          url:
            "/voluum/report/" + $scope.timeFrameFilter.toString().toLowerCase(),
          headers: {
            "x-token": $cookieStore.get("voluum_token"),
            "x-date": moment().format("YYYY-MM-DD")
          }
        }).then(
          function(data) {
            console.log("Voluum Report Success:", data);
            init();
            $scope.voluumLoading = false;
          },
          function(data) {
            console.log("Voluum Report Failure: ", data);
            $scope.voluumLoading = false;
          }
        );
      },
      httpGetCampaigns: function(callback) {
        $http({
          method: "GET",
          url: "/voluum/campaigns",
          headers: { "x-token": token }
        }).then(
          function(data) {
            console.log("Get Voluum Campaigns Success");
            callback(data);
          },
          function(data) {
            Errors.httpResponse(data);
            callback(data.status, data);
          }
        );
      },
      httpGetCampaignByAccount: function(callback) {
        //TODO
      }
    };
  }
]);
