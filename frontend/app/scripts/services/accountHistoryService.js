﻿"use strict";

angular.module("menciusapp").factory("accountHistoryService", [
  "$http",
  "$rootScope",
  "$cookieStore",
  "$location",
  "alertService",
  function accountHistoryService(
    $http,
    $rootScope,
    $cookieStore,
    $location,
    alertService
  ) {
    return {
      httpPutAccountHistory: function(accounts, callback) {
        var accountHistoryUpdateConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/account/history",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username
          },
          data: {
            accounts: accounts
          }
        };
        $http(accountHistoryUpdateConfig).then(
          function(data) {
            //console.log("account hisory data:", data);
            callback(data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      getAllHistory: function(callback) {
        var accountAllHistoryConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/accounts/history",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username
          },
          data: {
            history: history
          }
        };
        $http(accountAllHistoryConfig).then(
          function(data) {
            //console.log("account hisory data:", data);
            callback(data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      }
    };
  }
]);
