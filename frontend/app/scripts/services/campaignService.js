﻿"use strict";

angular.module("menciusapp").factory("campaignService", [
  "$http",
  "$rootScope",
  "$cookieStore",
  "$location",
  "alertService",
  function campaignService(
    $http,
    $rootScope,
    $cookieStore,
    $location,
    alertService
  ) {
    return {
      httpGetCampaigns: function(callback) {
        var getCampaignsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/campaigns",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getCampaignsConfig).then(
          function(data) {
            // console.log("Retreived campaigns from db");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpGetTotalSpendByCampaign: function(callback) {
        var getTotalSpendByCampaignsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/campaigns/totalspend",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getTotalSpendByCampaignsConfig).then(
          function(data) {
            console.log("Retreived totalspendbycampaign from db");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpRoiByCampaign: function(timeFrame, callback) {
        let getRoiByCamp = {
          method: "GET",
          url: $rootScope.urlRoot + "/campaigns/roi_by_account",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role,
            "x-timeframe": timeFrame
          }
        };

        $http(getRoiByCamp).then(
          function(data) {
            console.log("Retreived roiByCampaign from db");
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpAddUploadset: function(uploadset, callback) {
        //console.log("UPLOAD SET DATA: \n", uploadset);
        var addUploadsetConfig = {
          method: "POST",
          url: $rootScope.urlRoot + "/uploadset",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            uploadset: uploadset
          }
        };

        $http(addUploadsetConfig).then(
          function(data) {
            if (data.status == 200) {
              console.log("New Uploadset Created!", data.config);
              return callback(data.config.data);
            }
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpUpdateBadge: function(user, callback) {
        //Update Task count badge
        var getTasksConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/tasks",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };
        $http(getTasksConfig).then(
          function(data) {
            console.log("Retrieved task count from db");
            $rootScope.$broadcast("tasksCountUpdate", {
              number: data.data.length
            });

            return callback(data.data.length);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpUpdateCampaigns: function(campaigns, callback) {
        var updateCampaignConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/campaigns",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            campaigns: campaigns
          }
        };
        $http(updateCampaignConfig).then(
          function(data) {
            console.log("Successfully update campaigns.");
            return callback(data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      }
    };
  }
]);
