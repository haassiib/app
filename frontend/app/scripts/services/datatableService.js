﻿"use strict";

angular.module("menciusapp").factory("datatableService", [
  "$http",
  "$rootScope",
  "$cookieStore",
  "$location",
  function statusService($http, $rootScope, $cookieStore, $location) {
    return {
      httpGetBatches: function(callback) {
        var getBatchesConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/batches",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getBatchesConfig).then(
          function(data) {
            console.log("Retrieved batches from db");
            return callback(data.data.map(batch => batch));
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPostInsertBatch: function(
        batchName,
        BatchDescription,
        safeSpend,
        proxy,
        warmTime,
        safeAds,
        preWarmingActivities,
        preLaunchActivities,
        callback
      ) {
        var postInsertBatchesConfig = {
          method: "POST",
          url: $rootScope.urlRoot + "/batches",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            newBatch: {
              name: batchName,
              description: BatchDescription,
              safe_spend: safeSpend,
              proxy: proxy,
              warm_time: warmTime,
              safe_ads: safeAds,
              pre_warming_activities: preWarmingActivities,
              pre_launch_activities: preLaunchActivities
            }
          }
        };

        $http(postInsertBatchesConfig).then(
          function(data) {
            console.log("Data :", data);
            console.log("successful post");
            return callback(data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPutUpdateBatches: function(batches, callback) {
        var updateBatchesConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/batches",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            batches: batches
          }
        };

        $http(updateBatchesConfig).then(
          function(data) {
            console.log("Updated batches in db");
            console.log(data);
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      }
    };
  }
]);
