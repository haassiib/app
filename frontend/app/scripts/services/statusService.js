﻿'use strict';

angular.module('menciusapp').factory('statusService', [
  '$http',
  '$rootScope',
  '$cookieStore',
  '$location',
  'alertService',
  function statusService($http, $rootScope, $cookieStore, $location, alertService) {
    return {
      httpGetStatus: function(callback) {
        let getStatusConfig = {
          method: 'GET',
          url: $rootScope.urlRoot + '/status',
          headers: {
            'Content-Type': 'application/json',
            'x-username': $rootScope.user.username,
            'x-role': $rootScope.user.role
          }
        };

        $http(getStatusConfig).then(
          function(data) {
            // console.log("Retrieved status from db");
            return callback(getStatusByRole($rootScope.user.role, data.data));
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );

        function getStatusByRole(role, statusData) {
          role = Number(role);

          let hasPermission = function(status) {
            // user role || admin
            return (
              (Number(status.permissions) & role) === role ||
              (Number(status.permissions) & role) === Number(status.permissions)
            );
          };
          let userStatuses = statusData.filter(hasPermission).map(status => status.status);

          //console.log(userStatuses);
          return userStatuses;
        }
      },
      httpGetAllStatus: function(callback) {
        let getStatusConfig = {
          method: 'GET',
          url: $rootScope.urlRoot + '/status',
          headers: {
            'Content-Type': 'application/json',
            'x-username': $rootScope.user.username,
            'x-role': $rootScope.user.role
          }
        };

        $http(getStatusConfig).then(
          function(data) {
            // console.log("Retrieved all statuses from db");
            return callback(getStatusByRole(data.data));
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );

        function getStatusByRole(statusData) {
          let userStatus = statusData.map(status => status.status);
          return userStatus;
        }
      },
      httpGetFullStatus: function(callback) {
        let getStatusConfig = {
          method: 'GET',
          url: $rootScope.urlRoot + '/status',
          headers: {
            'Content-Type': 'application/json',
            'x-username': $rootScope.user.username,
            'x-role': $rootScope.user.role
          }
        };

        $http(getStatusConfig).then(
          function(data) {
            console.log('Retrieved full statuses from db');
            return callback(data.data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      }
    };
  }
]);
