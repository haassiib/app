﻿"use strict";

angular.module("menciusapp").factory("virtualMachineService", [
  "$http",
  "$rootScope",
  "$cookieStore",
  "$location",
  function statusService($http, $rootScope, $cookieStore, $location) {
    return {
      httpGetVms: function(callback) {
        var getVmsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/vms",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getVmsConfig).then(
          function(data) {
            // console.log("Retrieved VM's from db");
            return callback(data.data.map(vm => vm));
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPostInsertVm: function(vmName, vmUUID, callback) {
        var postInsertVmsConfig = {
          method: "POST",
          url: $rootScope.urlRoot + "/vms",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            newVm: {
              name: vmName,
              UUID: vmUUID
            }
          }
        };

        $http(postInsertVmsConfig).then(
          function(data) {
            console.log("successful post");
            return callback(data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      }
    };
  }
]);
