﻿"use strict";

angular.module("menciusapp").factory("verticalService", [
  "$http",
  "$rootScope",
  "$cookieStore",
  "$location",
  function statusService($http, $rootScope, $cookieStore, $location) {
    return {
      httpGetVerticals: function(callback) {
        var getVerticalsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/verticals",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getVerticalsConfig).then(
          function(data) {
            // console.log("Retrieved verticals from db");
            return callback(data.data.map(vertical => vertical.name));
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpGetCompleteVerticals: function(callback) {
        var getVerticalsConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/verticals/all",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getVerticalsConfig).then(
          function(data) {
            console.log("Retrieved verticals from db");
            return callback(data.data.map(vertical => vertical));
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      },
      httpPostInsertVertical: function(verticals, callback) {
        var postInsertVerticalsConfig = {
          method: "POST",
          url: $rootScope.urlRoot + "/verticals",
          json: true,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            verticals: verticals
          }
        };

        $http(postInsertVerticalsConfig).then(
          function(data) {
            console.log("successful post");
            return callback(data);
          },
          function(data, status) {
            Errors.httpResponse(data);
            return callback(data.status, data);
          }
        );
      }
    };
  }
]);
