﻿"use strict";

angular.module("menciusapp").factory("contentService", [
  "$http",
  "$rootScope",
  function statusService($http, $rootScope) {
    return {
      httpGetPerformantContent: function(callback) {
        var getContentConfig = {
          method: "GET",
          url: $rootScope.urlRoot + "/content/performant",
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          }
        };

        $http(getContentConfig).then(
          function(data) {
            // console.log("Retrieved ad content from db.");
            return callback(data.data);
          },
          function(data, status) {
            if (status === 404) {
              console.log(
                "could not retrieve ad content, something went wrong"
              );
              return callback(data, status);
            }
          }
        );
      },
      httpPutPerformantContent: function(ad, callback) {
        var getContentConfig = {
          method: "PUT",
          url: $rootScope.urlRoot + "/content/performant/" + ad.content_key,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            ad: ad
          }
        };

        $http(getContentConfig).then(
          function(data) {
            console.log("Retrieved ad content from db");
            return callback(data.data);
          },
          function(data, status) {
            if (status === 404) {
              console.log(
                "could not retrieve ad content, something went wrong"
              );
              return callback(data, status);
            }
          }
        );
      },
      httpDeleteContent: function(ad, callback) {
        var deleteContentConfig = {
          method: "DELETE",
          url: $rootScope.urlRoot + "/content/" + ad.content_key,
          headers: {
            "Content-Type": "application/json",
            "x-username": $rootScope.user.username,
            "x-role": $rootScope.user.role
          },
          data: {
            ad: ad
          }
        };

        $http(deleteContentConfig).then(function(data) {
          console.log("data:", data);
          console.log("args:", arguments);

          if (data.status >= 400) {
            console.log("could not delete ad content, something went wrong");
            console.log(data.status);
            return callback(data, data.status);
          }
          return callback(data);
        });
      }
    };
  }
]);
