﻿'use strict'

// A place for error style code snippets
let Errors = {
    httpResponse: function (data) {
        if (data.status < 400 && data.status >= 300) {
            console.error('Redirection Error: ', data);
        }
        else if (data.status < 500 && data.status >= 400) {
            console.error('Client Error: ', data);
        }
        else if (data.status < 600 && data.status >= 500) {
            console.error('Server Error: ', data);
        }
        else {
            console.error('Unofficial Error: ', data);
        }
    },
};