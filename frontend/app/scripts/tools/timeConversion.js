//TODO: DELETE THIS  - use moment
let Convert = {
    frontToBackStart: function (frontendTime) {

        var backendTime;

        //  frontendTime                       backendTime
        //  Javascript Date                    MySQL Date
        //  yyyy / MM / dd        ----         yyyy-MM-dd hh:mm:ss

        var year = frontendTime.toString().substring(0,4);
        var month = frontendTime.toString().substring(7,9);
        var day = frontendTime.toString().substring(12,14);

        // console.log('year', year)
        // console.log('month', month)
        // console.log('day', day)

        month = (Number(month) - 1).toString();

        var backendTime = new Date(year, month, day, 0, 0, 0, 0);
        
        return backendTime;
    },
    frontToBackEnd: function (frontendTime) {

        var backendTime;

        var year = frontendTime.toString().substring(0,4);
        var month = frontendTime.toString().substring(7,9);
        var day = frontendTime.toString().substring(12,14);

        // console.log('year', year)
        // console.log('month', month)
        // console.log('day', day)

        month = (Number(month) - 1).toString();

        var backendTime = new Date(year, month, day, 23, 59, 59, 999);

        return backendTime;
    }
};