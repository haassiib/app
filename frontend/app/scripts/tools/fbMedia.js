﻿let facebook = {
    createMedia: function(media_url) {

        let source = '';
        let media;
        let id = Tools.getParameterByName('id', media_url);
        let url = media_url.split('/');

        if (id !== null && id !== "") {
            // url id exists
            //TODO: this should do more advanced checking for video url
            source = 'https://drive.google.com/uc?export=download&id=' + id;
            media = document.createElement('video');
            media.src = source;
            media.style.height = "201px";
            media.style.width = "384px";
            media.setAttribute("controls", "controls")
        }
        else if (url.length > 0 && url[4] === "d") {

            source = 'https://drive.google.com/uc?export=download&id=' + url[5];
            media = document.createElement('video');
            media.src = source;
            media.style.height = "201px";
            media.style.width = "384px";
            media.setAttribute("controls", "controls")
        }
        else {
            media = new Image();
            media.onerror = function () {
                //image didn't load
                console.log("error retrieving image");
                media.src = defaultSrc;
            }
            media.style.height = "201px";
            media.style.width = "384px";
            media.src = media_url;
        }

        let divid = 'fb-media-';
        if ($scope.mobile.content_key)
            divid += $scope.mobile.content_key;

        let mediaDiv = document.getElementById(divid);
        if (mediaDiv)
            mediaDiv.appendChild(media);
    }
}