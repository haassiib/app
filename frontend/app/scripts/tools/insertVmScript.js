const mysql = require("promise-mysql");
const db = require("../../../../services/dbconnector");
const fs = require("fs-extra");
const csv = require("csv-parse");

exports.insertVms = (async function() {
  //console.log("Req body:", req.body);
  //console.log("Req params: ", req.params);

  await fs.readFile("./vmz.csv", { encoding: "utf-8" }, async (err, data) => {
    if (err) {
      console.error(err);
    }

    await csv(
      data,
      { quote: '"', ltrim: true, rtrim: true, delimiter: ";" },
      async (err, insertData) => {
        if (err) {
          console.error("error in the csv func => ", err);
        } else {
          console.log("Data => ", insertData);
          var connection = await db.init();

          await insertData.forEach(vm => {
            let name = vm[0];
            let UUID = vm[1];
            var queryStr = `insert ignore into virtual_machines set name = "${name}", UUID = "${UUID}", created=NOW();`;

            connection
              .query(queryStr)
              .then(results => {
                console.log("Setting new Vms! ", results);
                //console.log("Get Available Accounts res: ", results);
              })
              .catch(error => {
                if (error) {
                  console.log(error);
                }
              });
          });

          await connection.end();
        }
      }
    );
  });
})();
require("make-runnable");
