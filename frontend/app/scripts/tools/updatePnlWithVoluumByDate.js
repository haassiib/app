const vol = require("../../../../services/voluum.js");
const pnl = require("../../../../services/pnl.js");
const [, , ...args] = process.argv;

//args need to be acceptable voluum datestring e.g = 2018-09-08T06:00:00

(async function() {
  let reports = await vol.getAllReportsByDate([...args]);
  // reports = await JSON.parse(reports);
  let pixels = await reports.rows.map(r => {
    if (r.customVariable4 !== "" && r.customVariable4 !== undefined) {
      return r.customVariable4;
    }
  });
  pixels.shift();
  await pnl.insertDailyPnlByDateAndPixel([...args], pixels);
  await vol.reconcileDay([...args]);
})(args);
