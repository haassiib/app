﻿"use strict";

// A place for random code snippets that you reuse often
let Tools = {
  CapitalizeFirstLetter: function(_string) {
    if (_string != null || _string != undefined)
      return _string.charAt(0).toUpperCase() + _string.slice(1);

    return _string;
  },
  WatcherCounter: function() {
    let root = $(document.getElementsByTagName("body"));
    let watchers = [];

    let f = function(element) {
      if (element.data().hasOwnProperty("$scope")) {
        angular.forEach(element.data().$scope.$$watchers, function(watcher) {
          watchers.push(watcher);
        });
      }

      angular.forEach(element.children(), function(childElement) {
        f($(childElement));
      });
    };

    f(root);

    console.log("Number of Watchers: ", watchers.length);
  },
  generateSortFunction: function(props) {
    return function(a, b) {
      for (let i = 0; i < props.length; i++) {
        let prop = props[i];
        let name = prop.name;
        //console.log(a[name], b[name]);
        //console.log("a[name]: ", a[name], " |  b[name]: ", b[name]);
        //console.log("typeof a[name]: ", typeof a[name], " |  typeof b[name]: ", typeof b[name]);
        if (a[name] == null) {
          a[name] = "";
        }
        if (b[name] == null) {
          b[name] = "";
        }
        let reverse = prop.reverse;
        if (a[name] < b[name]) return reverse ? 1 : -1;
        if (a[name] > b[name]) return reverse ? -1 : 1;
      }
      return 0;
    };
  },
  getParameterByName: function(name, uri) {
    if (!uri) uri = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(uri);
    if (!results) return null;
    if (!results[2]) return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  },
  updateQueryParameter: function(key, value, uri) {
    // console.log(`inside update. key: ${key} | value: ${value} | uri: ${uri}` );
    if (!uri) uri = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi");

    if (re.test(uri)) {
      if (typeof value !== "undefined" && value !== null)
        return uri.replace(re, "$1" + key + "=" + value + "$2$3");
      else {
        return uri.replace(re, "$1$3").replace(/(&|\?)$/, "");
      }
    } else {
      if (typeof value !== "undefined" && value !== null) {
        var separator = uri.indexOf("?") !== -1 ? "&" : "?";
        return uri + separator + key + "=" + value;
      } else return uri;
    }
  },
  isValidUrl: function(uri) {
    //console.log("uri: ", uri);
    let r = new RegExp(
      "(https?://(?:www.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9].[^s]{2,}|www.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9].[^s]{2,}|https?://(?:www.|(?!www))[a-zA-Z0-9].[^s]{2,}|www.[a-zA-Z0-9].[^s]{2,})"
    );
    return r.test(uri);
  },
  convertToLocalTime: function(dt, logg) {
    if (typeof dt === "undefined" || dt === null) return null;
    let localDate = new Date(dt);
    let localOffset = moment().utcOffset(); //in minutes

    if (logg)
      console.log(
        "dt: ",
        dt,
        " | local Date",
        localDate,
        "localOffset",
        localOffset,
        "local / 60",
        localOffset / 60,
        " FINAL: ",
        moment(dt)
          .utc(localOffset / 60)
          .format()
      );
    return moment(dt)
      .utc(localOffset / 60)
      .format();
  },
  convertFromLocalTime: function(dt, logg) {
    if (typeof dt === "undefined" || dt === null) return null;
    let localDate = new Date(dt);
    let localOffset = moment().utcOffset(); //in minutes

    if (logg)
      console.log(
        "dt: ",
        dt,
        " | local Date",
        localDate,
        "localOffset",
        localOffset,
        "local / 60",
        localOffset / -60,
        " FINAL: ",
        moment(dt)
          .utc(localOffset / -60)
          .format()
      );
    return moment(dt).utc(localOffset / -60);
  }
};
