// let dbConnection = require("../../../../services/dbconnector");
const mysql = require("promise-mysql");

(async function() {
  // var dbConnection = mysql.createPool({
  //   user: "dennistouchet",
  //   password: "ccDB455455%",
  //   database: "sys",
  //   guid: "50ac9e5158824b8d929c0925081a76cd",
  //   host: "localhost",
  //   env: "production",
  //   sslpath: "/etc/letsencrypt/live/"
  // });
  // mencius-prod.c50kkl7jqor1.us-east-1.rds.amazonaws.com
  let queryStr = `UPDATE pnl p JOIN accounts a on p.account_key = a.account_key SET p.expenses = a.budget where p.created >= DATE(CURDATE()) AND p.created < (DATE(CURDATE()) + INTERVAL 1 DAY);`;

  //console.log("UPDATE QUERY STR: ", queryStr);

  dbConnection
    .query(queryStr)
    .then(results => {
      //console.log("success : ", results);
      console.log("PNL update expenses success. => ", results);
      dbConnection.end();
    })
    .catch(error => {
      if (error) {
        console.log(error);
      }
    });
  // await connection.end();
})();
