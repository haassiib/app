"use strict";

const bcrypt = require("bcryptjs");
const [, , ...args] = process.argv;

const saltRounds = 10;

// var config = require('/opt/apps/properties/config.json');
let dbConnection = require("../../../../services/dbconnector");

(async function(username, password) {
  console.log("args for hasher => ", args);
  let connection = await dbConnection.init();

  await connection
    .query(
      'SELECT employee_key FROM employees WHERE username = "' +
        username +
        '" limit 1;'
    )
    .then(async results => {
      //console.log("SELECT employees key success");
      //console.log("result", result);

      // console.log(query);
      return bcrypt
        .hash(password, saltRounds)
        .then(async hashPass => {
          console.log("hash => ", hashPass);
          let query =
            'UPDATE employees SET password = "' +
            hashPass +
            '" WHERE employee_key = ' +
            results[0].employee_key +
            ";";
          let res = await connection.query(query);
          connection.end();
          return res;
        })
        .then(results => {
          console.log(
            "successfully updated employee password reset => ",
            results
          );
        })
        .catch(error => {
          if (error) {
            console.log(error);
          }
        });
    })
    .catch(error => {
      if (error) {
        console.log(error);
      }
    });
})(args[0], args[1]);
