const mysql = require("promise-mysql");
const moment = require("moment-timezone");
let dbConnection = require("../../../../services/dbconnector");

const [, , ...args] = process.argv;

exports.setLaunchRates = async function() {
  let connection = await dbConnection.init();

  // var connection = mysql.createPool({
  //   user: "dennistouchet",
  //   password: "ccDB455455%",
  //   database: "sys",
  //   guid: "50ac9e5158824b8d929c0925081a76cd",
  //   host: "mencius-prod.c50kkl7jqor1.us-east-1.rds.amazonaws.com",
  //   env: "production",
  //   sslpath: "/etc/letsencrypt/live/"
  // });

  var queryStr = `select * from accounts;`;
  let allDeets = [];

  connection
    .query(queryStr)
    .then(async results => {
      // allDeets = allDeets.concat(getAccountDetails(acc.account_key));
      await Promise.all(
        results.map(async acc => {
          allDeets.push(acc);
        })
      );
      // console.log("got the results from the accounts Table ", allDeets);
      //console.log("Get Available Accounts res: ", results);
      return allDeets;
    })
    .then(async accounts => {
      for (let acc of accounts) {
        let query2 = `select * from account_history where account_key = ${
          acc.account_key
        };`;

        await connection.query(query2).then(history => {
          let launchCode = 0;
          let postLaunch;
          acc.history = history
            .sort(function(a, b) {
              return a.created < b.created ? -1 : b.created < a.created ? 1 : 0;
            })
            .map(item => {
              item.created = moment(item.created)
                .utc()
                .format();
              return item;
            });

          if (acc.history.length) {
            if (acc.status_key === 37 || acc.status_key === 38) {
              launchCode = 3;
            } else {
              for (let h = 0; h < acc.history.length; h++) {
                //Pre-Launch ends at finished upload
                if (acc.history[h].status_key === 35) {
                  // console.log(
                  //   "We reached the finished upload status! => ",
                  //   acc.history[h]
                  // );
                  //if there is a line in history after the finished upload
                  if (acc.history[h + 1]) {
                    // console.log(
                    //   "There is a history item after the finished upload => ",
                    //   acc.history[h + 1]
                    // );

                    //set post launch list
                    postLaunch = acc.history.slice(h + 1);
                    // console.log(
                    //   "this is the complete postlaunch => ",
                    //   postLaunch
                    // );

                    break;
                  }
                } else if (
                  acc.history[h].status_key === 47 ||
                  acc.history[h].status_key === 48
                ) {
                  launchCode = 4;
                }
              }
            }
          }
          if (postLaunch) {
            for (let l = 0; l < postLaunch.length; l++) {
              if (postLaunch[l].status_key === 28) {
                //set successful launch
                launchCode = 1;
                break;
              } else if (postLaunch[l].status_key === 29) {
                //set disapproved
                launchCode = 2;
                break;
              } else if (
                postLaunch[l].status_key === 26 ||
                postLaunch[l].status_key === 37 ||
                postLaunch[l].status_key === 38 ||
                postLaunch[l].status_key === 43 ||
                postLaunch[l].status_key === 44
              ) {
                //set dead/flagged
                launchCode = 3;
                break;
              } else if (
                l === postLaunch.length - 1 ||
                postLaunch[l].status_key === 25
              ) {
                //if we are at the end of the postLaunch array and none of the
                //3 statuses above are hit then we set it to the problem status
                launchCode = 4;
              }
            }
          }

          // console.log("accounts check => ", acc);

          updateQuery =
            "update `accounts` set `launch_success` = " +
            launchCode +
            " where `account_key` = " +
            acc.account_key +
            ";";
          console.log("updateQuery in script => ", updateQuery);
          connection.query(updateQuery).then(res => {
            // console.log("AllAccounts after the launch var updated => ", res);
            // console.log("results of update query! => ", res);
            // connection.end();
          });
        });
      }
      connection.end();
    })
    .catch(error => {
      if (error) {
        console.log(error);
      }
    });
};

require("make-runnable");

// update accounts set launch_success = 1 where account_key = "96";
