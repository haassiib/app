const mysql = require('promise-mysql');
const moment = require('moment-timezone');
let dbConnection = require('../../../../services/dbconnector');
const accounts = require('../../../../services/accounts.js');
(async function() {
  let connection = await dbConnection.init();
  let queryStr = `UPDATE accounts acct
                    INNER JOIN
                        (SELECT a.account_key as account_key, SUM(p.revenue) as revenue
                            FROM accounts a
                            JOIN pnl p ON p.account_key = a.account_key
                            GROUP BY p.account_key) 
                        as total ON total.account_key = acct.account_key
                    SET acct.total_revenue = total.revenue;`;

  console.log('UPDATE QUERY STR: ', queryStr);

  connection
    .query(queryStr)
    .then(results => {
      console.log('total_revenue update success. => ', results);
      connection.end();
    })
    .catch(error => {
      if (error) {
        console.log(error);
      }
    });
  // await connection.end();
})();
