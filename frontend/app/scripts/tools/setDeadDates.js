const mysql = require("promise-mysql");
const moment = require("moment-timezone");
let dbConnection = require("../../../../services/dbconnector");

const [, , ...args] = process.argv;

exports.setDeadDates = async function() {
  let connection = await dbConnection.init();

  var queryStr = `select * from accounts;`;
  let allDeets = [];

  connection
    .query(queryStr)
    .then(async results => {
      // allDeets = allDeets.concat(getAccountDetails(acc.account_key));
      await Promise.all(
        results.map(async acc => {
          allDeets.push(acc);
        })
      );
      // console.log("got the results from the accounts Table ", allDeets);
      //console.log("Get Available Accounts res: ", results);
      return allDeets;
    })
    .then(async accounts => {
      for (let acc of accounts) {
        let query2 = `select * from account_history where account_key = ${
          acc.account_key
        };`;

        await connection.query(query2).then(history => {
          let deadDate;
          acc.history = history
            .sort(function(a, b) {
              return a.created < b.created ? -1 : b.created < a.created ? 1 : 0;
            })
            .map(item => {
              item.created = moment(item.created)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss");
              return item;
            });

          if (acc.history.length) {
            if (
              acc.status_key === 26 ||
              acc.status_key === 37 ||
              acc.status_key === 38 ||
              acc.status_key === 43 ||
              acc.status_key === 44
            ) {
              //loop through and find the dead status
              for (let h = 0; h < acc.history.length; h++) {
                if (
                  acc.history[h].status_key === 26 ||
                  acc.history[h].status_key === 37 ||
                  acc.history[h].status_key === 38 ||
                  acc.history[h].status_key === 43 ||
                  acc.history[h].status_key === 44
                ) {
                  deadDate = acc.history[h].created;
                  console.log("deadDate => ", deadDate);

                  break;
                }
              }

              let updateQuery =
                "update `accounts` set `dead` = " +
                `"${deadDate}"` +
                " where `account_key` = " +
                acc.account_key +
                ";";
              console.log("updateQuery in script => ", updateQuery);
              connection.query(updateQuery).then(res => {
                // console.log("AllAccounts after the launch var updated => ", res);
                // console.log("results of update query! => ", res);
                // connection.end();
              });
            }
          }

          // console.log("accounts check => ", acc);
        });
      }
      connection.end();
    })
    .catch(error => {
      if (error) {
        console.log(error);
      }
    });
};

require("make-runnable");

// update accounts set launch_success = 1 where account_key = "96";
