let express = require("express");
let mysql = require("mysql");
let sqlString = require("sqlstring");
let moment = require("moment-timezone");
let dbConnection = require("../../../../services/dbconnector.js");
let transform = require("../../../../services/transforms");
// let pnlService = require("./pnl");

let request = require("request-promise");

(() => {
  var date1 = moment()
    .tz("America/Chicago")
    .subtract(5, "hours")
    .format();
  console.log("date1 => ", date1);
  // Get start date
  var date = new Date();
  //date.setDate(date.getDate() + 1.3); // +1.3 days because this corrects for when DST changes. Moment adds 24 hours, but the DST day has 25 hours so it considers it the same day.
  var start = moment
    .utc(date)
    .tz("America/Chicago")
    .format();
  console.log("date => ", date, " start => ", start);

  // Get hour offset
  var starthour = "T06:00:00";
  if (
    moment([date.getFullYear(), date.getMonth(), date.getDate()])
      .tz("America/Chicago")
      .isDST()
  ) {
    starthour = "T05:00:00";
  }

  // Get end date
  date.setDate(date.getDate() + 1); // +1.3 days because this corrects for when DST changes. Moment adds 24 hours, but the DST day has 25 hours so it considers it the same day.
  var end = moment
    .utc(date)
    .tz("America/Chicago")
    .format();
  console.log("2nd date => ", date, " start => ", end);

  // Get hour offset
  var endhour = "T06:00:00";
  if (
    moment([date.getFullYear(), date.getMonth(), date.getDate()])
      .tz("America/Chicago")
      .isDST()
  ) {
    endhour = "T05:00:00";
  }
  console.log("starthour => ", starthour, " endhour => ", endhour);
})();
