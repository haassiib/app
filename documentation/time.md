﻿# All things time, date, datetime, and timezone related.

Central time - UTC -6

EC2 default - UTC

Voluum - UTC- 5

##RFC 3339       Date and Time on the Internet: Timestamps       July 2002


###5.8. Examples

	Here are some examples of Internet date/time format.

		1985-04-12T23:20:50.52Z

	This represents 20 minutes and 50.52 seconds after the 23rd hour of
	April 12th, 1985 in UTC.

		1996-12-19T16:39:57-08:00

	This represents 39 minutes and 57 seconds after the 16th hour of
	December 19th, 1996 with an offset of -08:00 from UTC (Pacific
	Standard Time).  Note that this is equivalent to 1996-12-20T00:39:57Z
	in UTC.

		1990-12-31T23:59:60Z

	This represents the leap second inserted at the end of 1990.

		1990-12-31T15:59:60-08:00

	This represents the same leap second in Pacific Standard Time, 8
	hours behind UTC.