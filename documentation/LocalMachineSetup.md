﻿# Local Machine Installation and Setup Documentation
Preface
This documentation gives a detailed explanation of the basic development environment setup.  I will provide links and instructions to download and install the necessary software to get up and running for development and collaboration on the Mencius application.

Software that will be needed
MySQL 5.7+
MySQL Workbench
Git 2.10.1.windows.1
Node/NPM 6.10.0/3.10.10
IDE VS2017/Atom

Database
Download Mysql
https://dev.mysql.com/downloads/mysql/
Run installer
NOTE: keep track of the credentials you enter
Download Mysql Workbench
https://dev.mysql.com/downloads/workbench/
Connect to Staging Server
Connection Name: Mencius-stagin
Hostname:	mencius-stage.c50kkl7jqor1.us-east-1.rds.amazonaws.com
Port:		3306
Username:   menciusadminstg
Password:   ccSTG455455%
Create Export Image - Save Image locally
Server
Data Export


Create new Database
Run Script
Verify Data / Set Default to ‘sys’


NODE
Download Node https://nodejs.org/en/download/
For Mac download XCode
https://itunes.apple.com/us/app/xcode/id497799835?mt=12 
Download Homebrew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
> brew update
Install Node 
For Mac: (> brew install node / >brew upgrade node)
Verify Node and NPM (node -v / npm -v)
IDE

Download IDE
NOTE: You may use any ide you like for development. The instructions outlined here may still contain useful information, but are specifically geared towards Visual Studio 2017.
VS Community - https://www.visualstudio.com/thank-you-downloading-visual-studio/?sku=Community&rel=15 
VS Code - https://code.visualstudio.com/?wt.mc_id=vscom_downloads
Atom - https://atom.io/ 
Sublime - https://www.sublimetext.com/3 
Install IDE
Open IDE -> File -> Open Existing Project
Open Repo location
Begin Editing!

Code Repository
Github Credentials
Username - caviar89
Password - June2015
Repo Link:
https://github.com/dennistouchet/Mencius/
Download Git
Win: https://git-scm.com/download/win
Mac: https://git-scm.com/download/mac 
Linux: 	https://git-scm.com/download/linux 
Follow Installation Instructions 
Run the following to get the latest version of the repo
git clone github.com/dennistouchet/Mencius.git 

Running The Code
Open Cmd/Terminal
Navigate to Main Application Directory
e.g. > cd  c:/Users/MyUser/Documents/Mencius/ 
> npm install
> node app.js

NOTE: See Application Deployment Documentation to submit code changes.

FUTURE
Install nodemon in dev dependencies (if its not already there.)
Run from nodemon to allow watch on change and hot update.
