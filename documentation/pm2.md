﻿
# Start/Monitor Application

	> pm2 start app.js --name "mencius"
	
	> pm2 show mencius

# Log File Path

/home/ec2-user/.pm2/pm2.log
/home/ec2-user/.pm2/logs/mencius-out-0.log
/home/ec2-user/.pm2/logs/mencius-error-0.log

