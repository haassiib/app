﻿# Amazon EC2 Server Statuc Site 
 
### 0) Login to the EC2 Amazon Console
> https://console.aws.amazon.com/console/home

### 1) Create new EC2 Instance
> Clicking `Launch Instance`
> Select the Default Linux AMI
> Select t2.micro *free teir eligable*
> If a `Web DMZ` security group exists, select that, else see the next step.

### 1a) In the EC2's Security group section add an inbound rule to open ports `80` and `443`
> Type: `HTTP`, Protocol: `TCP`, Port: `80`, Source: `0.0.0.0/0`  
> Type: `HTTPS`, Protocol: `TCP`, Port: `443`, Source: `0.0.0.0/0`

### 2) Copy the Public DNS for your EC2 instance
> `ec2-##-##-##-##.us-west-2.compute.amazonaws.com`  

### 4) SSH into your EC2 instance and install `nginx`
> $ `sudo yum install nginx`

### 5) Start the `nginx` web server
> $ `sudo /etc/init.d/nginx start`

### 6) Change file permission to your EC2 Instance 
> `cd /usr/shar/nginx/`
> `sudo chmod 777 html`

### 7) Copy your web files to the `nginx` html folder on the EC2 instance 
> `/usr/share/nginx/html/`  

### 8) Restart the nginx server 
> `service nginx restart`

### 8) Restart the nginx server 
> `service nginx restart`

### 9) Set DNS to point to EC2 instance IP 
> Copy `Public IP` from the EC2 dashboard. (e.g. `192.168.0.1`)
> Set `A Name Records` for both @ and www, and set the timeout to Automatic.

<br/>  

## Notes

### config file location
> `/etc/nginx/conf.d/`
	Change root path

### nginx commands
> `service nginx status`
> `service nginx restart`