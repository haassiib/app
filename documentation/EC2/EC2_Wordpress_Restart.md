# EC2 Wordpress Instance Restart

Best way to approach this is to:

Step 1: After you stop & start instance again , get the new PUBLIC IP Address
Step 2: FTP into the server and edit functions.php file in the current active theme >> /var/www/html/wp-content/themes/xxx (theme name)/functions.php and add this line after <?php" line.

update_option( 'siteurl', 'http://X.X.X.X/domain name� ); >> IP address/Domain name
update_option( 'home', 'http://X.X.X.X/domain name' );  >> IP address/Domain name

Step 3: Load the login or admin page a couple of times�Once you login into Wp-admin with  new IP address..remove the above lines that we added�in functions.php and change SiteURL/Home from Wp-admin


## Issue and Resolution

		[8/31/2017 4:45:11 PM] Mike Yohe: Hey, so I have followed these directions, and I am still having the same issue. the IP I am working with is: http://54.204.69.185/
		[8/31/2017 4:46:07 PM] Mike Yohe: I turned the instance on, FTPd in, found the function.php for the theme, added those lines and changed the ip/domain to the ones were using (healthdance.win)
		[9/3/2017 2:21:02 PM] Mike Yohe: Any idea about this?
		[9/3/2017 2:54:19 PM] Stan Gershengoren: Hey Nitni, can you please let us know asap
		[10:42:26 AM] Stan Gershengoren: Hey nitin
		[10:42:30 AM] Stan Gershengoren: Can you please look into this asap
		[11:08:28 AM] nitin7181: Stan - this is becoming difficult to work with..instructions were very clear...

1. healthdance.win - DNS is still pointing to wrong IP address
2. functions.php was supposed to be set like instructions below - using either IP address or domain name
update_option( 'siteurl', 'http://X.X.X.X/domain name� ); >> IP address/Domain name
update_option( 'home', 'http://X.X.X.X/domain name' );  >> IP address/Domain name

What really was :

update_option( 'siteurl', '54.204.69.185/healthdance.win�);
update_option( 'siteurl', '54.204.69.185/healthdance.win');