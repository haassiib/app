﻿#Page Documentation 
##To Include Individual Page Descriptions, Functionality, and Business Rules

##Active Accounts

	Admin
		Display All Accounts to current Admin User

	User 
		Displays All of the Accounts that belongs to ONLY the current User

	*Account Ordering should be 'account.status' [Active] -> 'account.status' Dead
	*Should be able to change account.status from 'account.status' dropdown and *Save*
	*Should be able to click 'account.name' and be directed to ~Edit Account~ page


##Edit Accounts

	Admin
		View/Edit all fields available for the account

	User
		View All/Edit most fields available for the account
	Tabs

##Add Uploadset

	Select account

	Create Adset

	Create Ads for Adset

	Duplicate Adsets/Ads

##Add Resources

	Ability to Add Credit Cards, Urls, Accounts

	Futur: Angles, Verticals, Countries

##Resources

##Search

	NEEDS REQUIREMENTS
	
##P&L

	NEEDS REQUIREMENTS

##Report Bug

	Allow Any User to Add Details of an Issue they are currently having.

##Profile

	Allow User to Change Profile informtion
	Allow User to reset password