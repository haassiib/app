# AWS Linux AMI Setup for new instance

##setup ec2
- create account
- create iam user
- create key pair

create EC2 instance defaults

## SSH Connect to EC2 instance

- download/install putty

	> https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/putty.html

- use Puttygen to create ppk

## get node setup

	> curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
	> . ~/.nvm/nvm.

-download npm version

	> nvm install 6.10.0

## find version

	> node -e "console.log('Running Node.js ' + process.version)"

## Clone Git Repo

	> git clone https://github.com/[repo]/[project].git [dest_folder]

### Note: MIME issues seem to sprout up - caused by missing files/incorrect file name casing

# Linux Process Commands

	> ps -e|grep node
	
# Port 80

## Redirect port 80 to port 3000 with this command:

		sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 3000

	Launch my Node.js on port 3000. Requests to port 80 will get mapped to port 3000.

## Edit your /etc/rc.local file and add that line minus the sudo. That will add the redirect when the machine boots up. You don't need sudo in /etc/rc.local because the commands there are run as root when the system boots.
## Add your Node.js start script to the file you edited for port redirection, /etc/rc.local. That will run your Node.js launch script when the system starts.

## Setup For Windows

npm install
npm install typings

cd frontend

bower install

	#### Might need to rename


#Port 80

	What I do on my cloud instances is I redirect port 80 to port 3000 with this command:

		### sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 3000

	Then I launch my Node.js on port 3000. Requests to port 80 will get mapped to port 3000.

	You should also edit your /etc/rc.local file and add that line minus the sudo. That will add the redirect when the machine boots up. You don't need sudo in /etc/rc.local because the commands there are run as root when the system boots.

##Logs

	Use the forever module to launch your Node.js with. It will make sure that it restarts if it ever crashes and it will redirect console logs to a file.

##Launch on Boot

	Add your Node.js start script to the file you edited for port redirection, /etc/rc.local. That will run your Node.js launch script when the system starts.

##Digital Ocean & other VPS

	This not only applies to Linode, but Digital Ocean, AWS EC2 and other VPS providers as well. However, on RedHat based systems /etc/rc.local is /ect/rc.d/local.

## MONITORING

	### Arpwatch	
	
	#### chkconfig --level 35 arpwatch on	
	
	#### /etc/init.d/arpwatch start
	
    /etc/rc.d/init.d/arpwatch : The arpwatch service for start or stop daemon.
    /etc/sysconfig/arpwatch : This is main configuration file�
    /usr/sbin/arpwatch : Binary command to starting and stopping tool via the terminal.
    /var/arpwatch/arp.dat : This is main database file where IP/MAC addresses are recorded.
    /var/log/messages : The log file, where arpwatch writes any changes or unusual activity to IP/MAC.
