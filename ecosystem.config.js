module.exports = {
  apps: [
    {
      name: "mencius",
      script: "./app.js",
      log_date_format: "YYYY-MM-DD HH:mm Z",
      error_file: "./pm2Logs/err.log",
      out_file: "./pm2Logs/out.log"
    }
  ]
};
