let express = require('express');
let bodyParser = require('body-parser');
let methodOverride = require('method-override');
let cors = require('cors');
let app = express();
let path = require('path');
let config = require('./services/config/config.json');
const https = require('https');
const http = require('http');
const fs = require('fs');
const redirectToHTTPS = require('express-http-to-https').redirectToHTTPS;
const moment = require('moment-timezone');
const { URL } = require('url');
const axios = require('axios');

let env = config.env;

if (env === 'production') {
  sslOptions = {
    key: fs.readFileSync(config.sslpath + '/privkey.pem'),
    cert: fs.readFileSync(config.sslpath + '/fullchain.pem'),
    passphrase: config.passphrase
  };

    var httpServer = http.createServer(app).listen(3000);
    console.log('Using https... 3000');
  //var server = https.createServer(sslOptions, app).listen(3443);
  //testing commenting out to and handling in nginx cut down on redirects
  //app.use(redirectToHTTPS([], [], 301));

  //console.log('Using https... 443');
} else if (env === 'dev') {
  http.createServer(app).listen(3000);
  console.log('Using http... 3000');
}

app.use(cors());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(methodOverride());
app.use(express.static(path.join(__dirname + '/frontend/app')));
app.use(app.router);

console.log('Mencius Started...');
let dbConnection = require('./services/dbconnector');
// Sanity Test Database Connection
//const dbTester = async () => {
//  let connection = await dbConnection.init();
//  connection.end();
//};
//dbTester();
// dbConnection.closeConnection(connection);

// Middleware
function requireAuth() {
  return async function(req, res, next) {
    if (req.headers['x-auth'] != null) {
      let authtoken = req.headers['x-auth'];

      let db = await dbConnection.getDbConnection();
      if (!db) {
        return res.status(500).send('Failed to initialize the db.');
        //TODO: add token code
        db.end();
      }
    } else {
      return res.status(403).send('no x-auth');
      db.end();
    }
  };
}

const userService = require('./services/userauth');
const employeeService = require('./services/employees');
const utilityService = require('./services/utility');
const accountService = require('./services/accounts');
const campaignService = require('./services/campaigns');
const contentService = require('./services/content');
const accountCodeService = require('./services/accountcodes');
const accountCommentService = require('./services/accountcomments');
const safeUrlService = require('./services/safeurls');
const verticalService = require('./services/verticals');
const batchService = require('./services/batches.js');
const countryService = require('./services/countries');
const pnlService = require('./services/pnl');
const voluumService = require('./services/voluum');
const emailService = require('./services/email');
const notificationService = require('./services/notifications');
const virtualMachineService = require('./services/virtualMachines.js');
const analyticsService = require('./services/analytics');
const launchService = require('./frontend/app/scripts/tools/setLaunchRates');
const deadDateService = require('./frontend/app/scripts/tools/setDeadDates');
const adespressoService = require('./services/adespresso');
const searchService = require('./services/search');
const datatableService = require('./services/datatable');

app.all('*', function(req, res, next) {
  /**
   * Response settings
   * @type {Object}
   */
  var allowedOrigins = [
    'http://127.0.0.1:3000',
    'http://localhost:3000',
    'http://127.0.0.1:443',
    'http://localhost:443',
    'http://app.confucius.marketing',
    'https://app.confucius.marketing',
    'https://app2.confucius.marketing'
  ];
  let origin = req.headers.origin;

  if (allowedOrigins.indexOf(origin) > -1) {
    res.setHeader('Access-Control-Allow-Origin', origin);
  }
  let responseSettings = {
    AccessControlAllowHeaders:
      'Content-Type,X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name',
    AccessControlAllowMethods: 'POST, GET, PUT, DELETE, OPTIONS',
    AccessControlAllowCredentials: true
  };

  // Headers
  res.header('Access-Control-Allow-Credentials', responseSettings.AccessControlAllowCredentials);
  // res.header(
  //   "Access-Control-Allow-Origin",
  //   responseSettings.AccessControlAllowOrigin
  // );
  res.header(
    'Access-Control-Allow-Headers',
    req.headers['access-control-request-headers']
      ? req.headers['access-control-request-headers']
      : 'x-requested-with'
  );
  res.header(
    'Access-Control-Allow-Methods',
    req.headers['access-control-request-method']
      ? req.headers['access-control-request-method']
      : responseSettings.AccessControlAllowMethods
  );
  //console.log("req method: ", req.method);

  if ('OPTIONS' === req.method) {
    console.log('OPTIONS method');

    res.send(200);
  } else {
    next();
  }
});

app.get('/', function(req, res) {
  console.log(__dirname);
    res.sendfile(__dirname + '/frontend/app/login.html');
});

//API Endpoint for Thank You Pixel Cloaking
app.get('/api/safeurl', async function(req, res) {
  let pid = req.param('pid');
  console.log('pid => ', pid);

  let queryStr = `SELECT a.account_key as account_key, s.url as url, a.pixel as pixel
  FROM sys.accounts a
  join sys.safe_urls s 
  on s.safe_url_key = a.safe_url_key 
  where a.pixel = ${pid};`;

  let dbase = await dbConnection.init();
  if (!dbase) {
    return res.status(500).send('Failed to initialize the db.');
  } else {
    await dbase.query(queryStr).then(results => {
      let safeUrl = new URL(results[0].url);
      let hostname = safeUrl.hostname;
      let pixel = results[0].pixel;
      let pathname = safeUrl.pathname;
      let filename = path.basename(pathname);

      if (results[0].url.indexOf('shopify') > -1) {
        let shopifyRedirectUrl = `https://${hostname}${pathname.replace(
          filename,
          ''
        )}thank-you/one.php?pid=${pixel}&link=${hostname}`;

        console.log('test url to redirect to => ', shopifyRedirectUrl);
        res.redirect(shopifyRedirectUrl);
      } else {
        let otherRedirectUrl = `https://${hostname}/thank-you/one.php?pid=${pixel}&link=${hostname}`;

        console.log('test url to redirect to => ', otherRedirectUrl);
        res.redirect(otherRedirectUrl);
      }
      dbase.end();
    });
  }
});

/*Bug Reporting*/
app.post('/bugs', utilityService.reportBug());

/* User Management*/
app.post('/user/login', userService.login());
app.post('/signup', userService.signUp());
app.put('/user/profile', userService.resetPassword());
app.get('/user/settings', employeeService.getEmployeeSettings());
app.get('/usersAdmin', userService.getUsers());
app.put('/user/settings', employeeService.updateEmployeeSettings());

/* Campaign */
app.get('/campaigns', campaignService.getCampaigns());
app.put('/campaigns', campaignService.updateCampaigns());
app.get('/campaigns/totalspend', campaignService.getTotalSpendByCampaign());
app.get('/campaigns/roi_by_account', campaignService.getCampaignSpending());

/* Upload Copy */
app.post('/uploadset', campaignService.addUploadset());

/* Task */
app.get('/tasks', campaignService.getTasks());

/* Search */
app.get('/search', searchService.getAccountsBySearchTerm());

/* Accounting */
app.get('/accounts', accountService.getAccounts());
app.get('/accounts/warming', accountService.getWarmingAccounts());
app.get('/accounts/ready', accountService.getReadyAccounts());
app.put('/accounts', accountService.updateAccounts());
app.post('/accounts', accountService.insertAccount());
app.delete('/accounts', accountService.deleteAccounts());
app.get('/accounts/search', accountService.searchAccounts());
app.get('/account/code/:account_key', accountCodeService.getAccountCode());
app.get('/account/codes/:all', accountCodeService.getAccountCodes());
app.get('/account/angles', accountService.getAngles());
app.post('/account/codes/', accountCodeService.postAccountCodes());
app.put('/account/code/', accountCodeService.putAccountCode());
app.put('/account/audit', accountService.updateAccountAudit());
app.get('/accounts/codes/history/:account_key', accountCodeService.getCodeHistory());
app.post('/accounts/codes/history', accountCodeService.postCodeHistory());
app.post('/account/copy/', accountService.insertAccountCopy());
app.put('/account/copy/:account_key', accountService.updateAccountCopy());
app.delete('/account/copy/:account_key', accountService.deleteAccountCopy());
app.post('/account/info/content', accountService.getAccountByContentId());
app.put('/account/info/update/:account_key', accountService.updateAccountInfo());
app.put('/account/safe_url/update/:account_key', accountService.updateAccountSafeUrl());
app.put('/account/code/update/:account_key', accountService.updateAccountCode());
app.put('/accounts/spend', accountService.updateAccountTotalSpend());
app.put('/accounts/budget', accountService.updateAccountBudget());
app.put('/account/history', accountService.updateAccountHistory());
app.get('/account/comments/:account_key', accountCommentService.getAccountComments());
app.post('/account/comment/', accountCommentService.insertAccountComment());
app.post('/accounts/comments/', accountCommentService.insertAccountsComments());
app.get('/edit_account/:account_key', accountService.getAccountDetails());

app.get('/edit_copy/:account_key', accountService.getAccountCopy());

app.get('/adset/:adset_key', contentService.getAdsetContent());

app.get('/adset', contentService.getAdsets());

/* Content */
app.get('/content/performant', contentService.getPerformantContent());
app.put('/content/performant/:content_key', contentService.putPerformantContent());
app.delete('/content/:content_key', contentService.deleteContent());

//TODO: FIX. public facing
app.get('/safe_urls', safeUrlService.getSafeUrls());
app.post('/safe_urls', safeUrlService.createSafeUrl());
app.put('/safe_urls/update/', safeUrlService.updateSafeUrls());
app.get('/safe_urls/detailed', safeUrlService.getDetailedSafeUrls());

/* Status*/
app.get('/status', campaignService.getStatus());

/*Batches*/
app.get('/batches', batchService.getBatches());
app.post('/batches', batchService.insertBatches());
app.put('/batches', batchService.updateBatches());
app.delete('/batches/:batch_key', batchService.deleteBatch());
app.delete('/batches', batchService.deleteBatches());

/* Virtual machine*/
app.get('/vms', virtualMachineService.getVms());
app.post('/vms', virtualMachineService.insertVms());

/* Verticals*/
app.get('/verticals', verticalService.getVerticals());
app.get('/verticals/all', verticalService.getCompleteVerticals());
app.post('/verticals', verticalService.insertVerticals());

/* Countries*/
app.get('/countries', countryService.getCountries());

/* Profit And Loss*//* Profit And Loss*/
app.get('/pnl', pnlService.getPnl());
app.get('/pnl/:account_key', pnlService.getPnlByAccount());
app.get('/pnl/days/:account_key', pnlService.getNDayPnlByAccount());
app.get('/pnl/days/roi', pnlService.getNDaysPnlForROI());
app.post('/pnl', pnlService.insertPnlItems());
app.put('/pnl', pnlService.updatePnlItems());
app.put('/pnl/expenses', pnlService.updatePnlExpensesByAccount());
app.post('/pnl/new', pnlService.insertPnlItemByAccount());

/* Payouts*/
app.get('/payouts', pnlService.getPayoutsByRangeAndOwner());

app.get('/accounts/audit/:account_key', accountService.auditDate());

/* Profit And Loss*/
app.get('/voluum/auth', voluumService.authVoluum());
app.get('/voluum/campaigns', voluumService.getCampaigns());
app.get('/voluum/report/:date_range', voluumService.getReports());

app.get('/notifications', notificationService.getNotifications());
app.get('/launch_rates', accountService.getAccounts());
app.get('/accounts/history', accountService.getAllAccountsHistory());

/* Analytics */
app.get(
  '/analytics/account_life/:dtFrom/:dtTo',
  analyticsService.getAnalyticsDataByDateRangeForAccountLife()
);

app.get(
  '/analytics/launch_rates/:dtFrom/:dtTo',
  analyticsService.getAnalyticsDataByDateRangeForLaunchRates()
);

app.get(
  '/adespresso/lifetime/:account_key/:adespressoId',
  adespressoService.getLifetimeSpendByAccount()
);

let schedule = require('node-schedule');
/*
 TEST AREA
console.log("Scheduled Events starting...");
let eo = schedule.scheduleJob('0 51 * * * *', function () {
    console.log("HOURLY OFFER CHECK EMAIL JOB.");
    //emailService.testEmail();
    emailService.sendSuspiciousOfferEmail(voluumService);
});

*/
//voluumService.reconcileDay(["2018-03-03", "2018-03-04", "2018-03-05", "2018-03-06", "2018-03-07", "2018-03-08", "2018-03-09", "2018-03-10", "2018-03-11", "2018-03-12", "2018-03-13", "2018-03-14", "2018-03-15", ]);
if (env == 'production') {
  /* SCHEDULED JOBS */

  let zz = schedule.scheduleJob('2 7 * * *', function() {
    console.log('Set Dead Dates');
    deadDateService.setDeadDates();
  });

  console.log('Scheduled Events starting...');
  // seconds, minutes, hours, day of month, month, day of week
  // 7:1::10 AM UTC or 2:1:10 AM CST
  let s = schedule.scheduleJob('1 7 * * *', function() {
    console.log('INSERT DAILY PNL JOB');
    pnlService.insertDailyPnl();
  });

  let dp = schedule.scheduleJob('10 7 * * *', function() {
    console.log('UPDATE PNL WITH VOLUUM BY DATE JOB');
    let today = moment().format('YYYY-MM-DD');
    today += 'T07:10:01';
    pnlService.updatePnlWithVoluumByDate(today);
  });

  let dy = schedule.scheduleJob('15 7 * * *', function() {
    console.log('UPDATE PNL WITH VOLUUM BY DATE JOB FOR YESTERDAY');
    let yesterday = moment()
      .subtract(1, 'days')
      .format('YYYY-MM-DD');
    yesterday += 'T07:15:01';
    pnlService.updatePnlWithVoluumByDate(yesterday);
  });

  let dz = schedule.scheduleJob('20 7 * * *', function() {
    console.log('UPDATE PNL WITH VOLUUM BY DATE JOB FOR YESTERDAY');
    let dayBeforeYesterday = moment()
      .subtract(2, 'days')
      .format('YYYY-MM-DD');
    dayBeforeYesterday += 'T07:15:01';
    pnlService.updatePnlWithVoluumByDate(dayBeforeYesterday);
  });

  let dw = schedule.scheduleJob('20 9 * * *', function() {
    console.log('UPDATE PNL WITH VOLUUM BY DATE JOB FOR WEEK AGO');
    let weekAgo = moment()
      .subtract(7, 'days')
      .format('YYYY-MM-DD');
    weekAgo += 'T07:20:01';
    pnlService.updatePnlWithVoluumByDate(weekAgo);
  });

  const lr = schedule.scheduleJob('1 10 * * *', function() {
    console.log('UPDATE LAUNCH RATES DAILY');
    launchService.setLaunchRates();
  });

  //changed this from 11 to 7 to see if this is the function effecting the dupes on 11/19/18
  let sb = schedule.scheduleJob('1 7 * * *', function() {
    console.log('UPDATE DAILY PNL EXPENSES JOB');
    pnlService.updatePnlExpensesDaily();
  });

  // let dr = schedule.scheduleJob("1 15 12 * * *", function() {
  //   console.log("RECONCILE YESTERDAY PNL");
  //   voluumService.yesterdayPnl();
  // });

  // let wr = schedule.scheduleJob("1 15 13 * * *", function() {
  //   console.log("RECONCILE WEEK PNL");
  //   voluumService.weekPnl();
  // });
  //Supposed to run at 830 CST
  // let v = schedule.scheduleJob("0 30 14 * * *", function() {
  //   console.log("UPDATE PNL /W VOLUUM JOB.");
  //   voluumService.UpdatePnlWithVoluum();
  // });

  let e = schedule.scheduleJob('0 10 * * *', function() {
    console.log('DAILY SS EMAIL JOB.');
    //emailService.testEmail();
    emailService.sendPerformantContentEmail(pnlService);
  });

  let rx = schedule.scheduleJob('0 10 * * *', function() {
    console.log('DAILY POOR ROI EMAIL JOB.');
    emailService.sendPoorAccountROIEmail(pnlService);
  });

  // let eo = schedule.scheduleJob("0 5 * * * *", function() {
  //   console.log("HOURLY OFFER CHECK EMAIL JOB.");
  //   //emailService.testEmail();
  //   emailService.sendSuspiciousOfferEmail(voluumService);
  // });

  let t = schedule.scheduleJob('2 5 * * *', function() {
    console.log('UPDATE TOTAL SPEND JOB');
    accountService.updateDailyTotalSpend((result, error) => {
      if (error) {
        console.log('total update failed');
      } else {
        console.log('total update successful');
      }
    });
  });

  let txt = schedule.scheduleJob('22 * * * *', function() {
    console.log('UPDATE TOTAL REVENUE JOB');
    accountService.updateDailyTotalRevenue((result, error) => {
      if (error) {
        console.log('total update failed');
      } else {
        console.log('total update successful');
      }
    });
  });

  let txx = schedule.scheduleJob('0 */12 * * *', function() {
    console.log('UPDATE Days Alive JOB');
    accountService.updateDaysAliveDaily((result, error) => {
      if (error) {
        console.log('total update failed');
      } else {
        console.log('total update successful: ', result);
      }
    });
  });
}
