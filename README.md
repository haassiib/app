# Mencius
Confucius Marketing Administration Portal

## Setup

-Download and install MySQL 6.3.9

-Download and install MySQL Server 5.7.17

-Download and Install Node if you have not already done so.

-Run the following command to create a Mencius folder and clone the current project into the Mencius folder: 
    git clone https://github.com/dennistouchet/Mencius.git

-cd Mencius

-npm install -g -save

-cd frontend

-bower -install -g -save

-cd ..

-node app.js

-Open a browser and navigate to localhost:3000
