let db = require('../services/dbconnector');
const { getAccountsBySearchTermForSearchPageQuery } = require('./queries/index');

let config = require('./config/config.json');
let guid = config.guid || '';

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.split(search).join(replacement);
};

exports.getAccountsBySearchTerm = function() {
  return async function(req, res) {
    // console.log('req.params.term => ', req.params.term);

    let connection = await db.init();
    queryWithTerm = getAccountsBySearchTermForSearchPageQuery;

    // console.log('queryWithTerm: ', queryWithTerm);
    await connection
      .query(queryWithTerm)
      .then(results => {
        if (results) {
          console.log('Number of results in search.js getAccountsBySearchTerm => ', results.length);
          connection.end();
          res.status(200).send(results);
        }
      })
      .catch(error => {
        if (error) {
          console.log('search.js getAccountsBySearchTerm error: ', error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};
