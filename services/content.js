﻿"use strict";

let express = require("express");
let mysql = require("mysql");
let sqlString = require("sqlstring");
let dbConnection = require("../services/dbconnector");

exports.getAdsets = function() {
  return async function(req, res) {
    let queryStr = "SELECT * FROM adsets;";
    var connection = await dbConnection.init();

    connection
      .query(queryStr)
      .then(results => {
        //console.log("Get Content res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getContent = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);

    var queryStr = "SELECT * FROM content;";
    var connection = await dbConnection.init();
    connection
      .query(queryStr)
      .then(results => {
        //console.log("Get Content res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
    // await connection.end();
  };
};

exports.getPerformantContent = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);
    function getFilterVerticals(ads) {
      let verticalsWithDups = ads.map(a => a.vertical);
      let verticals = [];
      if (verticalsWithDups.length > 0) {
        for (let i = 0; i < verticalsWithDups.length; i++) {
          let exists = verticals.filter(c => c == verticalsWithDups[i]);
          if (exists.length === 0) verticals.push(verticalsWithDups[i]);
        }
      }
      return verticals;
    }

    function getFilterCountries(ads) {
      let countriesWithDups = ads.map(a => a.country);
      let countries = [];
      if (countriesWithDups.length > 0) {
        for (let i = 0; i < countriesWithDups.length; i++) {
          let exists = countries.filter(c => c == countriesWithDups[i]);
          if (exists.length === 0) countries.push(countriesWithDups[i]);
        }
      }
      return countries;
    }

    var queryStr = `select c.content_key, c.headline, c.body, c.link_to_graphic, c.newsfeed_link, c.comment, c.performant, c.disabled, c.created,
                        ads.adset_key, camp.campaign_key, a.account_key, s.status, a.fname, a.lname, a.angle, a.owner, a.audited, a.audit_revenue, v.name as vertical, ads.country, ads.sex, ROUND(a.total_spend) as total_spend, p.revenue, p.expenses 
                        from content c
                        JOIN adsets ads ON c.adset_key = ads.adset_key
                        JOIN campaigns camp on ads.campaign_key = camp.campaign_key
                        JOIN accounts a on camp.account_key = a.account_key
                        JOIN status s on a.status_key = s.status_key
                        JOIN verticals v on v.vertical_key = a.vertical_key
                        LEFT JOIN (SELECT content_key, SUM(revenue) as revenue, SUM(expenses) as expenses
									FROM pnl 
									GROUP BY content_key) AS p 
                                ON p.content_key = c.content_key
                                ORDER BY p.revenue DESC;`;

    var connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        results = calculatePerformantContentROI(results);
        let data = {
          ads: results
        };
        connection.end();
        res.status(200).send(data);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
    // await connection.end();
  };
};

function calculatePerformantContentROI(ads) {
  return ads.map(ad => {
    ad.profit = ad.revenue - ad.expenses;
    ad.roi =
      ad.profit === 0 || ad.expenses === 0
        ? 0
        : (ad.profit / ad.expenses) * 100;
    return ad;
  });
  // .sort((a, b) => b.roi - a.roi);
}

exports.putPerformantContent = function() {
  return async function(req, res) {
    console.log("Req body:", req.body.ad);
    console.log("Req params: ", req.params);
    let comment =
      req.body.ad.comment == null ? " NULL " : '"' + req.body.ad.comment + '"';
    let performant = req.body.ad.performant == true ? 1 : 0;

    let queryStr =
      "UPDATE content SET comment=" +
      comment +
      ", performant = " +
      performant +
      " WHERE content_key = " +
      req.params.content_key +
      ";";
    console.log(queryStr);

    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        //console.log("Get Content res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
    // await connection.end();
  };
};

exports.deleteContent = function() {
  return async function(req, res) {
    console.log("Req body:", req.body.ad);
    console.log("Req params: ", req.params);
    let c_key = req.params.content_key;

    //TODO: NEED TO DELETE ANY PNL FIRST

    let queryStr = "DELETE FROM content WHERE content_key = " + c_key + ";";
    console.log(queryStr);
    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        console.log("HERE IN RESULTS");
        //console.log("Get Content res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log("HERE IN ERROR");
          connection.end();
          res.status(500).send(error);
        }
      });
    // await connection.end();
  };
};

exports.getAdsetContent = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    console.log("Req params: ", req.params);
    let adset_key = req.params.adset_key;

    let queryStr = `select * from adsets where adset_key = ${adset_key};`;
    queryStr += ` select * from content where adset_key = ${adset_key};`;

    let connection = await dbConnection.init(true);
    await connection
      .query(queryStr)
      .then(results => {
        //console.log("Get Adset and Content res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
    // await connection.end();
  };
};
