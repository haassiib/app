"use strict";

var express = require("express");
var mysql = require("mysql");
var sqlString = require("sqlstring");
var moment = require("moment-timezone");
var dbConnection = require("../services/dbconnector");
var transform = require("../services/transforms");

exports.reportBug = function() {
  return async function(req, res) {
    var connection = dbConnection.init();
    var page = req.body.page;
    var description = req.body.description;
    var user = req.headers["x-username"];
    var queryStr =
      "INSERT INTO bugs (page, description, reported_by) VALUES " +
      '("' +
      page +
      '", "' +
      description +
      '", "' +
      user +
      '");';

    await connection
      .query(queryStr)
      .then(results => {
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          throw error;
        }
      });
    // await connection.end();
  };
};
