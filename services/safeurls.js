﻿"use strict";

let express = require("express");
let mysql = require("mysql");
let sqlString = require("sqlstring");
let moment = require("moment-timezone");

let dbConnection = require("../services/dbconnector");

exports.getSafeUrls = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    let role = req.headers["x-role"];
    let connection = await dbConnection.init();
    console.log("req.body", req.body);

    var queryStr = "SELECT * FROM safe_urls;";
    connection
      .query(queryStr)
      .then(results => {
        //console.log("getSafeUrls results: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (err) {
          console.log("safeurls.js getSafeUrls error: ", err);
          connection.end();
          res.status(500).send(err);
        }
      });
    // await connection.end();
  };
};

exports.createSafeUrl = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);
    let safeUrls = req.body.safeUrls;
    let queryAddition = "";
    // build query addition
    console.log("safeUrls", safeUrls);
    if (safeUrls.length < 1) {
      //TODO: update this object to be inline with SQL response
      res.status(200).send({
        message: "No Urls To Insert",
        affectedRows: 0,
        insertId: undefined
      });
      return;
    }

    for (let i = 0; i < safeUrls.length; i++) {
      let url =
        safeUrls[i].url == undefined || safeUrls[i].url === ""
          ? ""
          : '"' + safeUrls[i].url + '"';
      if (url === "") {
        //DO NOT ALLOW BLANK URLS
        //TODO: update this object to be inline with SQL response
        res.status(400).send({
          message: "No Urls To Insert",
          affectedRows: 0,
          insertId: undefined
        });
        return;
      }

      queryAddition += "(" + url + ', "no")';
      if (i != safeUrls.length - 1) {
        queryAddition += ", ";
      } else {
        queryAddition += ";";
      }
    }

    let queryStr =
      "INSERT INTO safe_urls (url, claimed) VALUES " + queryAddition;
    let connection = await dbConnection.init(true);

    console.log(queryStr);

    connection
      .query(queryStr)
      .then(results => {
        console.log("Create Safe URL res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
    // await connection.end();
  };
};

exports.updateSafeUrls = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    //console.log('req.body', req.body);
    let queryStr = "";

    req.body.safe_urls.forEach(safe_url => {
      if (safe_url.safe_url_key) {
        let url =
          safe_url.url == undefined || safe_url.url === ""
            ? ""
            : ' url = "' + safe_url.url + '",';
        let claimed =
          safe_url.claimed == undefined
            ? '"no",'
            : '"' + safe_url.claimed + '",';
        let status =
          safe_url.status == undefined
            ? '"open",'
            : '"' + safe_url.status + '",';

        queryStr +=
          "UPDATE safe_urls SET " +
          url +
          " claimed = " +
          claimed +
          " status =" +
          status +
          " modified = NOW()  WHERE safe_url_key = " +
          safe_url.safe_url_key +
          ";";
      }
    });

    console.log("Safe URL query str: ", queryStr);
    if (!queryStr.trim()) {
      // is empty or whitespace
      res
        .status(200)
        .send({ status: 200, message: "No safe url key in update query" });
      return;
    }

    let connection = await dbConnection.init(true);
    await connection
      .query(queryStr)
      .then(results => {
        //console.log("getSafeUrls results: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log("safeurls.js updateSafeUrls error: ", error);
          connection.end();
          res.status(500).send(error);
        }
      });
    // await connection.end();
  };
};

exports.getDetailedSafeUrls = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    let role = req.headers["x-role"];
    let connection = await dbConnection.init();
    console.log("req.body", req.body);

    var queryStr = `SELECT s.safe_url_key, s.url, s.status, s.claimed, s.created, 
                        s.modified, a.account_key, a.fname, a.lname, a.status_key, st.status as account_status
                        FROM safe_urls s
                        LEFT JOIN accounts a ON s.safe_url_key = a.safe_url_key
                        LEFT JOIN status st ON a.status_key = st.status_key
                        ORDER BY s.created desc, s.status desc;`;
    await connection
      .query(queryStr)
      .then(results => {
        //console.log("getSafeUrls results: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (err) {
          console.log("safeurls.js getSafeUrls error: ", err);
          connection.end();
          res.status(500).send(err);
        }
      });
    // await connection.end();
  };
};
