﻿"user strict";

let express = require("express");
let mysql = require("mysql");

let dbConnection = require("../services/dbconnector");

exports.getEmployee = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];

    let connection = await dbConnection.init();
    await connection
      .query('SELECT * FROM employee WHERE username="' + username + '";')
      .then(results => {
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log("employee.js getEmployee error:", error);
          connection.end();
          res.status(500).send(error);
        }
      });
    // await connection.end();
  };
};

exports.updateEmployee = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    let password = req.body.user.password;
    let employee_key = req.params.employee_key;

    let connection = await dbConnection.init();
    await connection
      .query(
        'UPDATE employee SET username="' +
          username +
          '", password= "' +
          password +
          '" WHERE employee_key = ' +
          employee_key +
          ";"
      )
      .then(results => {
        console.log("connected as id " + connection.threadId);
        console.log("successfully update employee to front-end");
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send("Update Employee Error: ", error);
        }
      });
    // await connection.end();
  };
};

exports.updateEmployeeSettings = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    let settings = req.body.userSettings;
    //console.log("USER SETTINGS: ", settings);
    let active_user_filter =
      typeof settings.userFilter == "undefined"
        ? " NULL "
        : '"' + settings.userFilter + '"';
    let active_status_filter =
      typeof settings.statusFilter == "undefined"
        ? " NULL "
        : '"' + settings.statusFilter + '"';
    let active_last_sort =
      typeof settings.lastSort == "undefined"
        ? " NULL "
        : '"' + settings.lastSort + '"';
    let active_last_order =
      typeof settings.lastOrder == "undefined"
        ? " NULL "
        : '"' + settings.lastOrder + '"';

    let queryStr =
      `INSERT INTO user_settings (employee_key, active_user_filter, active_status_filter, active_last_sort, active_last_order, modified) 
                        VALUES ( (select employee_key from employees where username =
                            "` +
      username +
      `" limit 1), ` +
      active_user_filter +
      ", " +
      active_status_filter +
      ", " +
      active_last_sort +
      `, ` +
      active_last_order +
      `, NOW())
                            ON DUPLICATE KEY 
                            UPDATE
                            active_user_filter = ` +
      active_user_filter +
      `,active_status_filter = ` +
      active_status_filter +
      `,active_last_sort = ` +
      active_last_sort +
      `,active_last_order = ` +
      active_last_order +
      `,modified = NOW();`;
    //console.log("QUERY STRING: ", queryStr);
    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        console.log("successfully retrieved user settings");
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
    // await connection.end();
  };
};

exports.getEmployeeSettings = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];

    let queryStr =
      'SELECT * FROM user_settings where employee_key = (SELECT employee_key FROM employees WHERE username = "' +
      username +
      '");';
    let connection = await dbConnection.init();
    connection.query(queryStr, function(error, results) {
      if (error) {
        console.log(error);
        connection.end();
        res.status(500).send("Get User Settings Error: ", error);
      } else {
        let result = transformSettings(results[0]);
        console.log("successfully retrieved user settings: ", result);
        connection.end();
        res.status(200).send(result);
      }
    });
    // await connection.end();
  };
};

function transformSettings(as) {
  if (typeof as === "undefined") return {};

  let userSettings = {
    statusFilter: as.active_status_filter,
    userFilter: as.active_user_filter,
    showAccount: as.active_sort_account,
    showVertical: as.active_sort_vertical,
    showAngle: as.active_sort_angle,
    showCountry: as.active_sort_country,
    showBudget: as.active_sort_budget,
    showSpend: as.active_sort_spend,
    showLaunched: as.active_sort_launched,
    showCC: as.active_sort_cc,
    showURL: as.active_sort_url,
    showStatus: as.active_sort_status,
    lastSort: as.active_last_sort,
    lastOrder: as.active_last_order
  };

  return userSettings;
}
