'use strict';

var express = require('express');
var mysql = require('mysql');
var sqlString = require('sqlstring');
var moment = require('moment-timezone');

exports.byAccount = function(query_results) {
  var number_of_ads = 1;
  var transformed_results = [];
  var ad_data = [];

  var pushAccount = function(i) {
    var temp_ad = {
      fname: query_results[i].fname,
      lname: query_results[i].lname,
      vertical: query_results[i].vertical,
      angle: query_results[i].angle,
      status: query_results[i].status,
      country: query_results[i].country,
      account_key: query_results[i].account_key,
      code_key: query_results[i].code_key,
      code_status: query_results[i].code_status,
      pixel: query_results[i].pixel,
      funding_type: query_results[i].funding_type,
      code_key: query_results[i].code_key,
      warmTime: query_results[i].warm_time,
      runTime: query_results[i].run_time,
      lifeTime: query_results[i].life_time,
      safeSpend: query_results[i].safe_spend,
      budget: query_results[i].budget,
      total_spend: query_results[i].total_spend,
      converstionEvent: query_results[i].conversion_event,
      interests: query_results[i].interests,
      proxy_type: query_results[i].proxy_type,
      owner: query_results[i].owner,
      source: query_results[i].source,
      safe_ad_count: query_results[i].safe_ad_count,
      safe_url_key: query_results[i].safe_url_key,
      safe_url: query_results[i].url,
      safe_url_status: query_results[i].safe_url_status,
      safe_url_claimed: query_results[i].safe_url_claimed,
      ads: query_results[i],
      media_buyer: query_results[i].media_buyer,
      last_account_comment: query_results[i].last_account_comment,
      recovered: query_results[i].recovered,
      created:
        query_results[i].created == null
          ? null
          : moment(query_results[0].created)
              .utc()
              .format(),
      creation:
        query_results[i].creation == null
          ? null
          : moment(query_results[0].creation)
              .utc()
              .format(),
      launched:
        query_results[i].launched == null
          ? null
          : moment(query_results[0].launched)
              .utc()
              .format(),
      initialBoost:
        query_results[i].initial_boost == null
          ? null
          : moment(query_results[0].initial_boost)
              .utc()
              .format(),
      modified:
        query_results[i].modified == null
          ? null
          : moment(query_results[0].modified)
              .utc()
              .format(),
      dead:
        query_results[i].dead == null
          ? null
          : moment(query_results[0].dead)
              .utc()
              .format()
    };
    //console.log("Temp Ad ", i , ' : ', temp_ad);
    transformed_results.push(temp_ad);
  };

  for (let i = 0; i < query_results.length; i++) {
    // check to see if the next row is for a different account.
    if (
      i !== query_results.length - 1 &&
      query_results[i].account_key !== query_results[i + 1].account_key
    ) {
      // different accounts
      pushAccount(i);
    }

    if (
      i !== query_results.length - 1 &&
      query_results[i].account_key === query_results[i + 1].account_key
    ) {
      // matching accounts
      // find out how many accounts match
      for (let j = i; j < query_results.length - 1; j++) {
        if (query_results[j].account_key === query_results[j + 1].account_key) {
          number_of_ads += 1;
        }
        if (query_results[j].account_key !== query_results[j + 1].account_key) {
          // break;
          j = query_results.length - 1;
        }
      }

      // build array of objects that contains ad data
      let k = i + number_of_ads;

      for (let j = i; j < k; j++) {
        ad_data.push(query_results[j]);
        // console.log('J: ' + j);
      }

      pushAccount(i);
      i = k - 1;
      ad_data = [];
    }

    // check if only one row
    if (query_results.length === 1) {
      pushAccount(i);
    }
    // check last row
    else if (
      i === query_results.length - 1 &&
      query_results[i].account_key !== query_results[i - 1].account_key
    ) {
      // different accounts
      pushAccount(i);
    }

    number_of_ads = 1; // reset number_of_ads back to 1 before next loop
  }
  return transformed_results;
};

exports.copyByAccount = function(query_results) {
  function Adset(row) {
    (this.account_key = row.account_key),
      (this.status_key = row.status_key),
      (this.safe_url_key = row.safe_url_key),
      (this.url = row.url),
      (this.vertical = row.vertical),
      (this.account_key = row.account_key),
      (this.campaign_key = row.campaign_key),
      (this.adset_key = row.adset_key),
      (this.disabled = row.adset_disabled),
      (this.sex = row.sex),
      (this.minAge = row.age_min),
      (this.maxAge = row.age_max),
      (this.interests = row.interests),
      (this.budget = row.budget),
      (this.audience = row.audience),
      (this.country = row.country),
      (this.mobile = row.mobile == 1 ? true : false),
      (this.desktop = row.desktop == 1 ? true : false),
      (this.instagram = row.instagram == 1 ? true : false),
      (this.network = row.network == 1 ? true : false),
      (this.rhs = row.rhs == 1 ? true : false),
      (this.ads = []);
  }

  function Ad(row) {
    (this.account_key = row.account_key),
      (this.adset_key = row.adset_key),
      (this.content_key = row.content_key),
      (this.headline = row.headline),
      (this.body = row.body),
      (this.link_to_graphic = row.link_to_graphic),
      (this.thumbnail = row.thumbnail),
      (this.comment = row.comment),
      (this.newsfeed_link = row.newsfeed_link),
      (this.disabled = row.content_disabled),
      (this.performant = row.performant);
  }

  var adsets = [];
  query_results.forEach(row => {
    if (adsets.length < 1) {
      // add if count is 1
      var adset = new Adset(row);
      adsets.push(adset);
    } else {
      // else check if adset id exists
      // if it does skip
      var exists = false;
      adsets.forEach(a => {
        if (a.adset_key === row.adset_key) {
          exists = true;
        }
      });
      if (!exists) {
        var adset = new Adset(row);
        adsets.push(adset);
      }
    }
  });

  query_results.forEach(row => {
    adsets.forEach(adset => {
      if (row.adset_key == adset.adset_key) {
        var ad = new Ad(row);
        adset.ads.push(ad);
      }
    });
  });

  //console.log("Adset Transform: ",adsets);
  return adsets;
};

exports.accountDetailsByAccount = function(query_results) {
  // console.log("query results", query_results);

  var transformed_result = {
    account_key: query_results[0].account_key,
    batch: query_results[0].batch,
    batchDescription: query_results[0].batchDescription,
    status: query_results[0].status,
    vertical: query_results[0].vertical,
    launch_status: query_results[0].launch_name,
    launch_status_key: query_results[0].launch_key,
    angle: query_results[0].angle,
    pixel: query_results[0].pixel,
    campaign_key: query_results[0].campaign_key,
    campaign_name: query_results[0].campaign_name,
    campaign_url: query_results[0].campaign_url,
    headline: query_results[0].headline,
    contentBody: query_results[0].contentBody,
    thumbnailLink: query_results[0].thumbnailLink,
    newsfeedLink: query_results[0].newsfeedLink,
    linkToGraphic: query_results[0].linkToGraphic,
    safe_url_key: query_results[0].safe_url_key,
    safe_url: query_results[0].url,
    safe_url_status: query_results[0].safe_url_status,
    safe_url_claimed: query_results[0].safe_url_claimed,
    fname: query_results[0].fname,
    lname: query_results[0].lname,
    conversionEvent: query_results[0].conversion_event,
    claimed: query_results[0].claimed,
    funding_type: query_results[0].funding_type,
    code_key: query_results[0].code_key,
    safeSpend: query_results[0].safe_spend,
    budget: query_results[0].budget,
    total_spend: query_results[0].total_spend,
    audited: query_results[0].audited,
    audit_revenue: query_results[0].audit_revenue,
    audit_expenses: query_results[0].audit_expenses,
    warmTime: query_results[0].warm_time,
    runTime: query_results[0].run_time,
    lifeTime: query_results[0].life_time,
    proxy_type: query_results[0].proxy_type,
    owner: query_results[0].owner,
    source: query_results[0].source,
    safe_ad_count: query_results[0].safe_ad_count,
    ban_type: query_results[0].ban_type,
    email: query_results[0].email,
    password: query_results[0].password,
    phone: query_results[0].phone,
    val1: query_results[0].val1,
    parent_manager: query_results[0].parent_manager,
    country: query_results[0].country,
    last_account_comment: query_results[0].last_account_comment,
    recovered: query_results[0].recovered,
    created:
      query_results[0].created == null
        ? null
        : moment(query_results[0].created)
            .utc()
            .format(),
    creation:
      query_results[0].creation == null
        ? null
        : moment(query_results[0].creation)
            .utc()
            .format(),
    launched:
      query_results[0].launched == null
        ? null
        : moment(query_results[0].launched)
            .utc()
            .format(),
    initialBoost:
      query_results[0].initial_boost == null
        ? null
        : moment(query_results[0].initial_boost)
            .utc()
            .format(),
    modified:
      query_results[0].modified == null
        ? null
        : moment(query_results[0].modified)
            .utc()
            .format(),
    dead:
      query_results[0].dead == null
        ? null
        : moment(query_results[0].dead)
            .utc()
            .format(),
    history: query_results.history,
    vm_key: query_results[0].virtual_machine,
    UUID: query_results[0].UUID,
    vm_name: query_results[0].vm_name,
    adespresso_key: query_results[0].adespresso_key
  };

  //console.log("transformed results", transformed_result);

  return transformed_result;
};
