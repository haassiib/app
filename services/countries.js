﻿"use strict";

var express = require("express");
var mysql = require("mysql");
var sqlString = require("sqlstring");
var dbConnection = require("../services/dbconnector");

exports.getCountries = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);

    var queryStr = "SELECT * FROM countries;";
    var connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        //console.log("Get Countries res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
    // await connection.end();
  };
};
