'use strict';

// let express = require("express");
// let mysql = require("mysql");
// let sqlString = require("sqlstring");
const dbConnection = require('../services/dbconnector');

exports.getVms = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);
    let queryStr = 'SELECT * FROM virtual_machines;';

    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log('virtualMachines.js getVms error: ', error);
          connection.end();
          res.status(500).send(results, error);
        }
      });
    // await connection.end();
  };
};

exports.insertVms = function() {
  return async function(req, res) {
    let vm = req.body.newVm;
    let vmName = vm.name;
    let vmUUID = vm.UUID;
    let vmCreatedTimeStamp = new Date().toISOString();
    // console.log("dateString => ", batchCreatedTimeStamp);
    // batchCreatedTimeStamp = `STR_TO_DATE( ${batchCreatedTimeStamp}, '%d/%m/%Y %H:%i:%s')`;

    // console.log("batches: ", batches);

    if (!vm || vm.length <= 0) {
      res
        .status(500)
        .send(
          { status: 500, message: 'No Vms to insert.' },
          { status: 500, message: 'No Vms to insert.' }
        );
      return;
    }

    let queryStr = `INSERT INTO virtual_machines (name, UUID, created) VALUES ('${vmName}', '${vmUUID}', NOW())`;
    // for (let i = 0; i < batches.length; i++) {
    //   if (typeof batches[i] === "string")
    //     queryStr += "( '" + batches[i] + "," + new Date() + "' )";

    //   if (i < batches.length - 1) {
    //     queryStr += ", ";
    //   }
    // }

    // queryStr += ";";

    console.log('Vm Insert QRY: ', queryStr);
    let connection = await dbConnection.init();
    await connection
      .query(queryStr, [vmName, vmUUID, vmCreatedTimeStamp])
      .then(results => {
        console.log('Insert Verticals res: ', results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log('virtualMachines.js insertVms error: ', error);
          connection.end();
          res.status(500).send(results, error);
        }
      });
    // await connection.end();
  };
};

exports.searchVms = function() {
  return async function(req, res) {
    //console.log('\n',req.headers)
    var status = req.headers['x-status'];
    var datefrom = req.headers['x-from'];
    var dateto = req.headers['x-to'];

    console.log('DATE From: ', datefrom);
    console.log('DATE to', dateto);
    //TODO: validate dates

    /*
        var queryStr = `SELECT *
        FROM accounts a
        JOIN status s ON s.status_key = a.status_key         
        WHERE status IN("`+ status + `")
        AND ((UNIX_TIMESTAMP(a.modified) < UNIX_TIMESTAMP("` + dateto +`"))
        AND (UNIX_TIMESTAMP(a.modified)) > (UNIX_TIMESTAMP("` + datefrom + `"))
        OR a.modified IS NULL) 
        ORDER BY account_key ASC;`
        */
    let queryStr =
      status.toLowerCase() === 'select all' ? '' : ' WHERE s.status = "' + status + '"';
    //console.log(queryStr);
    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        //console.log("RESULTS BEFORE: ", results);
        //results = transform.byAccount(results);
        //console.log("RESULTS AFTER: ", transform.byAccount(results));
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log('accounts.js searchAccounts: ', error);
          connection.end();
          res.status(500).send(error);
        }
      });

    // await connection.end();
  };
};
