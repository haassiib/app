let express = require('express');
let mysql = require('mysql');
let sqlString = require('sqlstring');
let moment = require('moment-timezone');
let dbConnection = require('../services/dbconnector');
let transform = require('../services/transforms');
let vol = require('./voluum');
let adespresso = require('./adespresso');

exports.getPnl = function() {
  return async function(req, res) {
    let username = req.headers['x-username'];
    let start = req.headers['x-start'];
    let end = req.headers['x-end'];
    console.log('start in pnl.js => ', start);
    console.log('end in pnl.js => ', end);

    let connection = await dbConnection.init();
    connection
      .query(
        `SELECT p.pnl_key, p.account_key, p.content_key, 
                          p.clicks, p.visits,
                          p.cpc, p.cpm,
                          p.lpctr, p.adctr, 
                          p.revenue, p.expenses, p.expenses as budget,
                          p.breakeven, p.profit,
                          p.roi, p.epv, p.epc, p.action,
                          p.created, p.modified,
                          a.fname, a.lname, a.pixel, 
                          a.owner, c.performant,
                          a.angle, a.total_spend, a.total_revenue,
                          camp.media_buyer, s.status, 
                          v.name as vertical, countries.name as geo
                            FROM pnl p
                            JOIN accounts a on p.account_key = a.account_key
                            JOIN content c on p.content_key = c.content_key
                            LEFT JOIN (SELECT 
                                  a.campaign_key AS campaign_key,
                                      a.account_key AS account_key,
                                      a.media_buyer AS media_buyer
                              FROM
                                  (campaigns a
                              JOIN (SELECT 
                                  b.account_key AS account_key,
                                      MAX(b.campaign_key) AS campaign_key
                              FROM
                                  campaigns b
                              GROUP BY b.account_key) b ON b.campaign_key = a.campaign_key)) camp ON camp.account_key = a.account_key
                            LEFT JOIN (SELECT 
                                  a.adset_key AS adset_key,
                                      a.campaign_key AS campaign_key,
                                      a.country AS country
                              FROM
                                  (adsets a
                              JOIN (SELECT 
                                  b.campaign_key AS campaign_key,
                                      MAX(b.adset_key) AS adset_key
                              FROM
                                  adsets b
                              GROUP BY b.campaign_key) b ON b.campaign_key = a.campaign_key
                                  AND b.adset_key = a.adset_key)) ads ON ads.campaign_key = camp.campaign_key
                            LEFT JOIN countries on countries.name = ads.country
                            JOIN status s on a.status_key = s.status_key
                            JOIN verticals v on v.vertical_key = a.vertical_key
                            WHERE p.created > "` +
          start +
          `" AND p.created < "` +
          end +
          `"
                            ORDER BY p.created DESC;`
      )

      .then(results => {
        //console.log("pnl get all in pnl.js => ", results);
        results = results.map(p => {
          if (p.roi === null || p.roi === undefined || p.roi === NaN) {
            p.roi = 0;
          }

          return p;
        });
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.error('pnl.js get Pnl error:', error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getPnlByAccount = function() {
  return async function(req, res) {
    let username = req.headers['x-username'];

    let connection = await dbConnection.init();
    connection
      .query(
        `SELECT p.pnl_key, p.account_key, p.content_key, 
                          p.clicks, p.visits,
                          p.cpc, p.cpm,
                          p.lpctr, p.adctr, 
                          p.revenue, p.expenses,
                          p.breakeven, p.profit,
                          p.action, p.roi, p.epv, p.epc,
                          p.created, p.modified,
                          a.fname, a.lname, a.pixel, c.performant, ads.disabled
                          FROM pnl p
                          JOIN accounts a on p.account_key = a.account_key
                          JOIN content c on p.content_key = c.content_key
                          JOIN adsets ads on ads.adset_key = c.adset_key
                          WHERE p.account_key = ` +
          req.params.account_key +
          `
                          ORDER BY p.created DESC;`
      )
      .then(results => {
        // console.log("got pnl:", results[0]);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log('pnl.js get Pnl error:', error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getPnlTotalsByAd = function() {
  return async function(req, res) {
    let username = req.headers['x-username'];

    let connection = await dbConnection.init();
    connection
      .query(
        `SELECT *
                          FROM pnl p
                          WHERE p.content_key = ` +
          req.params.content_key +
          `
                          ORDER BY p.created DESC;`
      )
      .then(results => {
        console.log('Get Pnl Totals By Ad Success');
        //console.log("got pnl:", results[0]);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log('pnl.js get Pnl Totals By Ad error:', error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.insertPnlItems = function() {
  return async function(req, res) {
    let items = req.body.items;
    let created = req.body.created;
    let username = req.headers['x-username'];
    console.log(
      'req.body.created in the insertPnlItems method in the pnl.js file => ',
      req.body.created
    );

    console.log('item: ', items);
    let queryStr = '';

    queryStr = await insertPnlQueryGenerator(items);

    let connection = await dbConnection.init(true);
    connection
      .query(queryStr)
      .then(results => {
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

function insertPnlQueryGenerator(items) {
  //create insert statement
  let queryInsert =
    'INSERT INTO pnl (account_key, content_key, action, clicks, visits, revenue, expenses, lpctr, epv, epc, created) VALUES ';
  //create values insert section
  let queryStr = items
    .map(item => {
      console.log('item.created => ', item.created);
      let action = item.action == undefined ? 'NULL,' : '"' + item.action + '",';
      let clicks = item.clicks == undefined ? 'NULL,' : '"' + item.clicks + '",';
      let visits = item.visits == undefined ? 'NULL,' : '"' + item.visits + '",';
      let revenue = item.revenue == undefined ? '0,' : item.revenue + ',';
      let expenses = item.expenses == undefined ? '0,' : item.expenses + ',';
      let lpctr = item.lpctr == undefined ? '0,' : item.lpctr + ',';
      let epv = item.epv == undefined ? '0,' : item.epv + ', ';
      let epc = item.epc == undefined ? '0,' : item.epc + ', ';
      //this need to be here because date needs to be set by user for pnl day
      let created =
        item.created == undefined
          ? ' NOW()'
          : '"' + moment(item.created, 'MM-DD-YYYY').format('YYYY-MM-DD') + ' 06:00:00' + '"';

      if (item.account_key && item.content_key && item.created) {
        return (
          '( ' +
          item.account_key +
          ',' +
          item.content_key +
          ',' +
          action +
          clicks +
          visits +
          revenue +
          expenses +
          lpctr +
          epv +
          epc +
          created +
          ') '
        );
      }
    })
    .join(',');

  console.log('INSERT INTO PNLY QUERY: ', queryInsert + queryStr + ';');
  return queryInsert + queryStr + ';';
}

exports.updatePnlItems = function() {
  return async function(req, res) {
    let items = req.body.items;
    let username = req.headers['x-username'];
    console.log('req.body in updatepnlitems => ', req.body);

    let queryStr = updatePnlQueryGenerator(items);
    console.log('UPDATE QUERY STR: ', queryStr);

    let connection = await dbConnection.init(true);
    connection
      .query(queryStr)
      .then(results => {
        console.log('success : ', results);
        console.log('PNL update success.');
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.updatePnlExpensesWithAdespressoByAccount = async function(account_key, items) {
  let connection = await dbConnection.init(true);

  await items.forEach(async item => {
    let upatePnlQueryStr =
      'UPDATE pnl p SET p.expenses = ' +
      item.spend +
      ' WHERE p.account_key = ' +
      account_key +
      " AND p.created >= DATE('" +
      item.date_start +
      "') AND p.created < (DATE(DATE('" +
      item.date_start +
      "') + INTERVAL 1 DAY));";
    //UPDATE pnl p SET p.expenses = 89.22 WHERE p.account_key = 5227 AND p.created >= DATE(2018-11-18) AND p.created < (DATE(DATE(2018-11-18) + INTERVAL 1 DAY));
    console.log('UPDATE QUERY STR: ', upatePnlQueryStr);

    connection
      .query(upatePnlQueryStr)
      .then(results => {
        //console.log("success : ", results);
        console.log('PNL Update expenses with Adespresso Spend Success. => ', results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
        }
      });
  });
  await connection.end();
};

function updatePnlQueryGenerator(items) {
  //create insert section
  let queryStr = '';
  console.log('items in querygenerator => ', items);
  items.forEach(item => {
    if (item && typeof item.pnl_key !== 'undefined') {
      console.log('Item.Expenses => ', item.expenses, '');
      console.log();

      // remove non-digit characters form currency
      item.expenses =
        typeof item.expenses === 'string'
          ? parseInt(item.expenses.replace(/[^\d.-]/g, ''))
          : item.expenses;
      item.revenue =
        typeof item.revenue === 'string'
          ? parseInt(item.revenue.replace(/[^\d.-]/g, ''))
          : item.revenue;

      let clicks = item.clicks == undefined ? ' 0 ' : item.clicks;
      let visits = item.visits == undefined ? ' 0 ' : item.visits;
      let expenses = item.expenses == undefined ? ' 0 ' : item.expenses;
      let revenue = item.revenue == undefined ? ' 0 ' : item.revenue;
      let lpctr = item.lpctr == undefined ? ' 0 ' : item.lpctr;
      let epv = item.epv == undefined ? ' 0 ' : item.epv;
      let epc = item.epc == undefined ? ' 0 ' : item.epc;
      let action = item.action == undefined ? ' NULL ' : '"' + item.action + '"';

      queryStr +=
        'UPDATE pnl SET ' +
        ' clicks = ' +
        clicks +
        ', visits = ' +
        visits +
        ', expenses = ' +
        expenses +
        ', revenue = ' +
        revenue +
        ', lpctr = ' +
        lpctr +
        ', epv = ' +
        epv +
        ', epc = ' +
        epc +
        ', action = ' +
        action +
        ', MODIFIED = NOW() WHERE pnl_key = ' +
        item.pnl_key +
        ';';
    }
  });

  console.log('querystr in querystr generator => ', queryStr);
  return queryStr;
}

//Need to refactor for adespresso spend to be incorporated
exports.updatePnlExpensesDaily = async function() {
  let queryStr = `UPDATE pnl p JOIN accounts a on p.account_key = a.account_key SET p.expenses = a.budget where p.created >= DATE(CURDATE()) AND p.created < (DATE(CURDATE()) + INTERVAL 1 DAY);`;

  //console.log("UPDATE QUERY STR: ", queryStr);

  let connection = await dbConnection.init(true);
  connection
    .query(queryStr)
    .then(results => {
      //console.log("success : ", results);
      console.log('PNL update expenses success. => ', results);
      connection.end();
    })
    .catch(error => {
      if (error) {
        connection.end();
        console.log(error);
      }
    });
};

const insertDailyPnlByDateAndPixel = (exports.insertDailyPnlByDateAndPixel = async function(
  dateStr,
  pixels
) {
  console.log('pixels in the insertfunc => ', pixels);
  // dateStr = dateStr.replace("T06:00:00", "");
  console.log('dateStr in the insertfunc => ', dateStr);

  let queryStr =
    `INSERT INTO pnl (account_key, content_key, expenses, created)
    SELECT a.account_key, c.content_key, a.budget, "` +
    dateStr +
    `" as created
    FROM content c
    JOIN adsets ads ON ads.adset_key = c.adset_key
    JOIN campaigns camp on camp.campaign_key = ads.campaign_key
    JOIN accounts a on a.account_key = camp.account_key
    WHERE ads.disabled = 0
    AND a.pixel IN ("` +
    pixels.join(',').replace(new RegExp(',', 'g'), '","') +
    `")
    AND a.account_key NOT IN (SELECT account_key FROM pnl WHERE created >= DATE("` +
    dateStr +
    `") AND created < (DATE( "` +
    dateStr +
    `" ) + INTERVAL 1 DAY));`;

  console.log('queryStr in insertDailyPnlByDateAndPixel => ', queryStr);
  let connection = await dbConnection.init();
  connection
    .query(queryStr)
    .then(results => {
      connection.end();
      console.log('Daily PNL Insert By Pixel and DateStr Success:');
    })
    .catch(error => {
      if (error) {
        connection.end();
        console.error('DAILY PNL INSERT ERROR:', error);
      }
    });
});

exports.insertDailyPnl = async function() {
  let queryStr = `INSERT INTO pnl (account_key, content_key, expenses)
    SELECT a.account_key, c.content_key, a.budget
    FROM content c
    JOIN adsets ads ON ads.adset_key = c.adset_key
    JOIN campaigns camp on camp.campaign_key = ads.campaign_key
    JOIN accounts a on a.account_key = camp.account_key
    WHERE ads.disabled = 0
    AND a.status_key IN (select status_key from status s where status in ('Spending, In PL', 'In Progress', 'Needs Bump', 'Finished Upload', 'Problem', 'Phone Lockout', 'Photo Lockout', 'Resolved', 'Reupload'))
    AND a.account_key NOT IN (SELECT account_key FROM pnl WHERE created >= DATE(CURDATE()) AND created < (DATE(CURDATE()) + INTERVAL 1 DAY));`;

  let connection = await dbConnection.init();
  connection
    .query(queryStr)
    .then(results => {
      connection.end();
      console.log('Daily PNL Insert Success:', results);
    })
    .catch(error => {
      if (error) {
        connection.end();
        console.log('DAILY PNL INSERT ERROR:', error);
      }
    });
};

exports.insertPnlItemByAccount = function() {
  return async function(req, res) {
    let username = req.headers['x-username'];
    let account_keys = req.body.account_keys;
    let created = req.body.created;
    console.log(
      'req.body.created in the insertPnlItemsByAccount method in the pnl.js file => ',
      req.body.created
    );

    console.log('INSIDE insert with ID: ', account_keys);

    let queryStr =
      `INSERT INTO pnl (account_key, content_key, expenses, created)
        SELECT a.account_key, c.content_key, a.budget, "` +
      created +
      `" as created
        FROM content c
        JOIN adsets ads ON ads.adset_key = c.adset_key
        JOIN campaigns camp on camp.campaign_key = ads.campaign_key
        JOIN accounts a on a.account_key = camp.account_key
        WHERE ads.disabled = 0
        AND a.account_key IN (` +
      account_keys.join(', ') +
      `)
        AND a.account_key NOT IN (SELECT account_key FROM pnl WHERE created >= DATE("` +
      created +
      `") AND created < (DATE( "` +
      created +
      `" ) + INTERVAL 1 DAY));`;

    let connection = await dbConnection.init();
    console.log(queryStr);
    connection
      .query(queryStr)
      .then(results => {
        //console.log("success : ", results);
        console.log('PNL update by account success');
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getDailyPerformantContent = async function(callback) {
  let queryStr = `SELECT p.pnl_key,
        p.account_key,
        c.content_key,        
        c.adset_key,
        c.headline,
        c.body,
        c.newsfeed_link,
        c.link_to_graphic,
        c.thumbnail,
        c.comment,
        p.roi,
        p.revenue,
        p.expenses,
        p.profit,
        ads.country,
        a.fname,
        a.lname,
        a.angle,
        v.name as vertical
    FROM content c
    JOIN pnl p ON p.content_key = c.content_key
    JOIN adsets ads ON ads.adset_key = c.adset_key
    JOIN accounts a ON a.account_key = p.account_key
    JOIN verticals v on v.vertical_key = a.vertical_key
    WHERE DATE(p.created) = (SUBDATE(CURDATE(), 1))
    AND p.roi > 100;`;

  let connection = await dbConnection.init();
  connection
    .query(queryStr)
    .then(results => {
      //console.log("success : ", results);
      connection.end();
      return callback(results);
    })
    .catch(error => {
      if (error) {
        connection.end();
        console.error('Error in the getDailyPerformantContent method in the pnl.js => ', error);
      }
    });
};

exports.updatePnlExpensesByAccount = function() {
  return async function(req, res) {
    let items = req.body.items;
    let username = req.headers['x-username'];

    let queryStr = '';
    items.forEach(item => {
      let account_key = item.account_key;
      let expenses = item.expenses;
      let date = item.date;

      queryStr += `UPDATE pnl SET expenses = ${expenses} WHERE account_key = ${account_key} and DATE(created) = DATE("${date}"); `;
    });
    //console.log("UPDATE QUERY STR: ", queryStr);

    let connection = await dbConnection.init(true);
    connection
      .query(queryStr)
      .then(results => {
        //console.log("success : ", results);
        console.log('PNL update expenses success.');
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getNDayPnlByAccount = function() {
  return async function(req, res) {
    let username = req.headers['x-username'];
    let days = req.headers['x-days'];

    let connection = await dbConnection.init();
    //AND DATE(created) > DATE(SUBDATE(p2.start_date,  ` + days + `+2))
    //AND DATE(created) < DATE(SUBDATE(p2.start_date,  ` + days + `+1))
    let queryStr =
      `SELECT *
        FROM pnl p
        JOIN
          (SELECT account_key, created AS start_date
            FROM pnl
            WHERE account_key = ` +
      req.params.account_key +
      ` ORDER BY created DESC
            LIMIT 1) p2 ON p2.account_key = p.account_key
        WHERE DATE(CURDATE()) = DATE(p2.start_date)
        AND DATE(created) > DATE(SUBDATE(p2.start_date,  ` +
      days +
      `+1))
	      AND DATE(created) < DATE(p2.start_date)
	      AND p.account_key = ` +
      req.params.account_key +
      ` OR
	      DATE(CURDATE()) != DATE(p2.start_date)
	      AND DATE(created) > DATE(SUBDATE(p2.start_date, ` +
      days +
      `)) 
        AND DATE(created) <= DATE(p2.start_date)
        AND p.account_key =` +
      req.params.account_key +
      ` ORDER BY p.created DESC;`;

    connection
      .query(queryStr)
      .then(results => {
        //console.log("Get " + days +" days Pnl Success");
        //console.log("got pnl:", results[0]);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log('pnl.js get ' + days + ' days Pnl error:', error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getNDaysPnlForROI = function() {
  return async function(req, res) {
    let username = req.headers['x-username'];
    let days = req.headers['x-days'];

    let queryStr =
      `SELECT p.account_key, p.content_key, 
	                        SUM(p.revenue) revenue, SUM(p.expenses) expenses, SUM(p.profit) profit,
	                        SUM(p.clicks) clicks, SUM(p.visits) visits,
	                        ((SUM(p.profit) / SUM(p.expenses)) * 100) as roi,
	                        (SUM(p.lpctr) / COUNT(distinct p.pnl_key)) as lpctr,
	                        COUNT(distinct p.pnl_key) as cnt,
	                        a.fname as fname, a.lname as lname, a.pixel as pixel
                        FROM pnl p
                        JOIN accounts a on p.account_key = a.account_key
                        WHERE DATE(p.created) >= DATE(SUBDATE(CURDATE(), ` +
      days +
      `))
                        AND DATE(p.created) < DATE(CURDATE())
                        AND p.expenses > 30
                        GROUP BY p.content_key                        
                        HAVING cnt = ` +
      days +
      ` and roi < 30
                        ORDER BY roi DESC;`;
    let connection = await dbConnection.init();
    connection
      .query(queryStr)
      .then(results => {
        console.log('Get ' + days + ' days Pnl for ROI Success');
        //console.log("got pnl:", results[0]);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log('pnl.js get ' + days + 'days Pnl for ROI error:', error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getDailyPoorAccountRoi = async function(callback) {
  let queryStr = `SELECT p.account_key, p.content_key, 
	                    SUM(p.revenue) revenue, SUM(p.expenses) expenses, SUM(p.profit) profit,
	                    SUM(p.clicks) clicks, SUM(p.visits) visits,
	                    ((SUM(p.profit) / SUM(p.expenses)) * 100) as roi,
	                    (SUM(p.lpctr) / COUNT(distinct p.pnl_key)) as lpctr,
	                    COUNT(distinct p.pnl_key) as cnt,
	                    a.fname as fname, a.lname as lname, 
                        a.pixel as pixel, a.owner as owner
                    FROM pnl p
                    JOIN accounts a on p.account_key = a.account_key
                    WHERE DATE(p.created) >= DATE(SUBDATE(CURDATE(), 3))
                    AND DATE(p.created) < DATE(CURDATE())
                    AND p.expenses > 30
                    GROUP BY p.content_key                        
                    HAVING cnt = 3 and roi < 30
                    ORDER BY roi DESC;`;

  let connection = await dbConnection.init();
  connection
    .query(queryStr)
    .then(results => {
      //console.log("success : ", results);
      connection.end();
      return callback(error, results);
    })
    .catch(error => {
      if (error) {
        connection.end();
        console.log(error);
      }
    });
};

exports.getPayoutsByRangeAndOwner = function() {
  return async function(req, res) {
    let owner = req.headers['x-username'];
    let enddate = moment(req.headers['x-enddate'], 'MM-DD-YYYY').format('YYYY-MM-DD HH:MM:SS');
    let startdate = moment(req.headers['x-startdate'], 'MM-DD-YYYY').format('YYYY-MM-DD HH:MM:SS');

    if (owner === 'Luke' || owner === 'Stan' || owner === 'Kathy') {
      console.log('payouts admin true!');
      owner = req.headers['x-owner'];
    }

    let queryStr =
      `SELECT a.account_key, s.status, a.fname, a.lname, coalesce(a.audit_revenue, 0) as audit_revenue, coalesce(a.audit_expenses, 0) as audit_expenses, a.created, a.modified, a.dead, a.audit_epoch
                FROM accounts a
                JOIN status s
	                ON s.status_key = a.status_key
                WHERE a.status_key IN (SELECT s.status_key from status s where s.status like 'Dead%' OR s.status = 'Hidden')
                AND a.audited = 1
                AND (a.audit_epoch < UNIX_TIMESTAMP("` +
      enddate +
      `") AND a.audit_epoch >= UNIX_TIMESTAMP("` +
      startdate +
      `"))
                AND a.owner = "` +
      owner +
      `"
                ORDER BY a.audit_epoch DESC;`;
    console.log('QUERY: ', queryStr);

    let connection = await dbConnection.init(true);
    connection
      .query(queryStr)
      .then(results => {
        //console.log("success : ", results);
        console.log('PNL Get Payouts success.', results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log('PNL Get Payouts Error: ', error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.updatePnlWithVoluumByDate = async function(dateStr) {
  console.log('dateStr => ', dateStr);
  let reports = await vol.getAllReportsByDate(dateStr);
  // reports = await JSON.parse(reports);
  let pixels = await reports.rows.map(r => {
    if (r.customVariable4 !== '' && r.customVariable4 !== undefined) {
      return r.customVariable4;
    }
  });
  pixels.shift();
  await insertDailyPnlByDateAndPixel(dateStr, pixels);
  await vol.reconcileDay([dateStr]);
};
