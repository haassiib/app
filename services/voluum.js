﻿"use strict";

let moment = require("moment-timezone");
let dbConnection = require("../services/dbconnector");
let transform = require("../services/transforms");
let pnlService = require("./pnl");

let request = require("request-promise");

const authVoluum = (exports.authVoluum = function() {
  return async function(req, res) {
    let headers = {
      "Content-Type": "application/json; charset=utf-8",
      Accept: "application/json"
    };

      const vEmail = "lionsheart1media@gmail.com";
      const vPass = "Goggins8000!";

   /*  '{ "email": "lionsheart1media@gmail.com", "password": "Goggins8000!"}'; */
    let dataString ={ email: vEmail , password: vPass};

    let options = {
      url: "https://api.voluum.com/auth/session",
      method: "POST",
      headers: headers,
      body: dataString,
      json : true
    };

    function callback(error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body);
        res.status(200).send(body);
      }
    }
    await request(options, callback);
  };
});

exports.getCampaigns = function() {
  return async function(req, res) {
    console.log("header token: ", req.headers["x-token"]);
    let token = req.headers["x-token"];

    let headers = {
      Accept: "application/json",
      "cwauth-token": token
    };

    let options = {
      url: "https://api.voluum.com/campaign",
      headers: headers
    };

    function callback(error, response, body) {
      if (!error && response.statusCode == 200) {
        //console.log(body);
        res.status(200).send(body);
      }
      //TODO: error validation
    }

    await request(options, callback);
  };
};

exports.getReports = function() {
  return async function(req, res) {
    console.log("Running getReports => ", req.params.date_range);
    let custom = req.headers["x-custom"];
    console.log("custom => ", custom);
    let dateRange = req.params.date_range;

    // Get start offset
    var startoffset = 6;
    if (
      moment()
        .tz("America/Chicago")
        .isDST()
    ) {
      startoffset = 5;
    }
    try {
      // Pull for the various types
      if (dateRange === "today") {
        var start = moment()
          .subtract(startoffset, "hours")
          .format("YYYY-MM-DD");
        // reconcileDay([start]);
        start += "T06:00:00";
        pnlService.updatePnlWithVoluumByDate(start);
      }

      // We want this disabled for now, but I am only commenting it out in case we want to add it back in

      if (dateRange === "yesterday") {
        var start = moment()
          .subtract(startoffset + 24, "hours")
          .format("YYYY-MM-DD");
        start += "T06:00:00";
        pnlService.updatePnlWithVoluumByDate(start);
        // reconcileDay([start]);
      }
      if (dateRange === "custom date") {
        var start = moment(custom, "MM-DD-YYYY").format("YYYY-MM-DD");
        start += "T06:00:00";
        pnlService.updatePnlWithVoluumByDate(start);
        // reconcileDay([start]);
      }

      await res.status(200).send("complete");
    } catch (error) {
      if (error) {
        console.error("Error in the getReports method in voluum.js => ", error);
        if (dateRange === "today") {
          var start = moment()
            .subtract(startoffset, "hours")
            .format("YYYY-MM-DD");

          start += "T06:00:00";
          pnlService.updatePnlWithVoluumByDate(start);
          // reconcileDay([start]);
        }
      }
    }
  };
};

function voluumQueryString(datetime) {
  // Get start date
  var date = new Date(datetime);
  //date.setDate(date.getDate() + 1.3); // +1.3 days because this corrects for when DST changes. Moment adds 24 hours, but the DST day has 25 hours so it considers it the same day.
  var start = moment.utc(date).format("YYYY-MM-DD");

  // Get hour offset
  var starthour = "T06:00:00";
  if (
    moment([date.getFullYear(), date.getMonth(), date.getDate()])
      .tz("America/Chicago")
      .isDST()
  ) {
    starthour = "T05:00:00";
  }

  // Get end date
  date.setDate(date.getDate() + 1); // +1.3 days because this corrects for when DST changes. Moment adds 24 hours, but the DST day has 25 hours so it considers it the same day.
  var end = moment.utc(date).format("YYYY-MM-DD");

  // Get hour offset
  var endhour = "T06:00:00";
  if (
    moment([date.getFullYear(), date.getMonth(), date.getDate()])
      .tz("America/Chicago")
      .isDST()
  ) {
    endhour = "T05:00:00";
  }

  var url = `https://api.voluum.com/report?from=${start}${starthour}&to=${end}${endhour}&groupBy=custom-variable-4&limit=500&conversionTimeMode=CONVERSION`;

  return url;
}

const getAllReportsByDate = (exports.getAllReportsByDate = async function(
  datetime
) {
  let authHeaders = {
    "Content-Type": "application/json; charset=utf-8",
    Accept: "application/json"
  };

    let authDataString = { email: vEmail, password: vPass };

  let authOptions = {
    url: "https://api.voluum.com/auth/session",
    method: "POST",
    headers: authHeaders,
    body: authDataString
  };
  let authData = await request(authOptions).then(d => d);
  let data = JSON.parse(authData);
  console.log("authData => ", authData, " data => ", data);

  // Get start date
  var date = new Date(datetime);
  console.log(
    "datetime and date in the getAllReportsByDate method in voluum.js => ",
    datetime,
    " ",
    date
  );
  //date.setDate(date.getDate() + 1.3); // +1.3 days because this corrects for when DST changes. Moment adds 24 hours, but the DST day has 25 hours so it considers it the same day.
  var start = moment.utc(date).format("YYYY-MM-DD");

  // Get hour offset
  var starthour = "T06:00:00";
  if (
    moment([date.getFullYear(), date.getMonth(), date.getDate()])
      .tz("America/Chicago")
      .isDST()
  ) {
    starthour = "T05:00:00";
  }

  // Get end date
  date.setDate(date.getDate() + 1); // +1.3 days because this corrects for when DST changes. Moment adds 24 hours, but the DST day has 25 hours so it considers it the same day.
  var end = moment.utc(date).format("YYYY-MM-DD");

  // Get hour offset
  var endhour = "T06:00:00";
  if (
    moment([date.getFullYear(), date.getMonth(), date.getDate()])
      .tz("America/Chicago")
      .isDST()
  ) {
    endhour = "T05:00:00";
  }
  console.log("start => ", start, " end => ", end);

  console.log("start Hour => ", starthour, " EndHour => ", endhour);

  // Request opts
  let headers = {
    Accept: "application/json; charset=utf-8",
    "cwauth-token": data.token
  };

  let options = {
    // During DST this needs to be 05:00:00, but not during DST it needs to be 06:00:00. On the exact date of DST, it's also 06:00:00
    url: `https://api.voluum.com/report?from=${start}${starthour}&to=${end}${endhour}&groupBy=custom-variable-4&limit=500&conversionTimeMode=CONVERSION`,
    headers: headers
  };

  let reports = await request(options);
  reports = await JSON.parse(reports);

  // console.log("reports ==> ", reports);
  return reports;
});

function reconcileDayQueries(data, datetimes) {
  var updates = [];
  let qs = 0;
  let target = datetimes.length;

  for (var d in datetimes) {
    var datetime = datetimes[d];
    // datetime = datetime.replace("T06:00:00", "");
    console.log("datetime in the reconcileDay method => ", datetime);

    // Get start date
    var date = new Date(datetime);
    //date.setDate(date.getDate() + 1.3); // +1.3 days because this corrects for when DST changes. Moment adds 24 hours, but the DST day has 25 hours so it considers it the same day.
    var start = moment.utc(date).format("YYYY-MM-DD");

    // Get hour offset
    var starthour = "T06:00:00";
    if (
      moment()
        .tz("America/Chicago")
        .isDST()
    ) {
      starthour = "T05:00:00";
    }

    // Get end date
    date.setDate(date.getDate() + 1); // +1.3 days because this corrects for when DST changes. Moment adds 24 hours, but the DST day has 25 hours so it considers it the same day.
    var end = moment.utc(date).format("YYYY-MM-DD");

    // Get hour offset
    var endhour = "T06:00:00";
    if (
      moment()
        .tz("America/Chicago")
        .isDST()
    ) {
      endhour = "T05:00:00";
    }

    // console.log("start Hour => ", starthour, " EndHour => ", endhour);

    // Request opts
    let headers = {
      Accept: "application/json; charset=utf-8",
      "cwauth-token": data.token
    };

    let options = {
      // During DST this needs to be 05:00:00, but not during DST it needs to be 06:00:00. On the exact date of DST, it's also 06:00:00
      url: `https://api.voluum.com/report?from=${start}${starthour}&to=${end}${endhour}&groupBy=custom-variable-4&limit=500&conversionTimeMode=CONVERSION`,
      headers: headers
    };

    // console.log("Options => ", options);

    async function callback(error, response, body) {
      try {
        if (!error && response.statusCode == 200) {
          let report = JSON.parse(body);

          // console.log("report Body => ", report);

          // For each result
          for (var x in report.rows) {
            var row = report.rows[x];
            var pixel = row.customVariable4;
            if (
              typeof pixel !== "undefined" &&
              pixel !== "undefined" &&
              pixel !== "" &&
              (row.customVariable4.length < 20 &&
                !isNaN(parseInt(row.customVariable4.toString())))
            ) {
              // Update pnl for this day, for this account
              var update = "";
              let clicks = row.clicks == null ? " 0 " : row.clicks;
              let visits = row.visits == null ? " 0 " : row.visits;
              let revenue = row.revenue == null ? " 0 " : row.revenue;
              let lpctr = row.ctr == null ? " 0 " : row.ctr;
              let epv = row.epv == null ? " 0 " : row.epv;
              let epc = row.epc == null ? " 0 " : row.epc;
              let hour = row.hour == null ? " 0 " : row.hour;
              let created = moment.unix(hour / 1000).format("YYYY-MM-DD");
              update +=
                "UPDATE pnl SET " +
                " clicks = " +
                clicks +
                ", visits = " +
                visits +
                ", revenue = " +
                revenue +
                ", lpctr = " +
                lpctr +
                ", epv = " +
                epv +
                ", epc = " +
                epc +
                ', MODIFIED = NOW() where pnl_key = ( select pnl_key from (select pnl_key, created, account_key, content_key from pnl) as pnlclone where created >"' +
                created +
                ' 00:01:01" and created <"' +
                created +
                ' 23:01:01" and account_key = ( select account_key from accounts where pixel = "' +
                pixel +
                '" ) order by content_key asc limit 1 );';
              updates.push(update);
            }
          }
        }

        qs++;

        if (qs == target) {
          let connection = await dbConnection.init(true);
          // console.log(
          //   "these are the updates in the reconcileDayQueries generator => ",
          //   updates
          // );
          await saveReconciles(connection, updates);
        }
      } catch (error) {
        if (error) {
          console.error(
            "Error in the reconcileDayQueries method in voluum.js => ",
            error
          );
        }
      }
    }
    (async function(start, options, callback) {
      await request(options, callback);
    })(start, options, callback);
  }
}

async function saveReconciles(connection, updates) {
  try {
    console.log("starting update query on " + updates.length + " queries");
    if (updates.length > 0) {
      await connection
        .query(updates.join(" "))
        .then(results => {
          connection.end();
          console.log("update query finished => ", results);
        })
        .catch(error => {

          if (error) {
            console.error(
              "Date => ",
              moment(),
              " Error in the .catch of the saveReconciles query => ",
              error
            );
            console.log(
              "Date => ",
              moment(),
              "Error in the .catch of the saveReconciles query => ",
              error
            );
            //connection.end();
          }
          connection.end();
        });
    }
  } catch (error) {
    console.error("Error in the saveReconciles method in voluum.js => ", error);
  }
}

exports.yesterdayPnl = function() {
  let yesterday = moment()
    .subtract(24, "hours")
    .format("YYYY-MM-DD");
  reconcileDay([yesterday]);
};

exports.weekPnl = function() {
  let weekago = moment()
    .subtract(168, "hours")
    .format("YYYY-MM-DD");
  reconcileDay([weekago]);
};

const reconcileDay = (exports.reconcileDay = async function(datetimes) {
  let headers = {
    "Content-Type": "application/json; charset=utf-8",
    Accept: "application/json"
  };

    let dataString = { email: vEmail, password: vPass };

  let options = {
    url: "https://api.voluum.com/auth/session",
    method: "POST",
    headers: headers,
    body: dataString
  };

  async function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      let data = JSON.parse(body);
      await reconcileDayQueries(data, datetimes);
    }
  }

  (async function(datetimes, options, callback) {
    await request(options, callback);
  })(datetimes, options, callback);
});

function getPixelIdList(body) {
  let report = JSON.parse(body);
  //console.log("VOLUUM REPORT: ", report);
  //console.log("type of:", typeof report);

  let pixelIds = report.rows.reduce((pixels, row) => {
    if (
      typeof row.customVariable4 !== "undefined" &&
      row.customeVariable4 !== "undefined" &&
      row.customVariable4 !== ""
    ) {
      //verify valid pixel id
      //console.log("Len: ", row.customVariable4.length, typeof row.customeVariable4, " | ", !isNaN(parseInt(row.customVariable4.toString())));
      if (
        row.customVariable4.length < 20 &&
        !isNaN(parseInt(row.customVariable4.toString()))
      ) {
        pixels.push(row.customVariable4);
      }
    }
    return pixels;
  }, []);

  console.log("PIXEL ID'S: ", pixelIds);
  return pixelIds;
}

async function updatePnlsByAccountPixel(pixels, body, clientDate, dateRange) {
  let date =
    typeof clientDate === "undefined"
      ? " DATE(CURDATE()) "
      : ' DATE("' + clientDate + '") ';

  console.log("client Date: ", clientDate, " | dateRange: ", dateRange);

  if (typeof dateRange !== "undefined" && dateRange === "yesterday") {
    date = " DATE(SUBDATE(" + date + ",1)) ";
  }

  let queryStr =
    `SELECT p.pnl_key, p.account_key, MIN(p.content_key) as content_key, p.clicks, p.visits,
                    p.cpc, p.cpm, p.lpctr, p.adctr, p.revenue, p.expenses, p.epv, p.epc, a.pixel
                    FROM pnl p
                    JOIN accounts a ON p.account_key = a.account_key
                    WHERE DATE(p.created) = ` +
    date +
    ` AND p.account_key IN (select account_key
		            FROM accounts
			        WHERE pixel IN ('` +
    pixels.join("', '") +
    `')) GROUP BY account_key;`;

  console.log("pixels query:", queryStr);
  console.log("updatePnlByAccountPixel called");
  let connection = await dbConnection.init(true);
  await connection
    .query(queryStr)
    .then(results => {
      console.log("success : ", results.length);
      let reports = JSON.parse(body);
      console.log("reports: ", reports.rows.length);
      if (reports.rows.length > 0 && results.length > 0) {
        console.log("Creating Voluum Pnl Updated Query..");
        let queryForPnl = updatePnlWithVoluumQueryGenerator(reports, results);
        if (queryForPnl != null) {
          console.log(
            "Query created. Inserting into DB. Query = => ",
            queryForPnl
          );
          connection
            .query(queryForPnl)
            .then(results => {
              console.log("Pnl Voluum Update Success! Results = => ", results);
              connection.end();
            })
            .catch(error => {
              if (error) {
                console.error(
                  "Error in the updatePnlsByAccountPixel method => ",
                  error
                );
                connection.end();
              }
            });
        } else {
          console.log(
            "Query is null. Check Voluum Reports and Pnl Item results."
          );
          connection.end();
        }
      } else {
        console.log(
          "Missing Values. Check Voluum Reports and Pnl Item results."
        );
        connection.end();
      }
    })
    .catch(error => {
      if (error) {
        console.log("Error in the updatePnlWithVoluum method => ", error);
      }
    });
}

function updatePnlWithVoluumQueryGenerator(reports, pnl) {
  //create insert section
  //console.log("REPORTS:",reports);
  //console.log("PNL: ", pnl);
  let queryStr = "";
  //verify values
  if (pnl.length < 1 || reports.length < 1) return null;

  pnl.forEach(item => {
    let report = reports.rows.filter(report => {
      return report.customVariable4 == item.pixel;
    });
    //console.log("filtered report by pixel: ", report);

    if (typeof item.pnl_key !== "undefined") {
      let clicks = report[0].clicks == null ? " 0 " : report[0].clicks;
      let visits = report[0].visits == null ? " 0 " : report[0].visits;
      let revenue = report[0].revenue == null ? " 0 " : report[0].revenue;
      let lpctr = report[0].ctr == null ? " 0 " : report[0].ctr;
      let epv = report[0].epv == null ? " 0 " : report[0].epv;
      let epc = report[0].epc == null ? " 0 " : report[0].epc;
      let expenses = item.expenses == undefined ? " 0 " : item.expenses;
      let action =
        item.action == undefined ? " NULL " : '"' + item.action + '"';

      queryStr +=
        "UPDATE pnl SET " +
        " clicks = " +
        clicks +
        ", visits = " +
        visits +
        ", revenue = " +
        revenue +
        ", lpctr = " +
        lpctr +
        ", epv = " +
        epv +
        ", epc = " +
        epc +
        ", expenses = " +
        expenses +
        ", action = " +
        action +
        ", MODIFIED = NOW() WHERE pnl_key = " +
        item.pnl_key +
        ";";
    }
  });
  //console.log(queryStr);
  return queryStr;
}

exports.UpdatePnlWithVoluum = async function() {
  console.log("Inside Voluum Update For Pnl");
  //get token
  let headers = {
    "Content-Type": "application/json; charset=utf-8",
    Accept: "application/json"
  };

    let dataString = { email: vEmail, password: vPass };

  let options = {
    url: "https://api.voluum.com/auth/session",
    method: "POST",
    headers: headers,
    body: dataString
  };

  async function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      //console.log(JSON.parse(body));
      let data = JSON.parse(body);

      let headers = {
        Accept: "application/json; charset=utf-8",
        "cwauth-token": data.token
      };

      //let from = moment().utcOffset(-5).format('YYYY-MM-DD') + 'T06:00:00Z';
      //let to = moment().utcOffset(-5).add(1, 'days').format('YYYY-MM-DD') + 'T06:00:00Z';

      // Get start offset
      var starthour = "T06:00:00";
      var startoffset = 6;
      if (
        moment()
          .tz("America/Chicago")
          .isDST()
      ) {
        starthour = "T05:00:00";
        startoffset = 5;
      }

      // Get end offset
      var endhour = "T06:00:00";
      var endoffset = 6;
      if (
        moment()
          .tz("America/Chicago")
          .add(1, "days")
          .isDST()
      ) {
        endhour = "T05:00:00";
        endoffset = 5;
      }

      // Get start date
      var future = 0;
      var start = moment()
        .tz("America/Chicago")
        .format("YYYY-MM-DD");

      // Get end date
      var end = moment()
        .tz("America/Chicago")
        .add(1, "days")
        .format("YYYY-MM-DD");

      let options = {
        url: `https://api.voluum.com/report?from=${start}${starthour}&to=${end}${endhour}&groupBy=custom-variable-4&limit=500&conversionTimeMode=CONVERSION`,
        headers: headers
      };
      console.log("voluum options.url => ", options.url);

      async function callback(error, response, body) {
        if (!error && response.statusCode == 200) {
          let pixels = getPixelIdList(body);
          var date = moment()
            .tz("America/Chicago")
            .format("YYYY-MM-DD");
          await updatePnlsByAccountPixel(pixels, body, date);
        }
      }
      await request(options, callback);
    }
  }
  await request(options, callback);
};

// exports.test = async function() {
//   for (var hours = 0; hours < 25; hours++) {
//     console.log(hours);
//     let from =
//       moment()
//         .add(hours, "hours")
//         .format("YYYY-MM-DD") + "T06:00:00Z";
//     let fromutc =
//       moment()
//         .utcOffset(-5)
//         .add(hours, "hours")
//         .format("YYYY-MM-DD") + "T06:00:00Z";
//     let to =
//       moment()
//         .add(hours, "hours")
//         .add(1, "days")
//         .format("YYYY-MM-DD") + "T06:00:00Z";
//     let toutc =
//       moment()
//         .utcOffset(-5)
//         .add(hours, "hours")
//         .add(1, "days")
//         .format("YYYY-MM-DD") + "T06:00:00Z";
//     console.log(from);
//     console.log(fromutc);
//     console.log(to);
//     console.log(toutc);
//   }

//   let connection = await dbConnection.init(true);
//   await connection
//     .query("Select NOW() from bugs")
//     .then(results => {
//       console.log("NOW()");
//       console.log(results);
//       connection.end();
//     })
//     .catch(error => {
//       if (error) {
//         connection.end();
//         console.log(error);
//       }
//     });

//   return true;

//   let headers = {
//     "Content-Type": "application/json; charset=utf-8",
//     Accept: "application/json"
//   };

//   let dataString =
//     '{ "email": "steveng87@gmail.com", "password": "Confucius8167"}';

//   let options = {
//     url: "https://api.voluum.com/auth/session",
//     method: "POST",
//     headers: headers,
//     body: dataString
//   };

//   async function callback(error, response, body) {
//     if (!error && response.statusCode == 200) {
//       let data = JSON.parse(body);

//       let headers = {
//         Accept: "application/json; charset=utf-8",
//         "cwauth-token": data.token
//       };

//       let from =
//         moment()
//           .utcOffset(-5)
//           .format("YYYY-MM-DD") + "T06:00:00Z";
//       let to =
//         moment()
//           .utcOffset(-5)
//           .add(1, "days")
//           .format("YYYY-MM-DD") + "T06:00:00Z";

//       let options = {
//         url: `https://api.voluum.com/report?from=${from}&to=${to}&groupBy=custom-variable-4&limit=500&conversionTimeMode=CONVERSION`,
//         headers: headers
//       };
//       async function callback(error, response, body) {
//         if (!error && response.statusCode == 200) {
//           let pixels = getPixelIdList(body);
//           await updatePnlsByAccountPixel(pixels, body);
//         }
//       }
//       await request(options, callback);
//     }
//   }
//   await request(options, callback);
// };

exports.getSuspiciousOffers = async function(cb) {
  //console.log("Inside Voluum Suspicious offers.");
  //get token
  let headers = {
    "Content-Type": "application/json; charset=utf-8",
    Accept: "application/json"
  };

    let dataString = { email: vEmail, password: vPass };

  let options = {
    url: "https://api.voluum.com/auth/session",
    method: "POST",
    headers: headers,
    body: dataString
  };

  async function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(JSON.parse(body));
      let data = JSON.parse(body);

      let headers = {
        Accept: "application/json; charset=utf-8",
        "cwauth-token": data.token
      };

      let from =
        moment()
          .subtract(2, "hour")
          .format("YYYY-MM-DDTHH:00:00") + "Z";
      let to = moment().format("YYYY-MM-DDTHH:00:00") + "Z";
      console.log(`from: ${from} \nto: ${to}`);
      let options = {
        url: `https://api.voluum.com/report?from=${from}&to=${to}&groupBy=offer&groupBy=hour-of-day&sort=offerName&limit=500&direction=DESC&conversionTimeMode=CONVERSION`,
        headers: headers
      };
      async function callback(error, response, body) {
        if (!error) {
          //console.log(body);
          let offers = JSON.parse(body);
          console.log("offers: ", offers);
          let filteredOffers = filterSuspiciousOffers(offers);
          console.log("fd offers: ", filteredOffers);
          cb(error, filteredOffers);
        } else {
          console.log(error);
          cb(error, response);
        }
      }
      await request(options, callback);
    }
  }
  await request(options, callback);
};

function filterSuspiciousOffers(body) {
  let groupedOffers = groupOffersById(body.rows);
  let offerKeys = Object.keys(groupedOffers);
  let suspiciousOffers = [];
  offerKeys.forEach(key => {
    if (groupedOffers[key].length == 2) {
      //if (groupedOffers[key][0].conversions == 0 && groupedOffers[key][1].conversions == 0) {
      //    suspiciousOffers.push(groupedOffers[key][1]);
      //}
      if (groupedOffers[key][0].clicks >= 30) {
        let convrate = groupedOffers[key][0].conversions * 0.25;
        let clickrate = groupedOffers[key][0].clicks * 0.5;

        if (
          groupedOffers[key][1].clicks < clickrate ||
          groupedOffers[key][1].conversions < convrate
        ) {
          groupedOffers[key][1].conversiondrop =
            (1 -
              groupedOffers[key][1].conversions /
                groupedOffers[key][0].conversions) *
            100;
          groupedOffers[key][1].clickdrop =
            (1 - groupedOffers[key][1].clicks / groupedOffers[key][0].clicks) *
            100;
          suspiciousOffers.push(groupedOffers[key][1]);
        }
      }
    }
  });
  console.log("SUSPICIOUS OFFERS: ", suspiciousOffers);
  return suspiciousOffers;
}

function groupOffersById(rows) {
  let offers = {};
  if (!rows || !rows.length) return offers;

  rows.forEach(row => {
    if (!offers[row.offerId]) {
      offers[row.offerId] = [row];
    } else {
      offers[row.offerId].push(row);
    }
  });
  return offers;
}

//Dont erase
require("make-runnable");
