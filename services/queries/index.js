// const guid = process.env.decrypt_guid;
let config = require('../config/config.json');
let guid = config.guid || '';

exports.getAnalyticsDataByDateRangeForAccountLife = `
SELECT a.fname                   AS first_name,
       a.lname                   AS last_name,
       a.account_key,
       a.ban_type,
       a.parent_manager,
       a.angle                   AS angle,
       a.total_spend             AS total_spend,
       a.total_revenue           AS total_revenue,
       a.days_alive,
       a.owner                   AS owner,
       a.launched,
       a.source,
       a.safe_ad_count,
       a.proxy_type,
       a.initial_boost           AS boost,
       a.status_key              AS status_key,
       ( CASE a.launch_success
           WHEN 1 THEN 'Successful'
           WHEN 2 THEN 'Disapproved'
           WHEN 3 THEN 'Flagged'
           WHEN 4 THEN 'Problem'
           ELSE 'Problem'
         END )                   AS launch_status,
       Max(ads.country)          AS geo,
       ba.NAME                   AS batch,
       ba.batch_key              AS batch_key,
       ba.description            AS batch_description,
       ba.safe_spend             AS batch_safe_spend,
       ba.proxy                  AS batch_proxy,
       ba.warm_time              AS batch_warm_time,
       ba.safe_ads               AS batch_safe_ads,
       ba.pre_warming_activities AS batch_pre_warming_activities,
       ba.pre_launch_activities  AS batch_pre_launch_activities,
       ba.created                AS batch_creation_date,
       Max(camp.campaign_key)    AS campaign_key,
       code.code_type            AS card_type,
       l.NAME                    AS launch_success_name,
       l.launch_key              AS launch_key,
       s.status                  AS status,
       v.NAME                    AS vertical,
       vm.NAME                   AS vm_name,
       vm.uuid                   AS vm_UUID,
       Count(*)
FROM   accounts a
       LEFT JOIN status s
              ON a.status_key = s.status_key
       LEFT JOIN codes code
              ON a.account_key = code.account_key
       LEFT JOIN launch l
              ON a.launch_success = l.launch_key
       LEFT JOIN verticals v
              ON a.vertical_key = v.vertical_key
       LEFT JOIN batches ba
              ON a.batch = ba.NAME
       LEFT JOIN virtual_machines vm
              ON a.virtual_machine = vm.vm_key
       LEFT JOIN campaigns camp
              ON a.account_key = camp.account_key
       LEFT JOIN adsets ads
              ON camp.campaign_key = ads.campaign_key
WHERE  Date(a.launched) >= Date(?)
       AND Date(a.launched) <= Date(?)
       AND a.status_key NOT IN ( 38, 37 )
       AND a.total_spend > 0
GROUP  BY a.fname,
          a.lname,
          a.account_key,
          a.ban_type,
          a.parent_manager,
          a.angle,
          a.total_spend,
          a.total_revenue,
          a.days_alive,
          a.owner,
          a.launched,
          a.source,
          a.safe_ad_count,
          a.proxy_type,
          a.initial_boost,
          a.status_key,
          ba.NAME,
          ba.batch_key,
          ba.description,
          ba.safe_spend,
          ba.proxy,
          ba.warm_time,
          ba.safe_ads,
          ba.pre_warming_activities,
          ba.pre_launch_activities,
          ba.created,
          code.code_type,
          l.NAME,
          l.launch_key,
          s.status,
          v.NAME,
          vm.uuid,
          vm.NAME
  ORDER BY total_spend DESC;`;

exports.getAnalyticsDataByDateRangeForLaunchRates = `
          SELECT a.fname                   AS first_name,
                 a.lname                   AS last_name,
                 a.account_key,
                 a.ban_type,
                 a.parent_manager,
                 a.angle                   AS angle,
                 a.total_spend             AS total_spend,
                 a.total_revenue           AS total_revenue,
                 a.days_alive,
                 a.owner                   AS owner,
                 a.launched,
                 a.source,
                 a.safe_ad_count,
                 a.proxy_type,
                 a.initial_boost           AS boost,
                 a.status_key              AS status_key,
                 ( CASE a.launch_success
                     WHEN 1 THEN 'Successful'
                     WHEN 2 THEN 'Disapproved'
                     WHEN 3 THEN 'Flagged'
                     WHEN 4 THEN 'Problem'
                     ELSE 'Problem'
                   END )                   AS launch_status,
                 Max(ads.country)          AS geo,
                 ba.NAME                   AS batch,
                 ba.batch_key              AS batch_key,
                 ba.description            AS batch_description,
                 ba.safe_spend             AS batch_safe_spend,
                 ba.proxy                  AS batch_proxy,
                 ba.warm_time              AS batch_warm_time,
                 ba.safe_ads               AS batch_safe_ads,
                 ba.pre_warming_activities AS batch_pre_warming_activities,
                 ba.pre_launch_activities  AS batch_pre_launch_activities,
                 ba.created                AS batch_creation_date,
                 Max(camp.campaign_key)    AS campaign_key,
                 code.code_type            AS card_type,
                 l.NAME                    AS launch_success_name,
                 l.launch_key              AS launch_key,
                 s.status                  AS status,
                 v.NAME                    AS vertical,
                 vm.NAME                   AS vm_name,
                 vm.uuid                   AS vm_UUID,
                 Count(*)
          FROM   accounts a
                 LEFT JOIN status s
                        ON a.status_key = s.status_key
                 LEFT JOIN codes code
                        ON a.account_key = code.account_key
                 LEFT JOIN launch l
                        ON a.launch_success = l.launch_key
                 LEFT JOIN verticals v
                        ON a.vertical_key = v.vertical_key
                 LEFT JOIN batches ba
                        ON a.batch = ba.NAME
                 LEFT JOIN virtual_machines vm
                        ON a.virtual_machine = vm.vm_key
                 LEFT JOIN campaigns camp
                        ON a.account_key = camp.account_key
                 LEFT JOIN adsets ads
                        ON camp.campaign_key = ads.campaign_key
          WHERE  Date(a.launched) >= Date(?)
                 AND Date(a.launched) <= Date(?)
          GROUP  BY a.fname,
                    a.lname,
                    a.account_key,
                    a.ban_type,
                    a.parent_manager,
                    a.angle,
                    a.total_spend,
                    a.total_revenue,
                    a.days_alive,
                    a.owner,
                    a.launched,
                    a.source,
                    a.safe_ad_count,
                    a.proxy_type,
                    a.initial_boost,
                    a.status_key,
                    ba.NAME,
                    ba.batch_key,
                    ba.description,
                    ba.safe_spend,
                    ba.proxy,
                    ba.warm_time,
                    ba.safe_ads,
                    ba.pre_warming_activities,
                    ba.pre_launch_activities,
                    ba.created,
                    code.code_type,
                    l.NAME,
                    l.launch_key,
                    s.status,
                    v.NAME,
                    vm.uuid,
                    vm.NAME
                    ORDER BY total_spend DESC;`;

exports.getAccountsBySearchTermForSearchPageQuery = `
SELECT a.fname AS first_name,
    a.lname AS last_name,
    a.account_key,
    a.ban_type,
    a.parent_manager,
    a.angle AS angle,
    a.total_spend AS total_spend,
    a.total_revenue AS total_revenue,
    a.days_alive,
    a.owner AS owner,
    a.launched,
    a.source,
    a.safe_ad_count,
    a.proxy_type,
    a.initial_boost AS boost,
    a.status_key AS status_key,
    (
        CASE a.launch_success WHEN 1 THEN 'Successful'
        WHEN 2 THEN 'Disapproved'
        WHEN 3 THEN 'Flagged'
        WHEN 4 THEN 'Problem'
        ELSE 'Problem'
        END
    ) AS launch_status,
    Max(ads.country) AS geo,
    ba.NAME AS batch,
    ba.batch_key AS batch_key,
    ba.description AS batch_description,
    ba.safe_spend AS batch_safe_spend,
    ba.proxy AS batch_proxy,
    ba.warm_time AS batch_warm_time,
    ba.safe_ads AS batch_safe_ads,
    ba.pre_warming_activities AS batch_pre_warming_activities,
    ba.pre_launch_activities AS batch_pre_launch_activities,
    ba.created AS batch_creation_date,
    Max(camp.campaign_key) AS campaign_key,
    code.code_type AS card_type,
    l.NAME AS launch_success_name,
    l.launch_key AS launch_key,
    s.status AS status,
    v.NAME AS vertical,
    vm.NAME AS vm_name,
    vm.uuid AS vm_UUID,
    Count( * )
FROM
accounts a
LEFT JOIN status s ON a.status_key = s.status_key
LEFT JOIN codes code ON a.account_key = code.account_key
LEFT JOIN launch l ON a.launch_success = l.launch_key
LEFT JOIN verticals v ON a.vertical_key = v.vertical_key
LEFT JOIN batches ba ON a.batch = ba.NAME
LEFT JOIN virtual_machines vm ON a.virtual_machine = vm.vm_key
LEFT JOIN campaigns camp ON a.account_key = camp.account_key
LEFT JOIN adsets ads ON camp.campaign_key = ads.campaign_key
GROUP BY
a.fname,
    a.lname,
    a.account_key,
    a.ban_type,
    a.parent_manager,
    a.angle,
    a.total_spend,
    a.total_revenue,
    a.days_alive,
    a.owner,
    a.launched,
    a.source,
    a.safe_ad_count,
    a.proxy_type,
    a.initial_boost,
    a.status_key,
    ba.NAME,
    ba.batch_key,
    ba.description,
    ba.safe_spend,
    ba.proxy,
    ba.warm_time,
    ba.safe_ads,
    ba.pre_warming_activities,
    ba.pre_launch_activities,
    ba.created,
    code.code_type,
    l.NAME,
    l.launch_key,
    s.status,
    v.NAME,
    vm.uuid,
    vm.NAME
ORDER BY
total_spend DESC;`;

//code to search by cc
/*
OR CAST(aes_decrypt(code.code, "` +
            guid +
            `") AS CHAR(16)) LIKE CONCAT('%', '?', '%')
*/
