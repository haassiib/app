﻿"use strict";

let express = require("express");
let mysql = require("mysql");
let sqlString = require("sqlstring");
let dbConnection = require("../services/dbconnector");

exports.getVerticals = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);
    let queryStr = "SELECT * FROM verticals;";

    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        //console.log("Get Verticals res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log("verticals.js getVerticals error: ", error);
          connection.end();
          res.status(500).send(results, error);
        }
      });
    // await connection.end();
  };
};

exports.getCompleteVerticals = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);
    let queryStr = "SELECT * FROM verticals;";

    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        console.log("Get Verticals res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log("verticals.js getVerticals error: ", error);
          connection.end();
          res.status(500).send(results, error);
        }
      });
    // await connection.end();
  };
};

exports.insertVerticals = function() {
  return async function(req, res) {
    let verticals = req.body.verticals;
    console.log("verticals: ", verticals);

    if (!verticals || verticals.length <= 0) {
      res
        .status(500)
        .send(
          { status: 500, message: "No Verticals to insert." },
          { status: 500, message: "No verticals to insert." }
        );
      return;
    }

    let queryStr = "INSERT INTO verticals (name) VALUES ";
    for (let i = 0; i < verticals.length; i++) {
      if (typeof verticals[i] === "string")
        queryStr += "( '" + verticals[i] + "' )";

      if (i < verticals.length - 1) {
        queryStr += ", ";
      }
    }
    queryStr += ";";

    console.log("Vertical Insert QRY: ", queryStr);
    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        //console.log("Insert Verticals res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log("verticals.js getVerticals error: ", error);
          connection.end();
          res.status(500).send(results, error);
        }
      });
    // await connection.end();
  };
};
