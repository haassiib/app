﻿"use strict";
const nodemailer = require("nodemailer");
const express = require("express");
const mysql = require("mysql");
const sqlString = require("sqlstring");
const dbConnection = require("../services/dbconnector");
const moment = require("moment-timezone");

function emailSetup() {
  //TODO: momve to config file
  let smtpConfig = {
    user: "confuciusmktg@gmail.com",
    pass: "mencius1"
  };
  let fullteam =
    "lukealhaj@gmail.com, zach@simplystrive.com, michaeljamesyohe@gmail.com, natedl98@yahoo.com, Sdi071000@gmail.com, ryancourson1@gmail.com";
  let team = "steveng87@gmail.com, zach@simplystrive.com";
  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    service: "Gmail",
    secure: true,
    auth: {
      user: smtpConfig.user,
      pass: smtpConfig.pass
    }
  });

  // setup email data with unicode symbols
  let mailOptions = {
    from: '"Mencius" <confuciusmktg@gmail.com>', // sender address
    to: "dennis.touchet@gmail.com", // list of receivers
    subject: "Hello ✔", // Subject line
    text: "Hello world ?", // plain text body
    html: "<b>Hello world ?</b>" // html body
  };

  return {
    getTransporter: function() {
      return transporter;
    },
    getMailOptions: function() {
      return mailOptions;
    },
    to: team,
    tofull: fullteam
  };
}

exports.testEmail = function() {
  let e = emailSetup();
  let transporter = e.getTransporter();
  let mailOptions = e.getMailOptions();

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log("Message %s sent: %s", info.messageId, info.response);
  });
};

exports.sendPerformantContentEmail = function(pnlService) {
  pnlService.getDailyPerformantContent((error, result) => {
    if (error) {
      return console.log(error);
    } else {
      //console.log("results: ", result);
      let html = buildPerformantContentHtml(result);
      //console.log("HTML: ", html);

      let e = emailSetup();
      let transporter = e.getTransporter();
      let mailOptions = e.getMailOptions();
      let team = e.tofull;

      mailOptions.subject =
        "Daily Secret Sauce " + moment().format("MM-DD-YYYY");
      mailOptions.html = html;
      mailOptions.to = team;

      // send mail with defined transport object
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
        console.log("Message %s sent: %s", info.messageId, info.response);
      });
    }
  });
};

function buildPerformantContentHtml(content) {
  let html = `<table width="80%" border="0" cellspacing="0" cellpadding="0" ><tr><td width="100%" align="center">
                    <table width="600" border="0" cellspacing="0" cellpadding="0" ><tr><td width="600" align="center">
                        <table><tr><td style="text-align:center;"><h1>Daily Secret Sauce</h1></td></tr></table><br />`;

  content.forEach(item => {
    html +=
      `<table width="30%" style="display:inline-block;"><tr><td width="100%">
                    <table width="100%">
                        <thead>
                            <tr><td style="text-align:center;padding:5px;" colspan="2"><b><h3>` +
      item.fname +
      `&nbsp;` +
      item.lname +
      `</h3></b></td></tr>
                        </thead>
                        <tbody>
                            <tr><td></td><td></td></tr>
                            <tr style="background: #eee;"><td style="padding:2px;">GEO</td><td style="padding:2px;">` +
      item.country +
      `</td></tr>
                            <tr><td style="padding:2px;">Vertical</td><td style="padding:2px;">` +
      item.vertical +
      `</td></tr>
                            <tr style="background: #eee;"><td style="padding:2px;">Angle</td><td style="padding:2px;">` +
      item.angle +
      `</td></tr>
                            <tr><td style="padding:10px;"></td><td style="padding:2px;"></td></tr>
                            <tr style="background: #eee;"><td style="padding:2px;"><b>Revenue</b></td><td style="text-align:right; padding:2px;">` +
      `$` +
      Math.floor(item.revenue) +
      `</td></tr>
                            <tr><td style="padding:2px;"><b>Expense</b></td><td style="text-align:right; padding:2px;">` +
      `$` +
      Math.floor(item.expenses) +
      `</td></tr>
                            <tr style="background: #eee;"><td style="padding:2px;"><b>Profit</b></td><td style="text-align:right; padding:2px;">` +
      `$` +
      Math.floor(item.profit) +
      `</td></tr>
                            <tr><td style="padding:2px;"><b>ROI</b></td><td style="text-align:right; padding:2px;">` +
      Math.floor(item.roi) +
      `%` +
      `</td></tr>
                            <tr style="background: #eee;"><td style="padding:2px;" colspan="2"><a href="http://mencius.confuciusmktg.com/#/edit_account/` +
      item.account_key +
      `?tab=Copy">View Ad</a></td></tr>
                        </tbody>
                    </table>
                </td></tr></table>`;
  });

  html += `</td></tr></table>
             </td></tr></table>`;

  return html;
}

exports.sendPoorAccountROIEmail = async function(pnlService) {
  await pnlService.getDailyPoorAccountRoi((error, result) => {
    if (error) {
      return console.log(error);
    } else {
      //console.log("results: ", result);
      let itemsByOwner = groupAccountsByOwner(result);
      console.log("Accounts grouped by owner: ", itemsByOwner);
      let html = buildPoorAccountRoiEmail(itemsByOwner);
      //console.log("HTML: ", html);

      let e = emailSetup();
      let transporter = e.getTransporter();
      let mailOptions = e.getMailOptions();
      let team = e.tofull;

      mailOptions.subject =
        "Daily Poor Performance Accounts " + moment().format("MM-DD-YYYY");
      mailOptions.html = html;
      mailOptions.to = team;

      // send mail with defined transport object
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
        console.log("Message %s sent: %s", info.messageId, info.response);
      });
    }
  });
};

function groupAccountsByOwner(accounts) {
  if (!accounts) return;

  let accountsGroupedByOwner = {};

  accounts.forEach(account => {
    if (!accountsGroupedByOwner[account.owner]) {
      accountsGroupedByOwner[account.owner] = [account];
    } else {
      accountsGroupedByOwner[account.owner].push(account);
    }
  });

  return accountsGroupedByOwner;
}

function buildPoorAccountRoiEmail(accountsByOwner) {
  let html = `<table width="80%" border="0" cellspacing="0" cellpadding="0" ><tr><td width="100%" align="center">
                    <table width="600" border="0" cellspacing="0" cellpadding="0" ><tr><td width="600" align="center">
                        <table><tr><td style="text-align:center;"><h1>Poor Performance Accounts</h1></td></tr></table><br />
                            <table width="100%">`;

  let owners = Object.keys(accountsByOwner);

  owners.forEach(owner => {
    html +=
      `<tr><td><h2>` +
      owner +
      `</h2></td></tr>
                 <tr><td>`;

    accountsByOwner[owner].forEach(account => {
      html +=
        `<table width="30%" style="display:inline-block;"><tr><td width="100%">
                        <table width="100%">
                            <thead style="border-top: 1px solid black;border-left:1px solid black;border-right: 1px solid black;">
                                <tr><td style="text-align:center;padding:5px;" colspan="2"><b><h3>` +
        account.fname +
        `&nbsp;` +
        account.lname +
        `</h3></b></td></tr>
                            </thead>
                            <tbody style="border-bottom: 1px solid black;border-left:1px solid black;border-right: 1px solid black;">
                                <tr style="background: #eee;"><td style="padding:2px;"><b>Revenue</b></td><td style="text-align:right; padding:2px;">` +
        `$` +
        Math.floor(account.revenue) +
        `</td></tr>
                                <tr><td style="padding:2px;"><b>Expense</b></td><td style="text-align:right; padding:2px;">` +
        `$` +
        Math.floor(account.expenses) +
        `</td></tr>
                                <tr style="background: #eee;"><td style="padding:2px;"><b>Profit</b></td><td style="text-align:right; padding:2px;">` +
        `$` +
        Math.floor(account.profit) +
        `</td></tr>
                                <tr><td style="padding:2px;"><b>ROI</b></td><td style="text-align:right; padding:2px;">` +
        Math.floor(account.roi) +
        `%` +
        `</td></tr>
                                <tr style="background: #eee;"><td style="padding:2px;" colspan="2"><a href="http://mencius.confuciusmktg.com/#/edit_account/` +
        account.account_key +
        `?tab=PnL">View Account PnL</a></td></tr>
                            </tbody>
                        </table>
                    </td></tr></table>`;
    });

    html += `</td></tr>`;
  });

  html += `</table>
                </td></tr></table>
             </td></tr></table>`;

  return html;
}

exports.sendSuspiciousOfferEmail = async function(voluumService) {
  await voluumService.getSuspiciousOffers((error, result) => {
    if (error) {
      return console.log(error);
    } else {
      console.log("results: ", result);
      if (!result || result.length == 0) {
        console.log("No suspicious offers this hour!");
        return;
      }

      let html = buildSuspiciousOfferEmail(result);
      //console.log("HTML: ", html);

      let e = emailSetup();
      let transporter = e.getTransporter();
      let mailOptions = e.getMailOptions();
      let team = e.to;

      mailOptions.subject =
        "Hourly Suspicious Offer check " +
        moment()
          .add(6, "hour")
          .format("MM-DD-YYYY hh:00:00");
      mailOptions.html = html;
      mailOptions.to = team;

      // send mail with defined transport object
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
        console.log("Message %s sent: %s", info.messageId, info.response);
      });
    }
  });
  console.log("sending email");
  return;
};

function buildSuspiciousOfferEmail(offers) {
  let html = `<table width="80%" border="0" cellspacing="0" cellpadding="0" ><tr><td width="100%" align="center">
                    <table width="600" border="0" cellspacing="0" cellpadding="0" ><tr><td width="600" align="center">
                        <table><tr><td style="text-align:center;"><h1>Suspicious Offers Data</h1></td></tr></table><br />
                            <table width="80%" style="display:inline-block;"><tr><td width="100%">
                                <table width="100%">
                                    <tbody>
                                        <tr><td></td><td></td></tr>`;

  offers.forEach(offer => {
    html +=
      `           <tr style="background: #eee;"><td style="padding:2px;"><b>Name:</b></td><td style="text-align:right; padding:2px;">` +
      `$` +
      offer.offerName +
      `</td></tr>
                            <tr style="background: #eee;"><td style="padding:2px;"><b>Clicks:</b></td><td style="text-align:right; padding:2px;">` +
      offer.clicks +
      `</td></tr>
                            <tr><td style="padding:2px;"><b>Click Drop:</b></td><td style="text-align:right; padding:2px;">` +
      parseFloat(offer.clickdrop).toFixed(2) +
      `%</td></tr>
                            <tr style="background: #eee;"><td style="padding:2px;"><b>Conversions:</b></td><td style="text-align:right; padding:2px;">` +
      offer.conversions +
      `</td></tr>
                            <tr><td style="padding:2px;"><b>Conversion Drop:</b></td><td style="text-align:right; padding:2px;">` +
      parseFloat(offer.conversiondrop).toFixed(2) +
      `%</td></tr>
                            <br />`;
  });

  html += `               </tbody>
                        </table>
                    </td></tr></table>
                </td></tr></table>
             </td></tr></table>`;
  return html;
}
