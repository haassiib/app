let moment = require('moment-timezone');
let db = require('../services/dbconnector');
let transform = require('../services/transforms');
const {
  getAnalyticsDataByDateRangeForAccountLife,
  getAnalyticsDataByDateRangeForLaunchRates
} = require('./queries/index');

let config = require('./config/config.json');
let guid = config.guid || '';

//Pull all filtered analytics data by date range with all filter params needed
exports.getAnalyticsDataByDateRangeForAccountLife = function() {
  return async function(req, res) {
    // console.log('req.params.dtFrom => ', req.params.dtFrom);
    // console.log('req.params.dtTo => ', req.params.dtTo);

    let connection = await db.init();

    await connection
      .query(getAnalyticsDataByDateRangeForAccountLife, [req.params.dtFrom, req.params.dtTo])
      .then(results => {
        if (results) {
          // console.log(
          //   'Number of results in analytics getAnalyticsDataByDateRange => ',
          //   results.length
          // );
          connection.end();
          res.status(200).send(results);
        }
      })
      .catch(error => {
        if (error) {
          console.log('analytics.js getAnalyticsDataByDateRange error: ', error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getAnalyticsDataByDateRangeForLaunchRates = function() {
  return async function(req, res) {
    // console.log('req.params.dtFrom => ', req.params.dtFrom);
    // console.log('req.params.dtTo => ', req.params.dtTo);

    let connection = await db.init();

    await connection
      .query(getAnalyticsDataByDateRangeForLaunchRates, [req.params.dtFrom, req.params.dtTo])
      .then(results => {
        if (results) {
          // console.log(
          //   'Number of results in analytics getAnalyticsDataByDateRange => ',
          //   results.length
          // );
          connection.end();
          res.status(200).send(results);
        }
      })
      .catch(error => {
        if (error) {
          console.log('analytics.js getAnalyticsDataByDateRange error: ', error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

// exports.getAllAnalyticsData = function() {
//   return async function(req, res) {
//     try {
//       var queryStr = `SELECT * from accounts a;`;

//       const connection = await db.init();

//       await connection
//         .query(queryStr)
//         .then(results => {
//           console.log("Get All Analytics Data Success => ", results);
//           //console.log("Get Available Accounts res: ", results);
//           connection.end();
//           res.status(200).send(results);
//         })
//         .catch(error => {
//           if (error) {
//             console.log(error);
//             res.status(500).send(error);
//           }
//         });
//     } catch (error) {
//       if (error) {
//         console.error("error in the getAllAnalyticsData method => ", error);
//       }
//     }
//   };
// };

const transformAllAnalyticsData = () => {
  console.log('query results', query_results);
  const allData = {};

  var transformed_result = {
    account_key: query_results[0].account_key,
    batch: query_results[0].batch,
    batchDescription: query_results[0].batchDescription,
    status: query_results[0].status,
    vertical: query_results[0].vertical,
    launch_status: query_results[0].launch_name,
    launch_status_key: query_results[0].launch_key,
    angle: query_results[0].angle,
    pixel: query_results[0].pixel,
    campaign_key: query_results[0].campaign_key,
    campaign_name: query_results[0].campaign_name,
    campaign_url: query_results[0].campaign_url,
    headline: query_results[0].headline,
    contentBody: query_results[0].contentBody,
    newsfeedLink: query_results[0].newsfeedLink,
    linkToGraphic: query_results[0].linkToGraphic,
    safe_url_key: query_results[0].safe_url_key,
    safe_url: query_results[0].url,
    safe_url_status: query_results[0].safe_url_status,
    safe_url_claimed: query_results[0].safe_url_claimed,
    fname: query_results[0].fname,
    lname: query_results[0].lname,
    conversionEvent: query_results[0].conversion_event,
    claimed: query_results[0].claimed,
    funding_type: query_results[0].funding_type,
    code_key: query_results[0].code_key,
    safeSpend: query_results[0].safe_spend,
    budget: query_results[0].budget,
    total_spend: query_results[0].total_spend,
    audited: query_results[0].audited,
    audit_revenue: query_results[0].audit_revenue,
    audit_expenses: query_results[0].audit_expenses,
    warmTime: query_results[0].warm_time,
    runTime: query_results[0].run_time,
    lifeTime: query_results[0].life_time,
    proxy_type: query_results[0].proxy_type,
    owner: query_results[0].owner,
    source: query_results[0].source,
    safe_ad_count: query_results[0].safe_ad_count,
    ban_type: query_results[0].ban_type,
    email: query_results[0].email,
    password: query_results[0].password,
    phone: query_results[0].phone,
    val1: query_results[0].val1,
    parent_manager: query_results[0].parent_manager,
    country: query_results[0].country,
    last_account_comment: query_results[0].last_account_comment,
    recovered: query_results[0].recovered,
    created:
      query_results[0].created == null
        ? null
        : moment(query_results[0].created)
            .utc()
            .format(),
    creation:
      query_results[0].creation == null
        ? null
        : moment(query_results[0].creation)
            .utc()
            .format(),
    launched:
      query_results[0].launched == null
        ? null
        : moment(query_results[0].launched)
            .utc()
            .format(),
    initialBoost:
      query_results[0].initial_boost == null
        ? null
        : moment(query_results[0].initial_boost)
            .utc()
            .format(),
    modified:
      query_results[0].modified == null
        ? null
        : moment(query_results[0].modified)
            .utc()
            .format(),
    dead:
      query_results[0].dead == null
        ? null
        : moment(query_results[0].dead)
            .utc()
            .format(),
    history: query_results.history,
    vm_key: query_results[0].virtual_machine,
    UUID: query_results[0].UUID,
    vm_name: query_results[0].vm_name
  };

  //console.log("transformed results", transformed_result);

  return transformed_result;
};
