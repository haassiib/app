﻿'use strict';

let express = require('express');
let mysql = require('mysql');
let sqlString = require('sqlstring');
let moment = require('moment-timezone');
let dbConnection = require('../services/dbconnector');
let transform = require('../services/transforms');

let config = require('./config/config.json');
let guid = config.guid || '';
//TODO: clean this file up; too much code duplication
exports.getAccounts = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);

    var queryStr = 'SELECT * from accounts;';
    var connection = await dbConnection.init();

    await connection
      .query(queryStr)
      .then(results => {
        // console.log('Get Available Accounts success => ', results);
        //console.log("Get Available Accounts res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getWarmingAccounts = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);

    var queryStr = `select a.account_key, a.status_key, a.fname, a.lname, a.claimed, 
                        a.pixel, a.angle, a.vertical_key, a.virtual_machine as vm_key, a.budget, a.total_spend, 
                        a.owner, s.status, v.name, ac.comment as last_account_comment 
                        from accounts a
                        JOIN status s on a.status_key = s.status_key
                        LEFT JOIN verticals v on a.vertical_key = v.vertical_key
                        LEFT JOIN (select a.comment, a.account_key from account_comments a
									INNER JOIN ( select b.account_key, MAX(b.created) as maxCreated from account_comments b group by b.account_key ) as b
									on b.account_key = a.account_key AND b.maxCreated = a.created ) 
                                    as ac on ac.account_key = a.account_key
                        where claimed = 'no'
                        AND a.status_key IN (select status_key from status where status IN ('Created', 'Warming'));`;
    var connection = await dbConnection.init();

    await connection
      .query(queryStr)
      .then(results => {
        // console.log('Get Available Warming Accounts success');
        //console.log("Get Available Accounts res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end()
          res.status(500).send(error);
        }
      });
  };
};

exports.getReadyAccounts = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);
    var user = req.headers['x-username'];

    let queryStr =
      `select * from accounts
                        where claimed = 'no'
                        AND status_key = (select status_key from status where status = 'Ready')
                        AND (owner  = "` +
      user +
      `" OR owner IS NULL);`;
    let connection = await await dbConnection.init();

    await connection
      .query(queryStr)
      .then(results => {
        // console.log('Get Available Accounts success');
        //console.log("Get Available Accounts res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getAccountDetails = function() {
  return async function(req, res) {
    console.log('req.params.account_key => ', req.params.account_key);

    let connection = await dbConnection.init();
    let queryStr =
      `SELECT a.account_key,
                    a.angle, 
                    a.fname, 
                    a.lname, 
                    a.pixel, 
                    a.funding_type,
                    a.code_key,
                    a.conversion_event,
                      a.claimed,
                      a.owner,
                    a.warm_time,
                      a.run_time,
                      a.life_time,
                    a.safe_spend,
                      a.budget,
                      a.total_spend,
                      a.total_revenue,
                      a.audited,
                      a.audit_revenue,
                      a.audit_expenses,
                      a.recovered,
                      a.created,
                      a.creation,
                      a.launched,
                      a.initial_boost,
                      a.dead,
                      a.virtual_machine,
                    a.modified,
                    a.adespresso_key,
                    l.name AS launch_success_name,
                    l.launch_key AS launch_key,
                      camp.campaign_key AS campaign_key, 
                      camp.campaign_name AS campaign_name,
                      camp.url AS campaign_url,
                    v.name as vertical,
                    b.name as batch,
                    vm.UUID as UUID,
                    vm.name as vm_name,
                    b.description as batchDescription,
                      u.safe_url_key, 
                    u.url,
                      u.claimed AS safe_url_claimed,
                      u.status AS safe_url_status,
                      cont.headline as headline,
                      cont.body as contentBody,
                      cont.newsfeed_link as newsfeedLink,
                      cont.thumbnail as thumbnailLink,
                      cont.link_to_graphic as linkToGraphic,
                    s.status,
                      a.proxy_type,
                      a.source, a.safe_ad_count, a.ban_type, a.email, a.password, a.phone, 
                    a.parent_manager,
                      ads.country,
                      ac.comment AS last_account_comment
                    FROM accounts a
                      LEFT JOIN status s on a.status_key = s.status_key
                      LEFT JOIN launch l on a.launch_success = l.launch_key
                      LEFT JOIN safe_urls u on a.safe_url_key = u.safe_url_key
                    LEFT JOIN verticals v on a.vertical_key = v.vertical_key
                    LEFT JOIN batches b on a.batch = b.name
                    LEFT JOIN virtual_machines vm on a.virtual_machine = vm.vm_key
          LEFT JOIN campaigns camp on a.account_key = camp.account_key
                      LEFT JOIN adsets ads on camp.campaign_key = ads.campaign_key
                      LEFT JOIN content cont on ads.adset_key = cont.adset_key
                      LEFT JOIN (select a.comment, a.account_key from account_comments a
                INNER JOIN ( select b.account_key, MAX(b.created) as maxCreated from account_comments b group by b.account_key ) as b
                on b.account_key = a.account_key AND b.maxCreated = a.created WHERE a.account_key = ` +
      req.params.account_key +
      `) as ac on ac.account_key = a.account_key
      WHERE a.account_key = ` +
      req.params.account_key +
      ` ORDER BY ads.created DESC, ads.disabled DESC LIMIT 1;`;

    //console.log("querystr in getAccountDetails method => ", queryStr);

    await connection.query(queryStr, async function(error, results) {
      if (error) {
        console.log(error);
        res.status(500).send(error);
        connection.end();
      } else {
        if (results.length > 0) {
          // console.log("results in getAccountDetails => ", results);
          let historyQueryStr =
            `SELECT ah.created,
                      ah.user,
                      s.status
                        FROM account_history ah,
                      status s
                        WHERE account_key = ` +
            req.params.account_key +
            ' AND ah.status_key = s.status_key ORDER BY created DESC;';
          await connection.query(historyQueryStr, async function(error, history) {
            if (error) {
              console.log(error);
              res.status(500).send(error);
              connection.end();
            } else {
              //console.log("get history res:", history);
              let commentQueryStr =
                'SELECT * FROM account_comments where account_key = ' +
                req.params.account_key +
                ' ORDER BY created DESC;';
              await connection.query(commentQueryStr, async function(error, comments) {
                if (error) {
                  console.log(error);
                  res.status(500).send(error);
                  connection.end();
                } else {
                  //console.log("Get Content res: ", comments);
                  let account_history = [...history, ...comments];
                  results.history = account_history
                    .sort(function(a, b) {
                      return a.created > b.created ? -1 : b.created > a.created ? 1 : 0;
                    })
                    .map(item => {
                      item.created = moment(item.created)
                        .utc()
                        .format();
                      return item;
                    });

                  results = await transform.accountDetailsByAccount(results);
                  //console.log("Account History res:" ,results);
                  connection.end();
                  res.status(200).send(results);
                }
              });
            }
          });
        } else {
          connection.end();
          res.status(204).send('No Account Found');
        }
      }
    });
  };
};

exports.getAllAccountsWithDetails = () => {
  return async function(req, res) {
    var queryAllStr = 'SELECT * from accounts;';
    var connection = await dbConnection.init();

    await connection
      .query(queryAllStr)
      .then(async results => {
        console.log('Get Available Accounts success');
        //console.log("Get Available Accounts res: ", results);

        let withDetails = [];
        await results.forEach(async acc => {
          let accKey = acc.account_key;
          let queryDetailsStr =
            `SELECT a.account_key,
                    a.angle,
                    a.fname,
                    a.lname,
                    a.pixel,
                    a.funding_type,
                    a.code_key,
                    a.conversion_event,
                      a.claimed,
                      a.owner,
                    a.warm_time,
                      a.run_time,
                      a.life_time,
                    a.safe_spend,
                      a.budget,
                      a.total_spend,
                      a.total_revenue,
                      a.audited,
                      a.audit_revenue,
                      a.audit_expenses,
                      a.recovered,
                      a.created,
                      a.creation,
                      a.launched,
                      a.initial_boost,
                      a.dead,
                      a.virtual_machine,
                    a.modified,
                      camp.campaign_key AS campaign_key,
                      camp.url AS campaign_url,
                    v.name as vertical,
                    b.name as batch,
                    vm.UUID as UUID,
                    vm.name as vm_name,
                    b.description as batchDescription,
                      u.safe_url_key,
                    u.url,
                      u.claimed AS safe_url_claimed,
                      u.status AS safe_url_status,
                      cont.headline as headline,
                      cont.body as contentBody,
                      cont.newsfeed_link as newsfeedLink,
                      cont.link_to_graphic as linkToGraphic,
                    s.status,
                      a.proxy_type,
                      a.source, a.safe_ad_count, a.ban_type, a.email, a.password, a.phone,
                    a.parent_manager,
                      ads.country,
                      ac.comment AS last_account_comment
                    FROM accounts a
                      LEFT JOIN status s on a.status_key = s.status_key
                      LEFT JOIN safe_urls u on a.safe_url_key = u.safe_url_key
                    LEFT JOIN verticals v on a.vertical_key = v.vertical_key
                    LEFT JOIN batches b on a.batch = b.name
                    LEFT JOIN virtual_machines vm on a.virtual_machine = vm.vm_key
          LEFT JOIN campaigns camp on a.account_key = camp.account_key
                      LEFT JOIN adsets ads on camp.campaign_key = ads.campaign_key
                      LEFT JOIN content cont on ads.adset_key = cont.adset_key
                      LEFT JOIN (select a.comment, a.account_key from account_comments a
                INNER JOIN ( select b.account_key, MAX(b.created) as maxCreated from account_comments b group by b.account_key ) as b
                on b.account_key = a.account_key AND b.maxCreated = a.created WHERE a.account_key = ` +
            accKey +
            `)
                                  as ac on ac.account_key = a.account_key
                      WHERE a.account_key = ` +
            accKey +
            ` ORDER BY ads.created DESC, ads.disabled DESC LIMIT 1;`;

          await connection
            .query(queryDetailsStr)
            .then(results => {
              withDetails = withDetails.concat(results);
              // console.log('withDetails in the forEach => ', withDetails);
            })
            .catch(error => {
              if (error) {
                console.error(error);
              }
            });
        });
        await connection.end();
        res.status(200).send(withDetails);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getAngles = function() {
  return async function(req, res) {
    let limit = req.headers['x-limit'];
    let limitQuery = '';
    let dateRangeQuery = '';
    if (limit) {
      limitQuery = ` LIMIT ${limit} `;
      // Couple be used in future to limit list more
      dateRangeQuery = ` AND DATE(created) > DATE(SUBDATE(CURDATE(), 45)) `;
    }

    let queryStr = `SELECT distinct angle, COUNT(*) as cnt 
	                    FROM accounts
	                    WHERE angle IS NOT NULL
                        GROUP BY angle
                        ORDER BY cnt DESC 
                        ${limitQuery};`;
    //console.log("query: ", queryStr);
    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getAccountByContentId = function() {
  return async function(req, res) {
    var user = req.headers['x-username'];
    var role = req.headers['x-role'];
    var content_keys = req.body.content_keys;

    var queryStr =
      `select c.content_key, ads.adset_key, camp.campaign_key, a.account_key, s.status, a.angle, v.name as vertical, ads.country 
                        from content c
                        JOIN adsets ads ON c.adset_key = ads.adset_key
                        JOIN campaigns camp on ads.campaign_key = camp.campaign_key
                        JOIN accounts a on camp.account_key = a.account_key
                        JOIN status s on a.status_key = s.status_key
                        JOIN verticals v on v.vertical_key = a.vertical_key
                        WHERE c.content_key IN (` +
      content_keys +
      `);`;

    var connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        // console.log('Account Info Results: ', results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.auditDate = function() {
  return async function(req, res) {
    // console.log(req);
    //var queryStr = `update accounts set audit_date=now() where account_key=`+account_key;
    /*
        var connection = await dbConnection.init();
        connection.query(queryStr,
            function (error, results) {
                if (error) {
                    console.log(error);
                    res.status(500).send(error);
                } else {
                    console.log("Account Info Results: ", results);
                    res.status(200).send(results);
                    await connection.end()
                }
            }
        );
        */
  };
};

exports.updateAccounts = function() {
  return async function(req, res) {
    let username = req.headers['x-username'];
    // console.log('req.body: ', req.body.accounts);

    let queryStr = '';
    if (req.body.accounts.length && req.body.accounts.length > 0) {
      req.body.accounts.forEach(acct => {
        let status =
          acct.status === null || typeof acct.status === 'undefined'
            ? ' NULL '
            : '"' + acct.status + '"';
        let budget =
          acct.budget === null || typeof acct.budget === 'undefined' || acct.budget == ''
            ? ' 0 '
            : acct.budget;
        let total_spend =
          acct.total_spend === null ||
          typeof acct.total_spend === 'undefined' ||
          acct.total_spend == ''
            ? ' 0 '
            : acct.total_spend;
        let total_revenue =
          acct.total_revenue === null ||
          typeof acct.total_revenue === 'undefined' ||
          acct.total_revenue == ''
            ? ' 0 '
            : acct.total_revenue;
        let owner =
          acct.owner === null || typeof acct.owner === 'undefined'
            ? ' NULL '
            : '"' + acct.owner + '"';
        let virtual_machine;
        if (acct.vm_key !== 0 && acct.vm_key !== null && acct.vm_key !== undefined) {
          virtual_machine = acct.vm_key;
        } else {
          virtual_machine = '1';
        }

        let statusChanged =
          acct.statusChanged === null || typeof acct.statusChanged === 'undefined'
            ? false
            : acct.statusChanged;
        let recovered =
          acct.recovered === null || typeof acct.recovered === 'undefined'
            ? ' NULL '
            : '"' + acct.recovered + '"';

        let queryAddition = '';
        let queryTotalSpend = '';
        let queryTotalRevenue = '';
        let s = acct.status.toString().toLowerCase();
        // console.log('status: ', s);
        if (s === 'created') {
          queryAddition = ', claimed = "no"';
        }

        if (s.indexOf('dead') >= 0 || s.indexOf('died') >= 0) {
          let spend = total_spend;
          queryTotalSpend =
            ` LEFT JOIN
                            (SELECT a.account_key as account_key, SUM(p.expenses) as spend
                            FROM accounts a
                            JOIN pnl p ON p.account_key = a.account_key
                            WHERE a.account_key = ` +
            acct.account_key +
            ` GROUP BY p.account_key) as total ON total.account_key = a.account_key `;
          total_spend =
            ` CASE 
                             WHEN total.spend IS NOT NULL AND ` +
            statusChanged +
            ` THEN total.spend
                             ELSE ` +
            spend +
            ` END`;
          //adding total_revenue to all accounts
          let revenue = total_revenue;
          queryTotalRevenue =
            ` LEFT JOIN
                            (SELECT a.account_key as account_key, SUM(p.revenue) as revenue
                            FROM accounts a
                            JOIN pnl p ON p.account_key = a.account_key
                            WHERE a.account_key = ` +
            acct.account_key +
            ` GROUP BY p.account_key) as totalRev ON totalRev.account_key = a.account_key `;
          total_revenue =
            ` CASE 
                             WHEN totalRev.revenue IS NOT NULL AND ` +
            statusChanged +
            ` THEN totalRev.revenue
                             ELSE ` +
            revenue +
            ` END`;
        }
        // console.log('qry ts: ', queryTotalSpend);
        queryStr +=
          'UPDATE accounts a ' +
          queryTotalSpend +
          queryTotalRevenue +
          ' SET status_key= (SELECT status_key FROM status where status = ' +
          status +
          '), budget= ' +
          budget +
          ', virtual_machine= ' +
          virtual_machine +
          ', total_spend = ' +
          total_spend +
          ', total_revenue = ' +
          total_revenue +
          ', recovered = ' +
          recovered +
          ', owner = ' +
          owner +
          queryAddition +
          ', modified = NOW() WHERE a.account_key="' +
          acct.account_key +
          '";';
      });

      // console.log('INSIDE MULTI ACCOUNT UPDATE: ', queryStr);
      let connection = await dbConnection.init(true);
      await connection
        .query(queryStr)
        .then(results => {
          //console.log('connected as id ' + connection.threadId);
          // console.log('successfully updated accounts in db');
          //console.log("results: ", results);
          connection.end();
          res.status(200).send(results);
        })
        .catch(error => {
          if (error) {
            console.log(error);
            connection.end();
            res.status(500).send(error);
          }
        });
    } else {
      res.status(200).send('No Accounts to Update');
    }
  };
};

exports.getAllAccountsHistory = function() {
  return async function(req, res) {
    let queryStr = 'select * from account_history;';
    var connection = await dbConnection.init(true);
    await connection
      .query(queryStr)
      .then(results => {
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.updateAccountHistory = function() {
  return async function(req, res) {
    var user = req.headers['x-username'];
    var accounts = req.body.accounts;

    let queryStr = '';
    accounts.forEach(account => {
      queryStr +=
        'INSERT INTO account_history (status_key, account_key, user) VALUES ' +
        '((SELECT status_key from status WHERE status="' +
        account.status +
        '"), "' +
        account.account_key +
        '", "' +
        user +
        '");';
    });

    var connection = await dbConnection.init(true);
    await connection
      .query(queryStr)
      .then(results => {
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.updateAccountInfo = function() {
  return async function(req, res) {
    //console.log("req body: ", req.body);
    //console.log("req.params: ", req.params);
    let username = req.headers['x-username'];
    // console.log('req.body.account => ', req.body.account);

    let fname = req.body.account.fname == undefined ? 'NULL ' : '"' + req.body.account.fname + '"';
    let lname = req.body.account.lname == undefined ? 'NULL ' : '"' + req.body.account.lname + '"';
    let status =
      req.body.account.status == undefined ? 'NULL ' : '"' + req.body.account.status + '"';
    let vertical =
      req.body.account.vertical == undefined ? 'NULL ' : '"' + req.body.account.vertical + '"';
    let angle = req.body.account.angle == undefined ? 'NULL ' : '"' + req.body.account.angle + '"';
    let owner = req.body.account.owner == undefined ? 'NULL ' : '"' + req.body.account.owner + '"';
    let funding =
      req.body.account.funding_type == undefined
        ? 'NULL '
        : '"' + req.body.account.funding_type + '"';
    let code = req.body.code_key == undefined ? 'NULL ' : req.body.code_key;
    let conversion =
      req.body.account.conversionEvent == undefined
        ? 'NULL '
        : '"' + req.body.account.conversionEvent + '"';
    let safeSpend =
      req.body.account.safeSpend == undefined || req.body.account.safeSpend == ''
        ? ' 0 '
        : req.body.account.safeSpend;
    let budget =
      req.body.account.budget == undefined || req.body.account.budget == ''
        ? ' 0 '
        : req.body.account.budget;
    let total_spend =
      req.body.account.total_spend == undefined || req.body.account.total_spend == ''
        ? ' 0 '
        : req.body.account.total_spend;
    let total_revenue =
      req.body.account.total_revenue == undefined || req.body.account.total_revenue == ''
        ? ' 0 '
        : req.body.account.total_revenue;
    let pixel = req.body.account.pixel == undefined ? 'NULL ' : '"' + req.body.account.pixel + '"';
    let adespresso_key =
      req.body.account.adespresso_key == undefined
        ? 'NULL '
        : '"' + req.body.account.adespresso_key + '"';
    let campaign_url =
      req.body.account.campaign_url == undefined
        ? 'NULL '
        : '"' + req.body.account.campaign_url + '"';
    let proxy =
      req.body.account.proxy_type == undefined ? 'NULL ' : '"' + req.body.account.proxy_type + '"';
    let source =
      req.body.account.source == undefined ? 'NULL ' : '"' + req.body.account.source + '"';
    let safe_ad_count =
      req.body.account.safe_ad_count == undefined
        ? 'NULL '
        : '"' + req.body.account.safe_ad_count + '"';
    let ban_type =
      req.body.account.ban_type == undefined ? 'NULL ' : '"' + req.body.account.ban_type + '"';
    let email = req.body.account.email == undefined ? 'NULL ' : '"' + req.body.account.email + '"';
    let password =
      req.body.account.password == undefined ? 'NULL ' : '"' + req.body.account.password + '"';
    let virtual_machine =
      req.body.account.vm_key === undefined ? 'NULL ' : '"' + req.body.account.vm_key + '"';
    let phone = req.body.account.phone == undefined ? 'NULL ' : '"' + req.body.account.phone + '"';
    let parent_manager =
      req.body.account.parent_manager == undefined
        ? 'NULL '
        : '"' + req.body.account.parent_manager + '"';
    let statusChanged =
      req.body.statusChanged === null || typeof req.body.statusChanged === 'undefined'
        ? false
        : req.body.statusChanged;
    let recovered =
      req.body.account.recovered == undefined ? 'NULL' : '"' + req.body.account.recovered + '"';
    let initialBoost =
      req.body.account.initialBoost == undefined
        ? 'NULL '
        : '"' +
          moment(req.body.account.initialBoost, 'MM-DD-YYYY').format('YYYY-MM-DD HH:MM:SS') +
          '"';
    let launched =
      req.body.account.launched == undefined
        ? 'NULL'
        : '"' + moment(req.body.account.launched, 'MM-DD-YYYY').format('YYYY-MM-DD HH:MM:SS') + '"';
    let creation =
      req.body.account.creation == undefined
        ? 'NULL'
        : '"' + moment(req.body.account.creation, 'MM-DD-YYYY').format('YYYY-MM-DD HH:MM:SS') + '"';
    // console.log('req.body.account.batch test in accounts files ', req.body.account.batch);
    let batchName;
    if (typeof req.body.account.batch === 'string') {
      batchName = '"' + req.body.account.batch + '"';
    } else if (req.body.account.batch === null || req.body.account.batch === undefined) {
      batchName = 'NULL';
    } else {
      batchName = '"' + req.body.account.batch.name + '"';
    }

    let queryAddition = '';
    let queryTotalSpend = '';
    let queryTotalRevenue = '';
    let s = req.body.account.status.toLowerCase();
    let l = typeof req.body.account.launched;
    let d = typeof req.body.account.dead;
    // console.log('dead: ', d, ' | launched: ', l, ' | status: ', s);
    // console.log('reqbodyaccountDEAD => ', req.body.account.dead);
    if (s.indexOf('dead') >= 0 || s.indexOf('died') >= 0) {
      queryAddition = ', dead = NOW() ';
      let spend = total_spend;
      //only calculate value automatically if status changed to dead, not if it was already dead
      if (req.body.account.dead === null) {
      } else {
        queryAddition = ''; // Not sure what Trey was doing here so instead of deleting anything, I am going to add this if..else block to overwrite queryAddition
      }
      if (req.body.account.statusChanged) {
        queryTotalSpend =
          ` LEFT JOIN
                            (SELECT a.account_key as account_key, SUM(p.expenses) as spend
                            FROM accounts a
                            JOIN pnl p ON p.account_key = a.account_key
                            WHERE a.account_key = ` +
          req.params.account_key +
          ` GROUP BY p.account_key) as totalRev ON totalRev.account_key = a.account_key `;
        total_spend =
          ` CASE 
                             WHEN totalRev.spend IS NOT NULL THEN totalRev.spend
                             ELSE ` +
          spend +
          ` END`;

        //adding total_revenue to all accounts
        let revenue = total_revenue;
        queryTotalRevenue =
          ` LEFT JOIN
                            (SELECT a.account_key as account_key, SUM(p.expenses) as revenue
                            FROM accounts a
                            JOIN pnl p ON p.account_key = a.account_key
                            WHERE a.account_key = ` +
          req.params.account_key +
          ` GROUP BY p.account_key) as total ON total.account_key = a.account_key `;
        total_revenue =
          ` CASE 
                             WHEN total.revenue IS NOT NULL AND ` +
          statusChanged +
          ` THEN total.revenue
                             ELSE ` +
          revenue +
          ` END`;
      }
    } else {
      queryAddition = ', dead = NULL ';
    }

    if (s === 'finished upload') {
      if (launched === 'NULL') {
        launched = 'NOW()';
      }
    } else if (s === 'created') {
      queryAddition += ', claimed = "no"';
    }
    var connection = await dbConnection.init();

    var queryStr1 =
      'UPDATE accounts a ' +
      queryTotalSpend +
      queryTotalRevenue +
      ' SET total_spend= ' +
      total_spend +
      ', total_revenue =' +
      total_revenue +
      ', fname =' +
      fname +
      ', lname =' +
      lname +
      ', status_key= (SELECT status_key FROM status WHERE status=' +
      status +
      '), vertical_key= (SELECT vertical_key FROM verticals WHERE name=' +
      vertical +
      '), angle=' +
      angle +
      ', owner=' +
      owner +
      ', virtual_machine=' +
      virtual_machine +
      ', funding_type= ' +
      funding +
      ', code_key = ' +
      code +
      ', conversion_event= ' +
      conversion +
      ', safe_spend=' +
      safeSpend +
      ', budget= ' +
      budget +
      ', pixel=' +
      pixel +
      ', adespresso_key=' +
      adespresso_key +
      ', proxy_type= ' +
      proxy +
      ', source=' +
      source +
      ', safe_ad_count=' +
      safe_ad_count +
      ', ban_type=' +
      ban_type +
      ', email=' +
      email +
      ', password=' +
      password +
      ', phone=' +
      phone +
      ', parent_manager=' +
      parent_manager +
      ', recovered=' +
      recovered +
      queryAddition +
      ', initial_boost = ' +
      initialBoost +
      ', launched = ' +
      launched +
      ', batch = ' +
      batchName +
      ', creation = ' +
      creation +
      ', modified = NOW() WHERE a.account_key= ' +
      req.params.account_key +
      ' ;';

    // console.log('Query String: ', queryStr1);
    await connection
      .query(queryStr1)
      .then(results => {
        //TODO: get rid of this crap
        // setAdsetAndCopyDisabled(
        //   req.params.account_key,
        //   req.body.account.status
        // );
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.getAccountCopy = function() {
  return async function(req, res) {
    var account_key = req.params.account_key;
    var connection = await dbConnection.init();
    var adset_ids = [];
    var content_ids = [];
    var queryStr =
      `select a.account_key, c.campaign_key, s.status_key, 
                        ads.adset_key, ct.content_key, su.safe_url_key, 
                        v.name as vertical, sex, age_min, age_max, 
                        ads.interests, ads.budget, ads.audience, country, mobile, desktop, 
                        instagram, network, rhs, 
                        ads.disabled as adset_disabled, ct.disabled as content_disabled,
                        headline, body, link_to_graphic, thumbnail,
                        ct.comment, newsfeed_link, performant
                        from accounts a
                        join campaigns c on c.account_key = a.account_key
                        left join adsets ads on c.campaign_key = ads.campaign_key
                        left join content ct on ads.adset_key = ct.adset_key
                        join status s on s.status_key = a.status_key
                        join safe_urls su on su.safe_url_key = a.safe_url_key
                        join verticals v on v.vertical_key = a.vertical_key
                        where c.account_key = ` +
      account_key +
      ' ORDER BY ads.disabled ASC, ads.created DESC;';

    await connection
      .query(queryStr)
      .then(results => {
        var adset = transform.copyByAccount(results);
        //console.log("backend adset: ", adset);
        connection.end();
        res.status(200).send(adset);
      })
      .catch(error => {
        if (error) {
          connection.end();
          console.log(error);
        }
      });
  };
};

//TODO: UPDATE FOR SQL INJECTION https://www.npmjs.com/package/mysql#escaping-query-values
exports.insertAccountCopy = function() {
  return async function(req, res) {
    // console.log('req body copy: ', req.body.copy);
    // console.log('req params: ', req.params);

    if (req.body.copy.length < 0) {
      res.status(500).send('Nothing to Update');
    }

    var queryAdsets = [];
    req.body.copy.forEach(row => {
      var q =
        'INSERT INTO adsets (disabled, sex, age_min, age_max, audience, interests, budget, country, mobile, desktop, instagram, network, rhs, campaign_key)' +
        ' VALUES (0, ' +
        (row.sex == undefined ? 'NULL,' : '"' + row.sex + '",') +
        (row.minAge == undefined ? 'NULL,' : '"' + row.minAge + '",') +
        (row.maxAge == undefined ? 'NULL,' : '"' + row.maxAge + '",') +
        (row.audience == undefined ? 'NULL,' : '' + sqlString.escape(row.audience) + ',') +
        (row.interests == undefined ? 'NULL,' : '' + sqlString.escape(row.interests) + ',') +
        (row.budget == undefined ? 'NULL,' : '"' + row.budget + '",') +
        (row.country == undefined ? 'NULL,' : '' + sqlString.escape(row.country) + ',') +
        (row.mobile == true ? 1 : 0) +
        ', ' +
        (row.desktop == true ? 1 : 0) +
        ', ' +
        (row.instagram == true ? 1 : 0) +
        ', ' +
        (row.network == true ? 1 : 0) +
        ', ' +
        (row.rhs == true ? 1 : 0) +
        ', ' +
        row.campaign_key +
        ' );';

      var content = [];
      // console.log('ROW ADS: ', row.ads, ' | ', row.ads.length);
      if (row.ads.length > 0) {
        var q2 =
          'INSERT INTO content ( headline, body, link_to_graphic, thumbnail, newsfeed_link,  comment, adset_key ) VALUES ';
        row.ads.forEach(ad => {
          var q =
            '(' +
            (ad.headline == undefined ? 'NULL,' : ' ' + sqlString.escape(ad.headline) + ' ,') +
            (ad.body == undefined ? 'NULL,' : ' ' + sqlString.escape(ad.body) + ' ,') +
            (ad.link_to_graphic == undefined
              ? 'NULL,'
              : ' ' + sqlString.escape(ad.link_to_graphic) + ' ,') +
            (ad.thumbnail == undefined ? 'NULL,' : ' ' + sqlString.escape(ad.thumbnail) + ' ,') +
            (ad.newsfeed_link == undefined
              ? 'NULL,'
              : ' ' + sqlString.escape(ad.newsfeed_link) + ',') +
            (ad.comment == undefined ? 'NULL,' : ' ' + sqlString.escape(ad.comment) + ' ,') +
            ' LAST_INSERT_ID())';
          content.push(q);
        });

        for (var i = 0; i < content.length; i++) {
          q2 += content[i];
          if (i != content.length - 1) {
            q2 += ',';
          } else {
            q2 += ';';
          }
        }
        q += q2;
      }

      queryAdsets.push(q);
    });
    // console.log('QUERY INSERT SETS: ', queryAdsets);

    var queryStr = '';
    for (var i = 0; queryAdsets.length > i; i++) {
      queryStr += queryAdsets[i];
    }

    // console.log('INSERT QUERYSTR: ', queryStr);
    var connection = await dbConnection.init(true);
    await connection
      .query(queryStr)
      .then(results => {
        // console.log('INSERT TEST: ', results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.updateAccountCopy = function() {
  return async function(req, res) {
    // console.log('req body copy: ', req.body.copy);
    // console.log('req params: ', req.params);

    //TODO better validation that copy exists/better http response
    if (req.body.copy.length < 0) {
      res.status(500).send('Nothing to Update');
    }

    var queryAdset = [];
    var queryContentSet = [];
    req.body.copy.forEach(row => {
      var q = 'UPDATE adsets SET disabled = ' + row.disabled + ', sex = "' + row.sex;
      q += '", age_min = "' + row.minAge + '", age_max = "' + row.maxAge;
      q +=
        '", interests = ' +
        (row.interests == undefined ? 'NULL' : ' ' + sqlString.escape(row.interests) + ' ');
      q += ', country = "' + row.country;
      q +=
        '", audience = ' +
        (row.audience == undefined ? 'NULL' : ' ' + sqlString.escape(row.audience) + ' ');
      q += ', mobile = ' + (row.mobile == true ? 1 : 0) + ', ';
      q += ' desktop = ' + (row.desktop == true ? 1 : 0) + ', ';
      q += ' instagram = ' + (row.instagram == true ? 1 : 0) + ', ';
      q += ' network = ' + (row.network == true ? 1 : 0) + ', ';
      q += ' rhs = ' + (row.rhs == true ? 1 : 0) + ', ';
      q += ' budget = ' + (typeof row.budget === 'undefined' ? 0 : row.budget) + ',';
      q += ' modified = NOW() ';
      q += ' WHERE adset_key = ' + row.adset_key + ';';

      queryAdset.push(q);

      row.ads.forEach(ad => {
        var q;
        if (!ad.new) {
          q =
            'UPDATE content SET ' +
            'body = ' +
            (ad.body == undefined ? ' NULL ' : sqlString.escape(ad.body)) +
            ', comment = ' +
            (ad.comment == undefined ? ' NULL ' : sqlString.escape(ad.comment)) +
            ', headline = ' +
            (ad.headline == undefined ? ' NULL ' : sqlString.escape(ad.headline)) +
            ', link_to_graphic = ' +
            (ad.link_to_graphic == undefined ? ' NULL ' : sqlString.escape(ad.link_to_graphic)) +
            ', thumbnail = ' +
            (ad.thumbnail == undefined ? ' NULL ' : sqlString.escape(ad.thumbnail)) +
            ', newsfeed_link = ' +
            (ad.newsfeed_link == undefined ? ' NULL ' : sqlString.escape(ad.newsfeed_link)) +
            ', performant = ' +
            (ad.performant == undefined ? ' 0 ' : sqlString.escape(ad.performant)) +
            ' WHERE content_key = ' +
            ad.content_key +
            ';';
        } else {
          q =
            'INSERT INTO content (headline, body, link_to_graphic, thumbnail, newsfeed_link, comment, performant, adset_key) VALUES ( ' +
            (ad.headline == undefined ? ' NULL ' : sqlString.escape(ad.headline)) +
            ',' +
            (ad.body == undefined ? ' NULL ' : sqlString.escape(ad.body)) +
            ',' +
            (ad.link_to_graphic == undefined ? ' NULL ' : sqlString.escape(ad.link_to_graphic)) +
            ',' +
            (ad.thumbnail == undefined ? ' NULL ' : sqlString.escape(ad.thumbnail)) +
            ',' +
            (ad.newsfeed_link == undefined ? ' NULL ' : sqlString.escape(ad.newsfeed_link)) +
            ',' +
            (ad.comment == undefined ? ' NULL ' : sqlString.escape(ad.comment)) +
            ',' +
            (ad.performant == undefined ? ' 0 ' : sqlString.escape(ad.performant)) +
            ',' +
            row.adset_key +
            ');';
        }
        queryContentSet.push(q);
      });
    });
    //console.log("QUERY UPDATE ADSETS: ", queryAdset);
    //console.log("QUERY UPDATE CONTENT: ", queryContentSet);

    var queryStr = '';
    for (let i = 0; queryAdset.length > i; i++) {
      queryStr += queryAdset[i];
    }
    for (let j = 0; queryContentSet.length > j; j++) {
      queryStr += queryContentSet[j];
    }
    // console.log('QUERY STRING: ', queryStr);
    var connection = await dbConnection.init(true);
    await connection
      .query(queryStr)
      .then(results => {
        //console.log("UPDATE TEST: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(results, error);
        }
      });
  };
};

exports.deleteAccountCopy = function() {
  return async function(req, res) {
    // console.log('Req body:', req.body.adset);
    // console.log('Req params: ', req.params);
    let adset = req.body.adset;
    //TODO: Check is admin

    let queryStr = '';

    //TODO: NEED TO DELETE PNL FIRST IF EXISTS

    if (typeof adset.ads !== 'undefined' && adset.ads.length > 0) {
      adset.ads.forEach(ad => {
        if (ad.content_key) {
          queryStr += ' DELETE FROM content WHERE content_key = ' + ad.content_key + ' ; ';
        }
      });
    }

    queryStr += ' DELETE FROM adsets WHERE adset_key = ' + adset.adset_key + ';';
    // console.log(queryStr);

    var connection = await dbConnection.init(true);
    await connection
      .query(queryStr)
      .then(results => {
        // console.log('HERE IN RESULTS');
        //console.log("Delete Adset res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          // console.log('HERE IN ERROR');
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.searchAccounts = function() {
  return async function(req, res) {
    //console.log('\n',req.headers)
    var status = req.headers['x-status'];
    var datefrom = req.headers['x-from'];
    var dateto = req.headers['x-to'];

    // console.log('DATE From: ', datefrom);
    // console.log('DATE to', dateto);
    //TODO: validate dates

    /*
        var queryStr = `SELECT *
        FROM accounts a
        JOIN status s ON s.status_key = a.status_key         
        WHERE status IN("`+ status + `")
        AND ((UNIX_TIMESTAMP(a.modified) < UNIX_TIMESTAMP("` + dateto +`"))
        AND (UNIX_TIMESTAMP(a.modified)) > (UNIX_TIMESTAMP("` + datefrom + `"))
        OR a.modified IS NULL) 
        ORDER BY account_key ASC;`
        */
    let status_addition =
      status.toLowerCase() === 'select all' ? '' : ' WHERE s.status = "' + status + '"';
    let queryStr =
      `SELECT CONCAT_WS(' ',a.fname, a.lname) AS account_name,
                        a.account_key as zid,
                        a.claimed,
                        a.pixel,
                        a.angle,
                        a.conversion_event,
                        a.interests,
                        a.funding_type,
                        a.safe_spend,
                        a.budget total_budget,
                        a.total_spend,
                        a.total_revenue,
                        a.warm_time,
                        a.run_time,
                        a.life_time,
                        a.owner,
                        a.created, a.launched, a.initial_boost, a.modified, a.dead,
                        a.proxy_type, a.source, a.safe_ad_count, 
                        a.ban_type, a.email, a.password, a.phone, a.parent_manager,
                        s.status,
                        v.name as vertical, 
                        su.url,
                        CAST(aes_decrypt(c.code, "` +
      guid +
      `") AS CHAR(16)) as credit_card,
                        c.status card_status,
                        c.claimed card_claimed,
                        c.code_type as card_type         
                        FROM accounts a ` +
      /*JOIN (SELECT account_key, created FROM account_history 
							WHERE status_key IN (SELECT status_key FROM status ` + status_addition + `)  
							ORDER BY created DESC) ah on ah.account_key = a.account_key*/
      `JOIN status s ON s.status_key = a.status_key
                        LEFT JOIN safe_urls su ON su.safe_url_key = a.safe_url_key
                        LEFT JOIN codes c ON c.account_key = a.account_key
                        LEFT JOIN verticals v on v.vertical_key = a.vertical_key` +
      status_addition +
      `;`;

    // console.log('search accounts queryString => ', queryStr);
    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        //console.log("RESULTS BEFORE: ", results);
        //results = transform.byAccount(results);
        //console.log("RESULTS AFTER: ", transform.byAccount(results));
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log('accounts.js searchAccounts: ', error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.updateAccountSafeUrl = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);
    let username = req.headers['x-username'];

    var queryStr =
      'Update accounts set safe_url_key = ' +
      req.body.safe_url.safe_url_key +
      ', modified = NOW() where account_key = ' +
      req.params.account_key;
    var connection = await dbConnection.init();

    await connection
      .query(queryStr)
      .then(results => {
        // console.log('Account Safe Url res: ', results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log('accounts.js updateAccountSafeUrl error: ', error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.updateAccountCode = function() {
  return async function(req, res) {
    // console.log('Req body:', req.body);

    var queryStr =
      'Update accounts set code_key = ' +
      req.body.code.code_key +
      ', modified = NOW() where account_key = ' +
      req.params.account_key;
    // console.log(queryStr);

    var connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        // console.log('Account Code res: ', results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log('accounts.js updateAccountCode error: ', error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.updateAccountAudit = function() {
  return async function(req, res) {
    // console.log("Req body in updateAccountAudit method: ", req.body);
    let user = req.headers['x-username'];

    if (
      user.toLowerCase() === 'stan' ||
      user.toLowerCase() === 'kathy' ||
      user.toLowerCase() === 'luke'
    ) {
      let queryStr =
        'UPDATE accounts set audit_revenue = ' +
        req.body.account.auditRevenue +
        ', audit_expenses = ' +
        req.body.account.auditExpenses +
        ', audited = 1 ' +
        ', audit_epoch = UNIX_TIMESTAMP() ' +
        ', modified = NOW() where account_key = ' +
        req.body.account.account_key;
      // console.log(queryStr);

      let connection = await dbConnection.init();
      await connection
        .query(queryStr)
        .then(results => {
          // console.log('Account Audit res: ', results);
          connection.end();
          res.status(200).send(results);
        })
        .catch(error => {
          if (error) {
            console.log('accounts.js updateAccountAudit error: ', error);
            connection.end();
            res.status(500).send(error);
          }
        });
    } else {
      // console.log('accounts.js updateAccountAudit user: ' + user + ' unauthorized.');
      connection.end();
      res.status(401).send(error);
    }
  };
};

exports.insertAccount = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);
    let accounts = req.body.accounts;
    let username = req.headers['x-username'];
    let safe_url = '';

    // console.log(accounts, '\n');

    let queryStr = `INSERT INTO accounts (fname, lname, pixel, safe_url_key,
                        status_key, vertical_key, funding_type, code_key, safe_spend, budget, total_spend, total_revenue,
                        proxy_type, source, safe_ad_count, ban_type, email, password,
                        phone, creation, initial_boost, batch, virtual_machine) VALUES `;
    let statusQuery = '(SELECT status_key FROM status WHERE status = "Created")';

    let queryStr2 = '';

    for (let i = 0; i < accounts.length; i++) {
      let urlQuery = ',(SELECT safe_url_key FROM safe_urls WHERE url = "' + accounts[i].url + '"),';
      let verticalQuery =
        ',(SELECT vertical_key FROM verticals WHERE name = "' + accounts[i].vertical + '")';
      let fname = accounts[i].fname == undefined ? ' NULL' : '"' + accounts[i].fname + '" ';
      let lname = accounts[i].lname == undefined ? ', NULL' : ', "' + accounts[i].lname + '" ';
      let pixel = accounts[i].pixel == undefined ? ', NULL' : ', "' + accounts[i].pixel + '" ';
      let funding =
        accounts[i].funding_type == undefined ? ',NULL' : ', "' + accounts[i].funding_type + '" ';
      let safe_spend = accounts[i].safeSpend == undefined ? ', 0' : ', ' + accounts[i].safeSpend;
      let budget = accounts[i].budget == undefined ? ', 0' : ', ' + accounts[i].budget;
      let total_spend =
        accounts[i].total_spend == undefined ? ', 0' : ', ' + accounts[i].total_spend;
      let total_revenue =
        accounts[i].total_revenue == undefined ? ', 0' : ', ' + accounts[i].total_revenue;
      let proxy =
        accounts[i].proxy_type == undefined ? ',NULL' : ', "' + accounts[i].proxy_type + '" ';
      let source = accounts[i].source == undefined ? ', NULL ' : ', "' + accounts[i].source + '" ';
      let safe_ad_count =
        accounts[i].safe_ad_count == undefined
          ? ', NULL '
          : ', "' + accounts[i].safe_ad_count + '" ';
      let ban_type =
        accounts[i].ban_type == undefined ? ', NULL ' : ', "' + accounts[i].ban_type + '" ';
      let email = accounts[i].email == undefined ? ', NULL ' : ', "' + accounts[i].email + '" ';
      let password =
        accounts[i].password == undefined ? ', NULL ' : ', "' + accounts[i].password + '" ';
      let phone = accounts[i].phone == undefined ? ', NULL ' : ', "' + accounts[i].phone + '" ';

      let creation =
        accounts[i].creation == undefined
          ? ', NULL'
          : ', "' + moment(accounts[i].creation, 'MM-DD-YYYY').format('YYYY-MM-DD HH:MM:SS') + '"';
      let initial_boost =
        accounts[i].initialBoost == undefined
          ? ', NULL'
          : ', "' +
            moment(accounts[i].initialBoost, 'MM-DD-YYYY').format('YYYY-MM-DD HH:MM:SS') +
            '"';
      let code = ', NULL';
      if (accounts[i].cc) {
        code =
          accounts[i].cc.code_key == undefined ? ',NULL' : ', "' + accounts[i].cc.code_key + '" ';
      }
      let batch = ', "' + accounts[i].batch.name + '" ';
      let virtual_machine = ', "' + accounts[i].vm.vm_key + '" ';

      queryStr2 +=
        '(' +
        fname +
        lname +
        pixel +
        urlQuery +
        statusQuery +
        verticalQuery +
        funding +
        code +
        safe_spend +
        budget +
        total_spend +
        total_revenue +
        proxy +
        source +
        safe_ad_count +
        ban_type +
        email +
        password +
        phone +
        creation +
        initial_boost +
        batch +
        virtual_machine +
        ')';
      //Add commas except for on last iteration
      if (i != accounts.length - 1) {
        queryStr2 += ', ';
      }
    }
    queryStr = queryStr + queryStr2;
    // console.log('Create Account: ', queryStr);
    let connection = await dbConnection.init(true);
    await connection
      .query(queryStr)
      .then(async results => {
        // console.log('Create Accounts Result: ', results);
        var startId = parseInt(results.insertId);
        var rowCount = results.affectedRows;
        queryStr = '';

        for (var i = 0; i < rowCount; i++) {
          // insert account history based on account
          queryStr +=
            `INSERT INTO account_history (status_key, account_key, user) VALUES
                          (` +
            statusQuery +
            `, ` +
            (startId + i) +
            `, "` +
            username +
            `"); `;
          // Update ccard based on account && insert code history
          if (accounts[i].cc && accounts[i].cc.code_key) {
            queryStr +=
              `UPDATE codes SET account_key = ` +
              (startId + i) +
              `,
                                  modified = NOW() WHERE code_key =` +
              accounts[i].cc.code_key +
              `;\n`;

            queryStr +=
              `INSERT INTO code_history (code_key, account_key, user) VALUES
                          (` +
              accounts[i].cc.code_key +
              `, ` +
              (startId + i) +
              `, "` +
              username +
              `"); `;
          }
        }
        // console.log('Multi Query:\n', queryStr);
        await connection
          .query(queryStr)
          .then(results => {
            // console.log('Results: ', results);
            connection.end();
            res.status(200).send(results);
          })
          .catch(error => {
            if (error) {
              console.log(error);
              connection.end();
              res.status(500).send(error);
            }
          });
        //TODO: add account comment fix here, mimic account history hack
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.deleteAccounts = function() {
  return async function(req, res) {
    // console.log('Req body:', req.body.accounts);
    // console.log('Req params: ', req.params);
    let accounts = req.body.accounts;
    //TODO: Check is admin

    let queryStr = '';

    //TODO: NEED TO EXPAND TO REMOVE ADSETS< CONTENT< SAFE URLS< PNL
    //THIS IS ONLY FOR WARMING ACCOUNTS
    for (let i = 0; i < accounts.length; i++) {
      //ONLY DELETE IF NO CAMPAIGNS EXIST FOR ACCOUNT
      queryStr +=
        ' DELETE FROM account_comments WHERE account_key = ' +
        accounts[i].account_key +
        ' AND 0 = (select COUNT(*) FROM campaigns WHERE account_key = ' +
        accounts[i].account_key +
        ');';
      queryStr +=
        ' DELETE FROM account_history WHERE account_key = ' +
        accounts[i].account_key +
        ' AND 0 = (select COUNT(*) FROM campaigns WHERE account_key = ' +
        accounts[i].account_key +
        ');';
      queryStr +=
        ' UPDATE safe_urls SET claimed = "no" WHERE safe_url_key = (SELECT safe_url_key FROM accounts WHERE account_key = ' +
        accounts[i].account_key +
        ' LIMIT 1)' +
        ' AND 0 = (select COUNT(*) FROM campaigns WHERE account_key = ' +
        accounts[i].account_key +
        '); ';
      // delete account last or keys will be gone and you'll have bad data - foreign_key constraints need to be reset
      queryStr +=
        ' DELETE FROM accounts where account_key = ' +
        accounts[i].account_key +
        ' AND 0 = (select COUNT(*) FROM campaigns WHERE account_key = ' +
        accounts[i].account_key +
        ');';
    }
    //console.log(queryStr);

    var connection = await dbConnection.init(true);
    await connection
      .query(queryStr)
      .then(results => {
        // console.log('Accounts Delete Success.');
        //console.log("Delete Account res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log('Accounts Delete Error.');
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });

    // await connection.end();
  };
};

//TODO: change the fundamentals of this? let this be manual
async function setAdsetAndCopyDisabled(account_key, status) {
  //console.log("acct: ", account_key, " | status: ", status);
  status = status.toLowerCase();

  if (status.indexOf('dead') >= 0 || status.indexOf('died') >= 0) {
    var queryStr = `select adset_key from adsets where campaign_key 
            in (select campaign_key from campaigns where account_key = ?);
            select content_key from content where adset_key
            in (select adset_key from adsets where campaign_key 
            in (select campaign_key from campaigns where account_key = ?));`;

    //console.log("QueryStr: ", queryStr);

    var queryAddition = '';
    var connection = await dbConnection.init(true);

    await connection
      .query(queryStr, [account_key, account_key])
      .then(async results => {
        //console.log('adsets keys: ', results[0]);
        //console.log('content keys: ', results[1]);
        //console.log("status: ", status)
        var queryStr = '';
        if (status == 'dead') {
          queryStr += 'UPDATE accounts SET dead = NOW() WHERE account_key = ' + account_key + ';';
        } else {
          queryStr +=
            'UPDATE accounts SET modified = NOW() WHERE account_key = ' + account_key + ';';
        }

        results[0].forEach(adset => {
          var queryAddition = '';
          if (status === 'dead');
          queryAddition = ', deactivated = NOW() ';

          queryStr +=
            'UPDATE adsets SET disabled = 1 ' +
            queryAddition +
            ', modified = NOW() WHERE adset_key="' +
            adset.adset_key +
            '";';
        });

        results[1].forEach(content => {
          var queryAddition = '';
          if (status == 'dead');
          queryAddition = ', deactivated = NOW() ';

          queryStr +=
            'UPDATE content SET disabled = 1 ' +
            queryAddition +
            ', modified = NOW() WHERE content_key="' +
            content.content_key +
            '";';
        });

        //console.log("Query Str: ", queryStr);
        await connection
          .query(queryStr)
          .then(results => {
            //TODO: need to look into dealing with this - related to ln.206
            //console.log("updated adsets: ",res);
            // res.status(200).send('Update Flags and Times successful!');
            connection.end();
          })
          .catch(error => {
            if (err) {
              // console.log('HERE: ', err, '\nEND OF HERE');
              connection.end();
              res.status(500).send(err);
            }
          });
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
    // connection.end();
  } //TODO disapproved sets disabled to 0
}

exports.updateAccountTotalSpend = function() {
  return async function(req, res) {
    // console.log('Req body accts: ', req.body.accounts);
    let accounts = req.body.accounts;
    //TODO: Check is admin
    if (accounts && accounts.length > 0) {
      let queryStr = '';
      for (let i = 0; i < accounts.length; i++) {
        queryStr +=
          `UPDATE accounts acct
                    INNER JOIN
                        (SELECT a.account_key as account_key, SUM(p.expenses) as spend
                            FROM accounts a
                            JOIN pnl p ON p.account_key = a.account_key
                            WHERE a.account_key = ` +
          accounts[i] +
          ` GROUP BY p.account_key) 
                        as total ON total.account_key = acct.account_key
                    SET acct.total_spend = total.spend; `;
      }
      // console.log(queryStr);
      var connection = await dbConnection.init(true);
      await connection
        .query(queryStr)
        .then(results => {
          // console.log('Accounts Total Spend Success res: ', results);
          connection.end();
          res.status(200).send(results);
        })
        .catch(error => {
          if (error) {
            console.log('Accounts Total Spend Error: ', error);
            connection.end();
            res.status(500).send(error);
          }
        });

      // await connection.end();
    } else {
      connection.end();
      res.status(200).send({ status: 200, message: 'no accounts to update.', data: {} });
    }
  };
};

exports.updateAccountTotalRevenue = function() {
  return async function(req, res) {
    // console.log('Req body accts: ', req.body.accounts);
    let accounts = req.body.accounts;
    //TODO: Check is admin
    if (accounts && accounts.length > 0) {
      let queryStr = '';
      for (let i = 0; i < accounts.length; i++) {
        queryStr +=
          `UPDATE accounts acct
                    INNER JOIN
                        (SELECT a.account_key as account_key, SUM(p.revenue) as revenue
                            FROM accounts a
                            JOIN pnl p ON p.account_key = a.account_key
                            WHERE a.account_key = ` +
          accounts[i] +
          ` GROUP BY p.account_key) 
                        as total ON total.account_key = acct.account_key
                    SET acct.total_revenue = total.revenue; `;
      }
      // console.log(queryStr);
      var connection = await dbConnection.init(true);
      await connection
        .query(queryStr)
        .then(results => {
          // console.log('Accounts Total Revenue Success res: ', results);
          connection.end();
          res.status(200).send(results);
        })
        .catch(error => {
          if (error) {
            console.log('Accounts Total Revenue Error: ', error);
            connection.end();
            res.status(500).send(error);
          }
        });

      // await connection.end();
    } else {
      connection.end();
      res.status(200).send({ status: 200, message: 'no accounts to update.', data: {} });
    }
  };
};

exports.updateAccountBudget = function() {
  return async function(req, res) {
    // console.log('Req body accts: ', req.body.accounts);
    let accounts = req.body.accounts;
    //TODO: Check is admin
    if (accounts && accounts.length > 0) {
      let queryStr = '';
      for (let i = 0; i < accounts.length; i++) {
        queryStr +=
          `UPDATE accounts
                             SET budget = ` +
          accounts[i].budget +
          ` WHERE account_key = ` +
          accounts[i].account_key +
          `;`;
      }
      // console.log(queryStr);
      var connection = await dbConnection.init(true);
      await connection
        .query(queryStr)
        .then(results => {
          console.log('Accounts Budget Success res: ', results);
          connection.end();
          res.status(200).send(results);
        })
        .catch(error => {
          if (error) {
            console.log('Accounts Budget Error: ', error);
            connection.end();
            res.status(500).send(error);
          }
        });

      // await connection.end();
    } else {
      connection.end();
      res.status(200).send({ status: 200, message: 'no accounts to update.', data: {} });
    }
  };
};

exports.updateDailyTotalSpend = async function(callback) {
  let queryStr = `UPDATE accounts acct
                    INNER JOIN
                        (SELECT a.account_key as account_key, SUM(p.expenses) as spend
                            FROM accounts a
                            JOIN pnl p ON p.account_key = a.account_key
                            WHERE a.status_key NOT IN (SELECT s.status_key FROM status s WHERE s.status IN ('Dead- Policy', 'Dead- On Upload', 'Dead- Before Upload', 'Dead- Suspicious'))
                            GROUP BY p.account_key) 
                        as total ON total.account_key = acct.account_key
                    SET acct.total_spend = total.spend;`;

  let connection = await dbConnection.init();
  await connection
    .query(queryStr)
    .then(results => {
      //console.log("success : ", results);
      connection.end();
      return callback(results);
    })
    .catch(error => {
      if (error) {
        connection.end();
        console.log(error);
        //return callback(results);
      }
    });
};

exports.updateDailyTotalRevenue = async function(callback) {
  let queryStr = `UPDATE accounts acct
                    INNER JOIN
                        (SELECT a.account_key as account_key, SUM(p.revenue) as revenue
                            FROM accounts a
                            JOIN pnl p ON p.account_key = a.account_key
                            WHERE a.status_key NOT IN (SELECT s.status_key FROM status s WHERE s.status IN ('Dead- Policy', 'Dead- On Upload', 'Dead- Before Upload', 'Dead- Suspicious'))
                            GROUP BY p.account_key) 
                        as total ON total.account_key = acct.account_key
                    SET acct.total_revenue = total.revenue;`;

  let connection = await dbConnection.init();
  await connection
    .query(queryStr)
    .then(results => {
      //console.log("success : ", results);
      connection.end();
      return callback(results);
    })
    .catch(error => {
      if (error) {
        connection.end();
        console.log(error);
        //return callback(results);
      }
    });
};

exports.updateDaysAliveDaily = async function(callback) {
  let queryStr = `update accounts a
  SET a.days_alive = 
    CASE
      WHEN a.dead IS NULL THEN datediff(DATE_FORMAT(CURDATE(), '%Y-%m-%d'), DATE_FORMAT(a.launched, '%Y-%m-%d'))
      WHEN a.dead IS NOT NULL THEN datediff(DATE_FORMAT(a.dead, '%Y-%m-%d'), DATE_FORMAT(a.launched, '%Y-%m-%d')) 
      ELSE NULL
    END
  WHERE a.launched IS NOT NULL;`;

  let connection = await dbConnection.init();
  await connection
    .query(queryStr)
    .then(results => {
      //console.log("success : ", results);
      connection.end();
      return callback(results);
    })
    .catch(error => {
      if (error) {
        connection.end();
        console.log(error);
        //return callback(results);
      }
    });
};
