﻿"use strict";

let express = require("express");
let mysql = require("mysql");
let sqlString = require("sqlstring");
let dbConnection = require("../services/dbconnector");
let transform = require("../services/transforms");

let config = require("./config/config.json");
let guid = config.guid || "";

//TODO: THIS IS NOT THE CORRECT SOLUTION - JUST A QUICK CHECK
let userRoles = {
    visitor: 1, // 0001
    user_media: 2, // 0010
    user_upload: 4, // 0100
    user_funder: 8, // 1000
    user_accountManager: 16, //10000
    admin: 31 // 11111
};

exports.getAccountCode = function () {
    return async function (req, res) {
        //console.log("Req body:", req.body);
        //console.log("Req params: ", req.params);

        let username = req.headers["x-username"];
        let transform =
            username.toLowerCase() === "stan" || username.toLowerCase() === "kathy"
                ? false
                : true;
        let role = req.headers["x-role"];
        console.log("User: ", username, " Role: ", role);
        if (role == userRoles.admin || role == userRoles.user_accountManager) {
            console.log("Correct user type");
        } else {
            console.log("CRUCIAL ERROR: incorrect user role");
        }
        //TODO: fix this by returning the whole card number THEN getting substr of last 4
        let queryStr =
            `SELECT code_key, account_key, status, code_type, CAST(aes_decrypt(code, "` + guid +
            `")AS CHAR(16)) as code FROM codes WHERE account_key = ` +
            req.params.account_key +
            `;`;

        let connection = await dbConnection.init();
        await connection
            .query(queryStr)
            .then(results => {
                //TODO: SOMETHING ABOUT EMPTY RETURN
                //console.log("Single Account Code res: ", results);
                //let cards = (results && transform) ? transformCardNumbers(results) : results;
                let cards = results;
                connection.end();
                res.status(200).send(cards[0]);
            })
            .catch(error => {
                if (error) {
                    console.log("accountcodes.js getAccountCode error: ", error);
                    connection.end();
                    res.status(500).send(error);
                }
            });
    };
};

// Resources > Cards // 
exports.getAccountCodes = function () {
    return async function (req, res) {
        //console.log("Req body:", req.body);
        //console.log("Req: ", req);

        //TODO: improve this by verifying user in DB
        let username = req.headers["x-username"];
        let role = req.headers["x-role"];
        console.log("User '" + username + "' role: ", role);
        console.log(
            "Admin: ",
            userRoles.admin,
            " manager: ",
            userRoles.user_accountManager
        );
        let transform =
            username.toLowerCase() === "stan" ||
                username.toLowerCase() === "kathy" ||
                username.toLowerCase() === "luke"
                ? false
                : true;

        let queryAddition = "";
        if (role == userRoles.admin) {
            if (req.params.all == "false") {
                queryAddition = " WHERE c.account_key IS NULL ";
            } else {
                //queryAddition = ";";
            }
        } else if (role == userRoles.user_accountManager) {
            if (req.params.all == "false") {
                queryAddition =
                    " WHERE c.account_key IS NULL AND c.permissions = " + 1;
            } else {
                queryAddition = " WHERE c.permissions = " + 1;
            }
        }
        console.log(
            "req.params.all: ",
            req.params.all,
            " | queryaddition: ",
            queryAddition
        );
        if (req.params.all == "false") {
            var queryStr =
                `SELECT c.code_key, c.account_key, c.code_type, c.claimed, c.permissions, c.status, c.created, c.modified, 
                                CAST(aes_decrypt(code, "` + guid +`") AS CHAR(16)) as code FROM codes c ` +
                queryAddition;
        } else {
            var queryStr =
                'SELECT row_number() over ( order by c.code_key) rowNum \n' +
                ', c.code_key, c.account_key, c.code_type, c.claimed \n' +
                ', c.permissions, c.status, c.created, c.modified \n' +
                ', CAST(aes_decrypt(c.code,"' + guid + '")AS CHAR(16)) as code \n' +
                ', a.fname \n' +
                ', a.lname \n' +
                ', s.status as Account_Status \n' +
                'FROM codes as c \n' +
                'inner join accounts as a on c.account_key = a.account_key \n' +
                'inner join status as s on s.status_key = a.status_key \n'
                + queryAddition +
                'order by c.code_key;';
        }
        let connection = await dbConnection.init();

        await connection
            .query(queryStr)
            .then(results => {
                //console.log("All Account Codes res: ", results);
                //let cards = (results && transform) ? transformCardNumbers(results) : results;
                let cards = results;
                connection.end();
                res.status(200).send(cards);
            })
            .catch(error => {
                if (error) {
                    console.log("accountcodes.js getCodes error:", error);
                    connection.end();
                    res.status(500).send(error);
                }
            });
        // await connection.end();
    };
};

exports.postAccountCodes = function () {
    return async function (req, res) {
        //console.log("Req body:", req.body);
        //console.log("Req params: ", req.params);
        let username = req.headers["x-username"];
        let role = req.headers["x-role"];
        console.log("User '" + username + "' role: ", role);
        console.log("Admin: ", userRoles.admin, " manager: ", userRoles.user_accountManager
        );

        let perm;
        let queryAddition = "";
        if (role == userRoles.user_accountManager) {
            console.log("Account Manager user type");
            perm = 1;
        } else if (role == userRoles.admin) {
            console.log("Admin user type");
            perm = 0;
        }

        let queryStr = req.body.codes.reduce((statement, code) => {
            let cvc =
                typeof code.cvc === "undefined"
                    ? "NULL, "
                    : 'AES_ENCRYPT("' + code.cvc + '", "' + guid + '"), ';
            let zip =
                typeof code.zip === "undefined"
                    ? "NULL, "
                    : 'AES_ENCRYPT("' + code.zip + '", "' + guid + '"), ';
            let exp_month =
                typeof code.exp_month === "undefined"
                    ? "NULL, "
                    : 'AES_ENCRYPT("' + code.exp_month + '", "' + guid + '"), ';
            let exp_year =
                typeof code.exp_year === "undefined"
                    ? "NULL, "
                    : 'AES_ENCRYPT("' + code.exp_year + '", "' + guid + '"), ';
            let accounttype =
                typeof code.accounttype === "undefined"
                    ? "NULL, "
                    : '"' + code.accounttype + '", ';
            let claimed =
                typeof code.claimed === "undefined" ? " 0," : ~~code.claimed + ",";
            let account_key =
                typeof code.account_key === "undefined" ? " NULL " : code.account_key;

            return (statement +=
                `INSERT INTO codes (code, cvc, zip, exp_month, exp_year, code_type, permissions, claimed, account_key) 
                           VALUES (AES_ENCRYPT("` +
                code.number +
                `", "` +
                guid +
                `"),` +
                cvc +
                zip +
                exp_month +
                exp_year +
                accounttype +
                perm +
                `,` +
                claimed +
                account_key +
                `); `);
        }, "");
        console.log("REDUCED STRING:\n", queryStr);
        let connection = await dbConnection.init(true);

        await connection
            .query(queryStr)
            .then(results => {
                console.log("Post Account Codes res: ", results);
                connection.end();
                res.status(200).send(results);
            })
            .catch(error => {
                if (error) {
                    console.log("accountcodes.js postAccountCodes error: ", error);
                    connection.end();
                    res.status(500).send(error);
                }
            });
        // await connection.end();
    };
};

exports.putAccountCode = function () {
    return async function (req, res) {
        console.log("PUT acct code - Req body:", req.body);
        console.log("PUT acct code - Req params: ", req.params);
        let username = req.headers["x-username"];
        let role = req.headers["x-role"];
        console.log("User: ", username, " Role: ", role);

        //TODO: actually base changes on user role!!
        if (role == userRoles.admin || role == userRoles.user_accountManager) {
            console.log("Correct user type");
        } else {
            console.log("CRUCIAL ERROR: incorrect user role");
        }

        let codes = req.body.codes;
        let queryStr = "";
        let codeHistoryQuery =
            "INSERT INTO code_history (code_key, account_key, user) VALUES ";
        let chcount = 0;
        //TODO EXPAND THIS FOR ALL CODE FIELDS
        codes.forEach(code => {
            let account_key =
                typeof code.account_key === "undefined" ? "NULL" : code.account_key;
            let status =
                typeof code.status === "undefined"
                    ? ""
                    : ', status = "' + code.status + '"';
            let code_type =
                typeof code.code_type === "undefined"
                    ? ""
                    : ', code_type = "' + code.code_type + '"';
            let claimed = ", claimed = " + (code.claimed == true ? 1 : 0);
            let code_key = code.code_key;

            queryStr +=
                "UPDATE codes " +
                "SET account_key = " +
                account_key +
                status +
                code_type +
                claimed +
                " WHERE code_key = " +
                code_key +
                "; ";

            if (code.account_key && code.code_key && code.changed) {
                chcount++;
                codeHistoryQuery +=
                    " ( " + code_key + "," + account_key + ', "' + username + '" ) ,';
            }
        });

        //console.log("PUT QRY STR: ", queryStr);
        //console.log("Code History Query: ", codeHistoryQuery.replace(/.$/, ";"), " | count: ", chcount);
        if (chcount > 0) queryStr += codeHistoryQuery.replace(/.$/, ";");

        let connection = await dbConnection.init(true);
        await connection
            .query(queryStr)
            .then(results => {
                console.log("Put Account Coded res: ", results);
                connection.end();
                res.status(200).send(results);
            })
            .catch(error => {
                if (error) {
                    console.log("accountcodes.js putAccountCode error:", error);
                    connection.end();
                    res.status(500).send(error);
                }
            });
    };
};

function transformCardNumbers(cards) {
    return cards.map(card => {
        card.code = card.code.substr(card.code.length - 4);
        return card;
    });
}

exports.getCodeHistory = function () {
    return async function (req, res) {
        let user = req.headers["x-username"];
        let account_key = req.params.account_key;
        let queryStr = `SELECT ch.code_history_key, ch.code_key, ch.account_key, ch.user, ch.created, CAST(aes_decrypt(code, "${guid}") AS CHAR(16)) as code 
                        FROM code_history ch
                        JOIN codes cc ON ch.code_key = cc.code_key
                        WHERE ch.account_key = ${account_key}
                        ORDER BY ch.created DESC;`;

        let connection = await dbConnection.init();
        await connection
            .query(queryStr)
            .then(results => {
                //if (user.toLowerCase() === 'stan' || user.toLowerCase() === 'kathy') {
                connection.end();
                res.status(200).send(results);
                //}
                //else {
                //    results = transformCardNumbers(results);
                //    res.status(200).send(results);
                //}
            })
            .catch(error => {
                if (error) {
                    connection.end();
                    res.status(500).send(error);
                }
            });
    };
};

exports.postCodeHistory = function () {
    return async function (req, res) {
        let username = req.headers["x-username"];
        let items = req.body.items;

        let queryStr = `INSERT INTO code_history (code_key, account_key, user) VALUES `;
        for (let i = 0; i < items.length; i++) {
            let code_key = items[i].code_key;
            let account_key = items[i].account_key;

            queryStr += ` ( ${code_key}, ${account_key}, "${username}" )`;

            if (i !== items.length - 1) queryStr += `,`;
            else queryStr += `;`;
        }

        let connection = await dbConnection.init();
        await connection
            .query(queryStr)
            .then(results => {
                connection.end();
                res.status(200).send(results);
            })
            .catch(error => {
                if (error) {
                    console.log("accountscodes.js putAccountCodeHistory error: ", error);
                    connection.end();
                    res.status(500).send(error);
                }
            });
    };
};
