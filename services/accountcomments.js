﻿"use strict";

let express = require("express");
let mysql = require("mysql");
let sqlString = require("sqlstring");
let moment = require("moment-timezone");
let dbConnection = require("../services/dbconnector");

exports.getAccountComments = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    console.log("Req params key: ", req.params.account_key);

    let queryStr =
      "SELECT * FROM account_comments where account_key = " +
      req.params.account_key +
      " ORDER BY created DESC;";
    console.log(queryStr);
    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        //console.log("Get Content res: ", results);
        results = results.map(item => {
          item.created = moment(item.created).format(
            "dddd, MMMM Do YYYY, h:mm:ss A"
          );
          return item;
        });
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.insertAccountComment = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    //console.log("Req body:", req.body);
    let account = req.body.account;

    let queryStr =
      "INSERT INTO account_comments (account_key, comment, user) VALUES (" +
      account.account_key +
      ', "' +
      account.comment +
      '"," ' +
      username +
      '") ;';

    console.log(queryStr);
    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        console.log("insert account comment res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log("error", error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};

exports.insertAccountsComments = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    //console.log("Req body:", req.body);
    let accounts = req.body.accounts;
    //TODO: validate accounts

    let queryStr =
      "INSERT INTO account_comments (account_key, comment, user) VALUES";
    for (let i = 0; i < accounts.length; i++) {
      queryStr +=
        "(" +
        accounts[i].account_key +
        ', "' +
        accounts[i].comment +
        '", "' +
        username +
        '") ';
      if (i < accounts.length - 1) {
        queryStr += ",";
      }
    }
    queryStr += "; ";

    console.log(queryStr);
    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        //console.log("Get Content res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
  };
};
