"use strict";

let express = require("express");
let mysql = require("mysql");
let sqlString = require("sqlstring");
let moment = require("moment-timezone");
let dbConnection = require("../services/dbconnector");
let transform = require("../services/transforms");
let voluum = require("../services/voluum");

exports.addUploadset = function() {
  return async function(req, res) {
    let connection = await dbConnection.init(true);
    let username =
      req.headers["x-username"].charAt(0).toUpperCase() +
      req.headers["x-username"].slice(1); //Capitalize name
    let adsetDetails = [];
    let content = [];

    //console.log("REQ BODY: \n", req.body);
    let uploadset = req.body.uploadset;
    let campaign = uploadset.campaign;
    let campaign_key, adset_key;
    let adsetCount = campaign.length;
    let account_budget = 0;

    for (let i = 0; i < campaign.length; i++) {
      //Get Total Account Budget
      if (typeof campaign[i].adsetDetails.budget !== "undefined")
        account_budget += parseInt(campaign[i].adsetDetails.budget);

      adsetDetails.push({
        sex: campaign[i].adsetDetails.sex,
        age_min: campaign[i].adsetDetails.minAge,
        age_max: campaign[i].adsetDetails.maxAge,
        country: campaign[i].adsetDetails.country,
        interests:
          campaign[i].adsetDetails.interests == undefined
            ? "NULL"
            : "'" + campaign[i].adsetDetails.interests + "'",
        budget:
          campaign[i].adsetDetails.budget == undefined
            ? "NULL"
            : "'" + campaign[i].adsetDetails.budget + "'",
        audience:
          campaign[i].adsetDetails.audience == undefined
            ? "NULL"
            : "'" + campaign[i].adsetDetails.audience + "'",
        mobile: campaign[i].adsetDetails.mobile === true ? 1 : 0,
        desktop: campaign[i].adsetDetails.desktop === true ? 1 : 0,
        instagram: campaign[i].adsetDetails.instagram === true ? 1 : 0,
        network: campaign[i].adsetDetails.audienceNetwork === true ? 1 : 0,
        rhs: campaign[i].adsetDetails.rhs === true ? 1 : 0
      });

      for (let j = 0; j < campaign[i].ads.length; j++) {
        content.push({
          headline: campaign[i].ads[j].headline,
          body: campaign[i].ads[j].body,
          link: campaign[i].ads[j].link_to_graphic,
          newsfeed_link:
            campaign[i].ads[j].newsfeed_link == undefined
              ? "NULL"
              : "'" + campaign[i].ads[j].newsfeed_link + "'",
          thumbnail:
            campaign[i].ads[j].thumbnail == undefined
              ? "NULL"
              : "'" + campaign[i].ads[j].thumbnail + "'",
          adset_number: i,
          ad_number: j,
          comment:
            campaign[i].ads[j].comment == undefined
              ? "NULL"
              : "'" + campaign[i].ads[j].comment + "'"
        });
      }
    }

    console.log("campaigns.js Insert Uploadset");
    console.log("Adsets: ", adsetDetails);
    console.log("Content: ", content);

    async function UpdateAccountHandler() {
      let angle =
        uploadset.account.angle == null
          ? "NULL"
          : '"' + uploadset.account.angle + '"';

      let queryStr =
        'UPDATE accounts SET claimed="yes", angle=' +
        angle +
        ', status_key= (SELECT status_key FROM status WHERE status = "Needs Upload")' +
        ', vertical_key = (SELECT vertical_key FROM verticals WHERE name = "' +
        uploadset.account.vertical +
        '")' +
        ", safe_url_key = " +
        uploadset.safe_url.safe_url_key +
        ', owner = "' +
        username +
        '", conversion_event="' +
        uploadset.account.conversion_event +
        '", pixel="' +
        uploadset.account.pixel +
        '", budget= ' +
        account_budget +
        ", launched = NOW(), modified = NOW()" +
        ' WHERE account_key="' +
        uploadset.account.account_key +
        '";';

      queryStr +=
        "INSERT INTO account_history (account_key, status_key, user)" +
        " VALUES ( " +
        uploadset.account.account_key +
        ', (SELECT status_key FROM status WHERE status = "Needs Upload"), "' +
        username +
        '");';

      queryStr +=
        'UPDATE safe_urls SET claimed="yes" WHERE safe_url_key="' +
        uploadset.safe_url.safe_url_key +
        '";';

      console.log(queryStr);
      let updateAccountPromise = await connection
        .query(queryStr)
        .then(results => results)
        .catch(error => {
          if (error) {
            console.error(error);
          }
        });

      return updateAccountPromise;
    }

    async function InsertCampaignHandler() {
      let voluumId =
        typeof uploadset.account.voluumId === "undefined"
          ? " NULL "
          : '"' + uploadset.account.voluumId + '"';
      let campaignUrl =
        typeof uploadset.account.campaignUrl === "undefined"
          ? " NULL "
          : '"' + uploadset.account.campaignUrl + '"';
      let mediaBuyer = '"' + username + '"';

      let queryStr =
        "INSERT INTO campaigns (voluum_id, url, media_buyer, account_key) VALUES" +
        " (" +
        voluumId +
        ", " +
        campaignUrl +
        ", " +
        mediaBuyer +
        ", " +
        uploadset.account.account_key +
        ");";
      console.log("camp query str: ", queryStr);
      let insertCampaignPromise = await connection
        .query(queryStr)
        .then(results => {
          console.log(
            "Successfully created campaign in database. res: ",
            results
          );
          campaign_key = results.insertId;
          return results;
        })
        .catch(error => {
          if (error) {
            console.error(error);
          }
        });

      // NEED THIS RESULT FOR ADSET INSERTS (contains insertId/campaign_key);

      return insertCampaignPromise;
    }

    async function InsertAdsetHandler() {
      // build query addition
      let queryAddition = "";
      let query_continue = "'),";
      let query_end = "');";

      for (let j = 0; j < adsetDetails.length; j++) {
        queryAddition +=
          " (" +
          campaign_key +
          ", '" +
          adsetDetails[j].sex +
          "', '" +
          adsetDetails[j].age_min +
          "', '" +
          adsetDetails[j].age_max +
          "', " +
          adsetDetails[j].interests +
          ", " +
          adsetDetails[j].budget +
          ", '" +
          adsetDetails[j].country +
          "', " +
          adsetDetails[j].audience +
          ", '" +
          adsetDetails[j].mobile +
          "', '" +
          adsetDetails[j].desktop +
          "', '" +
          adsetDetails[j].instagram +
          "', '" +
          adsetDetails[j].network +
          "', '" +
          adsetDetails[j].rhs;
        if (j == adsetDetails.length - 1) {
          queryAddition += query_end;
        } else {
          queryAddition += query_continue;
        }
      }

      let queryStr =
        "INSERT INTO adsets (campaign_key, sex, age_min, age_max, interests, budget, country, audience, mobile, desktop, instagram, network, rhs) VALUES" +
        queryAddition;
      console.log("INSERT ADSET QUERY: ", queryStr);

      let insertAdSetPromise = await connection
        .query(queryStr)
        .then(results => {
          console.log(
            "Successfully created adsets in database. res: ",
            results
          );
          // NEED THIS RESULT FOR CONTENT INSERTS (contains insertId/adset_key);
          adset_key = results.insertId;
          adsetCount = results.affectedRows;
          console.log("Adset insertId: ", adset_key);
          console.log("affectRows: ", adsetCount);
          return results;
        })
        .catch(error => {
          if (error) {
            console.error(error);
          }
        });

      return insertAdSetPromise;
    }

    async function InsertContentHandler() {
      // build query addition
      let queryAddition = "";
      let query_continue = "),";
      let query_end = ");";

      console.log(
        "adsetCount: ",
        adsetCount,
        " | content.length: ",
        content.length
      );

      for (let j = 0; j < content.length; j++) {
        let key = adset_key + content[j].adset_number;
        console.log("content[j]: ", j, " | adset_key: ", key);

        queryAddition +=
          " (" +
          key +
          ", " +
          sqlString.escape(content[j].headline) +
          ", " +
          sqlString.escape(content[j].body) +
          ", " +
          sqlString.escape(content[j].link) +
          ", " +
          content[j].thumbnail +
          ", " +
          content[j].newsfeed_link +
          ", " +
          content[j].comment;
        if (j == content.length - 1) {
          queryAddition += query_end;
        } else {
          queryAddition += query_continue;
        }
      }
      let queryStr =
        "INSERT INTO content (adset_key, headline, body, link_to_graphic, thumbnail, newsfeed_link, comment) VALUES" +
        queryAddition;
      console.log("Content Query String: \n", queryStr);

      let insertContentPromise = await connection
        .query(queryStr)
        .then(results => {
          console.log("Insert Content Results: \n", results);
          return results;
        })
        .catch(error => {
          if (error) {
            console.error(error);
          }
        });

      return insertContentPromise;
    }

    async function AddUploadsetProcess() {
      console.log("Inside add uploadset process");

      await UpdateAccountHandler()
        .then(result => {
          console.log("Update Account result: ", result);
          return InsertCampaignHandler();
        })
        .then(insertCampRes => {
          console.log("Insert Campaign result: ".result);
          return InsertAdsetHandler();
        })
        .then(results => {
          console.log("Insert Adset result: ".result);
          return InsertContentHandler();
        })
        .then(result => {
          console.log("Insert Content result: ", result);

          console.log("FINAL RESULT: ", result);
          connection.end();
          res.status(200).send("Success!");
          return result;
        })
        .catch(err => {
          console.log("Update Account Res Err: ", err);
          connection.end();
          res.status(404).send(err);
        });

      //TODO: result success?
    }

    await AddUploadsetProcess();
  };
};

exports.getCampaigns = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    let role = req.headers["x-role"];
    console.log(username, " ", role);
    let queryStr = "";

    queryStr = `SELECT 
                    account_key,
                    fname, lname,
                    claimed, budget, total_spend, 
                    pixel, angle, recovered,
                    owner, launched, campaign_url,
                    created, modified, camp_key,
                    adset_key, status, url, safe_url_key,
                    safe_url_claimed, safe_url_status,
                    vertical, country, code_key, code_status,
                    last_account_comment
                    FROM active_accounts_view WHERE status NOT IN ("Created", "Ready");`;

    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        console.log("Active Accounts send to front-end");
        connection.end();
        res.status(200).send(results);
      })
      .catch(err => {
        if (err) {
          console.log(err);
          connection.end();
          res.status(500).send(err);
        }
      });
    // await connection.end();
  };
};

exports.updateCampaigns = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    let role = req.headers["x-role"];
    let campaigns = req.body.campaigns;
    console.log("req.body.campaigns => ", req.body.campaigns);

    let queryStr = "";

    campaigns.forEach(camp => {
      if (camp.campaign_key) {
        console.log("camp.campaign_name => ", camp.campaign_name);
        let campaign_name =
          camp.campaign_name != undefined && camp.campaign_name !== null
            ? '"' + camp.campaign_name + '"'
            : " NULL ";
        let voluum_id =
          typeof camp.voluum_id != "undefined"
            ? '"' + camp.voluum_id + '"'
            : " NULL ";
        let url =
          typeof camp.url != "undefined" ? '"' + camp.url + '"' : " NULL ";
        console.log("inside foreach");
        queryStr +=
          "UPDATE campaigns SET campaign_name = " +
          campaign_name +
          ", voluum_id = " +
          voluum_id +
          ", url = " +
          url +
          " WHERE campaign_key = " +
          camp.campaign_key +
          ";";
      }
    });
    console.log("Update Campaigns QUERY =>  ", queryStr);

    let connection = await dbConnection.init(true);
    await connection
      .query(queryStr)
      .then(results => {
        console.log("Campaigns Updated.");
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error);
        }
      });
    // await connection.end();
  };
};

exports.getTotalSpendByCampaign = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];

    let queryStr = `SELECT country, sex, COUNT(c.campaign_key) as no_campaigns, SUM(total_spend) as total_spend
                        FROM adsets
                        JOIN campaigns c ON c.campaign_key = adsets.campaign_key
                        JOIN accounts a ON a.account_key = c.account_key
                        GROUP BY country, sex;`;

    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log("pnl.js get Pnl error:", error);
          connection.end();
          res.status(500).send(error);
        }
      });
    // await connection.end();
  };
};

exports.getCampaignSpending = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    let timeFrame = req.headers["x-timeframe"];
    //console.log("get frame: ", timeFrame, " |  typeof : ", typeof timeFrame);

    let rangeQuery = ``;
    if (!Number.isNaN(parseInt(timeFrame))) {
      rangeQuery =
        ` where DATE(p.created) >= DATE(SUBDATE(CURDATE(), ` +
        timeFrame +
        `)) `;
    }
    let queryStr =
      `SELECT v.name as vertical, a.angle, ads.country, ads.sex as gender, 
                        COUNT(camp.campaign_key) no_campaigns, 
                        SUM(revenue) as revenue, SUM(expenses) as expenses, 
                        SUM(profit) as profit, (SUM(profit) / SUM(expenses)) as roi 
                        FROM pnl p
                        JOIN content c ON c.content_key = p.content_key
                        JOIN adsets ads on ads.adset_key = c.adset_key
                        JOIN campaigns camp on camp.campaign_key = ads.campaign_key
                        JOIN accounts a on a.account_key = p.account_key
                        JOIN verticals v on v.vertical_key = a.vertical_key ` +
      rangeQuery +
      ` GROUP BY v.vertical_key, a.angle, ads.country, ads.sex;`;
    console.log("querystr: ", queryStr);

    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        console.log("Successfully sent roi by camp to front-end");
        // console.log(results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log(error);
          connection.end();
          res.status(500).send(error, results);
        }
      });
    // await connection.end();
  };
};

//TODO: unused - remove after utilizing permissions
exports.getTasks = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    let role = req.headers["x-role"];
    let connection = await dbConnection.init();

    //TODO: Refactor this and fix role/permission checks
    // media buyer
    if (role == 2) {
      console.log("that was easy");
      // send query to get media buyer's accounts that are in need of attention
      let queryStr =
        `SELECT * FROM campaign_view
                            WHERE media_buyer= "` +
        username +
        `"` +
        ` AND status IN ("Reupload", "Enable Cloaker", "No Spend, Reupload")
                            ORDER BY account_key ASC;`;
      await connection
        .query(queryStr)
        .then(results => {
          console.log("connected as id " + connection.threadId);
          console.log("successfully sent media buyer tasks to front-end");
          // insert code to modify the results so that they are organized by account
          results = transform.byAccount(results);
          // console.log(results);
          connection.end();
          res.status(200).send(results);
        })
        .catch(error => {
          if (error) {
            console.log(error);
            connection.end();
            res.status(500).send(error, results);
          }
        });

      // uploader
    } else if (role == 4) {
      // send query to get uploader's accounts that are in need of attention
      let queryStr =
        `SELECT * FROM campaign_view 
                            WHERE(uploader IN ("` +
        username +
        `", 'Not Assigned') 
                            AND status= "Needs Upload")
                            ORDER BY account_key ASC;`;
      await connection
        .query(queryStr)
        .then(results => {
          console.log("connected as id " + connection.threadId);
          console.log("successfully sent uploader tasks to front-end");
          results = transform.byAccount(results);
          connection.end();
          res.status(200).send(results);
        })
        .catch(error => {
          if (error) {
            console.log(error);
            connection.end();
            res.status(500).send(error, results);
          }
        });

      // funder
    } else if (role == 8 || role == 12) {
      // send query to get funder's accounts that are in need of attention
      let queryStr = `SELECT * FROM campaign_view WHERE status 
                            IN ("Needs Starting Funds", "Cloaked, Needs Bump","Needs Small Bump","Needs Medium Bump" ,"Needs Large Bump")
                            ORDER BY account_key ASC;`;
      await connection
        .query(queryStr)
        .then(results => {
          console.log("connected as id " + connection.threadId);
          console.log("successfully sent funder tasks to front-end");
          results = transform.byAccount(results);
          connection.end();
          res.status(200).send(results);
        })
        .catch(error => {
          if (error) {
            console.log(error);
            connection.end();
            res.status(500).send(error);
          }
        });

      // admin
    } else if (role == 31) {
      // send query to get all accounts that are not dead or hidden
      let queryStr = `SELECT * FROM campaign_view 
                            WHERE status NOT IN ("Dead- Policy", "Dead- On Upload", "Dead- Before Upload", "Dead- Suspicious","Hidden")
                            ORDER BY account_key ASC;`;
      await connection
        .query(queryStr)
        .then(results => {
          console.log("connected as id " + connection.threadId);
          console.log("successfully sent admin tasks to front-end");
          results = transform.byAccount(results);
          connection.end();
          res.status(200).send(results);
        })
        .catch(error => {
          if (error) {
            console.log(error);
            connection.end();
            res.status(500).send(error);
          }
        });
    }
    // await connection.end();
  };
};

//TODO: MOVE THIS TO ITS OWN FILE
exports.getStatus = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    let role = req.headers["x-role"];
    //console.log(role);
    //console.log(username);
    if (role) {
      let connection = await dbConnection.init();
      console.log(
        "connection thread id test => ",
        connection.connection.threadId
      );
      let queryStr =
        "SELECT status_key, status, permissions FROM status ORDER BY status ASC;";
      await connection
        .query(queryStr)
        .then(results => {
          console.log("connected as id " + { ...connection });
          console.log("Get Status sent to front-end");
          connection.end();
          res.status(200).send(results);
        })
        .catch(error => {
          if (error) {
            console.log("ERROR:", error);
            connection.end();
            res.status(500).send(error);
          }
        });
      // await connection.end();
    } else {
      connection.end();
      res.status(401).send("Unauthorized");
    }
  };
};
