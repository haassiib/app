"use strict";

let sha1 = require("sha1");
let uuid = require("node-uuid");
let format = require("util").format;
let sqlString = require("sqlstring");
const bcrypt = require("bcryptjs");

const saltRounds = 10;

// var config = require('/opt/apps/properties/config.json');
let dbConnection = require("../services/dbconnector");
exports.getUsers = function() {
  return async function(req, res) {
    console.log("Req body:", req.body);
    console.log("Req params: ", req.params);
    let queryStr = "SELECT row_number() over ( order by employee_key) rowNum, e.* from employees e order by e.employee_key;";
    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        // console.log("Get Batches res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log("userauth.js getBatches error: ", error);
          connection.end();
          res.status(500).send(results, error);
        }
      });
    // await connection.end();
  };
};

exports.login = function() {
  //TODO: Fix this error handling JS-08/31/18
  return async function(req, res) {
    try {
      let username = req.headers["x-username"];
      let password = req.headers["x-password"];
      var rememberme = req.headers["x-rememberme"];

      if (username === null || password === null) {
        res.status(400).send("Username and Password Required");
      }

      let db = await dbConnection.init();

      if (!db) {
        res.status(500).send("Failed to initialize the db.");
      }
      if (!password || password === "") {
        db.end();

        res.status(400).send("Password is required");

        // password = sha1(password);
      }

      console.log(username, password);
      let query = "SELECT * FROM employees WHERE username = ? ;";
      await db
        .query(query, [username])
        .then(users => {
          if (!users.length) {
            console.log("Bad Combo");
            db.end();
            res.status(404).send("Username/Password incorrect");
          } else if (users.length === 1) {
            let user = users[0];
            return bcrypt
              .compare(password, user.password)
              .then(match => {
                if (!match) {
                  console.error("Bad login combo");
                  db.end();
                  res.status(404).send("Username/Password is not correct!");
                } else if (match) {
                  // issue login token
                  let token = Math.random()
                    .toString(36)
                    .slice(2);
                  let currentTime = Date.now();
                  let expiry;
                  if (rememberme) {
                    //adding 14 days to the current
                    expiry = currentTime + 86400000 * 14;
                  } else {
                    expiry = currentTime + 86400000; //adding a day to the current
                  }
                  db.end();

                  res.status(200).send({
                    token: token,
                    role: user.role,
                    fname: user.fname,
                    lname: user.lname
                  });
                }
              })
              .catch(error => {
                if (error) {
                  console.error("Bad login combo => ", error);
                  res.status(404).send("Username/Password is not correct!");
                }
              });
          } else if (users.length > 1) {
            //EDGE CASE: This should NEVER happen.
            db.end();
            res.status(500).send("Critical Server Error! Notify Admin!");
          }
        })
        .catch(err => {
          console.log(err);
          res.status(400).send("Request Failed.");
        });
    } catch (error) {
      if (error) {
        console.error("this is an error in the auth file! => ", error);
      }
    }
  };
};

exports.verify = function() {
  return function(req, res) {
    let verificationCode = req.param("code");
    if (!verificationCode) db.end();
    res.status(400).send("Verification code is missing");
    let db = dbConnection.getDbConnection();
    if (!db) {
      db.end();
      res.status(500).send("Failed to initialize the db.");
    }
    // TODO: add lookup token in db code
  };
};

exports.signUp = function() {
  return async function(req, res) {
    let username = req.body.username;
    let password = req.body.password;
    let role = 31;
    let fname = req.body.firstName;
    let lname = req.body.lastName;

    console.log("username => ", req.body);

    if (username === null || password === null) {
      res.status(400).send("Username and Password Required");
    }

    let db = await dbConnection.init();
    if (!db) {
      db.end();
      res.status(500).send("Failed to initialize the db.");
    }
    if (!password || password === "") {
      db.end();
      res.status(400).send("Password is required");
      // password = sha1(password);
    }
    //check if user exists
    let testUserQuery = `SELECT * FROM employees WHERE username = ?;`;
    let addUserQuery = `INSERT into employees (fname, lname, username, password, role) VALUES (?, ?, ?, ?, ?)`;
    await db
      .query(testUserQuery, [username])
      .then(res => {
        if (res.length) {
          console.log("Username already exists!");
          db.end();
          return "Username already exists!";
        } else {
          return bcrypt.hash(password, saltRounds).then(hashPass => {
            return db.query(addUserQuery, [
              fname,
              lname,
              username,
              hashPass,
              role
            ]);
          });
        }
      })
      .then(final => {
        console.log("final! => ", final);
        db.end();
        res.status(200).json(final);
      })
      .catch(err => {
        db.end();
        if (err) throw err;
      });

    // await db.end();
  };
};

exports.resetPassword = function() {
  return async function(req, res) {
    let username = req.headers["x-username"];
    let password = req.body.password;
    let connection = await dbConnection.init();

    connection
      .query(
        'SELECT employee_key FROM employees WHERE username = "' +
          username +
          '" limit 1;'
      )
      .then(async results => {
        //console.log("SELECT employees key success");
        //console.log("result", result);

        // console.log(query);

        return bcrypt
          .hash(password, saltRounds)
          .then(async hashPass => {
            console.log("hash => ", hashPass);
            let query =
              'UPDATE employees SET password = "' +
              hashPass +
              '" WHERE employee_key = ' +
              results[0].employee_key +
              ";";
            let res = await connection.query(query);
            connection.end();
            return res;
          })
          .then(results => {
            console.log("connected as id " + connection.threadId);
            console.log("successfully updated employee password reset");
            connection.end();
            res.status(200).send(results);
          })
          .catch(error => {
            if (error) {
              connection.end();
              console.log(error);
            }
          });
        connection.end();
      })
      .catch(error => {
        if (error) {
          console.log(error);
        }
      });
  };
};
