let moment = require("moment-timezone");
let dbConnection = require("../services/dbconnector");
let pnlService = require("./pnl");
let axios = require("axios");

//Need to refactor to use config for security
const adespressoToken =
  "NDZhYjJhOGZmZjkwOTFjMmI3ZWI4YjMzMmM0YzY3NjIwNzM2NjU5MDUxYjY5Yzg3NzlkMjgzN2FhYjRhOWY2MQ";

axios.defaults.baseURL = "https://app.adespresso.com/api/statistics/";
let config = {
  headers: {
    Authorization: "Bearer " + adespressoToken,
    "Access-Control-Allow-Headers": "Allow"
  }
};

/*
Lifetime Spend BY Account

https://app.adespresso.com/api/statistics/data?date_preset=lifetime&fields=["account_id","account_name","spend"]&filtering=[{"field":"account.id","operator":"EQUAL","value":"472626"}]&time_increment=1
*/

getAccountKeyByAdespressoKey = async function(adespressoID) {
  let connection = await dbConnection.init(true);
  let selectAccountByAdespressoKeyQueryStr = `select account_key from accounts where adespresso_key = ${adespressoID};`;

  await connection
    .query(selectAccountByAdespressoKeyQueryStr)
    .then(results => {
      //console.log("success : ", results);
      console.log("Get account_key by adespresso_key Success => ", results);
      connection.end();
      return results;
    })
    .catch(error => {
      if (error) {
        connection.end();
        console.log("error getting account_key by adespresso_key =>", error);
      }
    });
};

exports.getLifetimeSpendByAccount = function() {
  return async function(req, res) {
    console.log("req params => ", req.params);
    adespressoId = req.params.adespressoId;
    account_key = req.params.account_key;

    console.log(
      "account_key in the getLifetimeSpendByAccount method => ",
      account_key,
      " adespresso_key => ",
      adespressoId
    );
    axios
      .get(
        `https://app.adespresso.com/api/statistics/data?date_preset=lifetime&fields=["account_id","account_name","spend"]&filtering=[{"field":"account.id","operator":"EQUAL","value":"${adespressoId}"}]&time_increment=1`,
        config
      )
      .then(items => {
        if (items.data.length) {
          pnlService.updatePnlExpensesWithAdespressoByAccount(
            account_key,
            items.data
          );
          console.log("Lieftime Spend By Account/Day => ", items.data);
          res.send(items.data);
        } else {
          res.send(
            "No items returned from Adespresso. Make sure the account is alive and the Adespresso ID is correct."
          );
        }
      })
      .catch(function(error) {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          console.log(error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log("Error", error.message);
        }
        console.log(error.config);
      });
  };
};

/*
exports.getCampaigns = function() {
  return async function(req, res) {
    console.log("header token: ", req.headers["x-token"]);
    let token = req.headers["x-token"];

    let headers = {
      Accept: "application/json",
      "cwauth-token": token
    };

    let options = {
      url: "https://api.voluum.com/campaign",
      headers: headers
    };

    function callback(error, response, body) {
      if (!error && response.statusCode == 200) {
        //console.log(body);
        res.status(200).send(body);
      }
      //TODO: error validation
    }

    await request(options, callback);
  };
};

exports.getReports = function() {
  return async function(req, res) {
    console.log("Running getReports => ", req.params.date_range);
    let custom = req.headers["x-custom"];
    console.log("custom => ", custom);
    let dateRange = req.params.date_range;

    // Get start offset
    var startoffset = 6;
    if (
      moment()
        .tz("America/Chicago")
        .isDST()
    ) {
      startoffset = 5;
    }
    try {
      // Pull for the various types
      if (dateRange === "today") {
        var start = moment()
          .tz("America/Chicago")
          .subtract(startoffset, "hours")
          .format("YYYY-MM-DD");
        // reconcileDay([start]);
        start += "T06:00:00";
        pnlService.updatePnlWithVoluumByDate(start);
      }

      // We want this disabled for now, but I am only commenting it out in case we want to add it back in

      if (dateRange === "yesterday") {
        var start = moment()
          .tz("America/Chicago")
          .subtract(startoffset + 24, "hours")
          .format("YYYY-MM-DD");
        start += "T06:00:00";
        pnlService.updatePnlWithVoluumByDate(start);
        // reconcileDay([start]);
      }
      if (dateRange === "custom date") {
        var start = moment(custom, "MM-DD-YYYY").format("YYYY-MM-DD");
        start += "T06:00:00";
        pnlService.updatePnlWithVoluumByDate(start);
        // reconcileDay([start]);
      }

      await res.status(200).send("complete");
    } catch (error) {
      if (error) {
        console.error("Error in the getReports method in voluum.js => ", error);
        if (dateRange === "today") {
          var start = moment()
            .tz("America/Chicago")
            .subtract(startoffset, "hours")
            .format("YYYY-MM-DD");

          start += "T06:00:00";
          pnlService.updatePnlWithVoluumByDate(start);
          // reconcileDay([start]);
        }
      }
    }
  };
};
*/
