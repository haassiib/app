"use strict";

let express = require("express");
let mysql = require("mysql");
let sqlString = require("sqlstring");
let dbConnection = require("../services/dbconnector");

exports.getBatches = function () {
    return async function (req, res) {
        console.log("Req body:", req.body);
        console.log("Req params: ", req.params);
        let queryStr = "SELECT row_number() over ( order by batch_key) rowNum, b.* from batches b order by b.batch_key;";
        let connection = await dbConnection.init();
        await connection
            .query(queryStr)
            .then(results => {
                // console.log("Get Batches res: ", results);
                connection.end();
                res.status(200).send(results);
            })
            .catch(error => {
                if (error) {
                    console.log("batches.js getBatches error: ", error);
                    connection.end();
                    res.status(500).send(results, error);
                }
            });
        // await connection.end();
    };
};

exports.updateBatches = function () {
    return async function (req, res) {
        let username = req.headers["x-username"];
        console.log("req.data: ", req.data);

        let queryStr = "";
        if (req.body.batches.length > 0) {
            req.body.batches.forEach(async batch => {
                let batchName = batch.name === null || typeof batch.name === "undefined" ? " NULL " : '"' + batch.name + '"';
                let batchDescription = batch.description === null || typeof batch.description === "undefined" || batch.description == "" ? " 0 " : batch.description;
                let safeSpend = batch.safe_spend === null || typeof batch.safe_spend === "undefined" || batch.safe_spend == "" ? " 0 " : batch.safe_spend;
                let proxy = batch.proxy === null || typeof batch.proxy === "undefined" || batch.proxy == "" ? " 0 " : batch.proxy;
                let warmTime = batch.warm_time === null || typeof batch.warm_time === "undefined" || batch.warm_time == "" ? " 0 " : batch.warm_time;
                let safeAds = batch.safe_ads === null || typeof batch.safe_ads === "undefined" || batch.safe_ads == "" ? " 0 " : batch.safe_ads;
                let preWarmingActivities = batch.pre_warming_activities === null || typeof batch.pre_warming_activities === "undefined" ||
                    batch.pre_warming_activities == "" ? " 0 " : batch.pre_warming_activities;
                let preLaunchActivities = !batch.pre_launch_activities || batch.pre_launch_activities == "" ? " 0 " : batch.pre_launch_activities;

                console.log("batch in updateBatches method: ", batch);

                queryStr +=
                    "UPDATE batches " +
                    " SET name = " + batchName +
                    ", description = " + "'" + batchDescription + "'" +
                    ", safe_spend = " + "'" + safeSpend + "'" +
                    ", proxy = " + "'" + proxy + "'" +
                    ", warm_time = " + "'" + warmTime + "'" +
                    ", safe_ads = " + "'" + safeAds + "'" +
                    ", pre_warming_activities = " + "'" + preWarmingActivities + "'" +
                    ", pre_launch_activities = " + "'" + preLaunchActivities + "'" +
                    ", modified = NOW() " +
                    ' WHERE batch_key = "' + batch.batch_key + '";';

                console.log("INSIDE MULTI batch UPDATE: ", queryStr);
                let connection = await dbConnection.init(true);
                await connection
                    .query(queryStr)
                    .then(results => {
                        //console.log('connected as id ' + connection.threadId);
                        console.log("successfully updated batches in db");
                        //console.log("results: ", results);
                        connection.end();
                        res.status(200).send(results);
                    })
                    .catch(error => {
                        if (error) {
                            console.log(error);
                            res.status(500).send(error);
                        }
                    });
            });
        } else {
            res.status(200).send("No Accounts to Update");
        }
    };
};

exports.insertBatches = function () {
    return async function (req, res) {
        let batch = req.body.newBatch;
        let batchName = batch.name;
        let batchDescription = batch.description;
        let safeSpend = batch.safe_spend;
        let proxy = batch.proxy;
        let warmTime = batch.warm_time;
        let safeAds = batch.safe_ads;
        let preWarmingActivities = batch.pre_warming_activities;
        let preLaunchActivities = batch.pre_launch_activities;
        let batchCreatedTimeStamp = new Date().toISOString();
        // console.log("dateString => ", batchCreatedTimeStamp);
        // batchCreatedTimeStamp = `STR_TO_DATE( ${batchCreatedTimeStamp}, '%d/%m/%Y %H:%i:%s')`;

        // console.log("batches: ", batches);

        if (!batch || batch.length <= 0) {
            res
                .status(500)
                .send(
                    { status: 500, message: "No Batches to insert." },
                    { status: 500, message: "No Batches to insert." }
                );
            return;
        }

        let queryStr = `INSERT INTO batches (name, description, safe_spend, proxy, warm_time, safe_ads, pre_warming_activities, pre_launch_activities, created) VALUES ('${batchName}', '${batchDescription}', '${safeSpend}','${proxy}','${warmTime}','${safeAds}','${preWarmingActivities}','${preLaunchActivities}',NOW())`;
        // for (let i = 0; i < batches.length; i++) {
        //   if (typeof batches[i] === "string")
        //     queryStr += "( '" + batches[i] + "," + new Date() + "' )";

        //   if (i < batches.length - 1) {
        //     queryStr += ", ";
        //   }
        // }

        // queryStr += ";";

        console.log("Batch Insert QRY: ", queryStr);
        let connection = await dbConnection.init();
        await connection
            .query(queryStr, [
                batchName,
                batchDescription,
                safeSpend,
                proxy,
                warmTime,
                safeAds,
                preWarmingActivities,
                preLaunchActivities,
                batchCreatedTimeStamp
            ])
            .then(results => {
                console.log("Insert Verticals res: ", results);
                connection.end();
                res.status(200).send(results);
            })
            .catch(error => {
                if (error) {
                    console.log("batches.js getBatches error: ", error);
                    connection.end();
                    res.status(500).send(results, error);
                }
            });
        // await connection.end();
    };
};

exports.deleteBatch = function () {
    return async function (req, res) {
        console.log("Req body:", req.body.batch_key);
        console.log("Req params: ", req.params);
        let b_key = req.params.batch_key;

        let queryStr = "DELETE FROM batches WHERE batch_key = " + b_key + ";";
        console.log(queryStr);
        let connection = await dbConnection.init();
        await connection
            .query(queryStr)
            .then(results => {
                console.log("HERE IN RESULTS");
                //console.log("Get Content res: ", results);
                connection.end();
                res.status(200).send(results);
            })
            .catch(error => {
                if (error) {
                    console.log("HERE IN ERROR");
                    connection.end();
                    res.status(500).send(error);
                }
            });
        // await connection.end();
    };
};

exports.deleteBatches = function () {
    return async function (req, res) {
        console.log("Req body:", req.body.selectedBatches);
        console.log("Req params: ", req.params);
        let queryStr = "";
        if (req.body.selectedBatches.length > 0) {
            req.body.selectedBatches.forEach(async batches => {
                let queryStr = "DELETE FROM batches WHERE batch_key = " + batches.batch_key + ";";
                console.log(queryStr);
                let connection = await dbConnection.init();
                await connection
                    .query(queryStr)
                    .then(results => {
                        console.log("HERE IN RESULTS");
                        connection.end();
                        res.status(200).send(results);
                    })
                    .catch(error => {
                        if (error) {
                            console.log("HERE IN ERROR");
                            connection.end();
                            res.status(500).send(error);
                        }
                    });
                // await connection.end();
            });
        } else {
            res.status(200).send("No Accounts to Update");
        }
    };
};
