﻿"use strict";

let express = require("express");
let mysql = require("mysql");
let sqlString = require("sqlstring");
let moment = require("moment-timezone");
let dbConnection = require("../services/dbconnector");

exports.getNotifications = function() {
  return async function(req, res) {
    //console.log("Req body:", req.body);
    //console.log("Req params: ", req.params);
    let queryStr = "SELECT * FROM notifications where reviewed = 0;";

    let connection = await dbConnection.init();
    await connection
      .query(queryStr)
      .then(results => {
        console.log("Get Notifications res: ", results);
        connection.end();
        res.status(200).send(results);
      })
      .catch(error => {
        if (error) {
          console.log("notifications.js getNotifications error: ", error);
          connection.end();
          res.status(500).send(results, error);
        }
      });
    // await connection.end();
  };
};
